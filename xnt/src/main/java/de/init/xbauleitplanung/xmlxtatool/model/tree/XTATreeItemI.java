package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.Serializable;

/**
 * This interface represents a basic entity within a hierarchical structure. It provides methods
 * for identification, type definition and getters for child entities and parent entity. 
 * 
 * @author treichling
 *
 */
public interface XTATreeItemI extends Serializable, Comparable<XTATreeItemI>{
	/**
	 * Returns the unique ID of this entity
	 * @return	the unique ID of this entity
	 */
	public String getItemId();
	/**
	 * Returns a name (label) for this entity which is appropriate for display purposes. Not necessarily unique
	 * @return	A name (label) for this entity (non-unique)
	 */
	public String getItemName();
	/**
	 * Returns the type of this entity, appropriate for filtering / organizing purposes
	 * @return	the type of this entity
	 */
	public String getItemType();
	/**
	 * Returns the number of child entities
	 * @return	the number of child entities
	 */
	public int getItemChildCount();
	/**
	 * Returns the child entity with a given index
	 * @param index	The index of the desired child entity
	 * @return	the child entity with a given index
	 */
	public XTATreeItemI getItemChildAt(int index);
	/**
	 * Returns the parent entity of this entity or null in case this is a root entity
	 * @return	the parent entity of this entity or null in case this is a root entity
	 */
	public XTATreeItemI getParent();
	/**
	 * Setter method for a parent entity
	 * @param parent	The new parent entity of this entity
	 */
	public void setParent(XTATreeItemI parent);
	/**
	 * This method returns a flag which marks this entity as "changed" or "edited" in some way for potential further processing
	 * @return	a flag which marks this entity as "changed" or "edited" 
	 */
	public boolean changes();
	/**
	 * Returns an arbitrary "advice" for further processing. Its actual meaning depends on the concrete implementation of this entity
	 * @return	an arbitrary "advice" for further processing
	 */
	public String getAdvice();
	/**
	 * Returns whether some given entity is a child of this entity
	 * @param item	The item to be checked for whether it is a child of this entity
	 * @return	True, in case item is a child of this entity, false otherwise
	 */
	public boolean contains(XTATreeItemI item);
	/**
	 * This is a shortcut method which recursively determines and returns the root entity of this entity
	 * @return	the root entity of this entity
	 */
	public XTATreeItemI getRootItem();
}
