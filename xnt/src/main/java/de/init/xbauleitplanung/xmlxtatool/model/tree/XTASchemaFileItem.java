package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.util.ArrayList;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import de.init.xbauleitplanung.xmlxtatool.model.schema.Import;
import de.init.xbauleitplanung.xmlxtatool.model.schema.Include;
import de.init.xbauleitplanung.xmlxtatool.model.schema.NameSpace;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XAttribute;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XAttributeGroup;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XCodelist;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XComplexType;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElement;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElementGroup;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XSimpleType;


public class XTASchemaFileItem extends XTAXMLFileItem implements XTATreeItemI{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2257851385707733429L;

	private TreeMap<String, XComplexType> rootTypes = new TreeMap<>();
	private TreeMap<String, XSimpleType> rootSimpleTypes = new TreeMap<>();
	
	// Was ist der Unterschied zwischen namedElements und rootElements? 
	// namedElements werden bereits vor dem Analysieren der XSD aufgenommen, rootElements erst beim Analysieren!
	// Je nachdem wann man abfragt, muss man nach root- oder namedElements fragen!
	private TreeMap<String, XElement> rootElements = new TreeMap<>();
	private TreeMap<String, XElement> namedElements = new TreeMap<>();
	
	private TreeMap<String, XCodelist> codelists = new TreeMap<>();
	private TreeMap<String, XElementGroup> elementGroups = new TreeMap<>();
	private TreeMap<String, XAttributeGroup> attributeGroups = new TreeMap<>();
	private TreeMap<String, XAttribute> attributes = new TreeMap<>();
	
	private NameSpace targetNamespace = null;
	private boolean targetNamespaceInherited = false;
	private TreeSet<NameSpace> namespaces = new TreeSet<>();
	
	private TreeSet<Include> includes = new TreeSet<>();
	private TreeSet<Import> imports = new TreeSet<>();
	
	private String defaultElementForm;
	private String defaultAttributeForm;
	
	private boolean analysisDone = false;
	
	public XTASchemaFileItem() {
		super();
	}
	
	public void clone(XTASchemaFileItem copy) {
		super.clone(copy);
		
		this.rootTypes = new TreeMap<>(copy.rootTypes);
		this.rootElements = new TreeMap<>(copy.rootElements);
		this.namedElements = new TreeMap<>(copy.namedElements);
		this.codelists = new TreeMap<>(copy.codelists);
		this.namespaces = new TreeSet<>(copy.namespaces);
	}
	
	public TreeSet<XComplexType> findImplementors(XComplexType type) {
		TreeSet<XComplexType> ret = new TreeSet<XComplexType>();
		
		try {
			for(String tpn : this.rootTypes.keySet()) {
				XComplexType tp = this.rootTypes.get(tpn);
				
				if(tp.getComplexBase() != null) {
					if(tp.getComplexBase().equals(type.getFullyQualifiedName())) {
						ret.add(tp);
					}
				}
			}
			
			for(Import imp : this.imports) {
				if(imp.getLocalFile() != null) {
					ret.addAll(imp.getLocalFile().findImplementors(type));
					if(ret != null) {
						break;
					}
				}
			}
			for(Include inc : this.includes) {
				if(inc.getLocalFile() != null) {
					ret = inc.getLocalFile().findImplementors(type);
					if(ret != null) {
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public XTASchemaFileItem findSchemaForElement(String element) {
		return this.findSchemaForElement(element, new TreeSet<>());
	}
	
	public XTASchemaFileItem findSchemaForElement(String element, TreeSet<String> visited) {
		if(visited.contains(this.getPath())) {
			return null;
		}
		visited.add(this.getPath());
		
		XTASchemaFileItem ret = null;
		
		String prefix = getPrefix(element);
		
		if(this.targetNamespace == null || this.targetNamespace.getPrefix() == null || this.targetNamespace.getPrefix().equals("null")) {
			if(this.rootElements.containsKey(getLocalName(element))) {
				ret = this;
			}
		}
		else if(prefix == null || prefix.equals(this.targetNamespace.getPrefix()) || prefix.equals("urn")) {
			if(this.rootElements.containsKey(getLocalName(element))) {
				ret = this;
			}
		}
		
		if(ret == null) {
			for(Import imp : this.imports) {
				if(imp.getLocalFile() != null) {
					ret = imp.getLocalFile().findSchemaForElement(element, visited);
					if(ret != null) {
						break;
					}
				}
			}
		}
		
		if(ret == null) {
			for(Include inc : this.includes) {
				if(inc.getLocalFile() != null) {
					ret = inc.getLocalFile().findSchemaForElement(element, visited);
					if(ret != null) {
						break;
					}
				}
			}
		}
		
		if(ret == null) {
			if("".equals(this.defaultElementForm) || "unqualified".equals(this.defaultElementForm)) {
				if(this.rootElements.containsKey(getLocalName(element))) {
					ret = this;
				}
			}
		}
		
		return ret;
	}
	
	public NameSpace findNamespaceForElement(String element) {
		return this.findNamespaceForElement(element, new TreeSet<>());
	}
	
	public NameSpace findNamespaceForElement(String element, TreeSet<String> visited) {
		if(visited.contains(this.getPath())) {
			return null;
		}
		visited.add(this.getPath());
		
		NameSpace ret = null;
		
		String prefix = getPrefix(element);
		
		try {
			if(this.targetNamespace != null && this.targetNamespace.getPrefix() != null && this.targetNamespace.getPrefix().equals(prefix)) {
				if(this.namedElements.containsKey(getLocalName(element))) {
					ret = this.targetNamespace;
				}
				
				if(ret == null) {
					for(Include inc : this.includes) {
						if(inc.getLocalFile() != null) {
							ret = inc.getLocalFile().findNamespaceForElement(element, visited);
							if(ret != null) {
								break;
							}
						}
					}
				}
			}
			
			if(ret == null) {
				for(Import imp : this.imports) {
					if(imp.getLocalFile() != null) {
						if(imp.getPrefix().equals(prefix)) {
							ret = imp.getLocalFile().findNamespaceForElement(element, visited);
							if(ret != null) {
								break;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public TreeSet<String> getAllPrefixes() {
		return this.getAllPrefixes(new TreeSet<String>());
	}
	
	public TreeSet<String> getAllPrefixes(TreeSet<String> visited) {
		if(visited.contains(this.getPath())) {
			return null;
		}
		visited.add(this.getPath());
		
		TreeSet<String> ret = new TreeSet<String>();
		try {
			if(this.targetNamespace != null && this.targetNamespace.getPrefix() != null) {
				if(!ret.contains(this.targetNamespace.getPrefix())) {
					ret.add(this.targetNamespace.getPrefix());
				}
				
				for(Include inc : this.includes) {
					if(inc.getLocalFile() != null) {
						TreeSet<String> sub = inc.getLocalFile().getAllPrefixes(visited);
						if(sub != null) {
							ret.addAll(sub);
						}
					}
				}
				
				for(Import imp : this.imports) {
					if(imp.getLocalFile() != null) {
						TreeSet<String> sub = imp.getLocalFile().getAllPrefixes(visited);
						if(sub != null) {
							ret.addAll(sub);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public XTASchemaFileItem findSchemaForType(String type) {
		return this.findSchemaForType(type, new TreeSet<>());
	}
	
	public XTASchemaFileItem findSchemaForType(String type, TreeSet<String> visited) {
		if(visited.contains(this.getPath())) {
			return null;
		}
		visited.add(this.getPath());
		
		XTASchemaFileItem ret = null;
		
		String prefix = getPrefix(type);
		if(this.targetNamespace == null || this.targetNamespace.getPrefix() == null || this.targetNamespace.getPrefix().equals("null")) {
			if(this.rootTypes.containsKey(getLocalName(type))) {
				ret = this;
			}
			else if(this.rootSimpleTypes.containsKey(getLocalName(type))) {
				ret = this;
			}
			else if(this.codelists.containsKey(getLocalName(type))) {
				ret = this;
			}
		}
		else if(prefix == null || prefix.equals(this.targetNamespace.getPrefix()) || prefix.equals("urn")) {
			if(this.rootTypes.containsKey(getLocalName(type))) {
				ret = this;
			}
			else if(this.rootSimpleTypes.containsKey(getLocalName(type))) {
				ret = this;
			}
			else if(this.codelists.containsKey(getLocalName(type))) {
				ret = this;
			}
		}
		
		if(ret == null) {
			for(Include inc : this.includes) {
				if(inc.getLocalFile() != null) {
					ret = inc.getLocalFile().findSchemaForType(type, visited);
					if(ret != null) {
						break;
					}
				}
			}
		}
		if(ret == null) {
			for(Import imp : this.imports) {
				if(prefix != null && prefix.equals(imp.getPrefix())) {
					if(imp.getLocalFile() != null) {
						ret = imp.getLocalFile().findSchemaForType(getLocalName(type), visited);
						if(ret != null) {
							break;
						}
					}
				}
				else {
					if(imp.getLocalFile() != null) {
						ret = imp.getLocalFile().findSchemaForType(type, visited);
						if(ret != null) {
							break;
						}
					}
				}
			}
		}
		
		return ret;
	}
	
	public XTASchemaFileItem findSchemaForAttributeGroup(String group) {
		return findSchemaForAttributeGroup(group, new TreeSet<>());
	}
	
	public XTASchemaFileItem findSchemaForAttributeGroup(String group, TreeSet<String> visited) {
		if(visited.contains(this.getPath())) {
			return null;
		}
		visited.add(this.getPath());
		
		XTASchemaFileItem ret = null;
		
		String prefix = getPrefix(group);
		if(this.targetNamespace == null || this.targetNamespace.getPrefix() == null || this.targetNamespace.getPrefix().equals("null")) {
			if(this.attributeGroups.containsKey(getLocalName(group))) {
				ret = this;
			}
		}
		else if(prefix == null || prefix.equals(this.targetNamespace.getPrefix()) || prefix.equals("urn")) {
			if(this.attributeGroups.containsKey(getLocalName(group))) {
				ret = this;
			}
		}
		
		if(ret == null) {
			for(Include inc : this.includes) {
				if(inc.getLocalFile() != null) {
					ret = inc.getLocalFile().findSchemaForType(group, visited);
					if(ret != null) {
						break;
					}
				}
			}
		}
		if(ret == null) {
			for(Import imp : this.imports) {
				if(prefix != null && prefix.equals(imp.getPrefix())) {
					if(imp.getLocalFile() != null) {
						ret = imp.getLocalFile().findSchemaForAttributeGroup(getLocalName(group), visited);
						if(ret != null) {
							break;
						}
					}
				}
				else {
					if(imp.getLocalFile() != null) {
						ret = imp.getLocalFile().findSchemaForAttributeGroup(group, visited);
						if(ret != null) {
							break;
						}
					}
				}
			}
		}
//		
//		if(ret == null) {
//			for(Import imp : this.imports) {
//				if(imp.getLocalFile() != null) {
//					ret = imp.getLocalFile().findSchemaForAttributeGroup(group, visited);
//					if(ret != null) {
//						break;
//					}
//				}
//			}
//		}
//		
//		if(ret == null) {
//			for(Include inc : this.includes) {
//				if(inc.getLocalFile() != null) {
//					inc.getLocalFile().forcePrefixIfNull(this.targetNamespace);
//					
//					ret = inc.getLocalFile().findSchemaForAttributeGroup(group, visited);
//					if(ret != null) {
//						break;
//					}
//				}
//			}
//		}
		
		return ret;
	}
	
	public XTASchemaFileItem findSchemaForAttribute(String group) {
		return findSchemaForAttribute(group, new TreeSet<>());
	}
	
	public XTASchemaFileItem findSchemaForAttribute(String att, TreeSet<String> visited) {
		if(visited.contains(this.getPath())) {
			return null;
		}
		visited.add(this.getPath());
		
		XTASchemaFileItem ret = null;
		
		String prefix = getPrefix(att);
		if(this.targetNamespace == null || this.targetNamespace.getPrefix() == null || this.targetNamespace.getPrefix().equals("null")) {
			if(this.attributes.containsKey(getLocalName(att))) {
				ret = this;
			}
		}
		else if(prefix == null || prefix.equals(this.targetNamespace.getPrefix()) || prefix.equals("urn")) {
			if(this.attributes.containsKey(getLocalName(att))) {
				ret = this;
			}
		}
		
		if(ret == null) {
			for(Include inc : this.includes) {
				if(inc.getLocalFile() != null) {
					ret = inc.getLocalFile().findSchemaForAttribute(att, visited);
					if(ret != null) {
						break;
					}
				}
			}
		}
		if(ret == null) {
			for(Import imp : this.imports) {
				if(prefix != null && prefix.equals(imp.getPrefix())) {
					if(imp.getLocalFile() != null) {
						ret = imp.getLocalFile().findSchemaForAttribute(getLocalName(att), visited);
						if(ret != null) {
							break;
						}
					}
				}
				else {
					if(imp.getLocalFile() != null) {
						ret = imp.getLocalFile().findSchemaForAttribute(att, visited);
						if(ret != null) {
							break;
						}
					}
				}
			}
		}
//		if(ret == null) {
//			for(Import imp : this.imports) {
//				if(imp.getLocalFile() != null) {
//					ret = imp.getLocalFile().findSchemaForAttribute(att, visited);
//					if(ret != null) {
//						break;
//					}
//				}
//			}
//		}
//		
//		if(ret == null) {
//			for(Include inc : this.includes) {
//				if(inc.getLocalFile() != null) {
//					inc.getLocalFile().forcePrefixIfNull(this.targetNamespace);
//					
//					ret = inc.getLocalFile().findSchemaForAttribute(att, visited);
//					if(ret != null) {
//						break;
//					}
//				}
//			}
//		}
		
		return ret;
	}
	
	public ArrayList<XElement> findByPrefix(String prefix){
		ArrayList<XElement> ret = new ArrayList<XElement>();
		
		for(String name : this.rootElements.keySet()) {
			XElement el = this.rootElements.get(name);
			
			if(prefix == null) {
				ret.add(el);
			}
			else{
				if(prefix.startsWith("!")) {
					String pr = prefix.substring(1);
					if(!pr.equals(el.getPrefix())) {
						ret.add(el);
					}
				}
				else {
					StringTokenizer tok = new StringTokenizer(prefix);
					while(tok.hasMoreElements()) {
						String token = tok.nextToken();
						
						if(token.equals(el.getPrefix())) {
							ret.add(el);
							break;
						}
					}
				}
			}
		}
		
		for(Include inc : this.includes) {
			if(inc.getLocalFile() != null) {
				ArrayList<XElement> included = inc.getLocalFile().findByPrefix(prefix);
				ret.addAll(included);
			}
		}
		
		for(Import imp : this.imports) {
			if(imp.getLocalFile() != null) {
				ArrayList<XElement> imported = imp.getLocalFile().findByPrefix(prefix);
				ret.addAll(imported);
			}
		}
		
		return ret;
	}
	
	public ArrayList<XElement> findByNamespace(String namespace){
		ArrayList<XElement> ret = new ArrayList<XElement>();
		
		for(String name : this.rootElements.keySet()) {
			XElement el = this.rootElements.get(name);
			
			if(namespace == null) {
				ret.add(el);
			}
			else{
				if(namespace.startsWith("!")) {
					String pr = namespace.substring(1);
					if(!pr.equals(el.getSchema().getTargetNamespace().getNamespaceURL())) {
						ret.add(el);
					}
				}
				else {
					StringTokenizer tok = new StringTokenizer(namespace);
					while(tok.hasMoreElements()) {
						String token = tok.nextToken();
						
						if(token.equals(this.getTargetNamespace().getNamespaceURL())) {
							ret.add(el);
							break;
						}
					}
				}
			}
		}
		
		for(Include inc : this.includes) {
			if(inc.getLocalFile() != null) {
				ArrayList<XElement> included = inc.getLocalFile().findByNamespace(namespace);
				ret.addAll(included);
			}
		}
		
		for(Import imp : this.imports) {
			if(imp.getLocalFile() != null) {
				ArrayList<XElement> imported = imp.getLocalFile().findByNamespace(namespace);
				ret.addAll(imported);
			}
		}
		
		return ret;
	}
	
	public void addRootType(String name, XComplexType type) {
		this.rootTypes.put(getLocalName(name), type);
		type.setParent(this);
	}
	
	public void removeRootType(String name) {
		this.rootTypes.remove(getLocalName(name));
	}
	
	public XComplexType getRootType(String name) {
		return this.rootTypes.get(getLocalName(name));
	}
	
	public Set<String> getRootTypeKeySet() {
		return rootTypes.keySet();
	}

	public int getRootTypeCount() {
		return rootTypes.size();
	}

	public void addSimpleRootType(String name, XSimpleType type) {
		this.rootSimpleTypes.put(getLocalName(name), type);
		type.setParent(this);
	}
	
	public void removeSimpleRootType(String name) {
		this.rootSimpleTypes.remove(getLocalName(name));
	}
	
	public XSimpleType getRootSimpleType(String name) {
		return this.rootSimpleTypes.get(getLocalName(name));
	}
	
	public Set<String> getRootSimpleTypeKeySet() {
		return rootSimpleTypes.keySet();
	}

	public int getRootSimpleTypeCount() {
		return rootSimpleTypes.size();
	}
	
	public void addRootElement(String name, XElement element) {
		if(this.rootElements.containsKey(getLocalName(name))) {
		}
		else if(!element.getElementName().equals(getLocalName(name))) {
		}
		else {
			this.rootElements.put(getLocalName(name), element);
			element.setParent(this);
		}
	}
	
	public void removeRootElement(String name) {
		this.rootElements.remove(getLocalName(name));
	}
	
	public XElement getRootElement(String name) {
		return this.rootElements.get(getLocalName(name));
	}
//	
//	public void addTypedElement(String name, XElement element) {
//		this.namedElements.put(name, element);
//		element.setParent(this);
//	}
//	
//	public void removeTypedElement(String name) {
//		this.namedElements.remove(name);
//	}
//	
//	public XElement getTypedElement(String name) {
//		return this.namedElements.get(name);
//	}
	
	public void addCodelist(String name, XCodelist codelist) {
		this.codelists.put(getLocalName(name), codelist);
		codelist.setParent(this);
	}
	
	public void removeCodelist(String name) {
		this.codelists.remove(getLocalName(name));
	}
	
	public XCodelist getCodelist(String name) {
		return this.codelists.get(getLocalName(name));
	}
	
	public Set<String> getCodelistKeys() {
		return this.codelists.keySet();
	}
	
	public int getCodelistCount() {
		return this.codelists.size();
	}

	@Override
	public int getItemChildCount() {
		return this.rootTypes.size() + this.rootElements.size() + this.namedElements.size() + this.codelists.size() + this.rootSimpleTypes.size();
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		if(index < this.rootTypes.size()) {
			Object[] keys = this.rootTypes.keySet().toArray();
			return this.rootTypes.get(keys[index]);
		}
		else if(index < (this.rootTypes.size() + this.rootElements.size())) {
			Object[] keys = this.rootElements.keySet().toArray();
			return this.rootElements.get(keys[index - this.rootTypes.size()]);
		}
		else if(index < (this.rootTypes.size() + this.rootElements.size() + this.namedElements.size())) {
			Object[] keys = this.namedElements.keySet().toArray();
			return this.namedElements.get(keys[index - (this.rootTypes.size() + this.rootElements.size())]);
		}
		else if(index < (this.rootTypes.size() + this.rootElements.size() + this.namedElements.size() + this.codelists.size())) {
			Object[] keys = this.codelists.keySet().toArray();
			return this.codelists.get(keys[index - (this.rootTypes.size() + this.rootElements.size() + this.namedElements.size())]);
		}
		else{
			Object[] keys = this.rootSimpleTypes.keySet().toArray();
			return this.rootSimpleTypes.get(keys[index - (this.rootTypes.size() + this.rootElements.size() + this.namedElements.size() + this.codelists.size())]);
		}
	}
	
	@Override
	public String getItemType() {
		return "XSD_FILE_ITEM";
	}

	public TreeSet<NameSpace> getNamespaces() {
		return namespaces;
	}

	public TreeSet<NameSpace> recurseAllNamespaces() {
		return this.recurseAllNamespaces(new TreeSet<String>());
	}
	
	public TreeSet<NameSpace> recurseAllNamespaces(TreeSet<String> checked) {
		if(checked.contains(this.getItemId())) {
			return new TreeSet<NameSpace>();
		}
		checked.add(this.getItemId());
		
		TreeSet<NameSpace> ret = this.getNamespaces();
		
		for(Import imp : this.imports) {
			if(imp.getLocalFile() != null) {
				ret.addAll(imp.getLocalFile().recurseAllNamespaces(checked));
			}
		}
		
		for(Include inc : this.includes) {
			if(inc.getLocalFile() != null) {
				ret.addAll(inc.getLocalFile().recurseAllNamespaces(checked));
			}
		}
		
		return ret;
	}

	public TreeSet<Include> getIncludes() {
		return includes;
	}

	public TreeSet<Import> getImports() {
		return imports;
	}

	public NameSpace getTargetNamespace() {
		return targetNamespace;
	}

	public void setTargetNamespace(NameSpace targetNamespace) {
		this.targetNamespace = targetNamespace;
	}

	public String getDefaultElementForm() {
		return defaultElementForm;
	}

	public void setDefaultElementForm(String defaultElementForm) {
		this.defaultElementForm = defaultElementForm;
	}

	public String getDefaultAttributeForm() {
		return defaultAttributeForm;
	}

	public void setDefaultAttributeForm(String defaultAttributeForm) {
		this.defaultAttributeForm = defaultAttributeForm;
	}
//
//	public TreeMap<String, XComplexType> getRootTypes() {
//		return rootTypes;
//	}
//	
//	public TreeMap<String, XSimpleType> getRootSimpleTypes() {
//		return rootSimpleTypes;
//	}

	public Set<String> getRootElementKeys() {
		return rootElements.keySet();
	}

	public TreeMap<String,XElement> getRootElements() {
		return rootElements;
	}
	
	public void addNamedElement(String name, XElement element) {
		this.namedElements.put(getLocalName(name), element);
	}
	
	public XElement getNamedElement(String name) {
		if("gml:metaDataProperty".equals(name)) {
			System.out.println("hier");
		}
		XElement ret = null;
		try {
			String prefix = getPrefix(name);
			if(prefix != null && !prefix.equals(this.targetNamespace.getPrefix())) {
				XTASchemaFileItem schm = this.findSchemaForElement(name);
				if(schm != null) {
					ret = schm.getRootElement(name);
				}
			}
			else {
				ret = this.namedElements.get(getLocalName(name));
				
				if(ret == null) {
					XTASchemaFileItem schm = this.findSchemaForElement(name);
					if(schm != null) {
						ret = schm.getRootElement(name);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public boolean containsNamedElement(String name) {
		return this.namedElements.containsKey(getLocalName(name));
	}
//
//	public TreeMap<String, XElement> getNamedElements() {
//		return namedElements;
//	}

	public XElementGroup getElementGroup(String name) {
		return elementGroups.get(getLocalName(name));
	}

	public void addElementGroup(XElementGroup newType) {
		this.elementGroups.put(getLocalName(newType.getName()), newType);
	}

	public XAttributeGroup getAttributeGroup(String name) {
		return attributeGroups.get(getLocalName(name));
	}

	public void addAttributeGroup(XAttributeGroup newType) {
		this.attributeGroups.put(getLocalName(newType.getName()), newType);
	}

	public XAttribute getAttribute(String name) {
		String prefix = getPrefix(name);
		if(prefix != null) {
			if(prefix.equals(this.targetNamespace.getPrefix())) {
				return attributes.get(getLocalName(name));
			}
			else if(this.targetNamespace == null || this.targetNamespace.getPrefix() == null || this.targetNamespace.getPrefix().equals("null")) {
				return attributes.get(getLocalName(name));
			}
			else {
				return null;
			}
		}
		else {
			return attributes.get(name);
		}
	}

	public void addAttribute(String name, XAttribute attribute) {
		this.attributes.put(getLocalName(name), attribute);
	}

	public XComplexType findRootType(String typeName) {
		XTASchemaFileItem schema = this.findSchemaForType(typeName);
		
		if(schema != null) {
			return schema.getRootType(typeName);
		}
		else {
			return null;
		}
	}

	public XSimpleType findSimpleType(String typeName) {
		XTASchemaFileItem schema = this.findSchemaForType(typeName);
		
		if(schema != null) {
			return schema.getRootSimpleType(typeName);
		}
		else {
			return null;
		}
	}

	public boolean isTargetNamespaceInherited() {
		return targetNamespaceInherited;
	}

	public void setTargetNamespaceInherited(boolean targetNamespaceInherited) {
		this.targetNamespaceInherited = targetNamespaceInherited;
	}

	public boolean isAnalysisDone() {
		return analysisDone;
	}

	public void setAnalysisDone(boolean analysisDone) {
		this.analysisDone = analysisDone;
	}
}
