package de.init.xbauleitplanung.xmlxtatool.model.schema;

import java.io.Serializable;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * This class is used for organizing references to genericode lists occurring in both schema files and in XML files. 
 * Since XML files and schema files are analyzed one after another it may happen that references (listURIs) to 
 * genericode lists occur without the according listVersionIDs. However those may be found later on. 
 * ReferenceRegister helps organizing instances of listURIs and linking them to according (potentially multiple) listVersionIDs 
 * as they get found. 
 * 
 * @author treichling
 *
 */
public class ReferenceRegister implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7401171187227432044L;
	
	private TreeMap<String, TreeMap<String, ListVersionEntry>> referenceCache = new TreeMap<>();
	
	public TreeMap<String, ListVersionEntry> listVersionIds(String listURI) {
		if(!this.referenceCache.containsKey(listURI)) {
			this.referenceCache.put(listURI, new TreeMap<String, ListVersionEntry>());
		}
		return referenceCache.get(listURI);
	}
	
	public int size() {
		return referenceCache.size();
	}

	public TreeSet<String> listExistingVersionIds(String listURI) {
		TreeSet<String> ret = new TreeSet<String>();
		
		if(this.referenceCache.containsKey(listURI)) {
			TreeMap<String, ListVersionEntry> entries = this.referenceCache.get(listURI);
			
			for(String lv : entries.keySet()) {
				ListVersionEntry entry = entries.get(lv);
				if(entry.isExists()) {
					ret.add(lv);
				}
			}
		}
		
		return ret;
	}

	public TreeSet<String> listURIs() {
		TreeSet<String> ret = new TreeSet<String>();
		ret.addAll(referenceCache.keySet());
		
		return ret;
	}

	public void set(String listURI) {
		if(!this.referenceCache.containsKey(listURI)) {
			this.referenceCache.put(listURI, new TreeMap<String, ListVersionEntry>());
		}
	}
	
	public void set(String listURI, String listVersionId) {
		if(!this.referenceCache.containsKey(listURI)) {
			this.referenceCache.put(listURI, new TreeMap<String, ListVersionEntry>());
		}
		if(!this.referenceCache.get(listURI).containsKey(listVersionId)) {
			this.referenceCache.get(listURI).put(listVersionId, ListVersionEntry.create());
		}
	}
	
	public void set(String listURI, String listVersionId, boolean exists) {
		if(!this.referenceCache.containsKey(listURI)) {
			this.referenceCache.put(listURI, new TreeMap<String, ListVersionEntry>());
		}
		if(!this.referenceCache.get(listURI).containsKey(listVersionId)) {
			this.referenceCache.get(listURI).put(listVersionId, ListVersionEntry.create());
		}
		this.referenceCache.get(listURI).get(listVersionId).setChecked(true).setExists(exists);
	}
	
	public boolean isChecked(String listURI, String listVersionId) {
		boolean ret = false;
		
		if(this.referenceCache.containsKey(listURI)) {
			TreeMap<String, ListVersionEntry> ref = this.referenceCache.get(listURI);
			if(ref.containsKey(listVersionId)) {
				ret = ref.get(listVersionId).isChecked();
			}
		}
		
		return ret;
	}
	
	public boolean exists(String listURI, String listVersionId) {
		boolean ret = false;
		
		if(this.referenceCache.containsKey(listURI)) {
			TreeMap<String, ListVersionEntry> ref = this.referenceCache.get(listURI);
			if(ref.containsKey(listVersionId)) {
				ret = ref.get(listVersionId).isExists();
			}
		}
		
		return ret;
	}
	
//	
//	public TreeMap<String, ListReference> get(String id) {
//		if(!this.referenceCache.containsKey(id)) {
//			this.referenceCache.put(id, new TreeMap<String, ListReference>());
//		}
//		return this.referenceCache.get(id);
//	}
//	
//	public boolean containsKey(String id) {
//		return this.referenceCache.containsKey(id);
//	}
//	
//	public void put(String id, TreeMap<String, ListReference> val) {
//		this.referenceCache.put(id, val);
//	}
}