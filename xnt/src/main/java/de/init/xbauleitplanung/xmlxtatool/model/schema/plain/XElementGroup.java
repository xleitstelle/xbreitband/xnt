package de.init.xbauleitplanung.xmlxtatool.model.schema.plain;

import java.io.Serializable;
import java.util.ArrayList;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;

/**
 * This class represents an XML element group defined within a schema file
 *  
 * @author treichling
 *
 */
public class XElementGroup extends XTATreeItem implements Serializable, XTATreeItemI{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7244415882745131392L;
	
	private String prefix;
	private String localName;
	
	private ArrayList<XElement> elements = new ArrayList<>();
	
	public String getName() {
		return this.prefix + ":" + this.getLocalName();
	}
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public XElementGroup() {
		super();
	}

	public ArrayList<XElement> getElements() {
		return elements;
	}
	
	public XElement getElementByLocalName(String name) {
		XElement ret = null;
		
		for(XElement child : this.elements) {
			if(name.equals(child.getElementName())) {
				ret = child;
				break;
			}
		}
		
		return ret;
	}
	
	public XElement getElementByFullyQualifiedName(String name) {
		XElement ret = null;
		
		for(XElement child : this.elements) {
			if(name.equals(child.getFullyQualifiedName())) {
				ret = child;
				break;
			}
		}
		
		return ret;
	}

	@Override
	public String getItemName() {
		return this.getName();
	}

	@Override
	public String getItemType() {
		return "XElementGroup";
	}

	@Override
	public int getItemChildCount() {
		return 0;
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		return null;
	}

	public String getFullyQualifiedName() {
		String ret = null;
		
		if(this.prefix != null) {
			if(this.localName != null) {
				ret = this.prefix + ":" + this.localName;
			}
		}
		else {
			if(this.localName != null) {
				ret = this.localName;
			}
		}
		
		return ret;
	}

	@Override
	public String toString() {
		try {
			return this.prefix + ":" + this.localName;
		} catch (Exception e) {
			e.printStackTrace();
			
			return this.localName;
		}
	}

	public void addElement(XElement typed) {
		this.elements.add(typed);
	}
}
