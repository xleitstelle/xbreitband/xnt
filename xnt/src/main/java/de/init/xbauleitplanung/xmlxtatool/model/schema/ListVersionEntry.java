package de.init.xbauleitplanung.xmlxtatool.model.schema;

import java.io.Serializable;

/**
 * This class represents meta information of a list version entry. That is whether or not it has been checked for 
 * actual existence. In case it is checked, it is valid to access the exists status
 * 
 * @author treichling
 *
 */
public class ListVersionEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4460490856858658663L;

	private boolean checked = false;
	private boolean exists = false;
	
	public ListVersionEntry() {
		super();
	}
	
	public static ListVersionEntry create() {
		ListVersionEntry entry = new ListVersionEntry();
		
		return entry;
	}

	public boolean isChecked() {
		return checked;
	}

	public ListVersionEntry setChecked(boolean checked) {
		this.checked = checked;
		
		return this;
	}

	public boolean isExists() {
		return exists;
	}

	public ListVersionEntry setExists(boolean exists) {
		this.exists = exists;
		
		return this;
	}
}
