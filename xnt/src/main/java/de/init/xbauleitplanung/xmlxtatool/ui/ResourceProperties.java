package de.init.xbauleitplanung.xmlxtatool.ui;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectReader;
import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectWriter;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ResourceProperties extends BorderPane {
	public static final int STRING = 1;
	public static final int INTEGER = 2;
	public static final int FLOAT = 3;
	public static final int FILE = 4;
	public static final int FOLDER = 5;
	public static final int BOOLEAN = 6;
	public static final int PASSWORD = 9;
	public static final int GROUP = 10;
	
	private Stage stage;
	
	private VBox propertiesPanel;
	private HBox actionPanel;
	private Button applyButton;
	private Button importButton;
	private Button exportButton;
	
	private TreeMap<String, Property> propertyNames = new TreeMap<>();
	private ArrayList<Property> properties = new ArrayList<>();
	
	private String itemID;
	
	private File lastSelected = null;
	
	public ResourceProperties(EventHandler listener, Stage stage){
		super();
		
		this.stage = stage;
		
		this.propertiesPanel = new VBox();
		this.setCenter(propertiesPanel);
		
		this.actionPanel = new HBox();
		this.actionPanel.setPadding(new Insets(10.0));
		this.setBottom(this.actionPanel);
		
		this.applyButton = new Button("Übernehmen");
		this.applyButton.setOnAction(listener);
		this.actionPanel.getChildren().add(this.applyButton);
		
		this.importButton = new Button("Einstellungen importieren...");
		this.importButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				FileChooser chose = new FileChooser();
				chose.setTitle("Bitte wählen Sie eine Datei zum Import");
				
				File file = chose.showOpenDialog(stage);
				if(file != null) {
					HashMap<String, Serializable> content = (HashMap<String, Serializable>) ObjectReader.readObject(file.getAbsolutePath());
					
					if(content != null) {
						for(String att : content.keySet()) {
							setPropertyValue(att, content.get(att));
						}
					}
				}
			}
		});
		this.actionPanel.getChildren().add(this.importButton);
		
		this.exportButton = new Button("Einstellungen exportieren...");
		this.exportButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				FileChooser chose = new FileChooser();
				chose.setTitle("Bitte wählen Sie eine Datei zum Export");
				
				File file = chose.showSaveDialog(stage);
				if(file != null) {
					HashMap<String, Serializable> content = new HashMap();
					
					for(String att : propertyNames.keySet()) {
						content.put(att, (Serializable) getPropertyValue(att));
					}
					
					ObjectWriter.writeObject(content, file.getAbsolutePath());
				}
			}
		});
		this.actionPanel.getChildren().add(this.exportButton);
	}
	
	public void hideApplyButton() {
		this.applyButton.setVisible(false);
		this.applyButton.setManaged(false);
	}
	
	public void showApplyButton() {
		this.applyButton.setVisible(true);
		this.applyButton.setManaged(true);
	}

	public VBox getPropertiesPanel() {
		return propertiesPanel;
	}
	
	public void defineProperty(String property, 
			String name, 
			int type, 
			ArrayList<Object> choiceValues, 
			ArrayList<String> choiceNames, 
			ArrayList<String> choiceDescriptions) {
		this.defineProperty(property, name, type, choiceValues, choiceNames, choiceDescriptions, true);
	}
	
	public void defineProperty(String property, 
			String name, 
			int type, 
			ArrayList<Object> choiceValues, 
			ArrayList<String> choiceNames, 
			ArrayList<String> choiceDescriptions, 
			boolean editable) {
		Property prop;
		
		if(this.propertyNames.containsKey(property)) {
			this.propertyNames.remove(property);
			for(int i=0;i<this.properties.size();i++) {
				if(this.properties.get(i).property.equals(property)) {
					this.properties.remove(i);
					this.getChildren().remove(i);
					break;
				}
			}
		}
		
		prop = new Property();
		this.properties.add(prop);
		this.propertyNames.put(property, prop);
		
		prop.carrier = new BorderPane();
		prop.carrier.setPadding(new Insets(5.0));
		
		HBox valueCarrier = new HBox();
		valueCarrier.getStyleClass().add("xta-value-carrier");
		prop.carrier.setCenter(valueCarrier);
		
		prop.nameLabel = new Label(name);
		prop.nameLabel.setPrefWidth(300);
		prop.carrier.setLeft(prop.nameLabel);
		prop.editable = editable;
		
		if(choiceValues != null && choiceValues.size() > 1) {
			prop.options = new ComboBox<>();
			prop.options.valueProperty().addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
					prop.value = prop.options.getSelectionModel().getSelectedIndex();
				}
			});
			valueCarrier.getChildren().add(prop.options);
			prop.options.setPrefWidth(400);
			
			for(int i=0;i<choiceValues.size();i++) {
				prop.options.getItems().add(choiceNames.get(i));
			}
		}
		else if(type == STRING) {
			prop.valueField = new TextField();
			prop.valueField.setPrefWidth(400);
			prop.valueField.setEditable(prop.editable);
			valueCarrier.getChildren().add(prop.valueField);
			HBox.setHgrow(prop.valueField, Priority.ALWAYS);
		}
		else if(type == PASSWORD) {
			prop.valueField = new PasswordField();
			prop.valueField.setPrefWidth(400);
			prop.valueField.setEditable(prop.editable);
			valueCarrier.getChildren().add(prop.valueField);
			HBox.setHgrow(prop.valueField, Priority.ALWAYS);
		}
		else if(type == INTEGER) {
			prop.valueSpinner = new Spinner<Integer>();
			prop.valueSpinner.setPrefWidth(80);
			prop.valueSpinner.setEditable(prop.editable);
			prop.valueSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 0));
			valueCarrier.getChildren().add(prop.valueSpinner);
			HBox.setHgrow(prop.valueSpinner, Priority.ALWAYS);
		}
		else if(type == BOOLEAN) {
			prop.check = new CheckBox(name);
			prop.check.setPrefWidth(400);
			prop.check.setDisable(!prop.editable);
			valueCarrier.getChildren().add(prop.check);
			HBox.setHgrow(prop.check, Priority.ALWAYS);
		}
		else if(type == FILE) {
			prop.valueField = new TextField();
			prop.valueField.setPrefWidth(400);
			prop.valueField.setEditable(false);
			valueCarrier.getChildren().add(prop.valueField);
			HBox.setHgrow(prop.valueField, Priority.ALWAYS);
			
			prop.chooseButton = new Button("Datei auswaehlen...");
			prop.chooseButton.setDisable(!prop.editable);
			prop.chooseButton.setOnMouseClicked(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					FileChooser choose = new FileChooser();
					if(lastSelected != null) {
						choose.setInitialDirectory(lastSelected);
					}
					if(prop.value != null && "".equals(prop.value.toString())) {
						File check = new File(prop.value.toString());
						if(check.exists() && check.isDirectory()) {
							choose.setInitialDirectory(check);
						}
					}
					File sel = choose.showOpenDialog(stage);
					
					if(sel != null) {
						lastSelected = sel.getParentFile();
						prop.valueField.setText(sel.getAbsolutePath());
						prop.value = sel.getAbsolutePath();
					}
				}
			});
			prop.carrier.setRight(prop.chooseButton);
		}
		else if(type == FOLDER) {
			prop.valueField = new TextField();
			prop.valueField.setPrefWidth(400);
			prop.valueField.setEditable(false);
			valueCarrier.getChildren().add(prop.valueField);
			HBox.setHgrow(prop.valueField, Priority.ALWAYS);
			
			prop.chooseButton = new Button("Ordner auswaehlen...");
			prop.chooseButton.setDisable(!prop.editable);
			prop.chooseButton.setOnMouseClicked(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					DirectoryChooser choose = new DirectoryChooser();
					if(lastSelected != null) {
						choose.setInitialDirectory(lastSelected);
					}
					if(prop.value != null && "".equals(prop.value.toString())) {
						File check = new File(prop.value.toString());
						if(check.exists()) {
							choose.setInitialDirectory(check);
						}
					}
					File sel = choose.showDialog(stage);
					
					if(sel != null) {
						lastSelected = sel;
						prop.valueField.setText(sel.getAbsolutePath());
						prop.value = sel.getAbsolutePath();
					}
				}
			});
			prop.carrier.setRight(prop.chooseButton);
		}
		else{
			prop.valueField = new TextField();
			prop.valueField.setPrefWidth(400);
			prop.valueField.setEditable(prop.editable);
			HBox.setHgrow(prop.valueField, Priority.ALWAYS);
			valueCarrier.getChildren().add(prop.valueField);
		}
		
		this.propertiesPanel.getChildren().add(prop.carrier);
		
		prop.property = property;
		prop.name = name;
		prop.choiceValues = choiceValues;
		prop.choiceNames = choiceNames;
		prop.choiceDescriptions = choiceDescriptions;
		prop.type = type;
	}
	
	public void setPropertyValue(String property, Object value) {
		Property prop = this.propertyNames.get(property);
		
		if(value == null) {
			value = "";
		}
		
		prop.value = value;
		
		if(prop.choiceValues != null) {
			int index = prop.choiceValues.indexOf(value);
			if(index >= 0) {
				prop.options.getSelectionModel().select(index);
			}
		}
		else if(prop.type == INTEGER) {
			try {
				prop.valueSpinner.getValueFactory().setValue(Integer.parseInt(value.toString()));
			} catch (Exception e) {
				prop.valueSpinner.getValueFactory().setValue(0);
			}
		}
		else if(prop.type == STRING) {
			prop.valueField.setText(value.toString());
		}
		else if(prop.type == FILE) {
			prop.valueField.setText(value.toString());
		}
		else if(prop.type == BOOLEAN) {
			try {
				prop.check.setSelected(Boolean.parseBoolean(prop.value.toString()));
			} catch (Exception e) {
				e.printStackTrace();
				prop.value = false;
				prop.check.setSelected(false);
			}
		}
		else{
			prop.valueField.setText(value.toString());
		}
	}
	
	public Object getPropertyValue(String property) {
		Property prop = this.propertyNames.get(property);
		
		if(prop == null) {
			System.out.println("hier");
		}
		if(prop.choiceValues != null) {
			return prop.options.getSelectionModel().getSelectedIndex();
		}
		else if(prop.type == STRING) {
			prop.value = prop.valueField.getText();
		}
		else if(prop.type == PASSWORD) {
			prop.value = prop.valueField.getText();
		}
		else if(prop.type == INTEGER) {
			prop.value = prop.valueSpinner.getValue();
		}
		else if(prop.type == BOOLEAN) {
			prop.value = prop.check.isSelected();
		}
		else{
			prop.value = prop.valueField.getText();
		}
		return prop.value;
	}
	
	public Object getProperty(String property) {
		return this.propertyNames.get(property).value;
	}
	
	class Property implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = -3545626147567663743L;
		
		private String property;
		private String name;
		private Object value;
		private boolean editable = true;
		
		private ArrayList<Object> choiceValues;
		private ArrayList<String> choiceNames;
		private ArrayList<String> choiceDescriptions;
		private int type;
		
		private BorderPane carrier;
		private Label nameLabel;
		private TextField valueField;
		private Spinner<Integer> valueSpinner;
		private ComboBox<String> options;
		private CheckBox check;
		private Button chooseButton;
	}

	public Button getApplyButton() {
		return applyButton;
	}

	public String getItemID() {
		return itemID;
	}

	public void setItemID(String itemID) {
		this.itemID = itemID;
	}
}
