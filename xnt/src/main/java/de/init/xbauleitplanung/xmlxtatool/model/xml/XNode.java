package de.init.xbauleitplanung.xmlxtatool.model.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.UUID;

import de.init.xbauleitplanung.xmlxtatool.model.schema.hierarchical.XModelTreeNode;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAXMLFileItem;

/**
 * XNode represents a single XML element node within an XML structure including all relevant 
 * properties (name, prefix, attributes, type, ...)
 * 
 * @author treichling
 *
 */
public class XNode extends XTATreeItem implements Serializable, XTATreeItemI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 261692559398920826L;

	// Different types of nodes
	public static final int UNKNOWN = 0;
	public static final int ELEMENT = 1;
	public static final int TEXT = 2;
	public static final int COMMENT = 3;

	// FIelds for prefix, name and value
	private String prefix;
	private String name;
	private String value;
	
	// In case this node contains a code value, the following fields provide deeper 
	// information on which type of code is used, whether its value is resolved ...
	private String resolved = null;
	private String codeType;
	private String codelistType;
	private int xoevType = 0;
	private String schemaType;
	private String schemaId;
	
	// Refers to the simple or complex type of this node
	private transient XModelTreeNode typeNode;
	
	// alternative and static ID including parent nodes IDs
	private transient String pathId = null;
	
	// In case this node is of type xs:any, offspring indicates the XModelTreeNode parent element of the xs:any
	private XModelTreeNode offspring;
	
	// Attributes and child nodes of this node
	private TreeMap<String, String> attributes = new TreeMap<>();
//	private ArrayList<XNode> children = new ArrayList<>();
	
	// type of this node
	private int type = UNKNOWN;
	
	private int lineNumber = -1;
	
	public XNode() {
		super();
		
		this.setItemId(UUID.randomUUID().toString());
	}
	
	public XNode(String name) {
		this();
		
		setName(name);
	}

	public String getName() {
		return name;
	}

	public String getQualifiedName() {
		if(this.prefix != null) {
			return this.prefix + ":" + this.name;
		}
		else {
			return this.name;
		}
	}

	public XNode setName(String name) {
		this.name = name;
		
		return this;
	}
	
	/**
	 * Calculate an alternative and static ID including parent nodes IDs
	 * @return	an alternative and static ID including parent nodes IDs
	 */
	public String getPathId() {
		if(this.pathId == null) {
			pathId = this.name;
			
			if(this.getParent() != null) {
				if(this.getParent() instanceof XNode) {
					XNode parent = (XNode) this.getParent();
					int index = parent.children.indexOf(this);
					
					pathId = parent.getPathId() + "[" + index + "]#" + pathId;
				}
				else {
					XTATreeItemI parent = this.getParent();
					
					pathId = parent.getItemId() + "#" + pathId;
				}
			}
		}
		
		return pathId;
	}
	
	/**
	 * Returns the corresponding XModelTreeNode to this XNode (that is the type of this Xnode)
	 * @return	the corresponding XModelTreeNode to this XNode (that is the type of this Xnode)
	 */
	public XModelTreeNode getTypeElement() {
		XModelTreeNode ret = null;
		
		if(this.getParent() != null && this.getParent() instanceof XNode) {
			XNode parent = (XNode) this.getParent();
			
			if(parent.getTypeNode() != null) {
				XModelTreeNode parentType = parent.getTypeNode();
				
				for(int i=0;i<parentType.getChildCount();i++) {
					XModelTreeNode siblingElement = parentType.getChildAt(i);
					if(siblingElement.getStandardElementName().equals(this.getName())) {
						ret = siblingElement;
						break;
					}
				}
			}
		}
		
		return ret;
	}

	public ArrayList<XNode> getNodes() {
		ArrayList<XNode> ret = new ArrayList<XNode>();
		
		for(XTATreeItemI child : getChildren()) {
			if(child instanceof XNode) {
				ret.add((XNode) child);
			}
		}
		return ret;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String getItemName() {
		return this.getName();
	}

	@Override
	public String getItemType() {
		return "XNode";
	}

	@Override
	public int getItemChildCount() {
		return this.getChildren().size();
	}

	@Override
	public XNode getItemChildAt(int index) {
		try {
			return (XNode) this.getChildren().get(index);
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	public XNode getChildForName(String name) {
		for(XTATreeItemI child : this.children) {
			if(child instanceof XNode) {
				if(name.equals(((XNode) child).getName())) {
					return (XNode) child;
				}
			}
		}
		return null;
	}

	public String getValue() {
		return value;
	}

	public XNode setValue(String value) {
		this.value = value;
		
		return this;
	}

	public TreeMap<String, String> getAttributes() {
		return attributes;
	}

	public String getResolved() {
		return resolved;
	}

	public void setResolved(String resolved) {
		this.resolved = resolved;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getCodelistType() {
		return codelistType;
	}

	public void setCodelistType(String codelistType) {
		this.codelistType = codelistType;
	}

	public int getXoevType() {
		return xoevType;
	}

	public void setXoevType(int xoevType) {
		this.xoevType = xoevType;
	}

	public void setSchemaId(String itemId) {
		this.schemaId = itemId;
	}

	public String getSchemaId() {
		return schemaId;
	}

	public String getSchemaType() {
		return schemaType;
	}

	public void setSchemaType(String schemaType) {
		this.schemaType = schemaType;
	}

	public int getType() {
		return type;
	}

	public XNode setType(int type) {
		this.type = type;
		
		return this;
	}

	/**
	 * Add a new child node to this XNode
	 * @param child	the new child node to be added to this XNode
	 */
	public void addChild(XNode child) {
		this.getChildren().add(child);
		child.setParent(this);
	}

	/**
	 * Inserts a child element node to this node
	 * @param child	the child to be added
	 * @param positions	a map of target positions for specific elements to be inserted at
	 */
	public void insertChild(XNode child, TreeMap<String, Integer> positions) {
		int position = 0;
		String fullName = child.getStandardName();
		if(positions.containsKey(fullName)) {
			position = positions.get(fullName);
		}
		
		int realPos = 0;
		int significantPos = 0;
		for(int i=0;i<this.getItemChildCount();i++) {
			if(realPos == position) {
				significantPos = i;
				break;
			}
			
			if(!"#text".equals(this.getItemChildAt(i).getName())) {
				realPos++;
			}
		}
		this.getChildren().add(significantPos, child);
		child.setParent(this);
	}

	private String getStandardName() {
		if(this.typeNode != null) {
			return this.typeNode.getStandardElementName();
		}
		else {
			return this.name;
		}
	}

	public void removeChild(XNode child) {
		this.getChildren().remove(child);
	}

	public XNode cloneNode() {
		XNode ret = new XNode();
		
		ret.setCodelistType(this.getCodelistType());
		ret.setCodeType(this.getCodeType());
		ret.setItemId(this.getItemId());
		ret.setItemName(this.getItemName());
		ret.setName(this.getName());
		ret.setPrefix(this.getPrefix());
		ret.setResolved(this.getResolved());
		ret.setSchemaId(this.getSchemaId());
		ret.setSchemaType(this.getSchemaType());
		ret.setType(this.getType());
		ret.setTypeNode(this.getTypeNode());
		ret.setValue(this.getValue());
		ret.setXoevType(this.getXoevType());
		
		for(String att : this.attributes.keySet()) {
			ret.getAttributes().put(att, new String(this.attributes.get(att)));
		}
		
		for(int i=0;i<this.getChildren().size();i++) {
			ret.addChild(((XNode) this.getChildren().get(i)).cloneNode());
		}
		
		return ret;
	}

	public XModelTreeNode getTypeNode() {
		return typeNode;
	}

	public void setTypeNode(XModelTreeNode typeNode) {
		this.typeNode = typeNode;
	}
	
	public XNode getNodeForName(String name) {
		XNode ret = null;
		
		for(XTATreeItemI node : this.getChildren()) {
			XNode nd = (XNode) node;
			if(nd.getName().equals(name)) {
				ret = nd;
				break;
			}
		}
		return ret;
	}
	
	public ArrayList<XNode> getElements(String name) {
		ArrayList<XNode> ret = new ArrayList<XNode>();
		
		for(XTATreeItemI node : this.getChildren()) {
			try {
				XNode nd = (XNode) node;
				if(name.equals(nd.getName())) {
					ret.add(nd);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	public void clearTextNodes() {
		for(int i=this.getChildren().size() - 1; i>=0;i--) {
			XNode child = (XNode) this.getChildren().get(i);
			
			if(child.type == XNode.TEXT) {
				this.getChildren().remove(i);
			}
			else {
				child.clearTextNodes();
			}
		}
	}
	
	public ArrayList<XNode> find(String element, String value) {
		ArrayList<XNode> ret = new ArrayList<XNode>();
		
		this.find(element, value, ret, -1);
		
		return ret;
	}
	
	public ArrayList<XNode> find(String element, String value, int depth) {
		ArrayList<XNode> ret = new ArrayList<XNode>();
		
		this.find(element, value, ret, depth);
		
		return ret;
	}
	
	public void find(String element, String value, ArrayList<XNode> buffer, int depth){
		if(this.getChildren().size() == 0) {
			if(this.type == XNode.ELEMENT) {
				if(depth <= 0) {
					if(this.name.equals(element)) {
						if(value != null && value.equals(this.value)) {
							buffer.add(this);
						}
					}
				}
			}
		}
		else {
			if(depth != 0) {
				for(XTATreeItemI child : this.getChildren()) {
					XNode nd = (XNode) child;
					nd.find(element, value, buffer, depth - 1);
				}
			}
		}
	}

	@Override
	public String toString() {
		try {
			if(this.prefix != null) {
				return this.prefix + ":" + this.name + (this.value != null ? " - " + this.value : "");
			}
			else {
				return this.name + (this.value != null ? " - " + this.value : "");
			}
		} catch (Exception e) {
			return super.toString();
		}
	}

	public String getSchemaLocation() {
		String ret = null;
		
		if(this.getType() == XNode.ELEMENT) {
			if(this.getTypeNode() != null) {
				ret = this.getTypeNode().getSchemaLocation();
			}
		}
		
		return ret;
	}

	public XModelTreeNode getOffspring() {
		return offspring;
	}

	public void setOffspring(XModelTreeNode offspring) {
		this.offspring = offspring;
	}
	
	public XTASchemaFileItem getRootSchema() {
		XTASchemaFileItem ret = null;
		
		XTATreeItemI cur = this;
		while(cur.getParent() != null && cur.getParent() instanceof XNode) {
			cur = cur.getParent();
		}
		
		if(cur.getParent() instanceof XTAXMLFileItem) {
			try {
				ret = ((XNode) cur).getTypeNode().getElement().getSchema();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return ret;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
}
