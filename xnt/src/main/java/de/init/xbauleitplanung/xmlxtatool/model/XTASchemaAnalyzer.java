package de.init.xbauleitplanung.xmlxtatool.model;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import de.init.xbauleitplanung.xmlxtatool.controller.XTAController;
import de.init.xbauleitplanung.xmlxtatool.model.schema.Import;
import de.init.xbauleitplanung.xmlxtatool.model.schema.Include;
import de.init.xbauleitplanung.xmlxtatool.model.schema.NameSpace;
import de.init.xbauleitplanung.xmlxtatool.model.schema.ReferenceRegister;
import de.init.xbauleitplanung.xmlxtatool.model.schema.WorkLeft;
import de.init.xbauleitplanung.xmlxtatool.model.schema.hierarchical.XModelTreeNode;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XAttribute;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XAttributeGroup;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XCodelist;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XComplexType;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElement;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElementGroup;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElementProxy;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XSimpleType;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFolderResource;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAGenericodeList;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;
import de.init.xbauleitplanung.xmlxtatool.model.xml.XNode;
import de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI;
import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectReader;
import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectWriter;

public class XTASchemaAnalyzer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 83263580408849674L;

    public final static String LINE_NUMBER_KEY_NAME = "lineNumber";

	private XTAClientI ui;
	private String sourceFolder;
	
	// For each folder resource there is a mapping of remote schema resources to their local copies
	private TreeMap<String, TreeMap<String, String>> localSchemaCache = new TreeMap<>();
	private TreeMap<String, TreeMap<String, String>> reverseLocalSchemaCache = new TreeMap<>();

	// Cache for found references of genericode lists within xml or xsd files belonging to a certain resource
	private TreeMap<String, ReferenceRegister> referenceCache = new TreeMap<String, ReferenceRegister>();
	
	// www cache for faster access
	private TreeMap<String, String> wwwCache = new TreeMap<String, String>();
	
	// Set of URLs which are currently processed for caching
	private TreeSet<String> workingOn = new TreeSet<>();

	// Set of URLs which were identified to become redirected (and don't need to be checked once more)
	TreeMap<String, String> redirectCache = new TreeMap<String, String>();

	public XTASchemaAnalyzer(
			XTAClientI ui, 
			String sourceFolder) {
		super();
		
		this.ui = ui;
		this.sourceFolder = sourceFolder;
		
		this.ui.setStatusText("Lade local schema cache...");
		this.localSchemaCache = (TreeMap<String, TreeMap<String, String>>) ObjectReader.readObject(this.sourceFolder + "/localSchemaCache.dat");
		if(this.localSchemaCache == null) {
			this.localSchemaCache = new TreeMap<>();
		}
		
		this.ui.setStatusText("Lade reverse local schema cache...");
		this.reverseLocalSchemaCache = (TreeMap<String, TreeMap<String, String>>) ObjectReader.readObject(this.sourceFolder + "/reverseLocalSchemaCache.dat");
		if(this.reverseLocalSchemaCache == null) {
			this.reverseLocalSchemaCache = new TreeMap<>();
		}
		
		this.ui.setStatusText("Lade reference cache...");
		referenceCache = (TreeMap<String, ReferenceRegister>) ObjectReader.readObject(this.sourceFolder + "/referenceCache.dat");
		if(referenceCache == null) {
			referenceCache = new TreeMap<String, ReferenceRegister>();
		}
		
		final String sf = sourceFolder;
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				ui.setStatusText("Lade wwwCache...");
				wwwCache = (TreeMap<String, String>) ObjectReader.readObject(sf + "/wwwCache.dat");
				if(wwwCache == null) {
					wwwCache = new TreeMap<>();
				}
				ui.setStatusText("fertig");
			}
		});
		t.start();
	}
	
	/**
	 * Persist the current state of this XTASchemaAnalyzer 
	 */
	public void write() {
		ObjectWriter.writeObject(this.localSchemaCache, this.sourceFolder + "/localSchemaCache.dat");
		ObjectWriter.writeObject(this.reverseLocalSchemaCache, this.sourceFolder + "/reverseLocalSchemaCache.dat");
		ObjectWriter.writeObject(this.wwwCache, this.sourceFolder + "/wwwCache.dat");
		ObjectWriter.writeObject(this.referenceCache, this.sourceFolder + "/referenceCache.dat");
	}

	/**
	 * Returns the ReferenceRegister object for a given folder resource
	 * @param id	A folder resource
	 * @return	The ReferenceRegister object for the folder resource
	 */
	public ReferenceRegister getReferenceCache(String id) {
		if(!this.referenceCache.containsKey(id)) {
			this.referenceCache.put(id, new ReferenceRegister());
		}
		
		return referenceCache.get(id);
	}
	
	/**
	 * Performes a XPath query (expression) on a given Node instance. Returns the according NodeList object resulting from that query. 
	 * No exceptions shall be thrown or printed since queries are considered to potentially crash
	 * @param base	The Node instance to perform the query on
	 * @param expression	The XPath expression to carry out
	 * @return	The resulting NodeList object
	 */
	public static NodeList getNodeList(Node base, String expression) {
		try {
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile(expression);
			NodeList elements = (NodeList) expr.evaluate(base, XPathConstants.NODESET);
			
			return elements;
		} catch (XPathExpressionException e) {
			return null;
		}
	}
	
	/**
	 * Performes a XPath query (expression) on a given Node instance. Returns the according Node object resulting from that query. 
	 * No exceptions shall be thrown or printed since queries are considered to potentially crash
	 * @param base	The Node instance to perform the query on
	 * @param expression	The XPath expression to carry out
	 * @return	The resulting Node object
	 */
	public static Node getNode(Node base, String expression) {
		try {
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile(expression);
			Node element = (Node) expr.evaluate(base, XPathConstants.NODE);
			
			return element;
		} catch (XPathExpressionException e) {
			return null;
		}
	}
	
	/**
	 * This method analyzes a given XNode object for contained and potential children. It assumes the XNode object is valid! Otherwise
	 * results may be misleading or wrong. 
	 * @param node	element node to be analyzed
	 * @param actualChildCount	container to store the number of present child elements for each child element of this element - the 
	 * number might be >1 in case maxOccurrences is >1
	 * @param potential	container to store potential element nodes which might be inserted later
	 * @return	a map which stores all (potential) insert positions for specific child elements which might be inserted later
	 */
	public TreeMap<String, Integer> analyzeNode(
			XNode node, 
			TreeMap<String, Integer> actualChildCount, 
			ArrayList<XModelTreeNode> potential) {
		TreeMap<String, Integer> insertPositions = new TreeMap<String, Integer>();
		
		// Hier wird registriert, welche Choices bereits 'belegt' sind
		TreeSet<String> choiceBasesCompleted = new TreeSet<String>();
		
		// Vorhandene Unterknoten analysieren und beziffern
		ArrayList<XNode> actualChildren = node.getNodes();
		for(XNode actC : actualChildren) {
			if(actC.getTypeNode() != null) {
				if(actC.getTypeNode().isChoiceChild() && actC.getTypeNode().getChoiceBase() != null) {
					choiceBasesCompleted.add(actC.getTypeNode().getChoiceBase());
				}
			}
			
			if(!actualChildCount.containsKey(actC.getName())) {
				actualChildCount.put(actC.getName(), 0);
			}
			int ct = actualChildCount.get(actC.getName());
			actualChildCount.put(actC.getName(), ++ct);
		}
		
		// Potenzielle Unterknoten (gem. Modell) analysieren
		int position = 0;
		if(node.getTypeNode() != null) {
			for(int i=0;i<node.getTypeNode().getChildCount();i++) {
				XModelTreeNode childAt = node.getTypeNode().getChildAt(i);
				
				if(childAt.isAny()) {
					String namespace = childAt.getAnyNamespacePrefix();
					XTASchemaFileItem rootSchema = node.getRootSchema();
					
					ArrayList<XElement> matchingElements = null;
					
					if("##other".equals(namespace)) {
						matchingElements = rootSchema.findByPrefix("!" + childAt.getSchema().getTargetNamespace().getPrefix());
					}
					else if("##any".equals(namespace)) {
						matchingElements = rootSchema.findByPrefix(null);
					}
					else {
						matchingElements = rootSchema.findByNamespace(namespace);
					}
					
					// prefixes enthält alle bekannten Präfixe
					TreeSet<String> prefixes = rootSchema.getAllPrefixes();
					
					// es sollen nur Typen aus bekannten Namespaces verwendet werden, deswegen werden alle 
					// aussortiert, die nicht bekannt sind
					for(int k=matchingElements.size() - 1;k>=0;k--) {
						if(!prefixes.contains(matchingElements.get(k).getPrefix())) {
							matchingElements.remove(k);
						}
					}
					
					for(XElement el : matchingElements) {
						XModelTreeNode temp = this.recurseElement(null, el, 0, new ArrayList<String>(), new TreeMap<String, XModelTreeNode>(), el.getSchema(), false);
						
						potential.add(temp);
						
						int count = 0;
						if(actualChildCount.containsKey(temp.getStandardElementName())) {
							count = actualChildCount.get(temp.getStandardElementName());
						}
						else if(temp.getChoiceBase() != null && choiceBasesCompleted.contains(temp.getChoiceBase())) {
							actualChildCount.put(temp.getStandardElementName(), 1);
						}
						position += count;
						insertPositions.put(temp.getStandardElementName(), position);
					}
				}
				else {
					potential.add(childAt);
					
					int count = 0;
					if(actualChildCount.containsKey(childAt.getStandardElementName())) {
						count = actualChildCount.get(childAt.getStandardElementName());
					}
					else if(childAt.getChoiceBase() != null && choiceBasesCompleted.contains(childAt.getChoiceBase())) {
						actualChildCount.put(childAt.getStandardElementName(), 1);
					}
					position += count;
					insertPositions.put(childAt.getStandardElementName(), position);
				}
			}
		}
		
		return insertPositions;
	}
	
	// TODO: Hinterher löschen
	public int reCount = 0;
	
	/**
	 * Convert the (plain) XElement and its referenced sub structure into a (hierarchical) XModelTreeNode
	 * @param target	The target XModelTreeNode which becomes parent of the newly created XModelTreeNode
	 * @param element	The source XElement
	 * @param depth	the depth in the hierarchy
	 * @param typesInChain	Buffer for storing types already used on higher levels in the type hierarchy - this is to 
	 * prevent infinite loops in case of recursive references in XML schemas
	 * @param typeRegister	Stores the XModelTreeNode for each type already used on higher levels in the type hierarchy
	 * @param rootSchema	Default Schema which is supposed to keep the definition of element
	 * @param substitution	True if element is defined as a substitution
	 * @return	The created XModelTreeNode (new child of target)
	 */
	public XModelTreeNode recurseElement(
			XModelTreeNode target, 
			XElement element, 
			int depth, 
			ArrayList<String> typesInChain, 
			TreeMap<String, XModelTreeNode> typeRegister, 
			XTASchemaFileItem rootSchema, 
			boolean substitution) {
		try {
			reCount++;
			XModelTreeNode newNode = null;
			XComplexType type = null;
			XSimpleType simple = null;
			XTASchemaFileItem schema = rootSchema; //(XTASchemaFileItem) element.getSchema();
			boolean anyType = false;
			
			if(element.getElementType() != null) {
				// TODO: Das hier ist Schwachsinn. Warum schema UND rootSchema? Macht keinen Sinn! 
				
				if("anyType".equals(element.getElementType())) {
					anyType = true;
				}
				else {
					if(schema == null) {
						schema = element.getSchema();
						rootSchema = schema;
					}
					
					schema = schema.findSchemaForType(element.getElementType());
					if(schema != null) {
						type = schema.getRootType(element.getElementType());
						if(type == null) {
							simple = schema.getRootSimpleType(element.getElementType());
						}
					}
				}
			}
			
			if(anyType) {
				XComplexType any = new XComplexType();
				any.setItemName("xs:anyType");
				any.setLocalName("anyType");
				any.setPrefix("xs");
				
				newNode = new XModelTreeNode(any, element, null);
				newNode.setNamespacePrefix(element.getPrefix());
			}
			else {
				newNode = new XModelTreeNode(type, element, element.getRootSchema() != null ? element.getRootSchema().getPath() : null);
				newNode.setNamespacePrefix(element.getPrefix());
				
				if(typesInChain.contains(newNode.toString())) {
					XModelTreeNode pNode = typeRegister.get(newNode.toString());
					
					newNode = pNode.clone();
					newNode.setCircle(true);
					
					if(target != null) {
						target.add(newNode);
					}
				}
				else {
					if(type != null) {
						recurseType(newNode, type, depth + 1, typesInChain, typeRegister, false, rootSchema);
					}
					else if(simple != null) {
						element.setAnonymousSimpleType(simple);
					}
					if(element.getAnonymousType() != null) {
						recurseType(newNode, element.getAnonymousType(), depth + 1, typesInChain, typeRegister, false, rootSchema);
						
						if(element.getAnonymousType().getDerivationType() == XComplexType.SEQUENCE) {
							newNode.setTransparent(true);
						}
						else if(element.getAnonymousType().getDerivationType() == XComplexType.CHOICE) {
							newNode.setTransparent(true);
						}
					}
					
					if(schema != null) {
						element.setDefaultForm(schema.getDefaultElementForm());
					}
					
					if(substitution) {
						newNode.setSubstitution(true);
					}
					
					if(target != null) {
						target.add(newNode);
					}
					
					if(element.isAbstr()) {
						try {
							String substitionGroup = element.getFullyQualifiedName();
							
							if(element instanceof XElementProxy) {
								schema = (XTASchemaFileItem) ((XElementProxy) element).getTemplateElement().getSchema();
							}
							else {
								schema = (XTASchemaFileItem) element.getSchema();
							}
							
							if(schema != null) {
								for(String tp : schema.getRootElementKeys()) {
									XElement substituion = schema.getRootElement(tp);
									if(substitionGroup.equals(substituion.getSubstitutionGroup())) {
										XModelTreeNode child = recurseElement(newNode, substituion, depth + 1, typesInChain, typeRegister, rootSchema, true);
										if(child != null) {
											substituion.setChoiceChild(true, substitionGroup);
										}
									}
								}
							}
							typesInChain.add(newNode.toString());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			
			return newNode;
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	/**
	 * Convert the (plain) XCompexType and its referenced sub structure into a (hierarchical) XModelTreeNode
	 * @param node	The target XModelTreeNode to which to add sub elements of the input XComplexType
	 * @param type	The input XComplexType to convert
	 * @param depth	The depth in the XML hierarchy
	 * @param typesInChain	Buffer for storing types already used on higher levels in the type hierarchy - this is to 
	 * prevent infinite loops in case of recursive references in XML schemas
	 * @param typeRegister	Stores the XModelTreeNode for each type already used on higher levels in the type hierarchy
	 * @param noCheck	Indicates whether a check for potential occurrence of this type in earlier loops shall be omitted
	 * @param rootSchema	Default Schema which is supposed to keep the definition of type
	 */
	public void recurseType(
			XModelTreeNode node, 
			XComplexType type, 
			int depth, 
			ArrayList<String> typesInChain, 
			TreeMap<String, XModelTreeNode> typeRegister, 
			boolean noCheck, 
			XTASchemaFileItem rootSchema) {
//		System.out.print(typeRegister.size() + ",");
//		if(typeRegister.size() > 50) {
//			System.out.println("hier");
//		}
		if(noCheck || !typesInChain.contains(node.toString())) {
			typesInChain.add(node.toString());
			typeRegister.put(node.toString(), node);
			
			if(type.getComplexBase() != null) {
				if(type.getDerivationType() == XComplexType.EXTENSION) {
					XComplexType extensionBase = null;
					XTASchemaFileItem schema = rootSchema;
					
					if(extensionBase == null) {
						schema = schema.findSchemaForType(type.getComplexBase());
						if(schema != null) {
							extensionBase = schema.getRootType(type.getComplexBase());
						}
					}
					if(extensionBase != null) {
						recurseType(node, extensionBase, depth + 1, typesInChain, typeRegister, true, rootSchema);
					}
				}
			}
			else if(type.getSimpleBase() != null) {
				if(type.getDerivationType() == XComplexType.EXTENSION) {
					XSimpleType extensionBase = null;
					XTASchemaFileItem schema = rootSchema;
					
					if(extensionBase == null) {
						schema = schema.findSchemaForType(type.getSimpleBase());
						if(schema != null) {
							extensionBase = schema.getRootSimpleType(type.getSimpleBase());
						}
					}
				}
			}
			for(XElement el : type.getElements()) {
				recurseElement(node, el, depth + 1, typesInChain, typeRegister, rootSchema, false);
			}
			
			node.getAttributes().addAll(type.getAttributes());
			
			for(int i=0;i<type.getAttributeGroups().size();i++) {
				String group = type.getAttributeGroups().get(i);
				
				XTASchemaFileItem schema = rootSchema.findSchemaForAttributeGroup(group);
				if(schema != null) {
					XAttributeGroup grp = schema.getAttributeGroup(group);
					if(grp != null) {
						recurseAttributeGroups(node, grp, schema);
					}
				}
			}
			
			typesInChain.remove(typesInChain.size() - 1);
		}
		else {
			node.setCircle(true);
		}
	}
	
	/**
	 * Add all attributes of an attribute group to a given XModelTreeNode and seek for potential embedded attribute groupe
	 * @param node	The target node to add attributes to
	 * @param attributeGroup	The attribute group to add to node
	 * @param rootSchema	The schema file item which covers the definition of the attribute group
	 */
	private void recurseAttributeGroups(XModelTreeNode node, XAttributeGroup attributeGroup, XTASchemaFileItem rootSchema) {
		node.getAttributes().addAll(attributeGroup.getAttributes());
		
		for(int i=0;i<attributeGroup.getAttributeGroups().size();i++) {
			String group = attributeGroup.getAttributeGroups().get(i);
			XTASchemaFileItem schema = rootSchema.findSchemaForAttributeGroup(group);
			if(schema != null) {
				XAttributeGroup grp = schema.getAttributeGroup(group);
				recurseAttributeGroups(node, grp, rootSchema);
			}
		}
	}
	
	/**
	 * This method does some prework for the analysis process of a given folder structure. It recurses a folder structure
	 * and initiates an analysis for each found XML schema file
	 * @param parent	The file or folder to be analyzed 
	 */
	public void recurseDirForNamedElementsAndAttributes(XTAFileItem parent) {
		File dir = new File(parent.getPath());
		
		if(dir.exists()) {
			File[] schemas = dir.listFiles();
			
			for(File sub : schemas) {
				if(sub.isFile()) {
					if(sub.getName().toLowerCase().endsWith(".xsd")) {
						if(!parent.coversPath(sub.getAbsolutePath())) {
							XTASchemaFileItem item = new XTASchemaFileItem();
							item.setPath(sub.getAbsolutePath());
							parent.addChild(item);
							this.scanSchemaForNamedElementsAndAttributes(item);
						}
					}
				}
				else if(sub.isDirectory()) {
					XTAFileItem item = new XTAFileItem();
					item.setPath(sub.getAbsolutePath());
					parent.addChild(item);
					recurseDirForNamedElementsAndAttributes(item);
				}
			}
		}
	}
	
	/**
	 * Elementary method for the analysis of a given XML schema file. Results of the analysis (elements types groups) will be written to the 
	 * target schema file item
	 * @param file	The XML schema file to be analyzed
	 * @param item	The target XML schema file item
	 * @param root	The folder resource which covers the schema file
	 * @param cache	The schema cache used to cache remote schema files to local copies
	 */
	public void scanSchema(
			File file, 
			XTASchemaFileItem item, 
			XTAFolderResource root, 
			Map<String, XTASchemaFileItem> 
			cache, TreeSet<WorkLeft> workLeft) {
		try {
			// Try to determine dafaultElementFor and default AttributeForm from the schema file
			String defaultElementForm = getDefaultElementFormFromSchema(file);
			item.setDefaultElementForm(defaultElementForm);
			
			String defaultAttributeForm = getDefaultAttributeFormFromSchema(file);
			item.setDefaultAttributeForm(defaultAttributeForm);
			
			// Try to determine a target namespace from the schema
			NameSpace nameSpace = item.getTargetNamespace();
			if(item.getTargetNamespace() == null) {
				nameSpace = getTargetNamespaceFromSchema(file);
				item.setTargetNamespace(nameSpace);
			}
			
			// Scan includes and imports first
			scanImports(file, item, root, cache, workLeft);
			
			// Scan content of the schema (elements, types, groups)
			scanSchemaContent(file, item, root, defaultElementForm, defaultAttributeForm, nameSpace, workLeft);
		} catch (DOMException e) {
			e.printStackTrace();
		}
		
		// A flag is set to indicate that this schema file has been completely analyzed
		item.setAnalysisDone(true);
	}

	private void scanSchemaContent(File file, XTASchemaFileItem item, XTAFolderResource root, String defaultElementForm,
			String defaultAttributeForm, NameSpace nameSpace, TreeSet<WorkLeft> workLeft) {
		// Analyze attributes first...
		NodeList attributeResult = getAttributesFromSchema(file);
		
		if(attributeResult != null) {
			for(int i=0;i<attributeResult.getLength();i++) {
				Node node = attributeResult.item(i);
				
				Node attNameNode = node.getAttributes().getNamedItem("name");
				Node typeNode = node.getAttributes().getNamedItem("type");
				Node fixedNode = node.getAttributes().getNamedItem("fixed");
				
				XAttribute att = new XAttribute();
				att.setName(nameSpace.getPrefix(), attNameNode.getNodeValue());
				if(typeNode != null) {
					att.setType(typeNode.getNodeValue());
				}
				if(fixedNode != null) {
					att.setFixed(fixedNode.getNodeValue());
				}
				
				// Global attributes MUST always be qualified
				att.setForm("qualified");
				
				String attName = att.getName();
				item.addAttribute(attName, att);
			}
		}
		
		// Analyze AttributeGroups afterwards...
		NodeList attributeGroupResult = getAttributeGroupsFromSchema(file);
		
		// Analyze attribute groups...
		if(attributeGroupResult != null) {
			for(int i=0;i<attributeGroupResult.getLength();i++) {
				Node node = attributeGroupResult.item(i);
				
				XAttributeGroup newAttGroup = scanAttributeGroup(item, nameSpace.getPrefix(), node, defaultElementForm, defaultAttributeForm);
				newAttGroup.setItemId(file.getAbsolutePath() + "#" + newAttGroup.getFullyQualifiedName());
				newAttGroup.setParent(item);
				
				item.addAttributeGroup(newAttGroup);
			}
		}
		
		// Analyze element groups...
		NodeList elementGroupResult = getElementGroupsFromSchema(file);
		
		if(elementGroupResult != null) {
			for(int i=0;i<elementGroupResult.getLength();i++) {
				Node node = elementGroupResult.item(i);
				
				XElementGroup newType = scanElementGroup(item, nameSpace.getPrefix(), node, root, defaultElementForm, defaultAttributeForm, workLeft);
				newType.setItemId(file.getAbsolutePath() + "#" + newType.getFullyQualifiedName());
				newType.setParent(item);
				
				item.addElementGroup(newType);
			}
		}
		
		// Analyze complex types...
		NodeList complexResult = getComplexTypesFromSchema(file);
		
		if(complexResult != null) {
			for(int i=0;i<complexResult.getLength();i++) {
				Node node = complexResult.item(i);
				
				// Check whether some complex type defines a code list
				NodeList codes = getCodelistFromComplexNode(node);
				NodeList headers = getCodelistHeadersFromComplexNode(node);
				String usedKey = getCodelistKeyFromComplexNode(node);
				
				XComplexType newType = scanComplexType(item, nameSpace.getPrefix(), node, root, defaultElementForm, defaultAttributeForm, workLeft);
				newType.setItemId(file.getAbsolutePath() + "#" + newType.getFullyQualifiedName());
				newType.setParent(item);
				
				if(codes.getLength() > 0) {
					XCodelist cl = new XCodelist();
					
					cl.setPrefix(newType.getPrefix());
					cl.setLocalName(newType.getLocalName());
					cl.setItemId(item.getPath() + "#" + nameSpace + ":" + newType.getName());
					
					if(headers.getLength() > 0) {
						for(int j=0;j<headers.getLength();j++) {
							Node hdr = headers.item(j);
							String id = hdr.getNodeName();
							if(usedKey != null && usedKey.equals(id)) {
								// Omit codeKey
							}
							else {
								cl.getHeader().add(id);
							}
						}
						if(usedKey != null) {
							cl.getHeader().add(0, usedKey);
						}
					}
					else {
						cl.getHeader().add("code");
						cl.getHeader().add("value");
					}
					this.ui.setProgressMax(codes.getLength());
					
					readCodes(codes, cl, headers.getLength() > 0, usedKey);
					
					item.addCodelist(cl.getName(), cl);
					item.addRootType(newType.getName(), newType);
				}
				else {
					item.addRootType(newType.getName(), newType);
				}
			}
		}
		
		// Analyze elements themselves
		NodeList elementResult = getElementsFromSchema(file);
		
		if(elementResult != null) {
			for(int i=0;i<elementResult.getLength();i++) {
				Node node = elementResult.item(i);

				XElement newElement = this.scanElement(item, nameSpace.getPrefix(), node, false, null, root, defaultElementForm, defaultAttributeForm, workLeft);
				newElement.setItemId(file.getAbsolutePath() + "#" + newElement.getFullyQualifiedName());
				newElement.setForm("qualified");
				
				item.addRootElement(newElement.getElementName(), newElement);
				
				root.addRootElement(newElement);
			}
		}
		
		// Analyze simple types
		NodeList simpleResult = getSimpleTypesFromSchema(file);
		
		if(simpleResult != null) {
			for(int i=0;i<simpleResult.getLength();i++) {
				try {
					Node child = simpleResult.item(i);
					
					XPathFactory xPathfactory = XPathFactory.newInstance();
					XPath xpath = xPathfactory.newXPath();
					
					XPathExpression expr = xpath.compile("restriction/enumeration/..");
					Node clData = (Node) expr.evaluate(child, XPathConstants.NODE);
					
					// Simple types and code lists will be distinguished
					if(clData != null) {
						expr = xpath.compile("restriction/enumeration");
						NodeList result = (NodeList) expr.evaluate(child, XPathConstants.NODESET);
						
						this.ui.setProgressMax(result.getLength());
						
						XCodelist cl = new XCodelist();
						
						cl.setPrefix(nameSpace.getPrefix());
						cl.setLocalName(child.getAttributes().getNamedItem("name").getNodeValue());
						cl.setItemId(item.getPath() + "#" + nameSpace.getPrefix() + ":" + child.getAttributes().getNamedItem("name").getNodeValue());
						
						expr = xpath.compile("annotation/appinfo/codelistenspalten/child::*");
						NodeList headerData = (NodeList) expr.evaluate(child, XPathConstants.NODESET);
						
						if(headerData != null) {
							for(int h=0;h<headerData.getLength();h++) {
								Node header = headerData.item(h);
								
								cl.getHeader().add(header.getNodeName());
							}
						}
						
						this.ui.setProgressMax(result.getLength());
						
						if(cl.getHeader().size() > 0) {
							// Header sind vorgegeben (Codelisten-Version > 2.1)
							
							for(int j=0;j<result.getLength();j++) {
								this.ui.setProgressValue(j);
								
								Node subChild = result.item(j);
								
								expr = xpath.compile("@value");
								String name = (String) expr.evaluate(subChild, XPathConstants.STRING);
								
								ArrayList<String> row = new ArrayList<String>();
								row.add(name);
								
								for(int hi = 1;hi<cl.getHeader().size();hi++) {
									expr = xpath.compile("annotation/appinfo/" + cl.getHeader().get(hi));
									String value = (String) expr.evaluate(subChild, XPathConstants.STRING);
									
									row.add(value);
								}
								cl.getValues().add(row);
							}
						}
						else {
							// Header sind NICHT vorgegeben (Codelisten-Version 1.0)
							
							TreeMap<String, Integer> headerIndex = new TreeMap<String, Integer>();
							headerIndex.put("code", 0);
							int indexCount = 1;
							cl.getHeader().add("code");
							
							for(int j=0;j<result.getLength();j++) {
								this.ui.setProgressValue(j);
								
								Node subChild = result.item(j);
								
								expr = xpath.compile("@value");
								String name = (String) expr.evaluate(subChild, XPathConstants.STRING);
								
								expr = xpath.compile("annotation/appinfo/child::*");
								NodeList rowData = (NodeList) expr.evaluate(subChild, XPathConstants.NODESET);
								
								TreeMap<Integer, String> rowDataMap = new TreeMap<Integer, String>();
								
								for(int ri = 0;ri<rowData.getLength();ri++) {
									String rowName = rowData.item(ri).getNodeName();
									String rowValue = rowData.item(ri).getTextContent();
									
									if(!headerIndex.containsKey(rowName)) {
										headerIndex.put(rowName, indexCount++);
										cl.getHeader().add(rowName);
									}
									rowDataMap.put(headerIndex.get(rowName), rowValue);
								}
								
								ArrayList<String> row = new ArrayList<String>();
								row.add(name);
								for(int rf=1;rf<headerIndex.size();rf++) {
									row.add("");
								}
								
								for(int ri : rowDataMap.keySet()) {
									row.set(ri, rowDataMap.get(ri));
								}
								
								cl.getValues().add(row);
							}
							
							// Ggf. leere Werte anfügen, wenn Zeilen zu kurz
							for(int j=0;j<cl.getValues().size();j++) {
								while(cl.getValues().get(j).size() < headerIndex.size()) {
									cl.getValues().get(j).add("");
								}
							}
						}
						
						// Codeliste ausgelesen. Wird hinzugefügt
						item.addCodelist(cl.getName(), cl);
					}
					else {
						XPathExpression exprGen = xpath.compile("restriction/enumeration");
						NodeList clInfoGen = (NodeList) exprGen.evaluate(child, XPathConstants.NODESET);
						
						if(clInfoGen != null && clInfoGen.getLength() > 0) {
							XCodelist cl = new XCodelist();
							
							cl.setPrefix(nameSpace.getPrefix());
							cl.setLocalName(child.getAttributes().getNamedItem("name").getNodeValue());
							cl.setItemId(item.getPath() + "#" + nameSpace.getPrefix() + ":" + child.getAttributes().getNamedItem("name").getNodeValue());
							
							cl.getHeader().add("Wert");
							
							for(int j=0;j<clInfoGen.getLength();j++) {
								Node subChild = clInfoGen.item(j);
								
								expr = xpath.compile("@value");
								String name = (String) expr.evaluate(subChild, XPathConstants.STRING);
								
								cl.addValues(name);
							}
							item.addCodelist(cl.getName(), cl);
						}
						else {
							String name = child.getAttributes().getNamedItem("name").getNodeValue();
							
							XSimpleType newType = scanSimpleType(child, file.getAbsolutePath(), nameSpace.getPrefix(), name);
							
							item.addSimpleRootType(newType.getName(), newType);
						}
					}
				} catch (XPathExpressionException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void scanImports(File file, XTASchemaFileItem item, XTAFolderResource root,
			Map<String, XTASchemaFileItem> cache, TreeSet<WorkLeft> workLeft) {
		// Try to read all used namespace attributes from the schema
		ArrayList<NameSpace> namespaces = new ArrayList<NameSpace>();
		ArrayList<Include> includes = new ArrayList<Include>();
		ArrayList<Import> imports = new ArrayList<Import>();
		
		readNamespacesAndImportsFromSchema(file, namespaces, includes, imports);
		
		item.getNamespaces().addAll(namespaces);
		item.getIncludes().addAll(includes);
		item.getImports().addAll(imports);
		
		// Recursively analyze all includes and imports first to make sure all referenced elements or type are known
		for(Include inc : includes) {
			if(!XTATreeItem.isWebResource(inc.getSchemaLocation())) {
				String incLocation = XTATreeItem.computePath(file.getAbsolutePath(), inc.getSchemaLocation());
				incLocation = XTATreeItem.trimPath(incLocation);
				
				if(!cache.containsKey(incLocation)) {
					File check = new File(incLocation);
					
					XTASchemaFileItem child = null;
					try {
						child = (XTASchemaFileItem) root.getDescendantForPath(check.getAbsolutePath());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(child != null) {
						child.setPath(check.getAbsolutePath());
						cache.put(incLocation, child);
						
						scanSchema(check, child, root, cache, workLeft);
					}
				}
				
				if(inc.getLocalFile() == null) {
					inc.setLocalFile(cache.get(incLocation));
				}
			}
		}
		
		for(Import imp : imports) {
			if(!XTATreeItem.isWebResource(imp.getSchemaLocation())) {
				String impLocation = XTATreeItem.computePath(file.getAbsolutePath(), imp.getSchemaLocation());
				impLocation = XTATreeItem.trimPath(impLocation);
				
				if(!cache.containsKey(impLocation)) {
					File check = new File(impLocation);
					
					XTASchemaFileItem child = null;
					try {
						child = (XTASchemaFileItem) root.getDescendantForPath(check.getAbsolutePath());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(child != null) {
						child.setPath(check.getAbsolutePath());
						
						cache.put(impLocation, child);
						scanSchema(check, child, root, cache, workLeft);
					}
				}
				
				if(imp.getLocalFile() == null) {
					imp.setLocalFile(cache.get(impLocation));
				}
			}
		}
	}

	/**
	 * Extract codes from a given NodeList object and write them to a codelist object (cl)
	 * @param codes	The NodeList which is considered to carry codes
	 * @param cl	The target code list
	 * @param useHeaders	If true, read headers from the first child of the NodeList object
	 * @param keyCol	ID of the key column
	 */
	private void readCodes(NodeList codes, XCodelist cl, boolean useHeaders, String keyCol) {
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		
		if(useHeaders) {
			try {
				Node child = codes.item(0);
				
				XPathExpression expr = xpath.compile("annotation/appinfo/*");
				NodeList buf  = (NodeList) expr.evaluate(child, XPathConstants.NODESET);
				
				for(int i=0;i<buf.getLength();i++) {
					Node cur = buf.item(i);
					String name = cur.getNodeName();
					if(!cl.getHeader().contains(name)) {
						cl.getHeader().clear();
						cl.getHeader().add("code");
						cl.getHeader().add("value");
						
						useHeaders = false;
					}
				}
			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}
		}
		
		for(int j=0;j<codes.getLength();j++) {
			this.ui.setProgressValue(j);
			
			Node child = codes.item(j);
			
			ArrayList<String> ret = new ArrayList<String>();
			
			String nodeValue = child.getAttributes().getNamedItem("value").getNodeValue();
			
			if(useHeaders) {
				for(int i=0;i<cl.getHeader().size();i++) {
					try {
						XPathExpression expr = xpath.compile("annotation/appinfo/" + cl.getHeader().get(i));
						String val = (String) expr.evaluate(child, XPathConstants.STRING);
						
						if(cl.getHeader().get(i).equals(keyCol)){
							ret.add(nodeValue);
						}
						else {
							if(val != null) {
								ret.add(val);
							}
							else {
								ret.add("");
							}
						}
					} catch (XPathExpressionException e) {
						e.printStackTrace();
					}
				}
			}
			else {
				try {
					XPathExpression expr = xpath.compile("annotation/appinfo");
					NodeList buf  = (NodeList) expr.evaluate(child, XPathConstants.NODESET);
					
					for(int i=0;i<buf.getLength();i++) {
						Node cur = buf.item(i);
						String val = cur.getTextContent().trim();
						ret.add(val);
					}
				} catch (XPathExpressionException e) {
					e.printStackTrace();
				}
				ret.add(0, nodeValue);
			}
			cl.addValues(ret);
		}
	}

	/**
	 * Analyze a simple type for its properties (lower / upper limits...)
	 * @param child	The XML Node object which contains the definition of the simple type
	 * @param path	The path of the schema file which contains the simple type
	 * @param prefix	The namespace prefix of the simple type
	 * @param name	The local name of the smple type
	 * @return	The simple type object as XSimpleType
	*/ 
	private XSimpleType scanSimpleType(Node child, String path, String prefix, String name)
			throws XPathExpressionException {
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr;
		
		XSimpleType newType = new XSimpleType();
		newType.setPrefix(prefix);
		newType.setLocalName(name);
		newType.setItemId(path + "#" + newType.getFullyQualifiedName());
		
		expr = xpath.compile("restriction");
		Node rsInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(rsInfo != null) {
			String rest = rsInfo.getAttributes().getNamedItem("base").getNodeValue();
			newType.setRestriction(rest);
		}
		
		expr = xpath.compile("restriction/pattern");
		Node stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String pattern = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setPattern(pattern);
		}
		
		expr = xpath.compile("restriction/minLength");
		stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String minLength = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setMinLenght(minLength);
		}
		
		expr = xpath.compile("restriction/maxLength");
		stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String maxLength = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setMaxLenght(maxLength);
		}
		
		expr = xpath.compile("restriction/length");
		stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String length = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setLenght(length);
		}
		
		expr = xpath.compile("restriction/minInclusive");
		stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String length = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setMinInclusive(length);
		}
		
		expr = xpath.compile("restriction/maxInclusive");
		stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String length = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setMaxInclusive(length);
		}
		
		expr = xpath.compile("restriction/minExclusive");
		stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String length = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setMinExclusive(length);
		}
		
		expr = xpath.compile("restriction/maxExclusive");
		stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String length = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setMaxExclusive(length);
		}
		
		expr = xpath.compile("restriction/fractionDigits");
		stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String length = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setFractionDigits(length);
		}
		
		expr = xpath.compile("restriction/totalDigits");
		stInfo = (Node) expr.evaluate(child, XPathConstants.NODE);
		if(stInfo != null) {
			String length = stInfo.getAttributes().getNamedItem("value").getNodeValue();
			newType.setTotalDigits(length);
		}
		return newType;
	}
	
	/**
	 * Read all simple type definitions from a schema file and return the according Nodelist
	 * @param schemaFile	The schema file to be analyzed
	 * @return	The NodeList of all nodes which define simple types
	*/ 
	public NodeList getSimpleTypesFromSchema(File file) {
		NodeList result = null;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/simpleType[@name!='']");
			result =(NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/**
	 * Reads all attributes from a complex type definition
	 * @param newType	The complex type object to be analyzed
	 * @param attNode	The Node object which may contain attribute information
	 * @param schema	The XML schema file item which contains the definition of newType
	*/ 
	private void readAttributes(XComplexType newType, Node attNode, XTASchemaFileItem schema) {
		NodeList attributes = getNodeList(attNode, "child::*[local-name()='attribute' or local-name()='attributeGroup']");
		
		if(attributes != null) {
			for(int i=0;i<attributes.getLength();i++) {
				try {
					Node attribute = attributes.item(i);
					
					if("attribute".equals(attribute.getNodeName()) || "xs:attribute".equals(attribute.getNodeName())) {
						XAttribute newAtt = new XAttribute();
						
						Node attNameNode = attribute.getAttributes().getNamedItem("name");
						Node attRefNode = attribute.getAttributes().getNamedItem("ref");
						
						if(attNameNode != null) {
							String attName = attNameNode.getNodeValue();
							newAtt.setName(schema.getTargetNamespace().getPrefix(), attName);
							
							Node attTypeNode = attribute.getAttributes().getNamedItem("type");
							if(attTypeNode != null) {
								String attType = attTypeNode.getNodeValue();
								newAtt.setType(attType);
							}
							Node attFormNode = attribute.getAttributes().getNamedItem("form");
							if(attFormNode != null) {
								String attForm = attFormNode.getNodeValue();
								newAtt.setForm(attForm);
							}
							else {
								newAtt.setForm(schema.getDefaultAttributeForm());
							}
						}
						else if(attRefNode != null) {
							XTASchemaFileItem schm = schema.findSchemaForAttribute(attRefNode.getNodeValue());
							if(schm != null) {
								XAttribute na = schm.getAttribute(attRefNode.getNodeValue());
								if(na != null) {
									newAtt.setName(schema.getTargetNamespace().getPrefix(), na.getName());
									newAtt.setType(na.getType());
									newAtt.setFixed(na.getFixed());
								}
							}
							
							if(schema.getTargetNamespace() != null && schema.getTargetNamespace().getPrefix() != null) {
								newAtt.setForm("qualified");
							}
						}
						Node attUseNode = attribute.getAttributes().getNamedItem("use");
						if(attUseNode != null) {
							String attUse = attUseNode.getNodeValue();
							newAtt.setUse(attUse);
						}
						Node attFixedNode = attribute.getAttributes().getNamedItem("fixed");
						if(attFixedNode != null) {
							String attFixed = attFixedNode.getNodeValue();
							newAtt.setFixed(attFixed);
						}
						
						newType.getAttributes().add(newAtt);
					}
					else if("attributeGroup".equals(attribute.getNodeName()) || "xs:attributeGroup".equals(attribute.getNodeName())) {
						Node refNode = attribute.getAttributes().getNamedItem("ref");
						if(refNode != null) {
							String reference = refNode.getNodeValue();
							newType.getAttributeGroups().add(reference);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Analyze an XML element and all of its sub structures
	 * @param item	The XML schema file item which contains the element definition
	 * @param nameSpacePrefix	The namespace prefix of the element
	 * @param node	The Node object which contains the element definition
	 * @param isChoiceChild	Indicates whether this element is a child of choice element
	 * @param choiceBase	The ID of the parent choice element (in case it is a choice child)
	 * @param root	The folder resource which contains the schema file of this element
	 * @param defauleElementForm	The defauleElementForm of the schema file which contains this element
	 * @param defauleAttributeForm	The defauleAttribute of the schema file which contains this element
	 * @return	The resulting XElement
	*/ 
	private XElement scanElement(
			XTASchemaFileItem item, 
			String nameSpacePrefix, 
			Node node, 
			boolean isChoiceChild, 
			String choiceBase, 
			XTAFolderResource root, 
			String defauleElementForm, 
			String defauleAttributeForm, 
			TreeSet<WorkLeft> workLeft) {
		XElement ret = new XElement();
		ret.setSchema(item);
		ret.setChoiceChild(isChoiceChild, choiceBase);
		
		Node nameNode = node.getAttributes().getNamedItem("name");
		Node typeNode = node.getAttributes().getNamedItem("type");
		Node formNode = node.getAttributes().getNamedItem("form");
		Node minOccursNode = node.getAttributes().getNamedItem("minOccurs");
		Node maxOccursNode = node.getAttributes().getNamedItem("maxOccurs");
		Node fixedNode = node.getAttributes().getNamedItem("fixed");
		Node abstractNode = node.getAttributes().getNamedItem("abstract");
		Node subGroupNode = node.getAttributes().getNamedItem("substitutionGroup");
		
		if(nameNode != null) {
			String name = nameNode.getNodeValue();
			ret.setElementName(name);
			ret.setPrefix(nameSpacePrefix);
			
			// Falls dieses Element bereits erfasst wurde, soll es nicht neu erzeugt werden, sondern die bereits erfasste 
			// Version weiter verarbeitet werden
			String qName = name;
			if(nameSpacePrefix != null) {
				qName = nameSpacePrefix + ":" + name;
			}
			if(item.containsNamedElement(qName)) {
				ret = item.getNamedElement(qName);
			}
			
			if(typeNode != null) {
				// Element hat einen bestimmten Typ, der im Attribut type angegeben ist
				String type = typeNode.getNodeValue();
				ret.setElementType(type);
			}
			else {
				try {
					// Pruefen, ob im Element ein anonymer komplexer oder simpler Typ definiert wird...
					XPathFactory xPathfactory = XPathFactory.newInstance();
					XPath xpath = xPathfactory.newXPath();
					
					XPathExpression expr = xpath.compile("complexType");
					Node check = (Node) expr.evaluate(node, XPathConstants.NODE);
					
					if(check != null) {
						// Im Element wird ein anonymer komplexer Typ definiert
						XComplexType anonymousSubType = this.scanComplexType(item, nameSpacePrefix, check, root, defauleElementForm, defauleAttributeForm, workLeft);
						anonymousSubType.setParent(ret);
						ret.setAnonymousType(anonymousSubType);
					}
					else {
						// Kein anonymer komplexer Typ. Nun pruefen, ob im Element statt dessen ein anonymer simpler Typ definiert wird...
						expr = xpath.compile("simpleType");
						check = (Node) expr.evaluate(node, XPathConstants.NODE);
						
						if(check != null) {
							XSimpleType anonymousSubType = this.scanSimpleType(check, item.getPath(), nameSpacePrefix, name + "_anonymous_simple_type_");
							
							// TODO: Hier dafuer sorgen, dass der anonyme Simple Subtyp irgendwo abgelegt wird
							ret.setAnonymousSimpleType(anonymousSubType);
						}
					}
				} catch (XPathExpressionException e) {
					e.printStackTrace();
				}
			}
		}
		
		if(formNode != null) {
			String form = formNode.getNodeValue();
			ret.setForm(form);
		}
		else {
			ret.setForm(defauleElementForm);
		}
		
		if(minOccursNode != null) {
			String minOccurs = minOccursNode.getNodeValue();
			ret.setMinOccurs(minOccurs);
		}
		else {
			// Default value for minOccurs is 1!
			ret.setMinOccurs("1");
		}
		if(maxOccursNode != null) {
			String maxOccurs = maxOccursNode.getNodeValue();
			ret.setMaxOccurs(maxOccurs);
		}
		else {
			// Default value for maxOccurs is 1!
			ret.setMaxOccurs("1");
		}
		if(fixedNode != null) {
			String fixed = fixedNode.getNodeValue();
			ret.setFixed(fixed);
		}
		else {
			// Default value for fixed is null!
			ret.setFixed(null);
		}
		if(abstractNode != null) {
			String abstr = abstractNode.getNodeValue();
			ret.setAbstr(Boolean.parseBoolean(abstr));
		}
		else {
			// Default value for abstract is false!
			ret.setAbstr(false);
		}
		if(subGroupNode != null) {
			String substGr = subGroupNode.getNodeValue();
			ret.setSubstitutionGroup(substGr);
		}
		
		return ret;
	}
	
	/**
	 * Analyze a given genericode file and write its codelist into a target codelist item
	 * @param file	The genericode file
	 * @param target	The target XTAGenericodeList object to write results into
	 * @param root	The folder resource which contains the genericode file
	 * @return	True in case the genericode list was read successfully, false otherwise
	*/ 
	public boolean scanGenericode(File file, XTAGenericodeList target, XTAFolderResource root) {
		try {
			if(!file.exists()) {
				return false;
			}
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			String filePath = "file:///" + file.getAbsolutePath().replace("\\", "/");
			Document doc = builder.parse(filePath);
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile("/CodeList/Identification/CanonicalUri/text()");
			String uri = (String) expr.evaluate(doc, XPathConstants.STRING);
			
			if(uri != null && uri.length() > 0) {
				expr = xpath.compile("/CodeList/Identification/Version/text()");
				String version = (String) expr.evaluate(doc, XPathConstants.STRING);
				
				TreeMap<String, String> shortNameForID = new TreeMap<>();
				TreeMap<String, String> longNameForID = new TreeMap<>();
				expr = xpath.compile("/CodeList/ColumnSet/Column");
				
				XCodelist cl = new XCodelist();
				
				cl.setPrefix("");
				cl.setLocalName(uri);
				cl.setVersion(version);
				cl.setItemId(target.getPath() + "#" + uri);
				
				target.setUrn(uri);
				target.setVersion(version);
				
				ArrayList<String> ids = new ArrayList<>();
				ArrayList<String> names = new ArrayList<>();
				
				NodeList columns = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
				for(int i=0;i<columns.getLength();i++) {
					Node node = columns.item(i);
					
					expr = xpath.compile("./@Id");
					String id = (String) expr.evaluate(node, XPathConstants.STRING);
					ids.add(id);
					
					expr = xpath.compile("./ShortName/text()");
					String shortName = (String) expr.evaluate(node, XPathConstants.STRING);
					expr = xpath.compile("./LongName/text()");
					String longName = (String) expr.evaluate(node, XPathConstants.STRING);
					names.add(longName);
					
					shortNameForID.put(id, shortName);
					longNameForID.put(id, longName);
					
					if(longName != null && longName.trim().length() > 0) {
						cl.getHeader().add(longName);
					}
					else {
						cl.getHeader().add(shortName);
					}
				}
				
				expr = xpath.compile("/CodeList/SimpleCodeList/Row");
				NodeList rows = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
				
				this.ui.setProgressMax(rows.getLength());
				this.ui.setStatusText("Analysiere Genericode-Codeliste" + uri);
				
				for(int i=0;i<rows.getLength();i++) {
					this.ui.setProgressValue(i);
					
					TreeMap<String, String> buffer = new TreeMap<>();
					
					Node node = rows.item(i);
					
					NodeList childs = node.getChildNodes();
					for(int j=0;j<childs.getLength();j++) {
						Node item = childs.item(j);
						if("Value".equals(item.getNodeName())) {
							String key = item.getAttributes().item(0).getNodeValue();
							
							NodeList childs2 = item.getChildNodes();
							for(int k=0;k<childs2.getLength();k++) {
								Node item2 = childs2.item(k);
								if("SimpleValue".equals(item2.getNodeName())) {
									try {
										String val = item2.getFirstChild().getNodeValue();
										buffer.put(key, val);
									} catch (Exception e) {
										buffer.put(key, "");
									}
								}
							}
						}
					}
					
					ArrayList<String> row = new ArrayList<>();
					for(String id : ids) {
						String val = buffer.get(id);
						if(val == null) {
							val = "";
						}
						row.add(val);
					}
					cl.getValues().add(row);
				}
				target.addCodelist(cl.getName() + "_" + cl.getVersion(), cl);
				
				root.addGenericodeList(uri + "_" + version, target);
			}
			
			return true;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			
			return false;
		} catch (SAXException e) {
			e.printStackTrace();
			
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			
			return false;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			
			return false;
		}
	}
	
	/**
	 * Analyze a given code file in an arbitrary format and try to write its codelist into a target codelist item
	 * @param file	The target code file
	 * @param target	The target XTAGenericodeList object to write results into
	 * @param root	The folder resource which contains the genericode file
	 * @return	True in case the genericode list was read successfully, false otherwise
	*/ 
	public boolean scanCode(File file, XTAGenericodeList target, XTAFolderResource root, String id) {
		try {
			if(!file.exists()) {
				return false;
			}
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			String filePath = "file:///" + file.getAbsolutePath().replace("\\", "/");
			Document doc = builder.parse(filePath);
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile("/child::*/label/text()");
			String uri = (String) expr.evaluate(doc, XPathConstants.STRING);
			
			if(uri != null && uri.length() > 0) {
				expr = xpath.compile("/child::*/thisversion/text()");
				String version = (String) expr.evaluate(doc, XPathConstants.STRING);
				
				XCodelist cl = new XCodelist();
				
				cl.setPrefix("");
				cl.setLocalName(uri);
				cl.setVersion(version);
				cl.setItemId(target.getPath() + "#" + uri);
				
				target.setUrn(uri);
				target.setVersion(version);
				
				cl.getHeader().add("label");
				cl.getHeader().add("definition");
				
				expr = xpath.compile("/child::*/containeditems/child::*");
				NodeList rows = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
				
				this.ui.setProgressMax(rows.getLength());
				this.ui.setStatusText("Analysiere Genericode-Codeliste" + uri);
				
				for(int i=0;i<rows.getLength();i++) {
					this.ui.setProgressValue(i);
					
					Node node = rows.item(i);
					
					String label = null;
					String definition = null;
					
					for(int j=0;j<node.getChildNodes().getLength();j++) {
						if("label".equals(node.getChildNodes().item(j).getNodeName())){
							label = node.getChildNodes().item(j).getTextContent();
						}
						else if("definition".equals(node.getChildNodes().item(j).getNodeName())){
							definition = node.getChildNodes().item(j).getTextContent();
						}
					}
					
					if(label != null && definition != null) {
						ArrayList<String> row = new ArrayList<>();
						row.add(label);
						row.add(definition);
						cl.getValues().add(row);
					}
				}
				target.addCodelist(id, cl);
				
				root.addGenericodeList(id, target);
			}
			
			return true;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			
			return false;
		} catch (SAXException e) {
			e.printStackTrace();
			
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			
			return false;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			
			return false;
		}
	}
	
	/**
	 * Analyze a given XTASchemaFileItem object for defined element and attribute definitions. Results are stored in that object. 
	 * @param item	The schema file item to be analyzed
	*/ 
	public void scanSchemaForNamedElementsAndAttributes(XTASchemaFileItem item) {
		try {
			File file = new File(item.getPath());
			NameSpace nameSpace = getTargetNamespaceFromSchema(file);
			
			NodeList elementResult = getElementsFromSchema(file);
			
			if(elementResult != null) {
				for(int i=0;i<elementResult.getLength();i++) {
					Node node = elementResult.item(i);

					// Hier werden nur Informationen (Attribute name, type, abstract) zu dem Element ausgelesen
					XElement newElement = this.scanNamedElement(nameSpace.getPrefix(), node);
					newElement.setSchema(item);
					
					// Falls das Element einen Namen hat, dient es als Deklaration eines Elementes
					if(newElement != null) {
						item.addNamedElement(newElement.getFullyQualifiedName(), newElement);
					}
				}
			}
			
			NodeList attributeResult = getAttributesFromSchema(file);
			
			if(attributeResult != null) {
				for(int i=0;i<attributeResult.getLength();i++) {
					Node node = attributeResult.item(i);
					
					Node attNameNode = node.getAttributes().getNamedItem("name");
					Node typeNode = node.getAttributes().getNamedItem("type");
					Node fixedNode = node.getAttributes().getNamedItem("fixed");
					
					XAttribute att = new XAttribute();
					att.setName(nameSpace.getPrefix(), attNameNode.getNodeValue());
					if(typeNode != null) {
						att.setType(typeNode.getNodeValue());
					}
					if(fixedNode != null) {
						att.setFixed(fixedNode.getNodeValue());
					}
					
					// Global attributes MUST always be qualified
					att.setForm("qualified");
					
					String attName = att.getName();
					item.addAttribute(attName, att);
				}
			}
		} catch (DOMException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Analyze a given Node for element information and write results into an XElement
	 * @param prefix	The namespace prefix of the schema file
	 * @param node	The Node object to be analyzed
	 * @return	The resulting XElement object
	*/ 
	private XElement scanNamedElement(String prefix, Node node) {
		XElement ret = null;
		
		Node nameNode = node.getAttributes().getNamedItem("name");
		Node typeNode = node.getAttributes().getNamedItem("type");
		Node abstractNode = node.getAttributes().getNamedItem("abstract");
		
		if(nameNode != null) {
			String name = nameNode.getNodeValue();
			ret = new XElement();
			ret.setElementName(name);
			ret.setPrefix(prefix);
			
			if(typeNode != null) {
				String type = typeNode.getNodeValue();
				ret.setElementType(type);
			}
			if(abstractNode != null) {
				String abstr = abstractNode.getNodeValue();
				ret.setAbstr(Boolean.parseBoolean(abstr));
			}
		}
		
		return ret;
	}
	
	/**
	 * Determines the default element form of a schema file
	 * @param file	The schema file
	 * @return	The default element form if defined or an empty string otherwise
	*/
	private String getDefaultElementFormFromSchema(File file) {
		String ret = "";
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/@elementFormDefault");
			ret = (String) expr.evaluate(doc, XPathConstants.STRING);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	/**
	 * Determines the default attribute form of a schema file
	 * @param file	The schema file
	 * @return	The default attribute form if defined or an empty string otherwise
	*/
	private String getDefaultAttributeFormFromSchema(File file) {
		String ret = "";
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/@attributeFormDefault");
			ret = (String) expr.evaluate(doc, XPathConstants.STRING);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	/**
	 * Determines the target namespace of a schema file
	 * @param file	The schema file
	 * @return	The target namespace if defined or an empty string otherwise
	*/
	private NameSpace getTargetNamespaceFromSchema(File file) {
		NameSpace ret = new NameSpace();
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema");
			Node result = (Node) expr.evaluate(doc, XPathConstants.NODE);
			Node namedItem = result.getAttributes().getNamedItem("targetNamespace");
			
			if(namedItem != null) {
				String targetNamespace = namedItem.getNodeValue();
				
				ret.setNamespaceURL(targetNamespace);
				
				for(int i=0;i<result.getAttributes().getLength();i++) {
					Node att = result.getAttributes().item(i);
					if(att.getNodeName().startsWith("xmlns:")) {
						if(att.getNodeValue().equals(targetNamespace)) {
							ret.setPrefix(att.getNodeName().substring("xmlns:".length()));
						}
					}
					else if(att.getNodeName().equals("xmlns")) {
						if(att.getNodeValue().equals(targetNamespace)) {
							String ns = XTAController.getHash(targetNamespace);
							ret.setPrefix(ns);
						}
					}
				}
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	/**
	 * Read namespace declarations as well as includes and imports from a given schema file and write them to the according target arrays
	 * @param file	The schema file to be analyzed
	 * @param namespaces	The target list to write the namespace declarations to
	 * @param includes	The target list to write the include declarations to
	 * @param imports	The target list to write the import declarations to
	*/ 
	private void readNamespacesAndImportsFromSchema(File file, ArrayList<NameSpace> namespaces, ArrayList<Include> includes, ArrayList<Import> imports) {
		try {
			TreeMap<String, String> prefixesForNamespaces = new TreeMap<String, String>();
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema");
			Node result = (Node) expr.evaluate(doc, XPathConstants.NODE);
			
			for(int i=0;i<result.getAttributes().getLength();i++) {
				Node att = result.getAttributes().item(i);
				String attName = att.getNodeName();
				if(attName.startsWith("xmlns:")) {
					String ns = attName.substring(6);
					String nsUrl = att.getNodeValue();
					if(nsUrl.startsWith("\"")) {
						nsUrl = nsUrl.substring(1);
					}
					if(nsUrl.endsWith("\"")) {
						nsUrl = nsUrl.substring(0, nsUrl.length() - 1);
					}
					NameSpace sp = new NameSpace();
					sp.setPrefix(ns);
					sp.setNamespaceURL(nsUrl);
					
					namespaces.add(sp);
					
					prefixesForNamespaces.put(nsUrl, ns);
				}
				else if(attName.equals("xmlns")) {
					String nsUrl = att.getNodeValue();
					if(nsUrl.startsWith("\"")) {
						nsUrl = nsUrl.substring(1);
					}
					if(nsUrl.endsWith("\"")) {
						nsUrl = nsUrl.substring(0, nsUrl.length() - 1);
					}
					String ns = XTAController.getHash(nsUrl);
					NameSpace sp = new NameSpace();
					sp.setPrefix(ns);
					sp.setNamespaceURL(nsUrl);
					
					namespaces.add(sp);
					
					prefixesForNamespaces.put(nsUrl, ns);
				}
			}
			
			expr = xpath.compile("/schema/include");
			NodeList columns = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			
			for(int i=0;i<columns.getLength();i++) {
				Node node = columns.item(i);
				Include inc = new Include();
				for(int j=0;j<node.getAttributes().getLength();j++) {
					if("schemaLocation".equals(node.getAttributes().item(j).getNodeName())) {
						inc.setSchemaLocation(node.getAttributes().item(j).getTextContent());
					}
				}
				includes.add(inc);
			}
			
			expr = xpath.compile("/schema/import");
			columns = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			
			for(int i=0;i<columns.getLength();i++) {
				Node node = columns.item(i);
				Import imp = new Import();
				
				for(int j=0;j<node.getAttributes().getLength();j++) {
					String value = node.getAttributes().item(j).getTextContent();
					if("schemaLocation".equals(node.getAttributes().item(j).getNodeName())) {
						imp.setSchemaLocation(value);
					}
					if("namespace".equals(node.getAttributes().item(j).getNodeName())) {
						imp.setNamespace(value);
						imp.setPrefix(prefixesForNamespaces.get(value));
					}
				}
				imports.add(imp);
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Seek for attribute group declarations in a given schema file and return the according NodeList
	 * @param file	The schema file
	 * @return	The resulting NodeList object
	*/ 
	private NodeList getAttributeGroupsFromSchema(File file) {
		NodeList result = null;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/attributeGroup");
			result =(NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Seek for attribute declarations in a given schema file and return the according NodeList
	 * @param file	The schema file
	 * @return	The resulting NodeList object
	*/ 
	private NodeList getAttributesFromSchema(File file) {
		NodeList result = null;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/attribute");
			result =(NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Seek for element group declarations in a given schema file and return the according NodeList
	 * @param file	The schema file
	 * @return	The resulting NodeList object
	*/ 
	private NodeList getElementGroupsFromSchema(File file) {
		NodeList result = null;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/group");
			result =(NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Seek for complex type declarations in a given schema file and return the according NodeList
	 * @param file	The schema file
	 * @return	The resulting NodeList object
	*/ 
	private NodeList getComplexTypesFromSchema(File file) {
		NodeList result = null;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/complexType");
			result =(NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Seek for element declarations in a given schema file and return the according NodeList
	 * @param file	The schema file
	 * @return	The resulting NodeList object
	*/ 
	private NodeList getElementsFromSchema(File file) {
		NodeList result = null;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/element");
			result =(NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/**
	 * Seek for include declarations in a schema file (input stream) and return an according Arraylist of Include objects
	 * @param in	The schema file input stream
	 * @return	The according Arraylist of Include objects
	*/ 
	private ArrayList<Include> getIncludesFromSchema(InputStream in) {
		ArrayList<Include> ret = new ArrayList<>();
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(in);
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/include");
			NodeList columns = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			
			for(int i=0;i<columns.getLength();i++) {
				Node node = columns.item(i);
				Include inc = new Include();
				for(int j=0;j<node.getAttributes().getLength();j++) {
					if("schemaLocation".equals(node.getAttributes().item(j).getNodeName())) {
						inc.setSchemaLocation(node.getAttributes().item(j).getTextContent());
					}
				}
				ret.add(inc);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ret;
	}

	/**
	 * Seek for import declarations in a schema file (input stream) and return an according Arraylist of Include objects
	 * @param in	The schema file input stream
	 * @return	The according Arraylist of Include objects
	*/ 
	private ArrayList<Import> getImportsFromSchema(InputStream in) {
		ArrayList<Import> ret = new ArrayList<>();
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(in);
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/schema/import");
			NodeList columns = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			
			for(int i=0;i<columns.getLength();i++) {
				Node node = columns.item(i);
				Import imp = new Import();
				
				for(int j=0;j<node.getAttributes().getLength();j++) {
					if("schemaLocation".equals(node.getAttributes().item(j).getNodeName())) {
						imp.setSchemaLocation(node.getAttributes().item(j).getTextContent());
					}
					if("namespace".equals(node.getAttributes().item(j).getNodeName())) {
						imp.setNamespace(node.getAttributes().item(j).getTextContent());
					}
				}
				ret.add(imp);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	/**
	 * Seek for codelist definitions (enumerations) within a given Node object
	 * @param node	The node to be analyzed
	 * @return	The according NodeList object
	*/ 
	public NodeList getCodelistFromComplexNode(Node node) {
		NodeList result = null;
		
		try {
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("complexContent/descendant::*/sequence/descendant::*/enumeration");
			result =(NodeList) expr.evaluate(node, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Seek for codelist header definitions according to XOEV method within a given Node object
	 * @param node	The node to be analyzed
	 * @return	The according NodeList object
	*/ 
	public NodeList getCodelistHeadersFromComplexNode(Node node) {
		NodeList result = null;
		
		try {
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("child::*[local-name()='annotation']/"
					+ "child::*[local-name()='appinfo']/"
					+ "child::*[local-name()='codelistenspalten']/"
					+ "child::*");
			result =(NodeList) expr.evaluate(node, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Determine codelist key from a given Node according to XOEV method
	 * @param node	The node to be analyzed
	 * @return	The according code key (String) or null if no code key was found
	*/ 
	public String getCodelistKeyFromComplexNode(Node node) {
		String result = null;
		
		try {
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			// Versuch, die Schlüsselspalte aus dem Element 'genutzteCodeSpalte' auszulesen, falls vorhanden
			XPathExpression expr = xpath.compile("child::*[local-name()='annotation']/"
					+ "child::*[local-name()='appinfo']/"
					+ "child::*[local-name()='genutzteCodeSpalte']");
			result = (String) expr.evaluate(node, XPathConstants.STRING);
			
			// 2. Versuch: Schlüsselspalte aus dem Element unter codelistenspalten/code/codeSpalte --> true auszulesen, falls vorhanden
			if(result == null || result.length() == 0) {
				expr = xpath.compile("child::*[local-name()='annotation']/child::*[local-name()='appinfo']/codelistenspalten/child::*[child::*[local-name()='codeSpalte' and text()='true']]");
				Node resNode = (Node) expr.evaluate(node, XPathConstants.NODE);
				if(resNode != null) {
					result = resNode.getNodeName();
				}
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/**
	 * Analyzes a given Node object for an attribute group declaration and return the according XAttributeGroup object
	 * @param item	The schema file item containing the attribute group declaration
	 * @param nameSpacePrefix	The namespace prefix of the schema file
	 * @param node	The node to be analyzed
	 * @param defauleElementForm	The defauleElementForm defined by the schema file
	 * @param defauleAttributeForm	The defauleAttributeForm defined by the schema file
	 * @return	The according XAttributeGroup object
	*/ 
	private XAttributeGroup scanAttributeGroup(
			XTASchemaFileItem item, 
			String nameSpacePrefix, 
			Node node, 
			String defauleElementForm, 
			String defauleAttributeForm) {
		Node nameNode = node.getAttributes().getNamedItem("name");
		String name = null;
		if(nameNode != null) {
			name = nameNode.getNodeValue();
		}
		else {
			name = String.valueOf(Math.random());
		}
		
		XAttributeGroup newType = new XAttributeGroup();
		newType.setLocalName(name);
		newType.setPrefix(nameSpacePrefix);
		
		try {
			NodeList elements = getNodeList(node, "attribute");
			
			for(int j=0;j<elements.getLength();j++) {
				Node element = elements.item(j);
				Node refNameNode = element.getAttributes().getNamedItem("ref");
				Node fixedNode = element.getAttributes().getNamedItem("fixed");
				
				if(refNameNode != null) {
					XTASchemaFileItem schm = item.findSchemaForAttribute(refNameNode.getNodeValue());
					
					if(schm != null) {
						XAttribute template = schm.getAttribute(refNameNode.getNodeValue());
						
						if(template != null) {
							XAttribute att = new XAttribute();
							att.setName(template.getPrefix(), template.getName());
							
							if(template.getType() != null) {
								att.setType(template.getType());
							}
							newType.addAttribute(att);
							
							if(template.getFixed() != null) {
								att.setFixed(template.getFixed());
							}
							
							if(template.getForm() != null) {
								att.setForm(template.getForm());
							}
							
							newType.addAttribute(att);
							
							if(fixedNode != null) {
								att.setFixed(fixedNode.getNodeValue());
							}
						}
					}
					else {
						XAttribute att = new XAttribute();
						att.setName(nameSpacePrefix, refNameNode.getNodeValue());
						if(fixedNode != null) {
							att.setFixed(fixedNode.getNodeValue());
						}
						newType.addAttribute(att);
					}
				}
				else {
					Node attNameNode = element.getAttributes().getNamedItem("name");
					Node typeNode = element.getAttributes().getNamedItem("type");
					
					XAttribute att = new XAttribute();
					att.setName(nameSpacePrefix, attNameNode.getNodeValue());
					if(typeNode != null) {
						att.setType(typeNode.getNodeValue());
					}
					if(fixedNode != null) {
						att.setFixed(fixedNode.getNodeValue());
					}
					newType.addAttribute(att);
				}
			}
			elements = getNodeList(node, "attributeGroup");
			
			for(int j=0;j<elements.getLength();j++) {
				Node element = elements.item(j);
				Node attRefNode = element.getAttributes().getNamedItem("ref");
				String groupRef = attRefNode.getNodeValue();
				newType.getAttributeGroups().add(groupRef);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return newType;
	}

	/**
	 * Analyzes a given Node object for an element group declaration and return the according XElementGroup object
	 * @param item	The schema file item containing the attribute group declaration
	 * @param nameSpacePrefix	The namespace prefix of the schema file
	 * @param node	The node to be analyzed
	 * @param root	The folder resource which contains the schema file item
	 * @param defauleElementForm	The defauleElementForm defined by the schema file
	 * @param defauleAttributeForm	The defauleAttributeForm defined by the schema file
	 * @return	The according XElementGroup object
	*/ 
	private XElementGroup scanElementGroup(
			XTASchemaFileItem item, 
			String nameSpacePrefix, 
			Node node, 
			XTAFolderResource root, 
			String defauleElementForm, 
			String defauleAttributeForm, 
			TreeSet<WorkLeft> workLeft) {
		Node nameNode = node.getAttributes().getNamedItem("name");
		String name = null;
		if(nameNode != null) {
			name = nameNode.getNodeValue();
		}
		else {
			name = String.valueOf(Math.random());
		}
		
		XElementGroup newType = new XElementGroup();
		newType.setLocalName(name);
		newType.setPrefix(nameSpacePrefix);
		
		try {
			boolean isChoice = false;
			
			NodeList elements = getNodeList(node, "sequence/element"); //descendant::*/element");
			
			if(elements.getLength() == 0) {
				elements = getNodeList(node, "choice/element"); //descendant::*/element");
				isChoice = true;
			}
			
			for(int j=0;j<elements.getLength();j++) {
				Node element = elements.item(j);
				Node ref = element.getAttributes().getNamedItem("ref");
				
				// TODO: Check, ob das hier so wie oben sein muss 
				if(ref != null) {
					String reference = ref.getNodeValue();
					XElement typed = new XElement();
					typed.setSchema(item);
					
					XElement childElement = this.scanElement(item, nameSpacePrefix, element, isChoice, newType.getName(), root, defauleElementForm, defauleAttributeForm, workLeft);
					XElement typedTemplate = item.getNamedElement(reference);
					if(typedTemplate != null) {
						typed = new XElementProxy(typedTemplate);
						newType.addElement(typed);
						
						if(XElement.getPrefix(reference) != null) {
							typed.setPrefix(XElement.getPrefix(reference));
						}
					}
					else {
						WorkLeft wl = new WorkLeft();
						wl.todo = "elementGroup";
						wl.subject = newType;
						wl.reference = reference;
						wl.schema = item;
						workLeft.add(wl);
					}
					typed.setChoiceChild(isChoice, newType.getName());
					typed.setMinOccurs(childElement.getMinOccurs());
					typed.setMaxOccurs(childElement.getMaxOccurs());
				}
				else {
					XElement childElement = this.scanElement(item, nameSpacePrefix, element, isChoice, newType.getName(), root, defauleElementForm, defauleAttributeForm, workLeft);
					newType.addElement(childElement);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return newType;
	}
	
	/**
	 * Analyzes a given Node for sequence or choice declaration and returns the according XComplexType object which carries the information 
	 * for sequences or choices
	 * @param item	The schema file item which contains the sequence element
	 * @param nameSpacePrefix	The namespace prefix of the schema file
	 * @param node	The node to be analyzed
	 * @param root	The folder resource containing the schema file
	 * @param defauleElementForm	The defauleElementForm defined by the schema file
	 * @param defauleAttributeForm	The defauleAttributeForm defined by the schema file
	 * @param isChoice	Indicates whether the parent element is a choice element 
	 * @return	The according XComplexType object
	 */
	private XComplexType scanSequence(
			XTASchemaFileItem item, 
			String nameSpacePrefix, 
			Node node, 
			XTAFolderResource root, 
			String defauleElementForm, 
			String defauleAttributeForm, 
			boolean isChoice, 
			TreeSet<WorkLeft> workLeft) {
		Node nameNode = node.getAttributes().getNamedItem("name");
		String name = null;
		if(nameNode != null) {
			name = nameNode.getNodeValue();
		}
		else {
			name = String.valueOf(Math.random());
		}
		
		Node abstrNode = node.getAttributes().getNamedItem("abstract");
		boolean abstr = false;
		if(abstrNode != null) {
			abstr = Boolean.parseBoolean(abstrNode.getNodeValue());
		}
		
		// Auf dieser (ersten) Ebene haben ALLE ComplexTypes einen Namen, sonst macht es keinen Sinn! 
		
		XComplexType newType = new XComplexType();
		newType.setLocalName(name);
		newType.setPrefix(nameSpacePrefix);
		newType.setSchema(item);
		newType.setAbstr(abstr);
		
		Node base = node;
		try {
			readAttributes(newType, base, item);
			NodeList elements = base.getChildNodes();
			
			for(int j=0;j<elements.getLength();j++) {
				Node element = elements.item(j);
				if("element".equals(element.getNodeName()) || "xs:element".equals(element.getNodeName())) {
					Node ref = element.getAttributes().getNamedItem("ref");

					if(ref != null) {
						String reference = ref.getNodeValue();
						XElement typed = new XElement();
						typed.setSchema(item);
						
						XElement childElement = this.scanElement(item, nameSpacePrefix, element, isChoice, newType.getName(), root, defauleElementForm, defauleAttributeForm, workLeft);
						XElement typedTemplate = item.getNamedElement(reference);
						if(typedTemplate != null) {
							typed = new XElementProxy(typedTemplate);
							newType.addElement(typed);
							
							if(XElement.getPrefix(reference) != null) {
								typed.setPrefix(XElement.getPrefix(reference));
							}
						}
						typed.setChoiceChild(isChoice, newType.getName());
						typed.setMinOccurs(childElement.getMinOccurs());
						typed.setMaxOccurs(childElement.getMaxOccurs());
					}
					else {
						XElement childElement = this.scanElement(item, nameSpacePrefix, element, isChoice, newType.getName(), root, defauleElementForm, defauleAttributeForm, workLeft);
						newType.addElement(childElement);
					}
				}
				else if("group".equals(element.getNodeName()) || "xs:group".equals(element.getNodeName())) {
					Node ref = element.getAttributes().getNamedItem("ref");

					if(ref != null) {
						String reference = ref.getNodeValue();
						int sep = reference.indexOf(":");
						if(sep < 0) {
							reference = nameSpacePrefix + ":" + reference;
						}
						XElementGroup group = item.getElementGroup(reference);
						if(group != null) {
							for(int i=0;i<group.getElements().size();i++) {
								XElement temp = group.getElements().get(i);
								XElement copy = new XElement();
								copy.clone(temp);
								newType.addElement(copy);
							}
						}
					}
				}
				else if("any".equals(element.getNodeName()) || "xs:any".equals(element.getNodeName())) {
					XElement el = new XElement();
					el.setElementName("any");
					el.setPrefix(nameSpacePrefix);
					el.setAny(true);
					el.setSchema(item);
					newType.addElement(el);
					
					for(int k=0;k<element.getAttributes().getLength();k++) {
						Node att = element.getAttributes().item(k);
						if("id".equals(att.getNodeName())){
							el.setAnyID(att.getNodeValue());
						}
						else if("minOccurs".equals(att.getNodeName())){
							el.setMinOccurs(att.getNodeValue());
						}
						else if("maxOccurs".equals(att.getNodeName())){
							el.setMaxOccurs(att.getNodeValue());
						}
						else if("namespace".equals(att.getNodeName())){
							el.setAnyNamespace(att.getNodeValue());
						}
						else if("processContents".equals(att.getNodeName())){
						}
					}
				}
				else if("sequence".equals(element.getNodeName()) || "xs:sequence".equals(element.getNodeName())) {
					XElement sequenceElement = new XElement();
					sequenceElement.setElementName("xs:sequence");
					sequenceElement.setPrefix(nameSpacePrefix);
					
					XComplexType sequenceType = scanSequence(item, nameSpacePrefix, element, root, defauleElementForm, defauleAttributeForm, false, workLeft);
					sequenceType.setDerivationType(XComplexType.SEQUENCE);
					
					sequenceElement.setAnonymousType(sequenceType);
					
					Node minOccursNode = element.getAttributes().getNamedItem("minOccurs");
					Node maxOccursNode = element.getAttributes().getNamedItem("maxOccurs");
					if(minOccursNode != null) {
						sequenceElement.setMinOccurs(minOccursNode.getNodeValue());
					}
					if(maxOccursNode != null) {
						sequenceElement.setMaxOccurs(maxOccursNode.getNodeValue());
					}
					
					newType.addElement(sequenceElement);
				}
				else if("choice".equals(element.getNodeName()) || "xs:choice".equals(element.getNodeName())) {
					XElement choiceElement = new XElement();
					choiceElement.setElementName("xs:choice");
					choiceElement.setPrefix(nameSpacePrefix);
					
					XComplexType choiceType = scanSequence(item, nameSpacePrefix, element, root, defauleElementForm, defauleAttributeForm, true, workLeft);
					choiceType.setDerivationType(XComplexType.CHOICE);
					
					choiceElement.setAnonymousType(choiceType);
					Node minOccursNode = element.getAttributes().getNamedItem("minOccurs");
					Node maxOccursNode = element.getAttributes().getNamedItem("maxOccurs");
					if(minOccursNode != null) {
						choiceElement.setMinOccurs(minOccursNode.getNodeValue());
					}
					if(maxOccursNode != null) {
						choiceElement.setMaxOccurs(maxOccursNode.getNodeValue());
					}
					
					newType.addElement(choiceElement);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return newType;
	}

	/**
	 * Analyzes a given Node for a complex type declaration and returns the according XComplexType object 
	 * @param item	The schema file item which contains the sequence element
	 * @param nameSpacePrefix	The namespace prefix of the schema file
	 * @param node	The node to be analyzed
	 * @param root	The folder resource containing the schema file
	 * @param defauleElementForm	The defauleElementForm defined by the schema file
	 * @param defauleAttributeForm	The defauleAttributeForm defined by the schema file
	 * @return	The according XComplexType object
	 */
	private XComplexType scanComplexType(
			XTASchemaFileItem item, 
			String nameSpacePrefix, 
			Node node, 
			XTAFolderResource root, 
			String defauleElementForm, 
			String defauleAttributeForm, 
			TreeSet<WorkLeft> workLeft) {
		Node nameNode = node.getAttributes().getNamedItem("name");
		String name = null;
		if(nameNode != null) {
			name = nameNode.getNodeValue();
		}
		else {
			name = String.valueOf(Math.random());
		}
		
		Node abstrNode = node.getAttributes().getNamedItem("abstract");
		boolean abstr = false;
		if(abstrNode != null) {
			abstr = Boolean.parseBoolean(abstrNode.getNodeValue());
		}
		
		// Auf dieser (ersten) Ebene haben ALLE ComplexTypes einen Namen, sonst macht es keinen Sinn! 
		
		XComplexType newType = new XComplexType();
		newType.setLocalName(name);
		newType.setPrefix(nameSpacePrefix);
		newType.setSchema(item);
		newType.setAbstr(abstr);
		
		Node restrictionBase = getNode(node, "complexContent/restriction");
		Node extensionBase = getNode(node, "complexContent/extension");
		Node simpleRestrictionBase = getNode(node, "simpleContent/restriction");
		Node simpleExtensionBase = getNode(node, "simpleContent/extension");
		
		Node base = node;
		if(restrictionBase != null) {
			newType.setDerivationType(XComplexType.RESTRICTION);
			newType.setComplexBase(restrictionBase.getAttributes().getNamedItem("base").getNodeValue());
			base = restrictionBase;
		}
		else if(extensionBase != null) {
			newType.setDerivationType(XComplexType.EXTENSION);
			newType.setComplexBase(extensionBase.getAttributes().getNamedItem("base").getNodeValue());
			base = extensionBase;
		}
		
		if(simpleRestrictionBase != null) {
			try {
				readAttributes(newType, simpleRestrictionBase, item);
				newType.setSimpleBase(simpleRestrictionBase.getAttributes().getNamedItem("base").getNodeValue());
				newType.setDerivationType(XComplexType.RESTRICTION);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else if(simpleExtensionBase != null) {
			try {
				readAttributes(newType, simpleExtensionBase, item);
				newType.setSimpleBase(simpleExtensionBase.getAttributes().getNamedItem("base").getNodeValue());
				newType.setDerivationType(XComplexType.EXTENSION);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else {
			try {
				boolean isChoice = false;
				
				readAttributes(newType, base, item);
				NodeList elementsCarrier = getNodeList(base, "sequence");
				
				if(elementsCarrier.getLength() == 0) {
					elementsCarrier = getNodeList(base, "choice");
					isChoice = true;
				}
				
				if(elementsCarrier.getLength() > 0) {
					NodeList elements = getNodeList(elementsCarrier.item(0), "child::*");
					
					for(int j=0;j<elements.getLength();j++) {
						Node element = elements.item(j);
						if("element".equals(element.getNodeName()) || "xs:element".equals(element.getNodeName())) {
							Node ref = element.getAttributes().getNamedItem("ref");

							if(ref != null) {
								String reference = ref.getNodeValue();
								XElement typed = new XElement();
								typed.setSchema(item);
								
								XElement childElement = this.scanElement(item, nameSpacePrefix, element, isChoice, newType.getName(), root, defauleElementForm, defauleAttributeForm, workLeft);
								XElement typedTemplate = item.getNamedElement(reference);
								if(typedTemplate != null) {
									typed = new XElementProxy(typedTemplate);
									newType.addElement(typed);
									
									if(XElement.getPrefix(reference) != null) {
										typed.setPrefix(XElement.getPrefix(reference));
									}
								}
								else {
									WorkLeft wl = new WorkLeft();
									wl.todo = "complexType";
									wl.subject = newType;
									wl.reference = reference;
									wl.schema = item;
									workLeft.add(wl);
								}
								typed.setChoiceChild(isChoice, newType.getName());
								typed.setMinOccurs(childElement.getMinOccurs());
								typed.setMaxOccurs(childElement.getMaxOccurs());
							}
							else {
								XElement childElement = this.scanElement(item, nameSpacePrefix, element, isChoice, newType.getName(), root, defauleElementForm, defauleAttributeForm, workLeft);
								newType.addElement(childElement);
							}
						}
						else if("group".equals(element.getNodeName()) || "xs:group".equals(element.getNodeName())) {
							Node ref = element.getAttributes().getNamedItem("ref");
		
							if(ref != null) {
								String reference = ref.getNodeValue();
								int sep = reference.indexOf(":");
								if(sep < 0) {
									reference = nameSpacePrefix + ":" + reference;
								}
								XElementGroup group = item.getElementGroup(reference);
								if(group != null) {
									for(int i=0;i<group.getElements().size();i++) {
										XElement temp = group.getElements().get(i);
										XElement copy = new XElement();
										copy.clone(temp);
										newType.addElement(copy);
									}
								}
							}
						}
						else if("any".equals(element.getNodeName()) || "xs:any".equals(element.getNodeName())) {
							XElement el = new XElement();
							el.setElementName("any");
							el.setPrefix(nameSpacePrefix);
							el.setAny(true);
							el.setSchema(item);
							newType.addElement(el);
							
							for(int k=0;k<element.getAttributes().getLength();k++) {
								Node att = element.getAttributes().item(k);
								if("id".equals(att.getNodeName())){
									el.setAnyID(att.getNodeValue());
								}
								else if("minOccurs".equals(att.getNodeName())){
									el.setMinOccurs(att.getNodeValue());
								}
								else if("maxOccurs".equals(att.getNodeName())){
									el.setMaxOccurs(att.getNodeValue());
								}
								else if("namespace".equals(att.getNodeName())){
									el.setAnyNamespace(att.getNodeValue());
								}
								else if("processContents".equals(att.getNodeName())){
								}
							}
						}
						else if("sequence".equals(element.getNodeName()) || "xs:sequence".equals(element.getNodeName())) {
							XElement sequenceElement = new XElement();
							sequenceElement.setElementName("xs:sequence");
							sequenceElement.setPrefix(nameSpacePrefix);
							
							XComplexType sequenceType = scanSequence(item, nameSpacePrefix, element, root, defauleElementForm, defauleAttributeForm, false, workLeft);
							sequenceType.setDerivationType(XComplexType.SEQUENCE);
							
							sequenceElement.setAnonymousType(sequenceType);

							Node minOccursNode = element.getAttributes().getNamedItem("minOccurs");
							Node maxOccursNode = element.getAttributes().getNamedItem("maxOccurs");
							if(minOccursNode != null) {
								sequenceElement.setMinOccurs(minOccursNode.getNodeValue());
							}
							if(maxOccursNode != null) {
								sequenceElement.setMaxOccurs(maxOccursNode.getNodeValue());
							}
							
							newType.addElement(sequenceElement);
						}
						else if("choice".equals(element.getNodeName()) || "xs:choice".equals(element.getNodeName())) {
							XElement choiceElement = new XElement();
							choiceElement.setElementName("xs:choice");
							choiceElement.setPrefix(nameSpacePrefix);
							
							XComplexType choiceType = scanSequence(item, nameSpacePrefix, element, root, defauleElementForm, defauleAttributeForm, true, workLeft);
							choiceType.setDerivationType(XComplexType.CHOICE);
							
							choiceElement.setAnonymousType(choiceType);
							
							Node minOccursNode = element.getAttributes().getNamedItem("minOccurs");
							Node maxOccursNode = element.getAttributes().getNamedItem("maxOccurs");
							if(minOccursNode != null) {
								choiceElement.setMinOccurs(minOccursNode.getNodeValue());
							}
							if(maxOccursNode != null) {
								choiceElement.setMaxOccurs(maxOccursNode.getNodeValue());
							}
							
							newType.addElement(choiceElement);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if("xoev-code:Code".equals(newType.getComplexBase())) {
			for(XAttribute att : newType.getAttributes()) {
				if("listURI".equals(att.getName())) {
					newType.setListURI(att.getFixed());
				}
				if("listVersionID".equals(att.getName())) {
					newType.setListVersionID(att.getFixed());
				}
			}
		}
		
		// Typ wird registriert, damit hinterher alle Base-Klassen den Implementoren zugeordnet werden können
		// TODO: Check, ob das hier wirklich nötig ist!
		root.addRootType(newType);
		
		return newType;
	}
	
	/**
	 * Process a given remote schema file (defined by a URL) and (i) cache its contents in wwwCache, (ii) make 
	 * local copies within the target folder and (iii) schemaCache and (iv) reverseSchemaCache (see JavaDoc). 
	 * Recursively process referenced schema files. Also (v) add an entry to a ReferenceRegister object which 
	 * combines listURIs and listVersionIDs to reference the actual genericode lists
	 * @param sourceURL	The source URL to be cached
	 * @param targetPath	The targetPath to store local copies of the remote files to
	 * @param schemaCache	Maps remote references to references to their local copies
	 * @param reverseSchemaCache	Maps references of local copies to their remote references
	 * @param listCache	Register of pairs of listURIs and listVersionIDs which are declared in schema files
	 */
	public void cacheRecursiveURL(
			URL sourceURL, 
			String targetPath, 
			TreeMap<String, String> schemaCache, 
			TreeMap<String, String> reverseSchemaCache, 
			ReferenceRegister listCache) {
		String original = sourceURL.toString();
		
		if(workingOn.contains(original)) {
			return;
		}
		if(schemaCache.containsKey(sourceURL.toString())){
			File check = new File(getFromSchemaCache(schemaCache, sourceURL.toString()));
			if(check.exists()) {
				return;
			}
		}
		this.workingOn.add(original);
		
		ui.setStatusText("Lade URL " + cleanURL(sourceURL).toString());
		
		try {
	        String contents;
	        
			if(wwwCache.containsKey(original)) {
				contents = wwwCache.get(original);
			}
			else {
				// Teste, ob URL umgeleitet wird und folge ggf. 
		        
				BufferedReader in = new BufferedReader(new InputStreamReader(followRedirect(sourceURL).openStream()));

		        String lf = System.getProperty("line.separator");
		        StringBuilder build = new StringBuilder();
		        String inputLine;
		        while ((inputLine = in.readLine()) != null) {
		        	build.append(inputLine + lf);
		        }
		        in.close();
		        contents = build.toString();
		        
		        wwwCache.put(sourceURL.toString(), contents);
		        if(!original.equals(sourceURL.toString())) {
			        wwwCache.put(original, contents);
		        }
			}
	        
			this.getListURIReferencesFromSchema(new ByteArrayInputStream(contents.getBytes()), listCache);
			
			String schemaFilePath = sourceURL.getPath();
			while(schemaFilePath.startsWith("/")) {
				schemaFilePath = schemaFilePath.substring(1);
			}
			schemaFilePath = targetPath + "/" + schemaFilePath;
			
			File schemaFileDir = new File(schemaFilePath);
			File schemaFileParentDir = schemaFileDir.getParentFile();
			String schemaFileParentDirRelativePath = schemaFileParentDir.getAbsolutePath().substring(targetPath.length()).replace("\\", "/");
			
			while(schemaFileParentDirRelativePath.startsWith("/")) {
				schemaFileParentDirRelativePath = schemaFileParentDirRelativePath.substring(1);
			}
			
	        // Hier werden im Schema die Referenzen auf lokale Dateien statt URLs umgebogen und das XSD lokal gespeichert
	        ArrayList<Include> includes = this.getIncludesFromSchema(new ByteArrayInputStream(contents.getBytes()));
	        for(Include include : includes) {
	        	String schemaReference = include.getSchemaLocation(); // targetPath
				contents = applyLocalSchemaFileContents(targetPath, schemaFileParentDirRelativePath, contents, schemaReference);
	        }
	        ArrayList<Import> imports = this.getImportsFromSchema(new ByteArrayInputStream(contents.getBytes()));
	        for(Import imp : imports) {
	        	String schemaLocation = imp.getSchemaLocation();
				contents = applyLocalSchemaFileContents(targetPath, schemaFileParentDirRelativePath, contents, schemaLocation);
	        }
	        
	        File checkFile = new File(schemaFilePath);
	        checkFile.getParentFile().mkdirs();
	        
	        PrintWriter out = new PrintWriter(schemaFilePath);
			out.println(contents);
	        out.close();
	        
	        putToSchemaCache(schemaCache, reverseSchemaCache, sourceURL.toString(), schemaFilePath);
	        
	        // Jetzt werden alle referenzierten XSDs in gleicher Weise analysiert und lokal gespeichert
	        for(Include include : includes) {
	        	if(include.getSchemaLocation().startsWith("http://") || include.getSchemaLocation().startsWith("https://")) {
	        		cacheRecursiveURL(new URL(include.getSchemaLocation()), targetPath, schemaCache, reverseSchemaCache, listCache);
	        	}
	        	else {
	        		int last = sourceURL.toString().lastIndexOf("/");
	        		String url = sourceURL.toString().substring(0, last) + "/" + include.getSchemaLocation();
	        		cacheRecursiveURL(new URL(url), targetPath, schemaCache, reverseSchemaCache, listCache);
	        	}
	        }
	        for(Import imp : imports) {
	        	if(imp.getSchemaLocation().startsWith("http://") || imp.getSchemaLocation().startsWith("https://")) {
	        		cacheRecursiveURL(new URL(imp.getSchemaLocation()), targetPath, schemaCache, reverseSchemaCache, listCache);
	        	}
	        	else {
	        		int last = sourceURL.toString().lastIndexOf("/");
	        		String url = sourceURL.toString().substring(0, last) + "/" + imp.getSchemaLocation();
	        		cacheRecursiveURL(new URL(url), targetPath, schemaCache, reverseSchemaCache, listCache);
	        	}
	        }
	    } catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			this.workingOn.remove(original);
		}
	}
//	
//	public static void main(String[] args) {
//		try {
//			System.out.println(cleanURL(new URL("http://www.tim.de/o1/02/03/../o4/o5")));
//			System.out.println(cleanURL(new URL("http://www.tim.de/o1/02/../../o4/o5")));
//			System.out.println(cleanURL(new URL("http://www.tim.de/o1/../../../o4/o5")));
//			System.out.println(cleanURL(new URL("http://www.tim.de/../02/../../o4/o5")));
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	/**
	 * cleans a given URL from potential path segments like "../"
	 * @param input	input URL to be cleaned
	 * @return	output URL cleaned from potential "../" path segments
	 */
	public static URL cleanURL(URL input) {
		try {
			String sourcePath = input.getPath();
			StringTokenizer tok = new StringTokenizer(sourcePath, "/");
			ArrayList<String> components = new ArrayList<>();
			while(tok.hasMoreElements()) {
				components.add(tok.nextToken());
			}
			
			int index = 0;
			while(index < components.size()) {
				String comp = components.get(index);
				if("..".equals(comp)) {
					components.remove(index);
					components.remove(index-1);
					index--;
				}
				else {
					index++;
				}
			}
			
			StringBuffer destPath = new StringBuffer();
			destPath.append(input.getProtocol() + "://" + input.getAuthority());
			for(int i=0;i<components.size();i++) {
				destPath.append("/" + components.get(i));
			}
			
			URL ret = new URL(destPath.toString());
			
			return ret;
		} catch (Exception e) {
			e.printStackTrace();
			
			return input;
		}
	}

	/**
	 * Determines whether a given URL gets redirected by the server and stores the redirect as a shortcut for further usage. 
	 * In case of serial redirects, this method follows each of them
	 * @param sourceURL	The source URL to be analyzed for redirection 
	 * @return	The resulting URL - if no redirection takes place, the original URL will be returned
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	private static URL followRedirect(URL sourceURL) throws MalformedURLException, IOException {
		TreeSet<String> visited = new TreeSet<String>();
		URL movedURL = new URL(sourceURL.toString());
		while(true) {
			URLConnection conn = movedURL.openConnection();
			String moved = conn.getHeaderField("Location");
			
			if(moved == null) {
				break;
			}
			if(moved.trim().length() == 0) {
				break;
			}
			try {
				if(visited.contains(moved)) {
					break;
				}
				visited.add(moved);
				
				movedURL = new URL(moved);
			} catch (Exception e) {
				break;
			}
		}
		return movedURL;
	}
	
	/**
	 * Returns an item from the schema cache map specified by its key String
	 * @param cache	The map that contains the values for key
	 * @param key	Identified the key String
	 * @return	The according value for key or null if key is not covered
	 */
	public String getFromSchemaCache(TreeMap<String, String> cache, String key) {
    	if(key.startsWith("http://") || key.startsWith("https://")) {
    		// clean url first from potential "../" path segemnts
            try {
				URL sourceURL = cleanURL(new URL(key));
				key = sourceURL.toString();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
    	}
		
		return cache.get(key.replace("\\", "/"));
	}
	
	/**
	 * This method is used to make a reference from a remote schema or codelist to an (identical) local file
	 * @param cache	references from remote to local files
	 * @param reverseCache	(reverse) references from local files to their remote counterparts
	 * @param key	remote file (URL / URI)
	 * @param value	path to local file
	 */
	public void putToSchemaCache(TreeMap<String, String> cache, TreeMap<String, String> reverseCache, String key, String value) {
    	if(key.startsWith("http://") || key.startsWith("https://")) {
    		// clean url first from potential "../" path segemnts
            try {
				URL sourceURL = cleanURL(new URL(key));
				key = sourceURL.toString();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
    	}
		
        String ref = key.replace("\\", "/");
		cache.put(ref, value);
		reverseCache.put(value, ref);
	}

	/**
	 * Seek for genericode list references within a given schema file. Since a genericode list is referenced by listURI AND listVersionID, 
	 * both entries need to be identified (if present)
	 * @param file	The input schema file (as Stream)
	 * @param refBuffer	Buffer object to store list references
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	private void getListURIReferencesFromSchema(
			InputStream file, 
			ReferenceRegister refBuffer) 
					throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(file);
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile(
					"/schema/complexType/complexContent/descendant-or-self::*[local-name()='attribute'][@name='listURI'][@fixed!='']/..");
			NodeList res = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			
			for(int i=0;i<res.getLength();i++) {
				Node nd = res.item(i);
				
				XPathExpression listExpr = xpath.compile("child::*[local-name()='attribute'][@name='listURI'][@fixed!='']/@fixed");
				String listRes = (String) listExpr.evaluate(nd, XPathConstants.STRING);
				
				XPathExpression versionExpr = xpath.compile("child::*[local-name()='attribute'][@name='listVersionID'][@fixed!='']/@fixed");
				String versionRes = (String) versionExpr.evaluate(nd, XPathConstants.STRING);
				
				if(listRes != null && listRes.trim().length() > 0) {
					refBuffer.set(listRes);
					
					if(versionRes != null && versionRes.trim().length() > 0) {
						refBuffer.set(listRes, versionRes);
					}
				}
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Seek for genericode list references within a given XML file. Since a genericode list is referenced by listURI AND listVersionID, 
	 * both entries need to be identified (if present)
	 * @param file	The input schema file (as Stream)
	 * @param refBuffer	Buffer object to store list references
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	public void getListURIReferencesFromMessage(File file, 
			ReferenceRegister refBuffer) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
		
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		
		XPathExpression expr = xpath.compile("/descendant-or-self::*[@listURI]");
		NodeList res = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		
		for(int i=0;i<res.getLength();i++) {
			Node nd = res.item(i);
			Node lu = nd.getAttributes().getNamedItem("listURI");
			
			if(lu != null) {
				Node lv = nd.getAttributes().getNamedItem("listVersionID");
				if(lv != null) {
					String listRes = lu.getNodeValue();
					String versionRes = lv.getNodeValue();
					
					if(listRes != null && listRes.trim().length() > 0) {
						refBuffer.set(listRes);
						
						if(versionRes != null && versionRes.trim().length() > 0) {
							refBuffer.set(listRes, versionRes);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Process a given URN for whether the according codelist is covered by the official XRepository (provided by KoSIT)
	 * If so (i) cache its contents in wwwCache, (ii) make a local copy within the target folder and (iii) schemaCache 
	 * and (iv) reverseSchemaCache (see JavaDoc). 
	 * @param urn	The urn to be checked and cached
	 * @param targetPath	The targetPath to store local copies of the remote files to
	 * @param schemaCache	Maps remote references to references to their local copies
	 * @param reverseSchemaCache	Maps references of local copies to their remote references
	 * @return	True in case urn is covered by XRepository, false otherwise
	 */
	public boolean cacheURN(
			String urn, 
			String targetPath, 
			TreeMap<String, String> schemaCache, 
			TreeMap<String, String> reverseSchemaCache) {
		if(workingOn.contains(urn)) {
			return false;
		}
		if(schemaCache.containsKey(urn)){
			return false;
		}
		this.workingOn.add(urn);
		
		ui.setStatusText("Lade aus XRepository: " + urn);
		
		try {
			URL schemaFileURL = new URL("https://www.xrepository.de/api/xrepository/" + urn + ":technischerBestandteilGenericode");
			String schemaFilePath = targetPath + "/" + urn.replace(":", "_") + ".gc";
			
	        String contents = "";
			try {
				if(wwwCache.containsKey(schemaFileURL.toString())) {
					contents = wwwCache.get(schemaFileURL.toString());
				}
				else {
					// Teste, ob URL umgeleitet wird und folge ggf. 
					
					BufferedReader in = new BufferedReader(new InputStreamReader(followRedirect(schemaFileURL).openStream()));

					String lf = System.getProperty("line.separator");
					StringBuilder build = new StringBuilder();
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						build.append(inputLine + lf);
					}
					in.close();
					contents = build.toString();
			        
			        wwwCache.put(schemaFileURL.toString(), contents);
				}
		        
		        File checkFile = new File(schemaFilePath);
		        checkFile.getParentFile().mkdirs();
		        
		        PrintWriter out = new PrintWriter(schemaFilePath);
				out.println(contents);
		        out.close();
		        
		        putToSchemaCache(schemaCache, reverseSchemaCache, urn, schemaFilePath);
		        
		        return true;
			} catch (Exception e) {
		        putToSchemaCache(schemaCache, reverseSchemaCache, urn, schemaFilePath);
		        
		        return false;
			}
	    } catch (IOException e) {
			e.printStackTrace();
	        
	        return false;
		}
		finally {
			this.workingOn.remove(urn.toString());
		}
	}
	
	/**
	 * Process a given URL for codelist content. If content is present, (i) cache it in wwwCache, 
	 * (ii) make a local copy within the target folder and (iii) schemaCache 
	 * and (iv) reverseSchemaCache (see JavaDoc). 
	 * @param url	The url to be checked and cached
	 * @param targetPath	The targetPath to store local copies of the remote files to
	 * @param schemaCache	Maps remote references to references to their local copies
	 * @param reverseSchemaCache	Maps references of local copies to their remote references
	 * @return	True in case urn is covered by XRepository, false otherwise
	 */
	public boolean cacheURL(
			String url, 
			String targetPath, 
			TreeMap<String, String> schemaCache, 
			TreeMap<String, String> reverseSchemaCache) {
		if(workingOn.contains(url)) {
			return false;
		}
		if(schemaCache.containsKey(url)){
			return false;
		}
		this.workingOn.add(url);
		
		ui.setStatusText("Lade externe Ressource: " + url);
		
		try {
			URL schemaFileURL = new URL(url);
			String schemaFilePath = targetPath + "/" + url.replace(":", "_").replace("/", "_") + ".gc";
			
	        String contents = "";
			try {
				if(wwwCache.containsKey(schemaFileURL.toString())) {
					contents = wwwCache.get(schemaFileURL.toString());
				}
				else {
					// Teste, ob URL umgeleitet wird und folge ggf. 
					
					BufferedReader in = new BufferedReader(new InputStreamReader(followRedirect(schemaFileURL).openStream()));

					String lf = System.getProperty("line.separator");
					StringBuilder build = new StringBuilder();
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						build.append(inputLine + lf);
					}
					in.close();
					contents = build.toString();
			        
			        wwwCache.put(schemaFileURL.toString(), contents);
				}
		        
		        File checkFile = new File(schemaFilePath);
		        checkFile.getParentFile().mkdirs();
		        
		        PrintWriter out = new PrintWriter(schemaFilePath);
				out.println(contents);
		        out.close();
		        
		        putToSchemaCache(schemaCache, reverseSchemaCache, url, schemaFilePath);
		        
		        return true;
			} catch (Exception e) {
		        putToSchemaCache(schemaCache, reverseSchemaCache, url, schemaFilePath);
		        
		        return false;
			}
	    } catch (IOException e) {
			e.printStackTrace();
	        
	        return false;
		}
		finally {
			this.workingOn.remove(url.toString());
		}
	}

	/**
	 * Adjust textual (XML) content of a schema file in order to redirect remote references to their local copies (which
	 * either already exist or get created afterwards)
	 * @param offlineFolderAbsolutePath	Path to the folder of local copies 
	 * @param contentRelativePath	The relative path of the current schema file (becomes part of the resulting local path)
	 * @param contents	The textual content of the schema file
	 * @param schemaReference	The reference to be checked and replaced (in case it points to a remote file or denotes a relative path)
	 * @return	The adjusted XML content
	 * @throws MalformedURLException
	 */
	private String applyLocalSchemaFileContents(
			String offlineFolderAbsolutePath, 
			String contentRelativePath, 
			String contents, 
			String schemaReference)
			throws MalformedURLException {
		String newReference = "";
		
		if(schemaReference.startsWith("http://") || schemaReference.startsWith("https://")) {
			URL check = new URL(schemaReference);
			String referencePath = check.getPath();
			if(referencePath.startsWith("/")) {
				referencePath = referencePath.substring(1);
			}
			newReference = offlineFolderAbsolutePath.replace("\\", "/") + "/" + referencePath;
		}
		else {
			File chk = new File(schemaReference);
			if(chk.isAbsolute()) {
				// Do nothing
				newReference = schemaReference;
			}
			else { // targetPath + "/" + schemaLocation
				newReference = offlineFolderAbsolutePath.replace("\\", "/") + "/" + contentRelativePath + "/" + schemaReference;
			}
		}
		
		String applyPath = Paths.get(offlineFolderAbsolutePath + "/" + contentRelativePath).relativize(Paths.get(newReference)).toString().replace("\\", "/");
		contents = contents.replace(schemaReference, applyPath);
		
		return contents;
	}
	
	/**
	 * Process a given local schema file (defined by a File object) and (i) make 
	 * a local copy within the target folder and (ii) schemaCache and (iii) reverseSchemaCache (see JavaDoc). 
	 * Recursively process referenced schema files. Also (v) add an entry to a ReferenceRegister object which 
	 * combines listURIs and listVersionIDs to reference the actual genericode lists
	 * @param root	The folder resource which contains the source file
	 * @param sourceFile	The source URL to be cached
	 * @param targetPath	The targetPath to store local copies of the remote files to
	 * @param schemaCache	Maps remote references to references to their local copies
	 * @param reverseSchemaCache	Maps references of local copies to their remote references
	 * @param listCache	Register of pairs of listURIs and listVersionIDs which are declared in schema files
	 */
	public void cacheRecursiveFile(
			XTAFolderResource root, 
			File sourceFile, 
			String targetPath, 
			TreeMap<String, String> schemaCache, 
			TreeMap<String, String> reverseSchemaCache, 
			ReferenceRegister listCache) {
		String sourcePath = sourceFile.getAbsolutePath().replace("\\", "/");
		
		if(workingOn.contains(sourcePath)) {
			return;
		}
		if(schemaCache.containsKey(sourcePath)) {
			File check = new File(getFromSchemaCache(schemaCache, sourceFile.toString()));
			if(check.exists()) {
				return;
			}
		}
		this.workingOn.add(sourcePath);
		
		ui.setStatusText("Lade lokale Datei " + sourcePath);
		
		try {
			this.getListURIReferencesFromSchema(new FileInputStream(sourceFile), listCache);
			
			String relativeTargetPath = sourceFile.getAbsolutePath().substring(root.getPath().length()).replace("\\", "/");
			while(relativeTargetPath.startsWith("/")) {
				relativeTargetPath = relativeTargetPath.substring(1);
			}
			
			String targetFilePath = targetPath + "/" + relativeTargetPath;
			
			File targetFileDir = new File(targetFilePath);
			File targetFileParentDir = targetFileDir.getParentFile();
			String targetFileParentDirRelativePath = targetFileParentDir.getAbsolutePath().substring(targetPath.length()).replace("\\", "/");
			
			while(targetFileParentDirRelativePath.startsWith("/")) {
				targetFileParentDirRelativePath = targetFileParentDirRelativePath.substring(1);
			}
			
	    	InputStream is = new FileInputStream(sourceFile);
			byte[] byteArray = IOUtils.toByteArray(is);
	        String contents = new String(byteArray);
	        
	        // Hier werden im Schema die Referenzen auf lokale Dateien statt URLs umgebogen und das XSD lokal gespeichert
	        InputStream inputStream = new ByteArrayInputStream(contents.getBytes(Charset.forName("UTF-8")));
	        ArrayList<Include> includes = this.getIncludesFromSchema(inputStream);
	        for(Include include : includes) {
	        	String schemaLocation = include.getSchemaLocation();
				contents = applyLocalSchemaFileContents(targetPath, targetFileParentDirRelativePath, contents, schemaLocation);
	        }
	        inputStream.reset();
	        ArrayList<Import> imports = this.getImportsFromSchema(inputStream);
	        for(Import 	imp : imports) {
	        	String schemaLocation = imp.getSchemaLocation();
				contents = applyLocalSchemaFileContents(targetPath, targetFileParentDirRelativePath, contents, schemaLocation);
	        }
	        inputStream.close();
	        
	        File checkFile = new File(targetFilePath);
	        checkFile.getParentFile().mkdirs();
	        
	        PrintWriter out = new PrintWriter(targetFilePath);
			out.println(contents);
	        out.close();
	        
	        putToSchemaCache(schemaCache, reverseSchemaCache, sourceFile.getAbsolutePath(), targetFilePath);
	        
	        for(Include include : includes) {
	        	if(include.getSchemaLocation().startsWith("http://") || include.getSchemaLocation().startsWith("https://")) {
	        		cacheRecursiveURL(new URL(include.getSchemaLocation()), targetPath, schemaCache, reverseSchemaCache, listCache);
	        	}
	        	else {
					File check = sourceFile.getParentFile();
					while(include.getSchemaLocation().startsWith("../")) {
						check = check.getParentFile();
						include.setSchemaLocation(include.getSchemaLocation().substring(3));
					}
					include.setSchemaLocation(check.getAbsolutePath() + "/" + include.getSchemaLocation());
					
	        		cacheRecursiveFile(root, new File(include.getSchemaLocation()), targetPath, schemaCache, reverseSchemaCache, listCache);
	        	}
	        }
	        for(Import imp : imports) {
	        	if(imp.getSchemaLocation().startsWith("http://") || imp.getSchemaLocation().startsWith("https://")) {
	        		cacheRecursiveURL(new URL(imp.getSchemaLocation()), targetPath, schemaCache, reverseSchemaCache, listCache);
	        	}
	        	else {
					File check = sourceFile.getParentFile();
					while(imp.getSchemaLocation().startsWith("../")) {
						check = check.getParentFile();
						imp.setSchemaLocation(imp.getSchemaLocation().substring(3));
					}
					imp.setSchemaLocation(check.getAbsolutePath() + "/" + imp.getSchemaLocation());
					
	        		cacheRecursiveFile(root, new File(imp.getSchemaLocation()), targetPath, schemaCache, reverseSchemaCache, listCache);
	        	}
	        }
	    } catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			this.workingOn.remove(sourcePath);
		}
	}

	/**
	 * Evaluates the schemaLocation attribute in a XML file and returns the according value (if present)
	 * @param file	The XML file to be analyzed
	 * @return	The schemaLocation attribute value if present or an empty string otherwise
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	public String getSchemaLocationFromMessage(File file) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		String ret = "";
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
		
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		
		XPathExpression expr = xpath.compile("/child::*[1]/@schemaLocation");
		ret = (String) expr.evaluate(doc, XPathConstants.STRING);
		
		int sep = ret.indexOf(" ");
		if(sep > 0) {
			ret = ret.substring(sep + 1);
			
			sep = ret.indexOf(" ");
			if(sep > 0) {
				ret = ret.substring(0, sep);
			}
		}
		
		return ret;
	}

	/**
	 * Evaluates the schemaLocation attribute in a XML file and returns the according values (if present)
	 * @param file	The XML file to be analyzed
	 * @return	The schemaLocation attribute values if present or an empty string otherwise
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	public ArrayList<String> getSchemaLocationsFromMessage(File file) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		ArrayList<String> ret = new ArrayList<>();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
		
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		
		XPathExpression expr = xpath.compile("/child::*[1]/@schemaLocation");
		String loc = (String) expr.evaluate(doc, XPathConstants.STRING);
		
		StringTokenizer tok = new StringTokenizer(loc, " \t\r\n");
		while(tok.hasMoreElements()) {
			String ns = tok.nextToken();
			
			if(tok.hasMoreElements()) {
				String sl = tok.nextToken();
				ret.add(sl);
			}
			else {
				ret.add(ns);
			}
		}
		
		return ret;
	}
	
	/**
	 * Get a schema cache map from the localSchemaCache defined by id. In case no map is present, a new map will be created and stored. 
	 * @param id	The id of the schema cache
	 * @return	Schema cache map from the localSchemaCache defined by id
	 */
	public TreeMap<String, String> getLocalSchemaCache(String id){
		if(!this.localSchemaCache.containsKey(id)) {
			this.localSchemaCache.put(id, new TreeMap<>());
		}
		
		return this.localSchemaCache.get(id);
	}
	
	/**
	 * Get a reverse schema cache map from the reverseLocalSchemaCache defined by id. In case no map is present, a new map will be created and stored. 
	 * @param id	The id of the schema cache
	 * @return	Schema cache map from the reverseLocalSchemaCache defined by id
	 */
	public TreeMap<String, String> getReverseLocalSchemaCache(String id){
		if(!this.reverseLocalSchemaCache.containsKey(id)) {
			this.reverseLocalSchemaCache.put(id, new TreeMap<>());
		}
		
		return this.reverseLocalSchemaCache.get(id);
	}
	
	/**
	 * Removes a schema cache map from the localSchemaCache defined by id. 
	 * @param id	The id of the schema cache
	 */
	public void removeLocalSchemaCache(String id) {
		this.localSchemaCache.remove(id);
	}
	
	/**
	 * Removes a schema cache map from the reverseLocalSchemaCache defined by id. 
	 * @param id	The id of the schema cache
	 */
	public void removeReverseLocalSchemaCache(String id) {
		this.reverseLocalSchemaCache.remove(id);
	}
	
	/**
	 * This method is originated from https://eyalsch.wordpress.com/2010/11/30/xml-dom-2/ 
	 * by Eyal Schneider on November 30, 2010
	 * @param is
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 */
    public static Document readXML(final InputStream is) throws IOException, SAXException {
        final Document doc;
        SAXParser parser;
        try {
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            parser = factory.newSAXParser();
            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.newDocument();
        } catch (final ParserConfigurationException e) {
            throw new RuntimeException("Can't create SAX parser / DOM builder.", e);
        }

        final Stack<Element> elementStack = new Stack<Element>();
        final StringBuilder textBuffer = new StringBuilder();
        final DefaultHandler handler = new DefaultHandler() {
            private Locator locator;

            @Override
            public void setDocumentLocator(final Locator locator) {
                this.locator = locator; // Save the locator, so that it can be used later for line tracking when traversing nodes.
            }

            @Override
            public void startElement(final String uri, final String localName, final String qName, final Attributes attributes)
                    throws SAXException {
                addTextIfNeeded();
                final Element el = doc.createElement(qName);
                for (int i = 0; i < attributes.getLength(); i++) {
                    el.setAttribute(attributes.getQName(i), attributes.getValue(i));
                }
                el.setUserData(LINE_NUMBER_KEY_NAME, String.valueOf(this.locator.getLineNumber()), null);
                elementStack.push(el);
            }

            @Override
            public void endElement(final String uri, final String localName, final String qName) {
                addTextIfNeeded();
                final Element closedEl = elementStack.pop();
                if (elementStack.isEmpty()) { // Is this the root element?
                    doc.appendChild(closedEl);
                } else {
                    final Element parentEl = elementStack.peek();
                    parentEl.appendChild(closedEl);
                }
            }

            @Override
            public void characters(final char ch[], final int start, final int length) throws SAXException {
                textBuffer.append(ch, start, length);
            }

            // Outputs text accumulated under the current node
            private void addTextIfNeeded() {
                if (textBuffer.length() > 0) {
                    final Element el = elementStack.peek();
                    final Node textNode = doc.createTextNode(textBuffer.toString());
                    el.appendChild(textNode);
                    textBuffer.delete(0, textBuffer.length());
                }
            }
        };
        parser.parse(is, handler);

        return doc;
    }
}
