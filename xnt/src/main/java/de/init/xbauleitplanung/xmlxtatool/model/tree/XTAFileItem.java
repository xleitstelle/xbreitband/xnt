package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.Serializable;

/**
 * This class represents an XTATreeItemI implementation of an arbitrary file within a folder structure
 * 
 * @author treichling
 *
 */
public class XTAFileItem extends XTATreeItem implements Serializable, XTATreeItemI{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8195750864431970766L;

	/**
	 * The path of this file
	 */
	private String path;
	
	/**
	 * Is this file item a folder? 
	 */
	private boolean folder = true;
	
	/**
	 * Are path entries case sensitive - true by default
	 */
	private boolean ignorePathCase = true;
	
	public XTAFileItem() {
		super();
	}

	@Override
	public String getItemType() {
		return "FILE_ITEM";
	}
	
	/**
	 * Returns whether or not this item covers a given path (that is the given path is 
	 * the path of this file or of one of its descendants)
	 * @param path	The path in question
	 * @return	True in case path is the path of this file or of one of its descendants, false otherwise
	 */
	public boolean coversPath(String path) {
		boolean ret = false;
		
		if(this.path.equals(path.replace("\\", "/"))) {
			ret = true;
		}
		else {
			for(int i=0;i<this.getItemChildCount();i++) {
				if(this.getItemChildAt(i) instanceof XTAFileItem) {
					ret = ((XTAFileItem) this.getItemChildAt(i)).coversPath(path);
					
					if(ret) {
						break;
					}
				}
			}
		}
		
		return ret;
	}
	
	/**
	 * Determines the file item corresponding with the specified path, in case path denotes the path 
	 * of this item or one of its descendants or null otherwise
	 * @param path	The path in question
	 * @return	The corresponding file item in case path denotes the path of this item or one of its descendants or null otherwise
	 */
	public XTAFileItem getDescendantForPath(String path) {
		XTAFileItem ret = null;
		
		if(this.path.equals(path.replace("\\", "/").replace("%20", " "))) {
			ret = this;
		}
		else if(this.ignorePathCase && this.path.equalsIgnoreCase(path.replace("\\", "/").replace("%20", " "))) {
			ret = this;
		}
		else {
			for(int i=0;i<this.getItemChildCount();i++) {
				XTATreeItemI childAt = this.getItemChildAt(i);
				
				if(childAt instanceof XTAFileItem) {
					ret = ((XTAFileItem) childAt).getDescendantForPath(path);
					
					if(ret != null) {
						break;
					}
				}
			}
		}
		
		return ret;
	}
	
	/**
	 * Returns a child of this file item that corresponds to the specified path or null if no such child exists
	 * @param path	The path in question
	 * @return	a child of this file item that corresponds to the specified path or null if no such child exists
	 */
	public XTAFileItem getChildForPath(String path) {
		for(int i=0;i<this.getItemChildCount();i++) {
			if(this.getItemChildAt(i) instanceof XTAFileItem) {
				try {
					XTAFileItem ret = (XTAFileItem) this.getItemChildAt(i);
					
					if(ret.path.equals(path.replace("\\", "/"))) {
						return ret;
					}
					else if(this.ignorePathCase && ret.path.equalsIgnoreCase(path.replace("\\", "/"))) {
						return ret;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Method for debug purposes displaying the entire structure of this item
	 */
	public void show(int depth) {
		for(int i=0;i<depth;i++) {
			System.out.print("  ");
		}
		System.out.println(" " + this.path);
		for(int i=0;i<depth;i++) {
			System.out.print("  ");
		}
		System.out.println("[" + this.getItemId() + "]");
		
		for(int i=0;i<this.getItemChildCount();i++) {
			if(this.getItemChildAt(i) instanceof XTAFileItem) {
				((XTAFileItem) this.getItemChildAt(i)).show(depth + 1);
			}
		}
	}

	/**
	 * Setter for path
	 * @param path	The new path of this item
	 */
	public void setPath(String path) {
		this.path = path.replace("\\", "/");
		
		this.setItemId(new String(this.path));
		
		int sep = this.path.lastIndexOf("/");
		if(sep > 0) {
			this.setItemName(this.path.substring(sep + 1));
		}
	}
	
	/**
	 * Returns the path of this item
	 * @return	the path of this item
	 */
	public String getPath() {
		return this.path;
	}
	
	/**
	 * Creates an exact copy of this item
	 * @param copy	an exact copy of this item
	 */
	public void clone(XTAFileItem copy) {
		super.clone(copy);
		
		this.path = new String(copy.path);
		this.folder = copy.folder;
	}

	@Override
	public int compareTo(XTATreeItemI o) {
		int ret;
		try {
			ret = this.getItemType().compareTo(o.getItemType());
			
			if(ret == 0) {
				if(o instanceof XTAFileItem) {
					ret = this.getPath().compareTo(((XTAFileItem) o).getPath());
				}
				else {
					ret = this.getItemName().compareTo(o.getItemName());
				}
			}
		} catch (Exception e) {
			ret = -1;
		}
		
		return ret;
	}

	/**
	 * Deletes all children of this item
	 */
	public void clear() {
		super.clear();
	}

	@Override
	public String toString() {
		return this.getPath();
	}
}
