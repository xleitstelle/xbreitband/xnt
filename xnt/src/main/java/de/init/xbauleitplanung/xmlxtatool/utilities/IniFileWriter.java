package de.init.xbauleitplanung.xmlxtatool.utilities;

import java.util.Enumeration;
import java.util.Hashtable;


/**
 * @author tim
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class IniFileWriter extends ObjectWriter {
	Hashtable values = new Hashtable();

	/**
	 * Constructor for IniFileWriter.
	 * @param fileName
	 */
	public IniFileWriter(String fileName) {
		super(fileName);
	}
	
	public void setValue(String var, String val){
		if(val != null){
			values.put(var, val);
		}
	}
	
	public void setIntValue(String var, int val){
		values.put(var, String.valueOf(val));
	}
	
	public void setLongValue(String var, long val){
		values.put(var, String.valueOf(val));
	}
	
	public void setDoubleValue(String var, double val){
		values.put(var, String.valueOf(val));
	}
	
	public void setBooleanValue(String var, boolean val){
		values.put(var, String.valueOf(val));
	}
	
	public void write(){
		Enumeration keys=values.keys();
		
		while(keys.hasMoreElements()){
			String key = (String) keys.nextElement();
			this.setString(key + "=" + values.get(key));
		}
		this.close();
	}
	
	public static void main(String[] args) {
	}
}
