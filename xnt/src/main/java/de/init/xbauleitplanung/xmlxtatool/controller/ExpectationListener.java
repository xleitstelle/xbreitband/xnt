package de.init.xbauleitplanung.xmlxtatool.controller;

public interface ExpectationListener {
	public void raise(String expectation);
	public void fulfill(String expectation);
}
