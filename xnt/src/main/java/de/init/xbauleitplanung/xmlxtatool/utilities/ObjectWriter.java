package de.init.xbauleitplanung.xmlxtatool.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;

public class ObjectWriter
{
	private String file;
	private Collection<String> lines;
	
	public static void writeObject(Object obj, String file){
		try{
	        FileOutputStream ostream = new FileOutputStream(file);
		    ObjectOutputStream p = new ObjectOutputStream(ostream);

			p.writeObject(obj);

	        p.flush();
		    ostream.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public ObjectWriter(String fileName){
		try{
			this.file = fileName;
			this.lines = new ArrayList<String>();
		}
		catch (Exception e) {
		}
	}

	public void setInt(int val){
		try{
			this.lines.add(String.valueOf(val));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void setLong(long val){
		try{
			this.lines.add(String.valueOf(val));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void setString(String val){
		try{
			String ret = new String(val);
			
//			ret = ret.replace("\u00E4", "__a__");
//			ret = ret.replace("\u00F6", "__o__");
//			ret = ret.replace("\u00FC", "__u__");
//			ret = ret.replace("\u00C4", "__A__");
//			ret = ret.replace("\u00D6", "__O__");
//			ret = ret.replace("\u00DC", "__U__");
//			ret = ret.replace("\u00DF", "__s__");
//			
//			ret = ret.replace("\u20AC", "__E__");
//			ret = ret.replace("\u0024", "__D__");
//			
//			ret = ret.replace("�", "__a__");
//			ret = ret.replace("�", "__o__");
//			ret = ret.replace("�", "__u__");
//			ret = ret.replace("�", "__A__");
//			ret = ret.replace("�", "__O__");
//			ret = ret.replace("�", "__U__");
//			ret = ret.replace("�", "__s__");
//			
//			ret = ret.replace("�", "__E__");
//			ret = ret.replace("$", "__D__");
			
//			ret = ret.replace("�", "\u00E4");
//			ret = ret.replace("�", "\u00F6");
//			ret = ret.replace("�", "\u00FC");
//			ret = ret.replace("�", "\u00C4");
//			ret = ret.replace("�", "\u00D6");
//			ret = ret.replace("�", "\u00DC");
//			ret = ret.replace("�", "\u00DF");
//			
//			ret = ret.replace("�", "\u20AC");
//			ret = ret.replace("$", "\u0024");
			
			this.lines.add(ret);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void close(){
		try{
			StringBuilder build = new StringBuilder();
			String sep = System.getProperty("line.separator");
			Iterator<String> it = this.lines.iterator();
			while(it.hasNext()){
				build.append(it.next() + sep);
			}
			HashSet<String> st = new HashSet<String>();
			st.add(build.toString());
			FileUtils.writeLines(new File(this.file), "utf-8", st);
//			
//			FileUtils.writeLines(new File(this.file), "utf-8", this.lines);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
