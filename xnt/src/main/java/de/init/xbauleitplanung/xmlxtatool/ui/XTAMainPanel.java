package de.init.xbauleitplanung.xmlxtatool.ui;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.IntFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.NavigationActions.SelectionPolicy;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.reactfx.value.Val;

import de.init.xbauleitplanung.xmlxtatool.controller.XTAController;
import de.init.xbauleitplanung.xmlxtatool.controller.XTAServerI;
import de.init.xbauleitplanung.xmlxtatool.model.schema.hierarchical.XModelTreeNode;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XAttribute;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XCodelist;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XComplexType;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElement;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XSimpleType;
import de.init.xbauleitplanung.xmlxtatool.model.tree.MessageMetaData;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFITConnectPostOfficeBox;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFolderResource;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAGenericodeList;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAPostOfficeBox;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAResource;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAResourceType;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAXMLFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.xml.XNode;
import de.init.xbauleitplanung.xmlxtatool.utilities.IniFileReader;
import de.init.xbauleitplanung.xmlxtatool.utilities.IniFileWriter;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class XTAMainPanel extends Application implements EventHandler<ActionEvent>, ChangeListener<TreeView<String>>, XTAClientI{
    
	// XML highlighting features
	// the following regex defines groups (those terms in parentheses). Groups are referred to by their index within the regex (e.g. 2, 3, 4...)
	// 1st group is defined by (</?\\h*), 2nd group is defined by ([\\w-:.]+)...
	private static final Pattern XML_TAG = Pattern.compile("(?<ELEMENT>(</?\\h*)([\\w-:.]+)([^<>]*)(\\h*/?>))"
    		+"|(?<COMMENT><!--(.|\\v)+?-->)");
    
    // Again groups are defined as above! 
	private static final Pattern ATTRIBUTES = Pattern.compile("([\\w-:.]+\\h*)(=)(\\h*\"[^\"]+\")");
    
	// Group indices in order to reference groups and their matching contents
    private static final int GROUP_OPEN_BRACKET = 2;
    private static final int GROUP_ELEMENT_NAME = 3;
    private static final int GROUP_ATTRIBUTES_SECTION = 4;
    private static final int GROUP_CLOSE_BRACKET = 5;
    private static final int GROUP_ATTRIBUTE_NAME = 1;
    private static final int GROUP_EQUAL_SYMBOL = 2;
    private static final int GROUP_ATTRIBUTE_VALUE = 3;

	/**
	 * Host instance (controller in MVC)
	 */
	private XTAServerI host;
	
	/**
	 * Here handles for tree nodes are kept for each node id (for quick access)
	 */
	private TreeMap<String, TreeItem<TreeViewItem>> nodesForId = new TreeMap<>();
	
	/**
	 * top level UI controlls
	 */
	private Stage stage;
	private Stage dialog;
	private Stage debugDialog;
	private Stage addressBook;
	private XTAAddressBook addressBookPanel;
	private boolean restoreWindowSettings = false;
	
	/*
	 * Proxy configuration
	 */
	private String httpProxyServer;
	private String httpProxyPort;
	private String httpNonProxyHosts;
	private String httpsProxyServer;
	private String httpsProxyPort;
	private String httpsNonProxyHosts;
	private XTAProxySettings proxySettings;
	
	/**
	 * Main panel of the UI
	 */
	private BorderPane mainPanel;
	
	/**
	 * Definition of menu components
	 */
	private MenuBar mainMenu;
	private Menu fileMenu;
	private MenuItem createCopyMenu;
	private MenuItem newMenuItem;
	private MenuItem exitMenuItem;
	
	private Menu editMenu;
	private MenuItem editXMLItem;
	private MenuItem editTextItem;
	
	private Menu toolsMenu;
	private MenuItem createInstancesItem;
	private MenuItem receiveMessagesItem;
	private MenuItem proxyItem;
	private MenuItem addressBookItem;
	
	private Menu helpMenu;
	private MenuItem aboutItem;
	private MenuItem debugItem;
	
	/**
	 * Main ribbon containing controlls for adding / removing resources
	 */
	private HBox ribbon;
	private Button newFolder;
	private Button newXTAAccount;
	private Button newRESTAccount;
	private Button removeButton;
	
	/**
	 * Left side UI elements (tree view components of resources)
	 */
	private BorderPane leftPane;
	private TreeView<TreeViewItem> resourceTree;
	private TreeItem<TreeViewItem> resourceRoot;
	private TreeItem<TreeViewItem> allFolderResources;
	private TreeItem<TreeViewItem> allPostboxResources;
	private TreeItem<TreeViewItem> allRESTResources;
	
	/**
	 * UI controlls for the file workbench (center area of the main window). It contains controlls for
	 * editing and displaying XML files, schemas or codelists
	 */
	private BorderPane workBench;
	private AnchorPane workBenchTitle;
	private HBox workBenchCommonActions;
	private HBox workBenchSendActions;
	private HBox workBenchXMLActions;
	private HBox workBenchEditActions;
	private HBox workBenchRepositoryActions;
	private Button workBenchOpenAction;
	private Button workBenchEmptyAction;
	private Button workBenchOpenXMLAction;
	private Button workBenchSendAction;
	private Button workBenchEditAction;
	private Button workBenchEditFreeAction;
	private Button workBenchAnalyseFreeAction;
	private Button workBenchStopEditAction;
	private Button workBenchSaveAction;
	private Button workBenchSaveAsAction;
	private Button workBenchCancelAction;
	private Button workBenchDeleteRepoEntryAction;
	private HBox workBenchElementActions;
	private Button workBenchInstanceAction;
	private Label workBenchTitleLabel;
	
	/**
	 * Status bar elements
	 */
	private AnchorPane statusBar;
	private Label statusLabel;
	private ProgressBar progress;
	
	/**
	 * Panel for displaying / editing codelists
	 */
	private BorderPane codelistPane;
	private TableView<List<StringProperty>> codelistTable;
	
	/**
	 * Panel for displaying / editing xml files
	 */
	private VirtualizedScrollPane<CodeArea> xmlPaneCarrier;
	private BorderPane xmlPane;
	private VBox xmlStructure;
	private ScrollPane xmlScroller;
//	private TextArea xmlPlaintext;
	private CodeArea xmlPlaintext;
	private VBox xmlInformation;
	private VBox xmlWarnings;
	private ScrollPane xmlWarningsScroller;
	private Label xmlAttachments;
	
	/**
	 * Panel for displaying / editing simple types
	 */
	private BorderPane elementPane;
	private VBox elementStructure;
	private ScrollPane elementScroller;

	/**
	 * Different images that occur on the UI
	 */
	private Image appImage;
	private Image folderImage;
	private Image folderResourceImage;
	private Image furtherOptionsImage;
	private Image folderResourcesImage;
	private Image xtaAccountImage;
	private Image fitkoAccountImage;
	private Image xtaAccountsImage;
	private Image fitkoAccountsImage;
	private Image plusImage;
	private Image minusImage;
	private Image removeImage;
	private Image codelistImage;
	private Image complextypeImage;
	private Image simpletypeImage;
	private Image elementImage;
	private Image fileImage;
	private Image xmlImage;
	private Image xmlReceivedImage;
	private Image xmlSentImage;
	private Image xmlWarnImage;
	private Image xmlUnvalidatedImage;
	private Image xmlFatalImage;
	private Image xsdImage;
	private Image gcImage;
	
	private ImageView plusImageView;
	private ImageView minusImageView;
	
	/**
	 * Reference to last used NodeValueEditors are kept in order to make sure they get
	 * closed when another one gets opened
	 */
	private NodeValueEditor lastEditor = null;
	private NodeValueEditor lastBuffer = null;
	
	/**
	 * Keeps open (visible) branches in XML files to make sure they get reopened after 
	 * storing or reopening in edit mode
	 */
	private HashSet<String> openBranches = new HashSet<>();
	
	/**
	 * ID of the last clicked tree item
	 */
	private String lastClickedID = null;
	
	/**
	 * A backup clone is created whenever a file is opened for editing, so the contents 
	 * can potentially be resored
	 */
	private XTAXMLFileItem backup = null;
	
	/**
	 * Property editors for general behavior and different types of folder resources
	 */
	private ResourceProperties generalProperties;
	private ResourceProperties folderResourceProperties;
	private ResourceProperties xtaResourceProperties;
	private ResourceProperties restResourceProperties;
	
	/**
	 * Controls for specifying sender, receiver, attachments etc. when sending xml files
	 */
	private BorderPane xtaChoiceMainPanel;
	private VBox xtaSenderAccountChoice;
	private Label xtaSenderAccountTitle;
	private ComboBox<String> xtaSenderAccountComboBox;
	private VBox xtaReceiverAccountChoice;
	private Label xtaReceiverAccountTitle;
	private ComboBox<String> xtaReceiverAccountComboBox;
	private Button showAddressBook;
	private BorderPane xtaAttachmentsCarrier;
	private Label xtaAttachmentsTitle;
	private FlowPane xtaAttachmentsList;
	private Button xtaAttachmentsAdd;
	private VBox xtaCaseChoice;
	private Label xtaCaseChoiceTitle;
	private ComboBox<String> xtaCaseComboBox;
	private HBox xtaChoiceOperations;
	private Button xtaChoiceOKButton;
	private Button xtaChoiceCancelButton;

	/**
	 * Default version string (the actual string will be read from an ini file)
	 */
	private String version = "0.1";
	
	/**
	 * Reference to the currently dragged tree item (in case of a drag event)
	 */
	private TreeViewItem dragged = null;

	/**
	 * Observer task makes sure that status texts will fade out after a while of displaying (5 sec)
	 */
	private Observer observer;
	
	/**
	 * Maximum value for displaying progresses
	 */
	private double progressMax = 0;
	
	/**
	 * When displaying complex structures, elements on this level and deeper levels will be displayed collapsed
	 */
	private int maxInitialDepth = 10;
	
	/**
	 * Reference to a potential open XML file which is just being edited and a flag to indicate unsaved changes
	 */
	private XTAXMLFileItem openXMLFile = null;
	private boolean unsavedChanges = false;
	
	/**
	 * References to the last open context menu and the carrier complonent in order to close
	 * when another context menu gets opened
	 */
	private ContextMenu last = null;
	private VBox lastCarrier = null;
	
	/**
	 * State indicator which indicated that some XML file is just being edited (1) or not (0)
	 */
	private int editMode = 0;
	
	/**
	 * Handle to the currently open editor component of an open XML file
	 */
	private NodeValueEditor currentEditor = null;
	
	public XTAMainPanel() {
		super();
	}
	
	public static void main(String[] args) {
		launch(XTAMainPanel.class);
	}

	/**
	 * This method performes a check of the Internet connectivity using a given url parameter
	 * @param url	the URL to be used for the connectivity check
	 * @return	true ist the check was successful - false otherwise
	 * @throws InterruptedException	Thrown in case the check takes too long (longer than 15 seconds)
	 */
	private boolean checkConnection(String url) throws InterruptedException {
		/*
		 * -Dhttp.proxyHost=192.168.178.63 -Dhttp.proxyPort=8866 -Dhttps.proxyHost=192.168.178.63 -Dhttps.proxyPort=8866 -DsocksProxyHost=192.168.178.63 -DsocksProxyPort=8866
		 */
		System.out.println("checkConnection: " + url);
		boolean ret = true;

		StringBuilder build = new StringBuilder();
		
		Thread chk = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					if (url.toLowerCase().trim().startsWith("https://")) {
						SSLContext sslContext = SSLContext.getInstance("SSL");

						// set up a TrustManager that trusts everything
						sslContext.init(null, new TrustManager[] { new X509TrustManager() {
							public X509Certificate[] getAcceptedIssuers() {
								return null;
							}

							public void checkClientTrusted(X509Certificate[] certs, String authType) {
							}

							public void checkServerTrusted(X509Certificate[] certs, String authType) {
							}
						} }, new SecureRandom());

						HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

						HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
							public boolean verify(String arg0, SSLSession arg1) {
								return true;
							}
						});

						URL resource = new URL(url);
						URLConnection conn = resource.openConnection();
						BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						String line;
						while ((line = reader.readLine()) != null) {
							build.append(line);
						}
					} else {
						URL resource = new URL(url);
						URLConnection conn = resource.openConnection();
						BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						String line;
						while ((line = reader.readLine()) != null) {
							build.append(line);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		chk.start();
		chk.join(15000);
		if(chk.isAlive()) {
			System.out.println("Website-Check wird beendet...");
			chk.interrupt();
			ret = false;
		}
		else if(build.length() == 0){
			System.out.println("Nichts gelesen");
			ret = false;
		}
		
		return ret;
	}

	@Override
	public void start(Stage stage) {
		this.stage = stage;
		
	    this.debugDialog = new Stage(StageStyle.DECORATED);
	    this.debugDialog.setTitle("Konsolenausgabe [Java Version " + System.getProperty("java.version") + "]");
	    
	    BorderPane debugMain = new BorderPane();
//	    debugMain.getStylesheets().add(null); //setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
	    debugMain.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
	    
	    TabPane debugPanel = new TabPane();
	    debugPanel.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
	    debugMain.setCenter(debugPanel);
	    
	    TextArea outArea = new TextArea();
	    TextArea errArea = new TextArea();
	    
	    debugPanel.getTabs().add(new Tab("Standard-Ausgaben", outArea));
	    debugPanel.getTabs().add(new Tab("Fehler-Ausgaben", errArea));
	    
	    Scene debugScene = new Scene(debugMain);
//	    debugScene.getStylesheets().add("de/init/xbauleitplanung/xmlxtatool/ui/layoutstyles.css");
	    debugDialog.setScene(debugScene);
	    debugDialog.setWidth(800);
	    debugDialog.setHeight(600);
	    debugDialog.setAlwaysOnTop(true);
	    
	    // program output will be directed into text areas rather than stdout / stderr
	    if(true) {
		    TextAreaStream outStream = new TextAreaStream(outArea);
		    System.setOut(new PrintStream(outStream));
		    
		    TextAreaStream errStream = new TextAreaStream(errArea);
		    System.setErr(new PrintStream(errStream));
	    }
		
	    this.addressBook = new Stage(StageStyle.DECORATED);
	    this.addressBook.setTitle("Adressbuch");
	    
	    this.addressBookPanel = new XTAAddressBook();
	    
	    Scene addreessBookScene = new Scene(addressBookPanel);
	    addressBook.setScene(addreessBookScene);
	    addressBook.setWidth(800);
	    addressBook.setHeight(600);
	    addressBook.setAlwaysOnTop(true);
	    
	    addressBookPanel.setCarrierScene(addreessBookScene);
	    addressBookPanel.setCarrierStage(addressBook);
	    addressBookPanel.setCarrier(this);

	    System.out.println("java.version:\t" + System.getProperty("java.version"));
	    System.out.println("java.home:\t" + System.getProperty("java.home"));
	    System.out.println("java.class.path:\t" + System.getProperty("java.class.path"));
	    System.out.println("java.vendor:\t" + System.getProperty("java.vendor"));
	    System.out.println("java.vendor.url:\t" + System.getProperty("java.vendor.url"));
	    System.out.println("os.arch:\t" + System.getProperty("os.arch"));
	    System.out.println("os.name:\t" + System.getProperty("os.name"));
	    System.out.println("os.version:\t" + System.getProperty("os.version"));
	    System.out.println("user.dir:\t" + System.getProperty("user.dir"));
	    System.out.println("user.home:\t" + System.getProperty("user.home"));
	    System.out.println("user.name:\t" + System.getProperty("user.name"));
		
		mainPanel = new BorderPane();

		this.folderImage = this.getImage("folder_16.png");
		
//		this.appImage = this.getImage("XML-XTA-Tool_t_64.png");
		this.appImage = this.getImage("XLeitstelle-Icon.png");
		
		this.plusImage = this.getImage("plus3_16.png");
		this.plusImageView = new ImageView(this.plusImage);
		this.plusImageView.setFitWidth(12);
		this.plusImageView.setFitHeight(12);
		
		this.minusImage = this.getImage("minus3_16.png");
		this.minusImageView = new ImageView(this.minusImage);
		this.minusImageView.setFitWidth(12);
		this.minusImageView.setFitHeight(12);
		
		this.removeImage = this.getImage("minus_16.png");
		this.codelistImage = this.getImage("codelist_16.png");
		this.complextypeImage = this.getImage("ComplexType_16.png");
		this.simpletypeImage = this.getImage("SimpleType_16.png");
		this.elementImage = this.getImage("XElement_16.png");
		this.fileImage = this.getImage("file_16.png");
		this.xmlImage = this.getImage("xml_32.png");
		this.xmlReceivedImage = this.getImage("xml_received_16.png");
		this.xmlSentImage = this.getImage("xml_sent_16.png");
		this.xmlUnvalidatedImage = this.getImage("xml_unvalidated_16.png");
		this.xmlWarnImage = this.getImage("xml_warn_32.png");
		this.xmlFatalImage = this.getImage("xml_fatal_16.png");
		this.xsdImage = this.getImage("xsd_16.png");
		this.gcImage = this.getImage("gc_16.png");
		
		this.folderResourcesImage = this.getImage("folder_resources_16.png");
		this.folderResourceImage = this.getImage("folder_resource_16.png");
		this.xtaAccountsImage = this.getImage("xtaaccounts_16.png");
		this.xtaAccountImage = this.getImage("xtaaccount_16.png");
		this.fitkoAccountsImage = this.getImage("fitkoaccounts_16.png");
		this.fitkoAccountImage = this.getImage("fitkoaccount_16.png");
		
		this.furtherOptionsImage = this.getImage("furtherOptions_16.png");
		
		BorderPane topHelp = new BorderPane();
		mainPanel.setTop(topHelp);
		
		mainMenu = new MenuBar();
		topHelp.setTop(mainMenu);
		
		fileMenu = new Menu("Datei");
		mainMenu.getMenus().add(fileMenu);
		
		newMenuItem = new MenuItem("Neuer Ordner...");
		newMenuItem.setOnAction(this);
		fileMenu.getItems().add(newMenuItem);
		
		createCopyMenu = new MenuItem("Kopie erstellen...");
		createCopyMenu.setOnAction(this);
		fileMenu.getItems().add(createCopyMenu);
		
		fileMenu.getItems().add(new SeparatorMenuItem());
		
		exitMenuItem = new MenuItem("Beenden");
		exitMenuItem.setOnAction(this);
		fileMenu.getItems().add(exitMenuItem);
		
		editMenu = new Menu("Bearbeiten");
		mainMenu.getMenus().add(editMenu);
		
		editXMLItem = new MenuItem("XML bearbeiten");
		editXMLItem.setOnAction(this);
		editMenu.getItems().add(editXMLItem);
		
		editTextItem = new MenuItem("Text bearbeiten");
		editTextItem.setOnAction(this);
		editMenu.getItems().add(editTextItem);
		
		toolsMenu = new Menu("Extras");
		mainMenu.getMenus().add(toolsMenu);
		
		createInstancesItem = new MenuItem("Instanzen erzeugen...");
		createInstancesItem.setOnAction(this);
		toolsMenu.getItems().add(createInstancesItem);
		
		receiveMessagesItem = new MenuItem("Nachrichten abrufen");
		receiveMessagesItem.setOnAction(this);
		toolsMenu.getItems().add(receiveMessagesItem);
		
		proxyItem = new MenuItem("Proxy-Konfiguration...");
		proxyItem.setOnAction(this);
		toolsMenu.getItems().add(proxyItem);
		
		addressBookItem = new MenuItem("Adressbuch anzeigen...");
		addressBookItem.setOnAction(this);
		toolsMenu.getItems().add(addressBookItem);
		
		helpMenu = new Menu("Hilfe");
		mainMenu.getMenus().add(helpMenu);
		
		aboutItem = new MenuItem("Über XML Nachrichten Tool...");
		aboutItem.setOnAction(this);
		helpMenu.getItems().add(aboutItem);
		
		debugItem = new MenuItem("Zeige Programm-Output...");
		debugItem.setOnAction(this);
		helpMenu.getItems().add(debugItem);
		
		ribbon = new HBox(5.0);
		ribbon.setPadding(new Insets(5.0));
		ribbon.setId("xta-ribbon");
		ribbon.setPrefWidth(20000);
		topHelp.setCenter(ribbon);
		
		newFolder = new Button("Neu Ordner...");
		newFolder.setId("xta-newfolder");
		newFolder.wrapTextProperty().setValue(true);
		newFolder.getStyleClass().add("xta-button");
		newFolder.setOnAction(this);
		ribbon.getChildren().add(newFolder);
		
		newXTAAccount = new Button("Neues XTA-Konto...");
		newXTAAccount.setId("xta-newxtaacount");
		newXTAAccount.wrapTextProperty().setValue(true);
		newXTAAccount.getStyleClass().add("xta-button");
		newXTAAccount.setOnAction(this);
		ribbon.getChildren().add(newXTAAccount);
		
		newRESTAccount = new Button("Neues FIT-Connect-Konto...");
		newRESTAccount.setId("xta-restaccount");
		newRESTAccount.wrapTextProperty().setValue(true);
		newRESTAccount.getStyleClass().add("xta-button");
		newRESTAccount.setOnAction(this);
		ribbon.getChildren().add(newRESTAccount);
		
		removeButton = new Button("Quelle entfernen...");
		removeButton.setId("xta-remove");
		removeButton.getStyleClass().add("xta-button");
		removeButton.setOnAction(this);
		ribbon.getChildren().add(removeButton);
		
		SplitPane split = new SplitPane();
		mainPanel.setCenter(split);
		
		leftPane = new BorderPane();
		split.getItems().add(leftPane);
		
		resourceRoot = new TreeItem<>(TreeViewItem.createByName("Alle Quellen"), new ImageView(this.folderImage));
		
		allFolderResources = new TreeItem<TreeViewItem>(TreeViewItem.createByName("Ordner"), new ImageView(this.folderResourcesImage));
		resourceRoot.getChildren().add(allFolderResources);
		
		allPostboxResources = new TreeItem<TreeViewItem>(TreeViewItem.createByName("XTA-Postfaecher"), new ImageView(this.xtaAccountsImage));
		resourceRoot.getChildren().add(allPostboxResources);
		
		allRESTResources = new TreeItem<TreeViewItem>(TreeViewItem.createByName("FIT-Connect-Konten"), new ImageView(this.fitkoAccountsImage));
		resourceRoot.getChildren().add(allRESTResources);
		
		resourceRoot.setExpanded(true);
		
		resourceTree = new AutoscrollTreeView<>(resourceRoot);
		
		EventHandler<MouseEvent> mouseEventHandle = (MouseEvent event) -> {
		    handleMouseClicked(event);
		};
		resourceTree.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventHandle);
		
		EventHandler<KeyEvent> keyEventHandle = (KeyEvent event) -> {
		    handleKeyClicked(event);
		};
		
		resourceTree.setEditable(true);
		resourceTree.addEventHandler(KeyEvent.KEY_RELEASED, keyEventHandle);
		
		// DnD handler for DnD events
		resourceTree.setCellFactory(new Callback<TreeView<TreeViewItem>, TreeCell<TreeViewItem>>() {
			
			@Override
			public TreeCell<TreeViewItem> call(TreeView<TreeViewItem> param) {
			    
		        TextFieldTreeCellImpl treeCell = new TextFieldTreeCellImpl();
		        
		        treeCell.setOnDragDetected(new EventHandler<MouseEvent>() {
				    @Override
				    public void handle(MouseEvent mouseEvent) {
				    	Dragboard db = treeCell.startDragAndDrop(TransferMode.ANY);
				    	dragged = treeCell.getItem();
				    	
				    	ClipboardContent c = new ClipboardContent();
				    	c.putString("TestTest");
				    	db.setContent(c);
				    	
				    	mouseEvent.consume();
				    }
				});

		        treeCell.setOnDragOver(new EventHandler<DragEvent>() {
		            public void handle(DragEvent event) {
		                /* data is dragged over the target */
		                /* accept it only if it is not dragged from the same node 
		                 * and if it has a string data */
		                if (event.getGestureSource() != treeCell &&
		                        event.getDragboard().hasString()) {
		                    /* allow for both copying and moving, whatever user chooses */
		                    event.acceptTransferModes(TransferMode.ANY);
		                }
		                
		                event.consume();
		            }
		        });
		        
		        treeCell.setOnDragDone(new EventHandler<Event>() {

					@Override
					public void handle(Event event) {
						event.consume();
					}
				});
		        
		        treeCell.setOnDragDropped(new EventHandler<DragEvent>() {

					@Override
					public void handle(DragEvent event) {
						boolean move = false;
						if(event.getAcceptedTransferMode().equals(TransferMode.MOVE)) {
							move = true;
						}
						
						TreeViewItem sourceItem = dragged;
						XTATreeItemI source = sourceItem.getUserObject();
						
						TreeViewItem targetItem = treeCell.getItem();
						XTATreeItemI target = targetItem.getUserObject();
						
						boolean analyze = false;
						if(source instanceof XTAXMLFileItem) {
							if(((XTAXMLFileItem) source).getPath().toLowerCase().endsWith(".xml")) {
								XTAFolderResource root = (XTAFolderResource) getRoot(target);
								
								if(root.validationDesired()) {
									File check = new File(((XTAXMLFileItem) source).getPath());
									
									int des = showConfrmDialog("Neue Datei", "Soll die neue Datei " + check.getName() + " analysiert werden? ");
									analyze = (des == 1);
								}
							}
						}
						
						final boolean mv = move;
						final boolean an = analyze;
						
						Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
								XTAFileItem fle  = host.copyItem(
										source.getItemId(), 
										target.getItemId(), 
										mv, 
										XTAController.ITERATE, 
										null, 
										(XTAFolderResource) getRoot(target), 
										an);
								
								if(fle == null) {
									showMessageDialog("Aktion war nicht erfolgreich", "Aktion war nicht erfolgreich", 0);
								}
							}
						});
					}
				});
                
				return treeCell;
			}
		});
		
		leftPane.setCenter(resourceTree);
		
		workBench = new BorderPane();
		split.getItems().add(workBench);
		
		workBenchTitle = new AnchorPane();
		workBenchTitle.setId("xta-workbenchtitle");
		workBench.setTop(workBenchTitle);
		
		workBenchTitleLabel = new Label("");
		workBenchTitleLabel.setPadding(new Insets(5.0));
		workBenchTitleLabel.setId("xta-workbenchtitlelabel");
		workBenchTitle.getChildren().add(workBenchTitleLabel);
		AnchorPane.setLeftAnchor(workBenchTitleLabel, 0.0);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 220.0);
		
		workBenchCommonActions = new HBox(5.0);
		workBenchCommonActions.setPadding(new Insets(5.0));
		workBenchCommonActions.setId("xta-workbenchcommonactions");
		workBenchTitle.getChildren().add(workBenchCommonActions);
		AnchorPane.setRightAnchor(workBenchCommonActions, 0.0);
		
		workBenchSendActions = new HBox(5.0);
		workBenchSendActions.setPadding(new Insets(5.0));
		workBenchSendActions.setId("xta-workbenchsendactions");
		workBenchTitle.getChildren().add(workBenchSendActions);
		workBenchSendActions.setVisible(false);
		workBenchSendActions.setManaged(false);
		AnchorPane.setBottomAnchor(workBenchSendActions, 0.0);
		
		workBenchOpenAction = new Button("Öffnen");
		workBenchOpenAction.setTooltip(new Tooltip("Öffnen mit Systemanwendung"));
		workBenchOpenAction.setId("xta-workbenchopenaction");
		workBenchOpenAction.wrapTextProperty().setValue(true);
		workBenchOpenAction.getStyleClass().add("xta-button");
		workBenchOpenAction.setOnAction(this);
		workBenchOpenAction.setDisable(true);
		workBenchCommonActions.getChildren().add(workBenchOpenAction);
		
		workBenchEmptyAction = new Button("Leeren...");
		workBenchEmptyAction.setTooltip(new Tooltip("Ordner leeren"));
		workBenchEmptyAction.setId("xta-workbenchemptyaction");
		workBenchEmptyAction.wrapTextProperty().setValue(true);
		workBenchEmptyAction.getStyleClass().add("xta-button");
		workBenchEmptyAction.setOnAction(this);
		workBenchEmptyAction.setDisable(true);
		workBenchCommonActions.getChildren().add(workBenchEmptyAction);
		
		workBenchXMLActions = new HBox(5.0);
		workBenchXMLActions.setPadding(new Insets(5.0));
		workBenchXMLActions.setId("xta-workbencheditactions");
		workBenchXMLActions.setVisible(false);
		workBenchXMLActions.setManaged(false);
		workBenchTitle.getChildren().add(workBenchXMLActions);
		AnchorPane.setRightAnchor(workBenchXMLActions, 0.0);
		
		workBenchOpenXMLAction = new Button("Öffnen");
		workBenchOpenXMLAction.setTooltip(new Tooltip("Öffnen mit Systemanwendung"));
		workBenchOpenXMLAction.setId("xta-workbenchopenxmlaction");
		workBenchOpenXMLAction.wrapTextProperty().setValue(true);
		workBenchOpenXMLAction.getStyleClass().add("xta-button");
		workBenchOpenXMLAction.setOnAction(this);
		workBenchOpenXMLAction.setDisable(true);
		workBenchXMLActions.getChildren().add(workBenchOpenXMLAction);
		
		workBenchSendAction = new Button("Senden...");
		workBenchSendAction.setId("xta-workbenchsendaction");
		workBenchSendAction.wrapTextProperty().setValue(true);
		workBenchSendAction.getStyleClass().add("xta-button");
		workBenchSendAction.setOnAction(this);
		workBenchSendAction.setDisable(true);
		workBenchXMLActions.getChildren().add(workBenchSendAction);
		
		workBenchSaveAsAction = new Button("Kopie erstellen...");
		workBenchSaveAsAction.setId("xta-workbenchsaveasaction");
		workBenchSaveAsAction.wrapTextProperty().setValue(true);
		workBenchSaveAsAction.getStyleClass().add("xta-button");
		workBenchSaveAsAction.setOnAction(this);
		workBenchSaveAsAction.setDisable(true);
		workBenchXMLActions.getChildren().add(workBenchSaveAsAction);
		
		workBenchEditAction = new Button("XML bearbeiten");
		workBenchEditAction.setId("xta-workbencheditaction");
		workBenchEditAction.wrapTextProperty().setValue(true);
		workBenchEditAction.getStyleClass().add("xta-button");
		workBenchEditAction.setOnAction(this);
		workBenchEditAction.setDisable(true);
		workBenchXMLActions.getChildren().add(workBenchEditAction);
		
		workBenchEditFreeAction = new Button("Text bearbeiten");
		workBenchEditFreeAction.setId("xta-workbencheditfreeaction");
		workBenchEditFreeAction.wrapTextProperty().setValue(true);
		workBenchEditFreeAction.getStyleClass().add("xta-button");
		workBenchEditFreeAction.setOnAction(this);
		workBenchEditFreeAction.setDisable(true);
		workBenchXMLActions.getChildren().add(workBenchEditFreeAction);
		
		workBenchAnalyseFreeAction = new Button("Neu analysieren");
		workBenchAnalyseFreeAction.setId("xta-workbenchanalyseaction");
		workBenchAnalyseFreeAction.wrapTextProperty().setValue(true);
		workBenchAnalyseFreeAction.getStyleClass().add("xta-button");
		workBenchAnalyseFreeAction.setOnAction(this);
		workBenchAnalyseFreeAction.setDisable(true);
		workBenchXMLActions.getChildren().add(workBenchAnalyseFreeAction);
		
		workBenchEditActions = new HBox(5.0);
		workBenchEditActions.setPadding(new Insets(5.0));
		workBenchEditActions.setId("xta-workbencheditactions");
		workBenchEditActions.setVisible(false);
		workBenchEditActions.setManaged(false);
		workBenchTitle.getChildren().add(workBenchEditActions);
		AnchorPane.setRightAnchor(workBenchEditActions, 0.0);
		
		workBenchStopEditAction = new Button("Fertig");
		workBenchStopEditAction.setId("xta-workbenchsaveaction");
		workBenchStopEditAction.wrapTextProperty().setValue(true);
		workBenchStopEditAction.getStyleClass().add("xta-button");
		workBenchStopEditAction.setOnAction(this);
		workBenchEditActions.getChildren().add(workBenchStopEditAction);
		
		workBenchSaveAction = new Button("Speichern");
		workBenchSaveAction.setId("xta-workbenchsaveaction");
		workBenchSaveAction.wrapTextProperty().setValue(true);
		workBenchSaveAction.getStyleClass().add("xta-button");
		workBenchSaveAction.setOnAction(this);
		workBenchSaveAction.setDisable(true);
		workBenchEditActions.getChildren().add(workBenchSaveAction);
		
		workBenchCancelAction = new Button("Zurücksetzen");
		workBenchCancelAction.setId("xta-workbenchcancelaction");
		workBenchCancelAction.wrapTextProperty().setValue(true);
		workBenchCancelAction.getStyleClass().add("xta-button");
		workBenchCancelAction.setOnAction(this);
		workBenchCancelAction.setDisable(true);
		workBenchEditActions.getChildren().add(workBenchCancelAction);
		
		workBenchElementActions = new HBox(5.0);
		workBenchElementActions.setPadding(new Insets(5.0));
		workBenchElementActions.setId("xta-workbenchelementactions");
		workBenchElementActions.setMaxWidth(120.0);
		workBenchElementActions.setMinWidth(120.0);
		workBenchTitle.getChildren().add(workBenchElementActions);
		AnchorPane.setRightAnchor(workBenchElementActions, 0.0);
		
		workBenchInstanceAction = new Button("Instanz erzeugen...");
		workBenchInstanceAction.setId("xta-workbenchinstanceaction");
		workBenchInstanceAction.wrapTextProperty().setValue(true);
		workBenchInstanceAction.getStyleClass().add("xta-button");
		workBenchInstanceAction.setOnAction(this);
		workBenchInstanceAction.setDisable(false);
		workBenchElementActions.getChildren().add(workBenchInstanceAction);
		this.workBenchElementActions.setVisible(false);
		this.workBenchElementActions.setManaged(false);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 0.0);
		
		workBenchRepositoryActions = new HBox(5.0);
		workBenchRepositoryActions.setPadding(new Insets(5.0));
		workBenchRepositoryActions.setId("xta-workbencheditactions");
		workBenchRepositoryActions.setVisible(false);
		workBenchRepositoryActions.setManaged(false);
		workBenchTitle.getChildren().add(workBenchRepositoryActions);
		AnchorPane.setRightAnchor(workBenchRepositoryActions, 0.0);
		AnchorPane.setRightAnchor(workBenchElementActions, 0.0);

		workBenchDeleteRepoEntryAction = new Button("Eintrag löschen");
		workBenchDeleteRepoEntryAction.setId("xta-workbenchdeleterepoaction");
		workBenchDeleteRepoEntryAction.wrapTextProperty().setValue(true);
		workBenchDeleteRepoEntryAction.getStyleClass().add("xta-button");
		workBenchDeleteRepoEntryAction.setOnAction(this);
		workBenchRepositoryActions.getChildren().add(workBenchDeleteRepoEntryAction);

		statusBar = new AnchorPane();
		statusBar.setId("xta-statusbar");
		mainPanel.setBottom(statusBar);
		
		statusLabel = new Label("Status");
		statusLabel.setId("xta-statuslabel");
		statusBar.getChildren().add(statusLabel);
		
		progress = new ProgressBar();
		progress.setProgress(0); // ProgressBar.INDETERMINATE_PROGRESS
		statusBar.getChildren().add(progress);
		
		AnchorPane.setRightAnchor(progress, 2d);
		AnchorPane.setTopAnchor(progress, 2d);
		AnchorPane.setBottomAnchor(progress, 2d);
		AnchorPane.setLeftAnchor(statusLabel, 10d);
		AnchorPane.setRightAnchor(statusLabel, 2d);
		AnchorPane.setTopAnchor(statusLabel, 2d);
		
		codelistPane = new BorderPane();
		codelistPane.setId("xta-codelistpane");
		codelistPane.setVisible(false);
		
		codelistTable = new TableView<List<StringProperty>>();
		codelistPane.setCenter(codelistTable);
		
		xmlPane = new BorderPane();
		xmlPane.setId("xta-xmlpane");
		xmlPane.setVisible(false);
		
		xmlStructure = new VBox();
		xmlStructure.getStyleClass().add("xta-xml-structure");
		xmlScroller = new ScrollPane(this.xmlStructure);
	    
		xmlPlaintext = new CodeArea();
//		xmlPlaintext.setLineHighlighterOn(true);
        
		// let xmlPlaintext show line numbers
		IntFunction<Node> numberFactory = LineNumberFactory.get(xmlPlaintext);
		
		// let xmlPlaintext show breakpoints if necessary
        IntFunction<Node> arrowFactory = new MultiBreakPointFactory(listValue);
        IntFunction<Node> graphicFactory = line -> {
            HBox hbox = new HBox(
                    numberFactory.apply(line),
                    arrowFactory.apply(line));
            hbox.setAlignment(Pos.CENTER_LEFT);

            return hbox;
        };
        xmlPlaintext.setParagraphGraphicFactory(graphicFactory);
        xmlPlaintext.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				xmlPlaintext.setLineHighlighterOn(false);
			}
		});
        xmlPlaintext.setOnKeyTyped(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				xmlPlaintext.setLineHighlighterOn(false);
			}
		});
        xmlPlaintext.setOnKeyPressed(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				KeyEvent ke = (KeyEvent) arg0;
				if(ke.isControlDown() && ke.getCode() == KeyCode.S) {
					try {
						TreeItem<TreeViewItem> selected = (TreeItem<TreeViewItem>) resourceTree.getSelectionModel().getSelectedItem();
						handleSaveXML(selected);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
        
		xmlPlaintext.textProperty().addListener((obs, oldText, newText) -> {
			xmlPlaintext.setStyleSpans(0, computeHighlighting(newText));
        });

		xmlPaneCarrier = new VirtualizedScrollPane<CodeArea>(xmlPlaintext);
		
		AnchorPane xmlPaneCarrierHost = new AnchorPane();
		xmlPaneCarrierHost.getChildren().add(xmlPaneCarrier);
		AnchorPane.setLeftAnchor(xmlPaneCarrier, 0.0);
		AnchorPane.setRightAnchor(xmlPaneCarrier, 0.0);
		AnchorPane.setBottomAnchor(xmlPaneCarrier, 0.0);
		AnchorPane.setTopAnchor(xmlPaneCarrier, 0.0);
		
	    xmlPlaintext.prefWidthProperty().bind(xmlPaneCarrierHost.widthProperty());
		xmlPlaintext.prefHeightProperty().bind(xmlPaneCarrierHost.heightProperty());
		
	    xmlPane.setCenter(this.xmlPaneCarrier);
		
		xmlInformation = new VBox();
		xmlPane.setBottom(xmlInformation);
		
		xmlWarnings = new VBox();
		xmlWarningsScroller = new ScrollPane(xmlWarnings);
		xmlWarningsScroller.setMinHeight(50);
		xmlWarningsScroller.setMaxHeight(150);
		xmlInformation.getChildren().add(xmlWarningsScroller);
		
		xmlAttachments = new Label();
		xmlAttachments.setVisible(false);
		xmlAttachments.setStyle("margin: 5");
		xmlAttachments.setWrapText(true);
		xmlInformation.getChildren().add(xmlAttachments);
 
		elementPane = new BorderPane();
		elementPane.setId("xta-xmlpane");
		elementPane.setVisible(false);
		
		elementStructure = new VBox();
		elementStructure.getStyleClass().add("xta-xml-structure");
		elementScroller = new ScrollPane(this.elementStructure);
		
		ArrayList<Object> booleanOptions = new ArrayList<Object>();
		booleanOptions.add("0");
		booleanOptions.add("1");
		booleanOptions.add("2");
		
		ArrayList<String> booleanOptionsNames = new ArrayList<String>();
		booleanOptionsNames.add("Unbestimmt");
		booleanOptionsNames.add("false");
		booleanOptionsNames.add("true");
		
		ArrayList<String> optionalElementPolicies = new ArrayList<String>();
		optionalElementPolicies.add("Optionale Element erzeugen");
		optionalElementPolicies.add("Optionale Element nicht erzeugen");
		optionalElementPolicies.add("Zufällig bestimmen");
		
		ArrayList<String> optionalAttributePolicies = new ArrayList<String>();
		optionalAttributePolicies.add("Optionale Attribute erzeugen");
		optionalAttributePolicies.add("Optionale Attribute nicht erzeugen");
		optionalAttributePolicies.add("Zufällig bestimmen");
		
		ArrayList<String> choicePolicies = new ArrayList<String>();
		choicePolicies.add("Immer das erste Choice-Element erzeugen");
		choicePolicies.add("Zufällig entscheiden");
		choicePolicies.add("Benutzer fragen");
		
		generalProperties = new ResourceProperties(this, this.stage);
		generalProperties.defineProperty("booleanDefault", "Default-Boolean-Wert", ResourceProperties.STRING, booleanOptions, booleanOptionsNames, null);
		generalProperties.defineProperty("optionalElementPolicy", "Erzeugung optionaler Elemente", ResourceProperties.STRING, booleanOptions, optionalElementPolicies, null);
		generalProperties.defineProperty("optionalAttributePolicy", "Erzeugung optionaler Attribute", ResourceProperties.STRING, booleanOptions, optionalAttributePolicies, null);
		generalProperties.defineProperty("askUserForChoiceOption", "Erzeugung von Choice-Elementen", ResourceProperties.STRING, booleanOptions, choicePolicies, null);
		generalProperties.defineProperty("optionalAnyPolicy", "Erzeugung von Any-Elementen", ResourceProperties.STRING, booleanOptions, optionalElementPolicies, null);
		generalProperties.defineProperty("expandNodesTo", "Knoten expandieren bis", ResourceProperties.INTEGER, null, null, null);
		
		folderResourceProperties = new ResourceProperties(this, this.stage);
		folderResourceProperties.defineProperty("path", "Ordnerpfad", ResourceProperties.FOLDER, null, null, null, false);
		
		ArrayList<Object> optionsValues = new ArrayList<>();
		optionsValues.add("0");
		optionsValues.add("1");
		optionsValues.add("2");
		
		ArrayList<String> optionsNames = new ArrayList<>();
		optionsNames.add("Offline");
		optionsNames.add("Online");
		optionsNames.add("Online (cached)");
		
		ArrayList<String> optionsDescriptions = new ArrayList<>();
		optionsDescriptions.add("Offline");
		optionsDescriptions.add("Online");
		optionsDescriptions.add("Online (cached)");
		
		folderResourceProperties.defineProperty("processMode", "Verarbeitungs-Modus", ResourceProperties.STRING, optionsValues, optionsNames, optionsDescriptions);
		
		xtaResourceProperties = new ResourceProperties(this, this.stage);
		xtaResourceProperties.defineProperty("path", "Postfach-Verzeichnis", ResourceProperties.FOLDER, null, null, null, false);
		
		xtaResourceProperties.defineProperty("managementServiceUrl", "Management-Service-URL", ResourceProperties.STRING, null, null, null);
		xtaResourceProperties.defineProperty("sendServiceUrl", "Send-Service.URL", ResourceProperties.STRING, null, null, null);
		xtaResourceProperties.defineProperty("msgBoxServiceUrl", "Messagebox-Service-URL", ResourceProperties.STRING, null, null, null);
		
		xtaResourceProperties.defineProperty("identifierA1", "Kennung A1", ResourceProperties.STRING, null, null, null);
		xtaResourceProperties.defineProperty("clientCertKeystoreA1", "A1-Keystore", ResourceProperties.FILE, null, null, null);
		xtaResourceProperties.defineProperty("clientCertKeystoreA1Password", "A1-Keystore-Kennwort", ResourceProperties.PASSWORD, null, null, null);

		xtaResourceProperties.defineProperty("trustCertKeystore", "Truststore", ResourceProperties.FILE, null, null, null);
		xtaResourceProperties.defineProperty("trustCertKeystorePassword", "Truststore-Kennwort", ResourceProperties.PASSWORD, null, null, null);
		
		xtaResourceProperties.defineProperty("service", "Dienst-ID", ResourceProperties.STRING, null, null, null);
		xtaResourceProperties.defineProperty("businessScenario", "Business Szenario", ResourceProperties.STRING, null, null, null);
		
		restResourceProperties = new ResourceProperties(this, this.stage);
		restResourceProperties.defineProperty("path", "Postfach-Verzeichnis", ResourceProperties.FOLDER, null, null, null, false);
		
		restResourceProperties.defineProperty("subscriberID", "Subscriber-ID", ResourceProperties.STRING, null, null, null);
		restResourceProperties.defineProperty("subscriberSecret", "Subscriber-Secret", ResourceProperties.STRING, null, null, null);
		restResourceProperties.defineProperty("subscriberDestination", "Subscriber-Destination-ID", ResourceProperties.STRING, null, null, null);
		restResourceProperties.defineProperty("privateKeyFile", "Private Key Decryption (Pfad)", ResourceProperties.FILE, null, null, null);
		restResourceProperties.defineProperty("privateSignFile", "Private Key Signing (Pfad)", ResourceProperties.FILE, null, null, null);
		restResourceProperties.defineProperty("publicSignFile", "Public Key Ecryption (Pfad)", ResourceProperties.FILE, null, null, null);
		
		restResourceProperties.defineProperty("senderID", "FIT-Connect Sender-ID", ResourceProperties.STRING, null, null, null);
		restResourceProperties.defineProperty("senderSecret", "FIT-Connect Sender-Secret", ResourceProperties.STRING, null, null, null);
		restResourceProperties.defineProperty("service", "Dienst-URN", ResourceProperties.STRING, null, null, null);
		
		ArrayList<Object> choiceValues = new ArrayList<Object>();
		choiceValues.add("0");
		choiceValues.add("1");
		
		ArrayList<String> choiceNames = new ArrayList<String>();
		choiceNames.add("Wie Mail-Postfach");
		choiceNames.add("Fallbasiert");
		
		ArrayList<String> choiceDescriptions = new ArrayList<String>();
		choiceDescriptions.add("Nachrichten werden in Posteingang, Postausgang, Gesendete usw. sortiert");
		choiceDescriptions.add("Nachrichten werden fallbasiert angeordnet und nach Eingangs- bzw. Sendedatum sortiert");
		
		restResourceProperties.defineProperty("folder", "Sortierung der Nachrichten", ResourceProperties.STRING, choiceValues, choiceNames, choiceDescriptions);
		
		xtaChoiceMainPanel = new BorderPane();
		xtaChoiceMainPanel.setId("xtaChoiceMainPanel");
		this.workBenchSendActions.getChildren().add(xtaChoiceMainPanel);
		
		Label accountTitle = new Label("Bitte wählen Sie Sender und Empfaenger aus");
		xtaChoiceMainPanel.setTop(accountTitle);

		HBox accountHelp = new HBox();
		xtaChoiceMainPanel.setCenter(accountHelp);
		
		xtaCaseChoice = new VBox();
		xtaCaseChoice.getStyleClass().add("xta-spacepanel");
		accountHelp.getChildren().add(xtaCaseChoice);
		
		xtaCaseChoiceTitle = new Label("Fall-Auswahl");
		xtaCaseChoice.getChildren().add(xtaCaseChoiceTitle);
		
		xtaCaseComboBox = new ComboBox<>();
		xtaCaseComboBox.setMinWidth(200);
		xtaCaseComboBox.setMaxWidth(250);
		xtaCaseChoice.getChildren().add(xtaCaseComboBox);
		
		xtaSenderAccountChoice = new VBox();
		xtaSenderAccountChoice.getStyleClass().add("xta-spacepanel");
		accountHelp.getChildren().add(xtaSenderAccountChoice);
		
		xtaSenderAccountTitle = new Label("Sender");
		xtaSenderAccountChoice.getChildren().add(xtaSenderAccountTitle);
		
		xtaSenderAccountComboBox = new ComboBox<>();
		xtaSenderAccountComboBox.setMinWidth(200);
		xtaSenderAccountComboBox.setMaxWidth(250);
		xtaSenderAccountComboBox.setOnAction(this);
		xtaSenderAccountChoice.getChildren().add(xtaSenderAccountComboBox);
        
		xtaReceiverAccountChoice = new VBox();
		xtaReceiverAccountChoice.getStyleClass().add("xta-spacepanel");
		accountHelp.getChildren().add(xtaReceiverAccountChoice);
		
		xtaReceiverAccountTitle = new Label("Empfaenger");
		xtaReceiverAccountChoice.getChildren().add(xtaReceiverAccountTitle);
		
		xtaReceiverAccountComboBox = new ComboBox<>();
		xtaReceiverAccountComboBox.setEditable(true);
		xtaReceiverAccountComboBox.setMinWidth(200);
		xtaReceiverAccountComboBox.setMaxWidth(250);
		xtaReceiverAccountChoice.getChildren().add(xtaReceiverAccountComboBox);
		
		showAddressBook = new Button("Aus Adressbuch wählen...");
		showAddressBook.setOnAction(this);
		VBox.setMargin(showAddressBook, new Insets(2, 2, 2, 2));
		xtaReceiverAccountChoice.getChildren().add(showAddressBook);
		
		xtaAttachmentsCarrier = new BorderPane();
		accountHelp.getChildren().add(xtaAttachmentsCarrier);
		
		xtaAttachmentsTitle = new Label("Anhänge");
		xtaAttachmentsCarrier.setTop(xtaAttachmentsTitle);
        
		xtaAttachmentsList = new FlowPane();
		xtaAttachmentsList.getStyleClass().add("xta-spacepanel");
		xtaAttachmentsList.setMinWidth(200);
		xtaAttachmentsList.setMaxWidth(250);
		xtaAttachmentsList.setBorder(new Border(new BorderStroke(Color.BLACK, 
	            BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		xtaAttachmentsCarrier.setCenter(xtaAttachmentsList);
		
		xtaAttachmentsAdd = new Button("+");
		xtaAttachmentsAdd.setOnAction(this);
		xtaAttachmentsCarrier.setRight(xtaAttachmentsAdd);
		
		xtaChoiceOperations = new HBox();
		xtaChoiceOperations.getStyleClass().add("xta-spacepanel");
		xtaChoiceMainPanel.setBottom(xtaChoiceOperations);
		
		xtaChoiceOKButton = new Button("Senden");
		xtaChoiceOKButton.setId("xta-choiceok");
		xtaChoiceOKButton.wrapTextProperty().setValue(true);
		xtaChoiceOKButton.getStyleClass().add("xta-button");
		xtaChoiceOKButton.setOnAction(this);
		xtaChoiceOperations.getChildren().add(xtaChoiceOKButton);
		
		xtaChoiceCancelButton = new Button("Abbrechen");
		xtaChoiceCancelButton.setId("xta-choicecancel");
		xtaChoiceCancelButton.wrapTextProperty().setValue(true);
		xtaChoiceCancelButton.getStyleClass().add("xta-button");
		xtaChoiceCancelButton.setOnAction(this);
		xtaChoiceOperations.getChildren().add(xtaChoiceCancelButton);
		
		InputStream in = getClass().getResourceAsStream("/resources/info.ini");
		IniFileReader read = new IniFileReader(in);
		String v = read.getValue("version");
		if(v != null && v.length() > 0) {
			this.version = v;
		}
		
		Rectangle2D screenBounds = Screen.getPrimary().getBounds();
		
		double x = 50;
		double y = 50;
		double width = screenBounds.getWidth() - 100;
		double height = screenBounds.getHeight() - 100;
		boolean maximized = false;
		
		if(this.restoreWindowSettings) {
			String source = System.getProperty("user.home") + "/xtaviewer/ui.ini";
			IniFileReader rd= new IniFileReader(source);
			
			try {
				x = rd.getDoubleValue("x");
				y = rd.getDoubleValue("y");
				width = rd.getDoubleValue("width");
				height = rd.getDoubleValue("height");
				maximized = rd.getBooleanValue("maximized");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			rd.close();
		}
		
		Scene scene = new Scene(mainPanel);
		// Add a style sheet to the scene        
		scene.getStylesheets().add("layoutstyles.css");
//		scene.getStylesheets().add("resources/layoutstyles.css");
		stage.setScene(scene);
		stage.setTitle("XML Nachrichten Tool - " + this.version);
		
		stage.setWidth(width);
		stage.setHeight(height);
		stage.setX(x);
		stage.setY(y);
		stage.setMaximized(maximized);
		
		if(this.appImage != null) {
			stage.getIcons().add(this.appImage);
		}
		
		stage.show();
		
		final XTAMainPanel main = this;
		
	    this.dialog = new Stage(StageStyle.DECORATED);
	    this.dialog.setTitle("Über XML Nachrichten Tool");
	    this.dialog.initModality(Modality.WINDOW_MODAL);
	    this.dialog.initOwner(this.stage);
	    this.dialog.setResizable(false);
	    this.dialog.setWidth(540);
	    this.dialog.setHeight(250);
		this.dialog.getIcons().add(this.appImage);
		
		this.stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				try {
					stop();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		Image xbaul = this.getImage("Logo4.png");
		
	    VBox dialogPanel= new VBox();
	    dialogPanel.getStyleClass().add("xta-button");
	    
		Label dialogTitle = new Label();
		dialogTitle.setGraphic(new ImageView(xbaul));
	    dialogTitle.getStyleClass().add("xta-button");
	    dialogPanel.getChildren().add(dialogTitle);
		
		Label dialogContent = new Label();
		dialogContent.setText("XML-Nachrichten-Tool " + this.version + "\r\n" + 
				"Darstellungskomponente zur Anzeige, Bearbeitung und Erstellung sowie zum Versand von\r\nXÖV-Testnachrichten innerhalb " + 
				"einer Sicherheitsinfrastruktur oder über FIT-Connect.\r\n" + 
				"Lizenz: Creative Commons 4.0 Namensnennung \"]init[ AG fuer BSW Hamburg\"\r\n" + 
				"Kontakt: xleitstelle@gv.hamburg.de");
	    dialogContent.getStyleClass().add("xta-about-label");
//	    dialogContent.setBorder(new Border(new BorderStroke(Color.BLACK, 
//            BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
	    dialogPanel.getChildren().add(dialogContent);
	    
	    Scene dialogScene = new Scene(dialogPanel);
		dialogScene.getStylesheets().add("layoutstyles.css");
	    dialog.setScene(dialogScene);
	    
	    // setup proxy configuration dialog
	    this.proxySettings = new XTAProxySettings(this);
	    this.readProxyConfiguration();
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				setStatusText("Lade Daten...");
				setProgressVisible(true);
				setProgress(ProgressBar.INDETERMINATE_PROGRESS);
				leftPane.setCursor(Cursor.WAIT);
				resourceTree.setDisable(true);
				
				host = new XTAController(main);
				
				// Load settings...
				generalProperties.setPropertyValue("booleanDefault", String.valueOf(host.getProperty("booleanDefault")));
				generalProperties.setPropertyValue("optionalElementPolicy", String.valueOf(host.getProperty("optionalElementPolicy")));
				generalProperties.setPropertyValue("optionalAttributePolicy", String.valueOf(host.getProperty("optionalAttributePolicy")));
				generalProperties.setPropertyValue("askUserForChoiceOption", String.valueOf(host.getProperty("askUserForChoiceOption")));
				generalProperties.setPropertyValue("optionalAnyPolicy", String.valueOf(host.getProperty("optionalAnyPolicy")));
				generalProperties.setPropertyValue("expandNodesTo", String.valueOf(host.getProperty("expandNodesTo")));
				
				// Lade alle FolderResources
				ArrayList<String> ids = host.getResourceIds(XTAResourceType.FOLDER);
				for(String id : ids) {
					resourceUpdated(id);
				}
				
				// Lade alle XTA-Accounts
				ids = host.getResourceIds(XTAResourceType.XTA_ACCOUNT);
				for(String id : ids) {
					resourceUpdated(id);
				}
				
				// Lade alle XTA-Accounts
				ids = host.getResourceIds(XTAResourceType.REST_ENDPOINT);
				for(String id : ids) {
					resourceUpdated(id);
					XTAPostOfficeBox resource = (XTAPostOfficeBox) host.getResourcesForId(id);
					if(resource.getDestination() != null) {
						addressBookPanel.addEntry(resource.getItemName(), "", "", "", "", resource.getDestination());
					}
				}
				
				leftPane.setCursor(Cursor.DEFAULT);
				resourceTree.setDisable(false);
			}
		});
		t.start();
		
		this.observer = new Observer();
		Thread o = new Thread(this.observer);
		o.start();
		
		split.getDividers().get(0).setPosition(0.3d);
		
		Thread check = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}
		});
		check.start();
		
	    checkConnectivity();
	}

    private static StyleSpans<Collection<String>> computeHighlighting(String text) {
    	
        Matcher matcher = XML_TAG.matcher(text);
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
        while(matcher.find()) {
        	
        	spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
        	if(matcher.group("COMMENT") != null) {
        		spansBuilder.add(Collections.singleton("comment"), matcher.end() - matcher.start());
        	}
        	else {
        		if(matcher.group("ELEMENT") != null) {
        			String attributesText = matcher.group(GROUP_ATTRIBUTES_SECTION);
        			
        			spansBuilder.add(Collections.singleton("tagmark"), matcher.end(GROUP_OPEN_BRACKET) - matcher.start(GROUP_OPEN_BRACKET));
        			spansBuilder.add(Collections.singleton("anytag"), matcher.end(GROUP_ELEMENT_NAME) - matcher.end(GROUP_OPEN_BRACKET));

        			if(!attributesText.isEmpty()) {
        				
        				lastKwEnd = 0;
        				
        				Matcher amatcher = ATTRIBUTES.matcher(attributesText);
        				while(amatcher.find()) {
        					spansBuilder.add(Collections.emptyList(), amatcher.start() - lastKwEnd);
        					spansBuilder.add(Collections.singleton("attribute"), amatcher.end(GROUP_ATTRIBUTE_NAME) - amatcher.start(GROUP_ATTRIBUTE_NAME));
        					spansBuilder.add(Collections.singleton("tagmark"), amatcher.end(GROUP_EQUAL_SYMBOL) - amatcher.end(GROUP_ATTRIBUTE_NAME));
        					spansBuilder.add(Collections.singleton("avalue"), amatcher.end(GROUP_ATTRIBUTE_VALUE) - amatcher.end(GROUP_EQUAL_SYMBOL));
        					lastKwEnd = amatcher.end();
        				}
        				if(attributesText.length() > lastKwEnd)
        					spansBuilder.add(Collections.emptyList(), attributesText.length() - lastKwEnd);
        			}

        			lastKwEnd = matcher.end(GROUP_ATTRIBUTES_SECTION);
        			
        			spansBuilder.add(Collections.singleton("tagmark"), matcher.end(GROUP_CLOSE_BRACKET) - lastKwEnd);
        		}
        	}
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }

	private void checkConnectivity() {
		setStatusText("Prüfe Interneverbindung...");
		
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				// Check connectivity - this tool will not be able to load remote resources references in XML files in case 
				// connectivity is broken. The check is very simple: It tries to connect to google which is assumed to be
				// reliably online
				try {
					boolean online = checkConnection("http://www.google.de/");
					if(online) {
						online = checkConnection("https://www.google.de/");
					}
					setStatusText("fertig");
					
					// In case of broken connectivity the user has to decide whether he wants to use this tool
					if(!online) {
						proxySettings.init();
						proxySettings.showDialog();
						
						showMessageDialog("Internetverbindung fehlerhaft", "Es konnte keine Internetverbindung hergestellt werden. Bitte überprüfen Sie die Proxy-Einstellungen", 0);
					}
				} catch (InterruptedException e2) {
					e2.printStackTrace();
				}
			}
		});
	}
	
	public void checkProxyConfiguration() {
		this.readProxyConfiguration();
		this.checkConnectivity();
	}
	
	private void readProxyConfiguration() {
		String source = System.getProperty("user.home") + "/xtaviewer/launch.ini";
		IniFileReader read = new IniFileReader(source);
		this.httpProxyServer = read.getValue("httpProxyServer");
		if(httpProxyServer == null) {
			httpProxyServer = "";
		}
		this.httpProxyPort = read.getValue("httpProxyPort");
		if(httpProxyPort == null) {
			httpProxyPort = "80";
		}
		this.httpNonProxyHosts = read.getValue("httpNonProxyHosts");
		if(httpNonProxyHosts == null) {
			httpNonProxyHosts = "";
		}
		this.httpsProxyServer = read.getValue("httpsProxyServer");
		if(httpsProxyServer == null) {
			httpsProxyServer = "";
		}
		this.httpsProxyPort = read.getValue("httpsProxyPort");
		if(httpsProxyPort == null) {
			httpsProxyPort = "443";
		}
		this.httpsNonProxyHosts = read.getValue("httpsNonProxyHosts");
		if(httpsNonProxyHosts == null) {
			httpsNonProxyHosts = "";
		}
		read.close();
	    
		System.setProperty("file.encoding", "UTF-8");
		System.setProperty("http.proxyHost", this.httpProxyServer);
		System.setProperty("http.proxyPort", this.httpProxyPort);
		System.setProperty("http.nonProxyHosts", this.httpNonProxyHosts);
		System.setProperty("https.proxyHost", this.httpsProxyServer);
		System.setProperty("https.proxyPort", this.httpsProxyPort);
		System.setProperty("https.nonProxyHosts", this.httpsNonProxyHosts);
	}
	
	/**
	 * Read an image file from the jar resource
	 * @param resourcePath	Path to the image file within the jar resource
	 * @return	The image file read
	 */
	private Image getImage(String resourcePath) {
		try {
			InputStream in = getClass().getResourceAsStream("/resources/images/" + resourcePath);
			
			Image image = new Image(in);
			
			return image;
		} catch (Throwable e) {
			e.printStackTrace();
			
			return null;
		}
	}
    
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#setWorkingStatus(boolean)
	 */
	@Override
	public void setWorkingStatus(boolean working) {
		if(working) {
			leftPane.setCursor(Cursor.WAIT);
			resourceTree.setDisable(true);
		}
		else {
			leftPane.setCursor(Cursor.DEFAULT);
			resourceTree.setDisable(false);
		}
	}
	
	/**
	 * Handler and dispatcher for top level mouse events
	 * @param event
	 */
	private void handleMouseClicked(MouseEvent event) {
	    Node node = event.getPickResult().getIntersectedNode();
	    // Accept clicks only on node cells, and not on empty spaces of the TreeView
	    TreeItem<TreeViewItem> selectedItem = (TreeItem<TreeViewItem>) resourceTree.getSelectionModel().getSelectedItem();
	    
	    if(selectedItem != null) {
			if(event.getClickCount() >= 2) {
		        TreeViewItem item = (TreeViewItem) selectedItem.getValue();
		    	XTATreeItemI panel = item.getUserObject();
		    	
		    	if(panel instanceof XTAXMLFileItem) {
		    		try {
						Desktop.getDesktop().open(new File(((XTAXMLFileItem) panel).getPath()));
					} catch (IOException e) {
						e.printStackTrace();
					}
		    	}
		    	else if(panel instanceof XTASchemaFileItem) {
		    		try {
						Desktop.getDesktop().open(new File(((XTASchemaFileItem) panel).getPath()));
					} catch (IOException e) {
						e.printStackTrace();
					}
		    	}
		    }
		    else if(!event.isConsumed()) {
		    	if(selectedItem == this.resourceRoot) {
			        ImageView folder = new ImageView(this.folderImage);
		        	folder = new ImageView(this.folderResourceImage);
					this.workBenchTitleLabel.setGraphic(folder);
					
					this.hideCommonActions();
					this.hideEditActions();
					this.hideElementActions();
					this.hideXMLActions();
					
		    		this.handleAllResourcesClicked();
		    	}
		    	else if (node instanceof TreeCell && ((TreeCell<TreeViewItem>) node).getText() != null) {
			        TreeViewItem item = (TreeViewItem) selectedItem.getValue();
			        handleNodeSelected(item);
			    }
		    	else if (node instanceof Text) {
			        TreeViewItem item = (TreeViewItem) selectedItem.getValue();
			        handleNodeSelected(item);
			    }
		    }
	    }
		event.consume();
	}
	
	/**
	 * Handler and dispatcher for top level key events
	 * @param event
	 */
	private void handleKeyClicked(KeyEvent event) {
		if(event.getSource() == this.resourceTree) {
			
		    // Accept clicks only on node cells, and not on empty spaces of the TreeView
		    TreeItem<TreeViewItem> selectedItem = (TreeItem<TreeViewItem>) resourceTree.getSelectionModel().getSelectedItem();
		    if(selectedItem != null) {
		        TreeViewItem item = (TreeViewItem) selectedItem.getValue();
		        XTATreeItemI tItem = item.getUserObject();
		    	
		    	if(event.getCode().equals(KeyCode.DELETE)) {
		    		if(tItem instanceof XTAFileItem) {
		    			String name = tItem.getItemName();
			    		if(this.showConfrmDialog("Löschen bestätigen", "Soll die Datei " + name + " gelöscht werden?") == 1) {
			    			this.host.deleteItem(tItem.getItemId());
			    		}
		    		}
		    	}
		    	else {
			        handleNodeSelected(item);
		    	}
		    }
		}
	}

	/**
	 * Handler and dispatcher for node clicked events
	 * @param item	The tree item just clicked by the user
	 */
	private void handleNodeSelected(TreeViewItem item) {
		if(!item.getId().equals(this.lastClickedID)) {
			if(lastClickedID != null) {
				TreeItem<TreeViewItem> last = this.nodesForId.get(this.lastClickedID);
				
				if(last != null && !checkForUnsavedChanges((TreeViewItem) last.getValue())) {
					return;
				}
			}
			
			this.lastClickedID = item.getId();
		    
		    this.codelistTable.getColumns().clear();
		    
		    XTATreeItemI element = item.getUserObject();
		    
		    XTAFolderResource root = (XTAFolderResource) getRoot(element);
		    
		    if(element != null) {
		    	// Bedienelemente ein- bzw. ausblenden
		        this.hideElementActions();
		        if(element instanceof XTAFileItem) {
		        	this.showCommonActions();
		        	
		        	this.workBenchCancelAction.setDisable(true);
		        	this.workBenchEditAction.setDisable(true);
		        	this.workBenchEditFreeAction.setDisable(true);
		        	this.workBenchAnalyseFreeAction.setDisable(true);
		        	this.workBenchOpenAction.setDisable(false);
		        	this.workBenchEmptyAction.setDisable(true);
		        	this.workBenchOpenXMLAction.setDisable(false);
		        	this.workBenchSendAction.setDisable(false);
		        	this.workBenchSaveAction.setDisable(true);
		        	this.workBenchSaveAsAction.setDisable(true);
		        }
		        else {
		        	this.hideCommonActions();
		        }
				
		        this.workBenchTitle.setMinHeight(20);
				this.workBenchSendActions.setVisible(false);
				this.workBenchSendActions.setManaged(false);
				
				this.xmlWarningsScroller.setVisible(false);
				this.xmlWarningsScroller.setManaged(false);
				
		        updateTitleIcon(element, root);
		        
				// ausgewaehlte Datei anzeigen
				if(element instanceof XCodelist) {
		    		handleCodelistClicked(item);
		        }
				else if(element instanceof XTAGenericodeList) {
		    		handleCodelistClicked(item);
		        }
		        else if(element instanceof XTASchemaFileItem) {
		        	this.showCommonActions();
		        	
		        	this.workBenchEditAction.setDisable(true);
		        	this.workBenchEditFreeAction.setDisable(true);
		        	
		    		handleSchemaFileClicked(item);
		        }
		        else if(element instanceof XTAXMLFileItem) {
		        	this.showCommonActions();
		        	
		        	this.workBenchEditAction.setDisable(false);
		        	this.workBenchEditFreeAction.setDisable(false);
		        	this.workBenchAnalyseFreeAction.setDisable(false);
		        	
		    		handleXMLFileClicked(item, false, false, true);
		        }
		        else if(element instanceof XTAFITConnectPostOfficeBox) {
		        	this.showCommonActions();
					
		    		handleRESTAccountClicked(item);
		        }
		        else if(element instanceof XTAPostOfficeBox) {
		        	this.showCommonActions();
					
		    		handleXTAAccountClicked(item);
		        }
		        else if(element instanceof XTAFolderResource) {
		        	this.showCommonActions();
					
		    		handleFolderResourceClicked(item);
		        }
		        else if(element instanceof XElement) {
		        	this.showElementActions();
		        	
		        	XTASchemaFileItem schema = (XTASchemaFileItem) element.getParent();
		        	
		        	XTATreeItemI res = getRoot(element);
		        	if(res instanceof XTAFolderResource) {
		        		XModelTreeNode rootNode = ((XTAFolderResource) res).getRootNode();
		        		XModelTreeNode elementnode = rootNode.getChildForName(element.getItemName(), schema.getPath());
		        		handleRootElementClicked(elementnode);
		        	}
		        }
		        else if(element instanceof XComplexType) {
		        	hideElementActions();
		        	hideCommonActions();
		        	
		        	XTATreeItemI res = getRoot(element);
		        	if(res instanceof XTAFolderResource) {
		        		handleComplexTypeClicked((XTAFolderResource) res, element.getItemId());
		        	}
		        }
		        else if(element instanceof XSimpleType) {
		        	hideElementActions();
		        	hideCommonActions();
		        	
		        	XTATreeItemI res = getRoot(element);
		        	if(res instanceof XTAFolderResource) {
		        		handleSimpleTypeClicked((XTAFolderResource) res, element.getItemId());
		        	}
		        }
		        else {
		    		try {
						this.workBenchTitleLabel.setText(((XTATreeItemI) element).getItemName());
						this.workBench.setCenter(null);
					} catch (Exception e) {
					}
		        }
		    }
		}
	}

	/**
	 * Updates the icon displayed on top of the workbench, depending of the type of the element just clicked by the user
	 * @param element	the element just clicked by the user
	 * @param root	The covering folder resource
	 */
	private void updateTitleIcon(XTATreeItemI element, XTAFolderResource root) {
		// richtiges Icon in Titelleiste anzeigen
		ImageView folder = new ImageView(this.folderImage);
		if("XCodelist".equals(element.getItemType())) {
			folder = new ImageView(this.codelistImage);
		}
		else if("XComplexType".equals(element.getItemType())) {
			folder = new ImageView(this.complextypeImage);
		}
		else if("XSimpleType".equals(element.getItemType())) {
			folder = new ImageView(this.simpletypeImage);
		}
		else if("XElement".equals(element.getItemType())) {
			folder = new ImageView(this.elementImage);
		}
		else if("FOLDER_RESOURCE".equals(element.getItemType())) {
			folder = new ImageView(this.folderResourceImage);
		}
		else if("POSTOFFICEBOX_RESOURCE".equals(element.getItemType())) {
			if(element instanceof XTAFITConnectPostOfficeBox) {
				folder = new ImageView(this.fitkoAccountImage);
			}
			else if(element instanceof XTAPostOfficeBox) {
				folder = new ImageView(this.xtaAccountImage);
			}
		}
		else if("XMLFile".equals(element.getItemType())) {
			XTAXMLFileItem chld = (XTAXMLFileItem) element;
			
			if(!root.validationDesired()) {
				folder = new ImageView(this.xmlImage);
			}
			else if(chld.getFatals().size() > 0) {
				folder = new ImageView(this.xmlFatalImage);
			}
			else if(chld.getWarnings().size() > 0) {
				folder = new ImageView(this.xmlWarnImage);
			}
			else if(!chld.isValidated()) {
				folder = new ImageView(this.xmlUnvalidatedImage);
			}
			else {
				folder = new ImageView(this.xmlImage);
			}
		}
		else if("XSD_FILE_ITEM".equals(element.getItemType())) {
			folder = new ImageView(this.xsdImage);
		}
		else if("XTA_GenericodeList".equals(element.getItemType())) {
			folder = new ImageView(this.gcImage);
		}
		else if("FILE_ITEM".equals(element.getItemType())) {
			File check = new File(((XTAFileItem) element).getPath());
			if(!check.isDirectory()) {
				folder = new ImageView(this.fileImage);
			}
		}
		else {
			folder = new ImageView(this.fileImage);
		}
		
		folder.setFitHeight(16);
		folder.setFitWidth(16);
		
		this.workBenchTitleLabel.setGraphic(folder);
	}

	/**
	 * This method askes the user whether he wants to save yet unsaved changes
	 * @param item	The tree item that represents the file item which contains unsaved changes
	 * @return	true, if changes shall be saved, false otherwise
	 */
	private boolean checkForUnsavedChanges(TreeViewItem item) {
		if(unsavedChanges) {
			if(openXMLFile != null) {
				int conf = showConfrmDialog("Ungesicherte Änderungen", "Möchten Sie die Änderungen speichern? ");
		    	
				// Antwort: Abbrechen - kehre zur letzten Auswahl zurück und bearbeite weiter
				if(conf == 0) {
					TreeItem<TreeViewItem> last = this.nodesForId.get(this.lastClickedID);
					int row = this.resourceTree.getRow(last);
					this.resourceTree.getSelectionModel().select(row);
					
					return false;
				}
				// Antwort: Ja - speichern und zur aktuellen Auswahl wechseln
				else if(conf == 1) {
					if(currentEditor != null) {
		        		currentEditor.closeEditor();
		        	}
		        	
		        	XTAXMLFileItem xtaxmlFileItem = openXMLFile;
		        	
		        	boolean plain = false;
					if(this.editMode == 1 || xtaxmlFileItem.getFatals().size() > 0) {
						xtaxmlFileItem.setPlainText(this.xmlPlaintext.getText());
						plain = true;
		        	}
		    		this.host.exportXMLFile(xtaxmlFileItem, plain);
		        	
		        	XTATreeItemI root = this.getRoot(item.getUserObject());
		        	
		        	if(plain) {
			        	this.host.validateAndStructXMLFile(root.getItemId(), item.getId());
		        	}
		        	else {
			        	this.host.validateXMLFile(root.getItemId(), item.getId(), xtaxmlFileItem);
		        	}
					
		        	this.processItem(resourceTree.getSelectionModel().getSelectedItem(), item.getUserObject());
		        	
		        	this.editMode = 0;
				}
				// Antwort: Nein - Nicht speichern (d.h. Backup zurückspielen) und zur aktuellen Auswahl wechseln
				else if(conf == 2) {
					// Write back the origin values from backup node!
					if(this.backup != null) {
						XTAXMLFileItem xml = (XTAXMLFileItem) item.getUserObject();
						XTATreeItemI parent = xml.getParent();
						
						xml.clear();
						xml.clone(this.backup);
						xml.setParent(parent);
					}
				}
			}
			
			unsavedChanges = false;
		}
		
		return true;
	}
	
	/**
	 * Display the default set of controls on top of the workbench
	 */
	private void showCommonActions() {
		this.hideXMLActions();
		this.hideElementActions();
		this.hideEditActions();
		this.hideRepositoryActions();
		
		this.workBenchCommonActions.setVisible(true);
		this.workBenchCommonActions.setManaged(true);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 220.0);
	}
	
	/**
	 * Hide the default set of controls on top of the workbench
	 */
	private void hideCommonActions() {
		this.workBenchCommonActions.setVisible(false);
		this.workBenchCommonActions.setManaged(false);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 0.0);
	}
	
	/**
	 * Display the specific set of XML controls on top of the workbench
	 */
	private void showXMLActions() {
		this.hideCommonActions();
		this.hideElementActions();
		this.hideEditActions();
		this.hideRepositoryActions();
		
		this.workBenchXMLActions.setVisible(true);
		this.workBenchXMLActions.setManaged(true);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 220.0);
	}
	
	/**
	 * Hide the specific set of XML controls on top of the workbench
	 */
	private void hideXMLActions() {
		this.workBenchXMLActions.setVisible(false);
		this.workBenchXMLActions.setManaged(false);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 0.0);
	}
	
	/**
	 * Display the specific set of controls for editing XML on top of the workbench
	 */
	private void showEditActions() {
		this.hideCommonActions();
		this.hideElementActions();
		this.hideXMLActions();
		this.hideRepositoryActions();
		
		this.workBenchEditActions.setVisible(true);
		this.workBenchEditActions.setManaged(true);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 220.0);
	}
	
	/**
	 * Hide the specific set of controls for editing XML on top of the workbench
	 */
	private void hideEditActions() {
		this.workBenchEditActions.setVisible(false);
		this.workBenchEditActions.setManaged(false);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 0.0);
	}
	
	/**
	 * Display the specific set for elements display on top of the workbench
	 */
	private void showElementActions() {
		this.hideXMLActions();
		this.hideCommonActions();
		this.hideRepositoryActions();
		
		this.workBenchElementActions.setVisible(true);
		this.workBenchElementActions.setManaged(true);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 120.0);
	}
	
	/**
	 * Hide the specific set for elements display on top of the workbench
	 */
	private void hideElementActions() {
		this.workBenchElementActions.setVisible(false);
		this.workBenchElementActions.setManaged(false);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 0.0);
	}
	
	/**
	 * Hide the specific set for repository editing on top of the workbench
	 */
	// TODO: Remove if repositories are not fully implemented
	private void hideRepositoryActions() {
		this.workBenchRepositoryActions.setVisible(false);
		this.workBenchRepositoryActions.setManaged(false);
		AnchorPane.setRightAnchor(workBenchTitleLabel, 0.0);
	}

	/**
	 * Called when a user chooses the all resource tree item
	 */
	private void handleAllResourcesClicked() {
		this.lastClickedID = "";
		
		this.workBenchTitleLabel.setText("Allgemeine Einstellungen");
		this.workBench.setCenter(this.generalProperties);
		this.generalProperties.setPropertyValue("booleanDefault", String.valueOf(this.host.getProperty("booleanDefault")));
		this.generalProperties.setPropertyValue("optionalElementPolicy", String.valueOf(this.host.getProperty("optionalElementPolicy")));
		this.generalProperties.setPropertyValue("optionalAttributePolicy", String.valueOf(this.host.getProperty("optionalAttributePolicy")));
		this.generalProperties.setPropertyValue("askUserForChoiceOption", String.valueOf(this.host.getProperty("askUserForChoiceOption")));
		this.generalProperties.setPropertyValue("optionalAnyPolicy", String.valueOf(this.host.getProperty("optionalAnyPolicy")));
		this.generalProperties.setPropertyValue("expandNodesTo", String.valueOf(this.host.getProperty("expandNodesTo")));
		this.generalProperties.setVisible(true);
	}

	/**
	 * Called when a user chooses a folder resource tree item - According properties will be displayed
	 */
	private void handleFolderResourceClicked(TreeViewItem item) {
		XTAFolderResource folderResource = (XTAFolderResource) item.getUserObject();
		
		folderResource = (XTAFolderResource) this.host.getResourcesForId(folderResource.getItemId());
		
		this.workBenchTitleLabel.setText("Eigenschaften von " + folderResource.getItemName());
		this.workBench.setCenter(this.folderResourceProperties);
		this.folderResourceProperties.setItemID(folderResource.getItemId());
		this.folderResourceProperties.setPropertyValue("path", folderResource.getPath());
		this.folderResourceProperties.setPropertyValue("processMode", String.valueOf(folderResource.getProcessMode()));
		this.folderResourceProperties.setVisible(true);
	}

	/**
	 * Called when a user chooses an XTA account tree item - According properties will be displayed
	 */
	private void handleXTAAccountClicked(TreeViewItem item) {
		XTAPostOfficeBox folderResource = (XTAPostOfficeBox) item.getUserObject();
		
		folderResource = (XTAPostOfficeBox) this.host.getResourcesForId(folderResource.getItemId());
		
		this.workBenchTitleLabel.setText("Eigenschaften von " + folderResource.getItemName());
		this.workBench.setCenter(this.xtaResourceProperties);
		this.xtaResourceProperties.setItemID(folderResource.getItemId());
		this.xtaResourceProperties.setPropertyValue("path", folderResource.getPath());
		
		this.xtaResourceProperties.setPropertyValue("managementServiceUrl", String.valueOf(folderResource.getManagementServiceUrl()));
		this.xtaResourceProperties.setPropertyValue("sendServiceUrl", String.valueOf(folderResource.getSendServiceUrl()));
		this.xtaResourceProperties.setPropertyValue("msgBoxServiceUrl", String.valueOf(folderResource.getMsgBoxServiceUrl()));
		
		this.xtaResourceProperties.setPropertyValue("identifierA1", String.valueOf(folderResource.getIdentifier()));
		this.xtaResourceProperties.setPropertyValue("clientCertKeystoreA1", String.valueOf(folderResource.getClientCertKeystoreA1()));
		this.xtaResourceProperties.setPropertyValue("clientCertKeystoreA1Password", String.valueOf(folderResource.getClientCertKeystoreA1Password()));
		
		this.xtaResourceProperties.setPropertyValue("trustCertKeystore", String.valueOf(folderResource.getTrustCertKeystore()));
		this.xtaResourceProperties.setPropertyValue("trustCertKeystorePassword", String.valueOf(folderResource.getTrustCertKeystorePassword()));
		
		this.xtaResourceProperties.setPropertyValue("service", String.valueOf(folderResource.getService()));
		this.xtaResourceProperties.setPropertyValue("businessScenario", String.valueOf(folderResource.getBusinessScenario()));
		
		this.xtaResourceProperties.setVisible(true);
	}

	/**
	 * Called when a user chooses an FIT connect account tree item - According properties will be displayed
	 */
	private void handleRESTAccountClicked(TreeViewItem item) {
		XTAFITConnectPostOfficeBox folderResource = (XTAFITConnectPostOfficeBox) item.getUserObject();
		
		folderResource = (XTAFITConnectPostOfficeBox) this.host.getResourcesForId(folderResource.getItemId());
		
		this.workBenchTitleLabel.setText("Eigenschaften von " + folderResource.getItemName());
		this.workBench.setCenter(this.restResourceProperties);
		this.restResourceProperties.setItemID(folderResource.getItemId());
		this.restResourceProperties.setPropertyValue("path", folderResource.getPath());
		
		this.restResourceProperties.setPropertyValue("subscriberID", String.valueOf(folderResource.getSubscriberID()));
		this.restResourceProperties.setPropertyValue("subscriberSecret", String.valueOf(folderResource.getSubscriberSecret()));
		this.restResourceProperties.setPropertyValue("subscriberDestination", String.valueOf(folderResource.getSubscriberDestination()));
		this.restResourceProperties.setPropertyValue("privateKeyFile", String.valueOf(folderResource.getPrivateKeyFile()));
		this.restResourceProperties.setPropertyValue("privateSignFile", String.valueOf(folderResource.getPrivateSignFile()));
		this.restResourceProperties.setPropertyValue("publicSignFile", String.valueOf(folderResource.getPublicSignFile()));
		
		this.restResourceProperties.setPropertyValue("senderID", String.valueOf(folderResource.getSenderID()));
		this.restResourceProperties.setPropertyValue("senderSecret", String.valueOf(folderResource.getSenderSecret()));
		this.restResourceProperties.setPropertyValue("service", String.valueOf(folderResource.getService()));
		
		this.restResourceProperties.setPropertyValue("folder", String.valueOf(folderResource.getMessageOrganization()));
		
		this.restResourceProperties.setVisible(true);
	}

	/**
	 * Called when a user chooses complex type tree item - According content will be displayed
	 */
	private void handleComplexTypeClicked(XTAFolderResource root, String nodeId) {
		XTATreeItemI item = this.host.getItemForId(nodeId);
		this.workBenchTitleLabel.setText(item.getItemName()); //node.getFullyQualifiedElementName());
		this.workBench.setCenter(this.elementPane);
		this.elementPane.setVisible(true);
		
    	this.workBenchOpenAction.setDisable(false);
    	this.workBenchEmptyAction.setDisable(true);
    	this.workBenchOpenXMLAction.setDisable(false);
    	this.workBenchSendAction.setDisable(true);
    	
		this.workBenchCancelAction.setDisable(true);
    	this.workBenchEditAction.setDisable(true);
    	this.workBenchEditFreeAction.setDisable(true);
    	this.workBenchSaveAction.setDisable(true);
    	this.workBenchSaveAsAction.setDisable(false);
		
		this.elementStructure.getChildren().clear();
		
		this.elementPane.setCenter(elementScroller);
		recurseComplexType(root, (XComplexType) item, this.elementStructure, 0, null, false, 0, null);
	}

	/**
	 * Called when a user chooses simple type tree item - According content will be displayed
	 */
	private void handleSimpleTypeClicked(XTAFolderResource root, String nodeId) {
		XTATreeItemI item = this.host.getItemForId(nodeId);
		this.workBenchTitleLabel.setText(item.getItemName()); //node.getFullyQualifiedElementName());
		this.workBench.setCenter(this.elementPane);
		this.elementPane.setVisible(true);
		
    	this.workBenchOpenAction.setDisable(false);
    	this.workBenchEmptyAction.setDisable(true);
    	this.workBenchOpenXMLAction.setDisable(false);
    	this.workBenchSendAction.setDisable(true);
    	
		this.workBenchCancelAction.setDisable(true);
    	this.workBenchEditAction.setDisable(true);
    	this.workBenchEditFreeAction.setDisable(true);
    	this.workBenchSaveAction.setDisable(true);
    	this.workBenchSaveAsAction.setDisable(false);
		
		this.elementStructure.getChildren().clear();
		
		this.elementPane.setCenter(elementScroller);
		displaySimpleType(root, (XSimpleType) item, this.elementStructure);
	}
	
	/**
	 * This method displays contents of a simple type
	 * @param root	The covering folder resource
	 * @param node	the simple type node
	 * @param panel	the target panel to display contents on
	 */
	private void displaySimpleType(XTAFolderResource root, XSimpleType node, VBox panel) {
		HBox carrier = new HBox();
		
		Label nodeNameLabel = new Label(node.getLocalName());
		nodeNameLabel.setFont(Font.font("arial", FontPosture.ITALIC, 16));
		
		carrier.getChildren().add(nodeNameLabel);
		
		String attString = "";
		if(node.getRestriction() != null) {
			attString = attString + "restriction of " + node.getRestriction() + ", ";
		}
		if(node.getMinLenght() != null) {
			attString = attString + "minLenght: " + node.getMinLenght() + ", ";
		}
		if(node.getMaxLenght() != null) {
			attString = attString + "maxLenght: " + node.getMaxLenght() + ", ";
		}
		if(node.getMinInclusive() != null) {
			attString = attString + "minInclusive: " + node.getMinInclusive() + ", ";
		}
		if(node.getMaxInclusive() != null) {
			attString = attString + "maxInclusive: " + node.getMaxInclusive() + ", ";
		}
		if(node.getMinExclusive() != null) {
			attString = attString + "minExclusive: " + node.getMinExclusive() + ", ";
		}
		if(node.getMaxExclusive() != null) {
			attString = attString + "maxExclusive: " + node.getMaxExclusive() + ", ";
		}
		if(node.getFractionDigits() != null) {
			attString = attString + "fractionDigits: " + node.getFractionDigits() + ", ";
		}
		if(node.getTotalDigits() != null) {
			attString = attString + "totalDigits: " + node.getTotalDigits() + ", ";
		}
		if(node.getLenght() != null) {
			attString = attString + "lenght: " + node.getLenght() + ", ";
		}
		if(node.getPattern() != null) {
			attString = attString + "pattern: " + node.getPattern() + ", ";
		}
		
		if(attString.length() > 0) {
			if(attString.endsWith(", ")) {
				attString = attString.substring(0, attString.length() - 2);
			}
			
			attString = "[" + attString + "]";
			
			Label att = new Label();
			att.setText(attString);
			att.setFont(Font.font("arial", FontPosture.ITALIC, 16));
			att.setTextFill(Color.BLUE);
			att.setPadding(new Insets(0, 0, 0, 5));
			carrier.getChildren().add(att);
		}
		
		ImageView minus = new ImageView(this.minusImage);
		minus.setFitHeight(10);
		minus.setFitWidth(10);
		nodeNameLabel.setGraphic(minus);
		
		nodeNameLabel.setTooltip(new Tooltip(node.getFullyQualifiedName()));
		nodeNameLabel.setCursor(Cursor.HAND);
		panel.getChildren().add(carrier);
	}
	
	/**
	 * Display contents of a complex type recursively - element by element. In case an element is itself of a 
	 * complex type, recurseElement is called which potentially calls this method again
	 * @param root	The covering folder resource
	 * @param node	The complex type node to be displayed
	 * @param panel	The target panel for display
	 * @param depth	The current depth within the complex structure
	 * @param elementName	The display name of the element node
	 * @param anonymous	Specifies whether the complex type is anonymous
	 * @param extensionType	Specifies the (potential) extension type (extension or restriction)
	 * @param cardinality	Specifies the cardinality of the element node
	 */
	private void recurseComplexType(
			XTAFolderResource root, 
			XComplexType node, 
			VBox panel, 
			int depth, 
			String elementName, 
			boolean anonymous, 
			int extensionType, 
			String cardinality) {
		HBox carrier = new HBox();
		panel.getChildren().add(carrier);
		
		if(extensionType == XComplexType.EXTENSION) {
			panel.getStyleClass().add("xta-extension");
		}
		
		Label nodeNameLabel = new Label();
		if(elementName != null) {
			nodeNameLabel.setText(elementName);
		}
		else {
			nodeNameLabel.setText(node.getLocalName());
		}
		nodeNameLabel.setFont(Font.font("arial", FontPosture.ITALIC, 16));
		
		carrier.getChildren().add(nodeNameLabel);
		
		if(node.getElements().size() > 0) {
			if(depth > maxInitialDepth) {
				ImageView plus = new ImageView(plusImage);
				plus.setFitHeight(10);
				plus.setFitWidth(10);
				nodeNameLabel.setGraphic(plus);
			}
			else {
				ImageView minus = new ImageView(this.minusImage);
				minus.setFitHeight(10);
				minus.setFitWidth(10);
				nodeNameLabel.setGraphic(minus);
			}
		}
		
		VBox baseTypeBox = null;
		if(node.getName() != null) {
			Label typeNameLabel = new Label();
			typeNameLabel.setTextFill(Color.BLUE);
			typeNameLabel.setFont(Font.font("arial", FontPosture.ITALIC, 14));
			carrier.getChildren().add(typeNameLabel);
			
			if(node.getComplexBase() != null) {
				if(node.getDerivationType() == XComplexType.EXTENSION) {
					typeNameLabel.setText(" [" + node.getName() + " ++> " + node.getComplexBase() + "]");
					XTASchemaFileItem schema = node.getRootSchema().findSchemaForType(node.getComplexBase());
					XComplexType base = schema.getRootType(node.getComplexBase());
					
					baseTypeBox = new VBox();
					VBox.setMargin(baseTypeBox, new Insets(0, 0, 0, 12));
					panel.getChildren().add(baseTypeBox);
					
					this.recurseComplexType(root, base, baseTypeBox, depth, null, false, XComplexType.EXTENSION, cardinality);
				}
				else if(node.getDerivationType() == XComplexType.RESTRICTION) {
					typeNameLabel.setText(" [" + node.getName() + " --> " + node.getComplexBase() + "]");
				}
				else if(node.getDerivationType() == XComplexType.NONE) {
					typeNameLabel.setText(" [" + node.getName() + " nnn " + node.getComplexBase() + "]");
				}
				else{
					typeNameLabel.setText(" [" + node.getName() + " ??? " + node.getComplexBase() + "]");
				}
			}
			else if(node.getSimpleBase() != null) {
				if(node.getDerivationType() == XComplexType.EXTENSION) {
					typeNameLabel.setText(" [" + node.getName() + " ++> " + node.getSimpleBase() + "]");
				}
				else if(node.getDerivationType() == XComplexType.RESTRICTION) {
					typeNameLabel.setText(" [" + node.getName() + " --> " + node.getSimpleBase() + "]");
				}
				else if(node.getDerivationType() == XComplexType.NONE) {
					typeNameLabel.setText(" [" + node.getName() + " nnn " + node.getSimpleBase() + "]");
				}
				else{
					typeNameLabel.setText(" [" + node.getName() + " ??? " + node.getSimpleBase() + "]");
				}
			}
			else {
				if(anonymous) {
					typeNameLabel.setText(" [Anonymer Datentyp]");
				}
				else {
					typeNameLabel.setText(" [" + node.getName() + "]");
				}
			}
		}
		
		String tooltip = node.getFullyQualifiedName();
		if(anonymous) {
			tooltip = "Anonymer Datentyp";
		}
		if(cardinality != null) {
			tooltip = tooltip + " [" + cardinality + "]";
		}
		nodeNameLabel.setTooltip(new Tooltip(tooltip));
		nodeNameLabel.setCursor(Cursor.HAND);
		
		if(node.getElements().size() > 0) {
			VBox subBox = new VBox();
			VBox.setMargin(subBox, new Insets(2, 2, 2, 12));
			panel.getChildren().add(subBox);
			
			if(depth > maxInitialDepth) {
				subBox.setVisible(false);
				subBox.setManaged(false);
			}
			
			final VBox baseTempBox = baseTypeBox;
			nodeNameLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					if(baseTempBox != null) {
						baseTempBox.setVisible(!baseTempBox.isVisible());
						baseTempBox.setManaged(!baseTempBox.isManaged());
					}
					
					subBox.setVisible(!subBox.isVisible());
					subBox.setManaged(!subBox.isManaged());
					if(node.getElements().size() > 0) {
						if(subBox.isVisible()) {
							ImageView minus = new ImageView(minusImage);
							minus.setFitHeight(10);
							minus.setFitWidth(10);
							nodeNameLabel.setGraphic(minus);
						}
						else {
							ImageView plus = new ImageView(plusImage);
							plus.setFitHeight(10);
							plus.setFitWidth(10);
							nodeNameLabel.setGraphic(plus);
						}
					}
				}
			});
			
			for(int i=0;i<node.getElements().size();i++) {
				recurseElement(root, node.getElements().get(i), subBox, depth + 1);
			}
		}
	}

	/**
	 * Display contents of an element recursively. In case the element is itself of a complex type, 
	 * recurseComplexType will be called which potentially calls this method again 
	 * @param root
	 * @param node
	 * @param panel
	 * @param depth
	 */
	private void recurseElement(XTAFolderResource root, XElement node, VBox panel, int depth) {
		HBox carrier = new HBox();
		
		Label nodeNameLabel = new Label(node.getElementName());
		nodeNameLabel.setFont(Font.font("arial", FontPosture.ITALIC, 16));
		
		carrier.getChildren().add(nodeNameLabel);
		
		if(node.getElementName() != null) {
			Label typeNameLabel = new Label();
			typeNameLabel.setTextFill(Color.BLUE);
			typeNameLabel.setFont(Font.font("arial", FontPosture.ITALIC, 14));
			
			if(node.getElementType() != null) {
				typeNameLabel.setText(" [" + node.getElementType() + "]");
			}
			carrier.getChildren().add(typeNameLabel);
		}
		
		String tooltip = node.getFullyQualifiedName() + " [" + node.getMinOccurs() + ".." + node.getMaxOccurs() + "]";
		nodeNameLabel.setTooltip(new Tooltip(tooltip));
		nodeNameLabel.setCursor(Cursor.HAND);
		
		if(node.getElementType() != null) {
			VBox subBox = new VBox();
			VBox.setMargin(subBox, new Insets(0, 0, 0, 0));
			panel.getChildren().add(subBox);
			
			XTASchemaFileItem schema = node.getRootSchema().findSchemaForType(node.getElementType());
			if(schema != null) {
				XComplexType typeNode = schema.getRootType(node.getElementType());
				
				if(typeNode != null) {
					this.recurseComplexType(root, typeNode, subBox, depth + 1, node.getElementName(), false, 0, node.getMinOccurs() + ".." + node.getMaxOccurs());
				}
				else {
					panel.getChildren().add(carrier);
				}
			}
			else {
				panel.getChildren().add(carrier);
			}
		}
		else if(node.getAnonymousType() != null) {
			VBox subBox = new VBox();
			VBox.setMargin(subBox, new Insets(0, 0, 0, 0));
			panel.getChildren().add(subBox);
			
			this.recurseComplexType(root, node.getAnonymousType(), subBox, depth + 1, node.getElementName(), true, 0, node.getMinOccurs() + ".." + node.getMaxOccurs());
		}
		else {
			panel.getChildren().add(carrier);
		}
	}

	/**
	 * Called when the user clicks a root element within a schema file
	 * @param node	The chosen root element node
	 */
	private void handleRootElementClicked(XModelTreeNode node) {
		if(node != null) {
			if(node.getLabel() != null) {
				this.workBenchTitleLabel.setText(node.getFullyQualifiedElementName());
			}
			this.workBench.setCenter(this.elementPane);
			this.elementPane.setVisible(true);
			
	    	this.workBenchOpenAction.setDisable(false);
	    	this.workBenchEmptyAction.setDisable(true);
	    	this.workBenchOpenXMLAction.setDisable(false);
	    	this.workBenchSendAction.setDisable(true);
	    	
			this.workBenchCancelAction.setDisable(true);
	    	this.workBenchEditAction.setDisable(true);
	    	this.workBenchEditFreeAction.setDisable(true);
	    	this.workBenchAnalyseFreeAction.setDisable(true);
	    	this.workBenchSaveAction.setDisable(true);
	    	this.workBenchSaveAsAction.setDisable(false);
			
			this.elementStructure.getChildren().clear();
			
			this.elementPane.setCenter(elementScroller);
			recurseElementStructure(node, this.elementStructure, 0);
		}
	}
	
	/**
	 * Display contents of a root element and its element structure
	 * @param node	The root element
	 * @param panel	The target panel for display
	 * @param depth	The depth within the element structure
	 */
	private void recurseElementStructure(XModelTreeNode node, VBox panel, int depth) {
		HBox carrier = new HBox();
		
		String tooltip = node.getFullyQualifiedElementName();
		Label nodeNameLabel = new Label(tooltip);
		
		if(node.isChoiceChild()) {
			nodeNameLabel.setFont(Font.font("arial", FontPosture.ITALIC, 16));
		}
		else {
			nodeNameLabel.setFont(Font.font("arial", FontWeight.NORMAL, 16));
		}
		
		String cardinality = "1..1";
		if(node.getMinOccurs() != null && node.getMinOccurs().length() > 0) {
			if(node.getMaxOccurs() != null && node.getMaxOccurs().length() > 0) {
				cardinality = node.getMinOccurs() + ".." + node.getMaxOccurs();
			}
			else {
				cardinality = node.getMinOccurs() + "..1";
			}
		}
		else if(node.getMaxOccurs() != null && node.getMaxOccurs().length() > 0) {
			cardinality = "1.." + node.getMaxOccurs();
		}
		
		if("0".equals(node.getMinOccurs())) {
			nodeNameLabel.setTextFill(Color.GRAY);
		}
		
		if(node.isAbstr()) {
			nodeNameLabel.setTextFill(Color.ORANGE);
		}
		
		carrier.getChildren().add(nodeNameLabel);
		
		if(node.getChildCount() > 0) {
			if(depth > maxInitialDepth) {
				ImageView plus = new ImageView(plusImage);
				plus.setFitHeight(10);
				plus.setFitWidth(10);
				nodeNameLabel.setGraphic(plus);
			}
			else {
				ImageView minus = new ImageView(this.minusImage);
				minus.setFitHeight(10);
				minus.setFitWidth(10);
				nodeNameLabel.setGraphic(minus);
			}
		}
		
		nodeNameLabel.setText(node.getElementName());
		if(node.getElementTypeName() != null) {
			Label typeNameLabel = new Label();
			typeNameLabel.setTextFill(Color.BLUE);
			typeNameLabel.setFont(Font.font("arial", FontPosture.ITALIC, 14));
			
			if(node.getBaseTypeName() != null) {
				if(node.getDerivationType() == XComplexType.EXTENSION) {
					typeNameLabel.setText(" " + node.getElementTypeName() + " ++> " + node.getBaseTypeName() + " [" + cardinality + "]");
				}
				else if(node.getDerivationType() == XComplexType.RESTRICTION) {
					typeNameLabel.setText(" " + node.getElementTypeName() + " --> " + node.getBaseTypeName() + " [" + cardinality + "]");
				}
				else if(node.getDerivationType() == XComplexType.NONE) {
					typeNameLabel.setText(" " + node.getElementTypeName() + " nnn " + node.getBaseTypeName() + " [" + cardinality + "]");
				}
				else{
					typeNameLabel.setText(" " + node.getElementTypeName() + " ??? " + node.getBaseTypeName() + " [" + cardinality + "]");
				}
			}
			else {
				typeNameLabel.setText(" " + node.getElementTypeName() + " [" + cardinality + "]");
			}
			
			if(node.isCircle()) {
				typeNameLabel.setTextFill(Color.RED);
			}
			
			carrier.getChildren().add(typeNameLabel);
		}
		nodeNameLabel.setTooltip(new Tooltip(tooltip));
		nodeNameLabel.setCursor(Cursor.HAND);
		panel.getChildren().add(carrier);
		
		int md = 10;
		try {
			md = Integer.parseInt(this.generalProperties.getProperty("expandNodesTo").toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		if(node.getChildCount() > 0) {
			VBox subBox = new VBox();
			VBox.setMargin(subBox, new Insets(2, 2, 2, 12));
			panel.getChildren().add(subBox);
			
//			if(depth > maxInitialDepth) {
//				subBox.setVisible(false);
//				subBox.setManaged(false);
//			}
			
			if(depth > md || depth > maxInitialDepth) {
				subBox.setVisible(false);
				subBox.setManaged(false);
				
				ImageView plus = new ImageView(plusImage);
				plus.setFitHeight(10);
				plus.setFitWidth(10);
				nodeNameLabel.setGraphic(plus);
			}
			
			nodeNameLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					subBox.setVisible(!subBox.isVisible());
					subBox.setManaged(!subBox.isManaged());
					if(node.getChildCount() > 0) {
						if(subBox.isVisible()) {
							ImageView minus = new ImageView(minusImage);
							minus.setFitHeight(10);
							minus.setFitWidth(10);
							nodeNameLabel.setGraphic(minus);
						}
						else {
							ImageView plus = new ImageView(plusImage);
							plus.setFitHeight(10);
							plus.setFitWidth(10);
							nodeNameLabel.setGraphic(plus);
						}
					}
				}
			});
			
			if(!node.isCircle()) {
				for(int i=0;i<node.getChildCount();i++) {
					recurseElementStructure(node.getChildAt(i), subBox, depth + 1);
				}
			}
		}
	}

	/**
	 * This method is called when the user clicks an XML file for display / editing
	 * @param item	The XML file (node)
	 * @param editable	Indicates whether the user wants to edit (true) or just display (false) the file
	 * @param freetext	Indicated whether free text mode shall be used (true) or XML editing (false)
	 * @param reload	If reload is set true, the cursor and scroll bar position will be reset to 0, otherwise they remain as before
	 */
	private void handleXMLFileClicked(TreeViewItem item, boolean editable, boolean freetext, boolean reload) {
		XTAXMLFileItem xml = (XTAXMLFileItem) item.getUserObject();
		this.openXMLFile = xml;
		
		this.workBenchTitleLabel.setText(xml.getItemName());
		this.workBench.setCenter(this.xmlPane);
		this.xmlPane.setVisible(true);
		
    	this.workBenchOpenAction.setDisable(false);
    	this.workBenchEmptyAction.setDisable(true);
    	this.workBenchOpenXMLAction.setDisable(false);
    	this.workBenchSendAction.setDisable(false);
    	
    	this.xtaAttachmentsList.getChildren().clear();
    	if(xml.getAttachments() != null) {
    		for(String att : xml.getAttachments()) {
    			this.xtaAttachmentsList.getChildren().add(new AttachmentItem(att, xtaAttachmentsList));
    		}
    	}
    	
    	this.lastBuffer = null;
    	
		if(editable) {
	    	this.showEditActions();
	    	
			this.backup = new XTAXMLFileItem();
			this.backup.clone(xml);
	    	
			this.workBenchCancelAction.setDisable(false);
	    	this.workBenchEditAction.setDisable(true);
	    	this.workBenchEditFreeAction.setDisable(true);
	    	this.workBenchAnalyseFreeAction.setDisable(false);
	    	this.workBenchSaveAction.setDisable(false);
	    	this.workBenchSaveAsAction.setDisable(false);
		}
		else {
	    	this.showXMLActions();
	    	
			this.backup = null;
	    	this.workBenchCancelAction.setDisable(true);
	    	this.workBenchEditAction.setDisable(false);
	    	this.workBenchEditFreeAction.setDisable(false);
	    	this.workBenchAnalyseFreeAction.setDisable(false);
	    	this.workBenchSaveAction.setDisable(true);
	    	this.workBenchSaveAsAction.setDisable(false);
		}
		
		// Clear list of highlighted line indexes
		olistValue.clear();
		
		HashMap<Integer, Marker> markers = new HashMap<>();
		this.xmlWarnings.getChildren().clear();
		if(xml.getWarnings().size() > 0 || xml.getFatals().size() > 0) {
			XTAFolderResource root = (XTAFolderResource) getRoot(xml);
			
			if(root.validationDesired()) {
//				String warnings = "Warnungen: \n";
				for(String wn : xml.getWarnings()) {
//					warnings = warnings + wn + "\n";
					Label wl = new Label(wn);
					this.xmlWarnings.getChildren().add(wl);
					
					int sep = wn.lastIndexOf(" - Line ");
					if(sep > 0) {
						int line = Integer.parseInt(wn.substring(sep + 8));
						markers.put(line, new Marker(line, wn.substring(0, sep)));
						wl.setCursor(Cursor.HAND);
						
						// add line to the list of highlighted lines
						olistValue.add(line);
						
						wl.setOnMouseClicked(new EventHandler<Event>() {

							@Override
							public void handle(Event arg0) {
								if(freetext) {
									xmlPlaintext.setLineHighlighterOn(true);
									xmlPlaintext.moveTo(line - 1, 0, SelectionPolicy.CLEAR);
									xmlPlaintext.requestFollowCaret();
								}
								else {
									if(markers.get(line).getBox() != null) {
										NodeNameLabel nodeNameLabel = markers.get(line).getBox().getNodeNameLabel();
										nodeNameLabel.makeVisible();
										
										double yd = 0d;
										Parent p = nodeNameLabel;
										while(p != xmlStructure) {
											yd += p.getBoundsInParent().getMinY();
											p = p.getParent();
										}
										
										double height = xmlScroller.getContent().getBoundsInLocal().getHeight();
										
										xmlScroller.setVvalue(yd / height);
										nodeNameLabel.requestFocus();
									}
								}
							}
						});
					}
				}
//				String fatals = "Fehler: \n";
				for(String ft : xml.getFatals()) {
//					fatals = fatals + ft + "\n";
					Label wl = new Label(ft);
					this.xmlWarnings.getChildren().add(wl);
					
					int sep = ft.lastIndexOf(" - Line ");
					if(sep > 0) {
						int line = Integer.parseInt(ft.substring(sep + 8));
						markers.put(line, new Marker(line, ft.substring(0, sep)));
						wl.setCursor(Cursor.HAND);
						
						// add line to the list of highlighted lines
						olistValue.add(line);
						
						wl.setOnMouseClicked(new EventHandler<Event>() {

							@Override
							public void handle(Event arg0) {
								if(freetext) {
									xmlPlaintext.setLineHighlighterOn(true);
									xmlPlaintext.moveTo(line - 1, 0, SelectionPolicy.CLEAR);
									xmlPlaintext.requestFollowCaret();
								}
								else {
									if(markers.get(line).getBox() != null) {
										NodeNameLabel nodeNameLabel = markers.get(line).getBox().getNodeNameLabel();
										nodeNameLabel.makeVisible();
										
										double yd = 0d;
										Parent p = nodeNameLabel;
										while(p != xmlStructure) {
											yd += p.getBoundsInParent().getMinY();
											p = p.getParent();
										}
										
										double height = xmlScroller.getContent().getBoundsInLocal().getHeight();
										
										xmlScroller.setVvalue(yd / height);
										nodeNameLabel.requestFocus();
									}
								}
							}
						});
					}
				}
				
//				if(xml.getFatals().size() > 0 && xml.getWarnings().size() > 0) {
//					this.xmlWarnings.setText(fatals + "\n" + warnings + "\n");
//				}
//				else if(xml.getFatals().size() > 0) {
//					this.xmlWarnings.setText(fatals + "\n");
//				}
//				else {
//					this.xmlWarnings.setText(warnings + "\n");
//				}
				this.xmlWarningsScroller.setVisible(true);
				this.xmlWarningsScroller.setManaged(true);
			}
			else {
				this.xmlWarningsScroller.setVisible(false);
				this.xmlWarningsScroller.setManaged(false);
			}
		}
		else {
			this.xmlWarningsScroller.setVisible(false);
			this.xmlWarningsScroller.setManaged(false);
		}
		
		File folder = new File(xml.getPath()).getParentFile();
		File attFolder = new File(folder + "/." + xml.getItemName() + "_Attachments");
		if(attFolder.exists() && attFolder.isDirectory()) {
			ImageView icon = new ImageView(this.folderImage);
			this.xmlAttachments.setGraphic(icon);
			
			this.xmlAttachments.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					String path = attFolder.getAbsolutePath();
					TreeItem<TreeViewItem> ti = nodesForId.get(path.replace("\\", "/"));
					if(ti != null) {
						ti.setExpanded(true);
						resourceTree.getSelectionModel().select(ti);
					}
				}
			});
			
			this.xmlAttachments.setText("Anhänge zu dieser Datei: " + attFolder.listFiles().length);
			this.xmlAttachments.setCursor(Cursor.HAND);
			
			this.xmlAttachments.setVisible(true);
			this.xmlAttachments.setManaged(true);
		}
		else {
			this.xmlAttachments.setVisible(false);
			this.xmlAttachments.setManaged(false);
		}
		
		this.xmlStructure.getChildren().clear();
		
		if(freetext || xml.getFatals().size() > 0) {
			String plainText = host.getPlainText(xml.getItemId());
			
	    	this.workBenchEditAction.setDisable(true);
	    	
			xmlPane.setCenter(this.xmlPaneCarrier);
			xmlPlaintext.setEditable(editable);
//			xmlPlaintext.setText(host.getPlainText(xml.getItemId()));
			xmlPlaintext.replaceText(plainText);
			
			if(reload) {
				xmlPlaintext.moveTo(0);
				xmlPlaintext.requestFollowCaret();
			}
			xmlPlaintext.setStyleSpans(0, computeHighlighting(plainText));
		}
		else {
			xmlPane.setCenter(xmlScroller);
			if(editable) {
				this.xmlStructure.setBackground(new Background(new BackgroundFill(Color.web("#FFFFDD"), CornerRadii.EMPTY, Insets.EMPTY)));
//				this.xmlStructure.getStyleClass().add("xta-edit");
			}
			else {
				this.xmlStructure.setBackground(new Background(new BackgroundFill(Color.web("#F4F4F4"), CornerRadii.EMPTY, Insets.EMPTY)));
//				this.xmlStructure.getStyleClass().remove("xta-edit");
			}
			
			for(int i=0;i<xml.getItemChildCount();i++) {
				recurseXMLStructure((XNode) xml.getItemChildAt(i), this.xmlStructure, -1, editable, 0, markers);
			}
		}
	}

	/**
	 * Called when a schema file was clicked by the user
	 * @param item	The schema file (node)
	 */
	private void handleSchemaFileClicked(TreeViewItem item) {
		XTASchemaFileItem xml = (XTASchemaFileItem) item.getUserObject();
		this.workBenchTitleLabel.setText(xml.getItemName());
		this.workBench.setCenter(this.xmlPane);
		this.xmlPane.setVisible(true);
		xmlPane.setCenter(xmlScroller);
		
    	this.workBenchOpenAction.setDisable(false);
    	this.workBenchEmptyAction.setDisable(true);
    	this.workBenchOpenXMLAction.setDisable(false);
		
		this.xmlStructure.getChildren().clear();
		this.xmlStructure.getStyleClass().remove("xta-edit");
		
		Label nodeNameLabel = new Label(xml.getItemName());
		nodeNameLabel.setFont(Font.font("arial", FontWeight.BOLD, 16));
		this.xmlStructure.getChildren().add(nodeNameLabel);
		
		Label titleLabel = new Label("Root-Elemente (" + xml.getRootElements().size() + ")");
		titleLabel.setFont(Font.font("arial", FontWeight.SEMI_BOLD, 16));
		titleLabel.setPadding(new Insets(10, 0, 0, 0));
		this.xmlStructure.getChildren().add(titleLabel);
		
		for(String name : xml.getRootElements().keySet()) {
			HBox carrier = new HBox();
			
			Label subNameLabel = new Label(name);
			subNameLabel.setFont(Font.font("arial", FontWeight.SEMI_BOLD, FontPosture.ITALIC, 14));
			subNameLabel.setPadding(new Insets(0, 0, 0, 5));
			carrier.getChildren().add(subNameLabel);
			
			this.xmlStructure.getChildren().add(carrier);
		}
		
		titleLabel = new Label("Datentypen (" + xml.getRootTypeCount() + ")");
		titleLabel.setFont(Font.font("arial", FontWeight.SEMI_BOLD, 16));
		titleLabel.setPadding(new Insets(10, 0, 0, 0));
		this.xmlStructure.getChildren().add(titleLabel);
		
		for(String name : xml.getRootTypeKeySet()) {
			HBox carrier = new HBox();
			
			Label subNameLabel = new Label(name);
			subNameLabel.setFont(Font.font("arial", FontWeight.SEMI_BOLD, FontPosture.ITALIC, 14));
			subNameLabel.setPadding(new Insets(0, 0, 0, 5));
			carrier.getChildren().add(subNameLabel);
			
			this.xmlStructure.getChildren().add(carrier);
		}
		
		titleLabel = new Label("Codelisten (" + xml.getCodelistCount() + ")");
		titleLabel.setFont(Font.font("arial", FontWeight.SEMI_BOLD, 16));
		titleLabel.setPadding(new Insets(10, 0, 0, 0));
		this.xmlStructure.getChildren().add(titleLabel);
		
		for(String name : xml.getCodelistKeys()) {
			HBox carrier = new HBox();
			
			Label subNameLabel = new Label(name);
			subNameLabel.setFont(Font.font("arial", FontWeight.SEMI_BOLD, FontPosture.ITALIC, 14));
			subNameLabel.setPadding(new Insets(0, 0, 0, 5));
			carrier.getChildren().add(subNameLabel);
			
			this.xmlStructure.getChildren().add(carrier);
		}
		
		titleLabel = new Label("Einfache Datentypen (" + xml.getRootSimpleTypeCount() + ")");
		titleLabel.setFont(Font.font("arial", FontWeight.SEMI_BOLD, 16));
		titleLabel.setPadding(new Insets(10, 0, 0, 0));
		this.xmlStructure.getChildren().add(titleLabel);
		
		for(String name : xml.getRootSimpleTypeKeySet()) {
			HBox carrier = new HBox();
			
			Label subNameLabel = new Label(name);
			subNameLabel.setFont(Font.font("arial", FontWeight.SEMI_BOLD, FontPosture.ITALIC, 14));
			subNameLabel.setPadding(new Insets(0, 0, 0, 5));
			carrier.getChildren().add(subNameLabel);
			
			this.xmlStructure.getChildren().add(carrier);
		}
	}

	/**
	 * Recurive method to display hierarchical XML content. 
	 * @param node	The current XML (element) node to be displayed
	 * @param panel	The target panel for display
	 * @param position	The position index of the current XML node relative to its siblings
	 * @param editable	Indicated whether this XML element may be edited by the user
	 * @param depth	The depth within the XML structure
	 * @param markers	Assignment of warnings / fatals to the according lines in XML
	 */
	private void recurseXMLStructure(
			XNode node, 
			VBox panel, 
			int position, 
			boolean editable, 
			int depth, 
			HashMap<Integer, Marker> markers) {
		if(XNode.ELEMENT != node.getType()) {
			return;
		}
		VBox carrier = new VBox();
		carrier.setId("XMLContent");
		if(position >= 0) {
			panel.getChildren().add(position, carrier);
		}
		else {
			panel.getChildren().add(carrier);
		}
		
		ElementInfoBox elementInfo = new ElementInfoBox();
		carrier.getChildren().add(elementInfo);
		
		int lineNumber = node.getLineNumber();
		if(lineNumber >= 0 && markers != null) {
			if(markers.containsKey(lineNumber)) {
				elementInfo.setMarked("Zeile " + lineNumber + ": " + markers.get(lineNumber).getMessage());
				markers.get(lineNumber).setBox(elementInfo);
			}
		}
		
		NodeNameLabel nodeNameLabel = new NodeNameLabel(node.getName());
		nodeNameLabel.setFont(Font.font("arial", FontWeight.SEMI_BOLD, 16));
		
		// TODO: Clean up or implement alternatives (for user choice of elements to add)
//		ArrayList<String> alternatives = new ArrayList<String>();
		int min = 1;
//		int max = 1;
		if(node.getTypeNode() != null) {
			if(node.getTypeNode().getMinOccurs() != null && node.getTypeNode().getMinOccurs().length() > 0) {
				min = Integer.parseInt(node.getTypeNode().getMinOccurs());
			}
//			if(node.getTypeNode().getMaxOccurs() != null && node.getTypeNode().getMaxOccurs().length() > 0) {
//				try {
//					max = Integer.parseInt(node.getTypeNode().getMaxOccurs());
//				} catch (NumberFormatException e) {
//					max = Integer.MAX_VALUE;
//				}
//			}
		}
		
		if(min == 0) {
			nodeNameLabel.setTextFill(Color.GREY);
		}
		
		elementInfo.getChildren().add(nodeNameLabel);
		elementInfo.setNodeNameLabel(nodeNameLabel);
		nodeNameLabel.setBox(elementInfo);
		
		TreeMap<String, ArrayList<Node>> attLabels = new TreeMap<String, ArrayList<Node>>();
		if(node.getAttributes().size() > 0) {
			for(String attribute : node.getAttributes().keySet()) {
				if(editable) {
					Label att = new Label();
					att.setText(attribute + ": ");
					att.setFont(Font.font("arial", FontPosture.ITALIC, 16));
					att.setTextFill(Color.BLUE);
					att.setPadding(new Insets(0, 0, 0, 5));
					elementInfo.getChildren().add(att);
					
					NodeValueEditor attControll = new NodeValueEditor(node, "attribute#" + attribute, lastBuffer);
					lastBuffer = attControll;
					elementInfo.getChildren().add(attControll);
					
					ArrayList<Node> nodes = new ArrayList<Node>();
					nodes.add(att);
					nodes.add(attControll);
					attLabels.put(attribute, nodes);
				}
				else {
					Label att = new Label();
					att.setText(attribute + ": " + node.getAttributes().get(attribute));
					att.setFont(Font.font("arial", FontPosture.ITALIC, 16));
					att.setTextFill(Color.BLUE);
					att.setPadding(new Insets(0, 0, 0, 5));
					elementInfo.getChildren().add(att);
					
					ArrayList<Node> nodes = new ArrayList<Node>();
					nodes.add(att);
					attLabels.put(attribute, nodes);
				}
			}
		}
		
		if(node.getItemChildCount() > 0) {
			ImageView minus = new ImageView(this.minusImage);
			minus.setFitHeight(10);
			minus.setFitWidth(10);
			nodeNameLabel.setGraphic(minus);
			if(node.getValue() != null) {
				nodeNameLabel.setText(" " + node.getName() + ": " + node.getValue().trim());
			}
			else {
				nodeNameLabel.setText(" " + node.getName());
			}
		}
		else {
			if("code".equals(node.getName()) && node.getResolved() != null){
				if(editable) {
					nodeNameLabel.setText(node.getName() + " ");
					
					NodeValueEditor valueControll = new NodeValueEditor(node, "value", lastBuffer);
					lastBuffer = valueControll;
					elementInfo.getChildren().add(valueControll);
				}
				else {
					if(node.getValue() != null) {
						nodeNameLabel.setText(node.getName() + ": " + node.getValue().trim());
					}
					else {
						nodeNameLabel.setText(node.getName());
					}
					
					Label more = new Label();
					more.setFont(Font.font("arial", FontPosture.ITALIC, 16));
					elementInfo.getChildren().add(more);
					
					more.setText(" [" + node.getResolved() + "]");
					more.setTooltip(new Tooltip("XOEV-Typ: " + (node.getXoevType() > 0 ? node.getXoevType() : "unbekannt") + 
							"\nCode-Typ: " + node.getCodeType() + 
							"\nCodelisten-Typ: " + (node.getCodelistType() != null ? node.getCodelistType() : "unbekannt")));
					
					// Bei Klick soll direkt zu der Codeliste gesprungen werden!
					more.setOnMouseClicked(new EventHandler<Event>() {

						@Override
						public void handle(Event event) {
							if(nodesForId.containsKey(node.getItemId())) {
								TreeItem<TreeViewItem> item = nodesForId.get(node.getItemId());
								item.setExpanded(true);
								
								int row = resourceTree.getRow(item);
								resourceTree.getSelectionModel().select(row);
								
								item = item.getParent();
								if(item != null) {
									while(item != null) {
										item.setExpanded(true);
										item = item.getParent();
									}
								}
							}
						}
					});
				}
			}
			else {
				if(editable) {
					nodeNameLabel.setText(node.getName() + " ");
					
					NodeValueEditor valueControll = new NodeValueEditor(node, "value", lastBuffer);
					lastBuffer = valueControll;
					elementInfo.getChildren().add(valueControll);
				}
				else {
					if(node.getValue() != null) {
						nodeNameLabel.setText(node.getName() + ": " + node.getValue().trim());
					}
					else {
						nodeNameLabel.setText(node.getName());
					}
				}
			}
		}
		
		nodeNameLabel.setCursor(Cursor.HAND);
		
		if(editable) {
			Label further = new Label();
			ImageView furtherOptions = new ImageView(this.furtherOptionsImage);
			further.setGraphic(furtherOptions);
			further.getStyleClass().add("xta-further-options");
			further.setCursor(Cursor.HAND);
	        
	        further.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					Background saveB = carrier.getBackground();
					carrier.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
					
					if(lastCarrier != null && lastCarrier != carrier) {
						lastCarrier.setBackground(saveB);
					}
					lastCarrier = carrier;
					
					ContextMenu contextMenu = new ContextMenu();
					contextMenu.setOnAutoHide(new EventHandler<Event>() {
						
						@Override
						public void handle(Event event) {
							carrier.setBackground(saveB);
						}
					});
					contextMenu.setOnHidden(new EventHandler<WindowEvent>() {
						
						@Override
						public void handle(WindowEvent event) {
							carrier.setBackground(saveB);
						}
					});
					
					if(last != null) {
						last.hide();
					}
					last = contextMenu;
					
			        MenuItem removeElementItem = new MenuItem("Element entfernen");
					removeElementItem.setDisable(true);
			        if(depth > 0 && node.getParent() instanceof XNode) {
			        	XNode parent = (XNode) node.getParent();
			        	
						TreeMap<String, Integer> siblingReg = new TreeMap<String, Integer>();
						ArrayList<XModelTreeNode> siblingList = new ArrayList<XModelTreeNode>();
						
						TreeMap<String, Integer> positions = host.getAnalyzer().analyzeNode(parent, siblingReg, siblingList);
						
						// how many siblings of this type are there? 
						int ct = siblingReg.get(node.getName());
						
						XModelTreeNode element = node.getTypeElement();
						if(element == null) {
							element = node.getTypeNode();
						}
				        
						Menu replaceElementsItem = new Menu("Element ersetzen durch >");
				        contextMenu.getItems().addAll(replaceElementsItem);
						
						try {
							int mn = 0;
							
							if(element != null) {
								mn = Integer.parseInt(element.getMinOccurs());
								
								if(element.isChoiceChild()) {
									String choiceBase = element.getChoiceBase();
									
									final int pos = positions.get(element.getStandardElementName());
									
									for(XModelTreeNode siblingAt : siblingList) {
										if(choiceBase.contentEquals(siblingAt.getChoiceBase())) {
											MenuItem replaceItem = new MenuItem(siblingAt.getStandardElementName());
											replaceElementsItem.getItems().add(replaceItem);
											
											replaceItem.setOnAction(new EventHandler<ActionEvent>() {
												 
									            @Override
									            public void handle(ActionEvent event) {
									            	unsavedChanges = true;
									            	parent.removeChild(node);
									            	carrier.getChildren().clear();
									            	
										        	XTATreeItemI root = getRoot(node);
									            	XNode replaceSibling = host.createInstance(siblingAt, (XTAFolderResource) root, parent);
									            	
									            	VBox box = (VBox) carrier;
									            	recurseXMLStructure(replaceSibling, box, pos - 1, editable, depth + 1, markers);
									            }
									        });
										}
									}
								}
							}
							
							if(node.getOffspring() != null) {
								XModelTreeNode template = node.getOffspring();
								String namespace = template.getAnyNamespacePrefix();
								
								XTASchemaFileItem rootSchema = template.getComplexType().getSchema();
								
								ArrayList<XElement> matchingElements = null;
								
								if("##other".equals(namespace)) {
									matchingElements = rootSchema.findByPrefix("!" + template.getSchema().getTargetNamespace().getPrefix());
								}
								else if("##any".equals(namespace)) {
									matchingElements = rootSchema.findByPrefix(null);
								}
								else {
									matchingElements = rootSchema.findByNamespace(namespace);
								}
								
								// prefixes enthält alle bekannten Präfixe
								TreeSet<String> prefixes = rootSchema.getAllPrefixes();
								
								// es sollen nur Typen aus bekannten Namespaces verwendet werden, deswegen werden alle 
								// aussortiert, die nicht bekannt sind
								for(int k=matchingElements.size() - 1;k>=0;k--) {
									if(!prefixes.contains(matchingElements.get(k).getPrefix())) {
										matchingElements.remove(k);
									}
								}
							}
							
							if(ct > mn) {
								removeElementItem.setDisable(false);
						        
						        removeElementItem.setOnAction(new EventHandler<ActionEvent>() {
						 
						            @Override
						            public void handle(ActionEvent event) {
						            	unsavedChanges = true;
						            	parent.removeChild(node);
						        		panel.getChildren().remove(carrier);
						            }
						        });
							}
						} catch (Exception e) {
						}
			        }
			        
			        contextMenu.getItems().addAll(removeElementItem);
			        contextMenu.getItems().add(new SeparatorMenuItem());
					
			        Menu addElementsItem = new Menu("Element hinzufügen >");
			        contextMenu.getItems().addAll(addElementsItem);
			        
					TreeMap<String, Integer> childReg = new TreeMap<String, Integer>();
					ArrayList<XModelTreeNode> childList = new ArrayList<XModelTreeNode>();
					
					TreeMap<String, Integer> positions = host.getAnalyzer().analyzeNode(node, childReg, childList);
					
//					int position = 0;
					for(XModelTreeNode childAt : childList) {
						MenuItem addItem = new MenuItem(childAt.getStandardElementName());
						
						if(childReg.containsKey(childAt.getStandardElementName())) {
							int ct = childReg.get(childAt.getStandardElementName());
							int mx = 1;
							try {
								if("unbounded".equals(childAt.getMaxOccurs().trim())) {
									mx = Integer.MAX_VALUE;
								}
								else {
									mx = Integer.parseInt(childAt.getMaxOccurs());
								}
							} catch (Exception e) {
							}
							if(ct >= mx) {
								addItem.setDisable(true);
							}
						}
						
						final int pos = positions.get(childAt.getStandardElementName());
						addItem.setOnAction(new EventHandler<ActionEvent>() {
				 
				            @Override
				            public void handle(ActionEvent event) {
				            	unsavedChanges = true;
					        	XTATreeItemI root = getRoot(node);
				            	XNode newChild = host.createInstance(childAt, (XTAFolderResource) root, node);
				            	
				            	if(carrier.getChildren().size() < 2) {
				        			VBox subBox = new VBox();
				        			VBox.setMargin(subBox, new Insets(2, 2, 2, 12));
				        			carrier.getChildren().add(subBox);
				            	}
				            	
				            	VBox box = (VBox) carrier.getChildren().get(1);
				            	recurseXMLStructure(newChild, box, pos, editable, depth + 1, markers);
				            }
				        });
						addElementsItem.getItems().addAll(addItem);
//						position++;
					}
					
					
			        Menu addAttributesItem = new Menu("Attribut hinzufügen >");
			        contextMenu.getItems().addAll(addAttributesItem);
			        Menu removeAttributesItem = new Menu("Attribut entfernen >");
			        contextMenu.getItems().addAll(removeAttributesItem);
			        
			        if(node.getTypeNode() != null) {
			        	for(XAttribute att : node.getTypeNode().getAttributes()) {
			        		if(node.getAttributes().containsKey(att.getStandardAttributeName())) {
			        			MenuItem removeItem = new MenuItem(att.getStandardAttributeName());
								
			        			if(!"optional".equals(att.getUse())) {
			        				removeItem.setDisable(true);
								}
			        			
			        			removeItem.setOnAction(new EventHandler<ActionEvent>() {
									 
						            @Override
						            public void handle(ActionEvent event) {
						            	node.getAttributes().remove(att.getStandardAttributeName());
						            	if(attLabels.containsKey(att.getStandardAttributeName())) {
						            		for(Node nd : attLabels.get(att.getStandardAttributeName())) {
								            	elementInfo.getChildren().remove(nd);
						            		}
						            	}
						            }
						        });
			        			
								removeAttributesItem.getItems().add(removeItem);
			        		}
			        		else {
								MenuItem addItem = new MenuItem(att.getStandardAttributeName());
								addItem.setOnAction(new EventHandler<ActionEvent>() {
									 
						            @Override
						            public void handle(ActionEvent event) {
										String attribute = att.getStandardAttributeName();
										
										node.getAttributes().put(attribute, " ");
										
						            	if(editable) {
											Label att = new Label();
											att.setText(attribute + ": ");
											att.setFont(Font.font("arial", FontPosture.ITALIC, 16));
											att.setTextFill(Color.BLUE);
											att.setPadding(new Insets(0, 0, 0, 5));
											elementInfo.getChildren().add(att);
											
											NodeValueEditor attControll = new NodeValueEditor(node, "attribute#" + attribute, lastBuffer);
											lastBuffer = attControll;
											elementInfo.getChildren().add(attControll);
											
											ArrayList<Node> nodes = new ArrayList<Node>();
											nodes.add(att);
											nodes.add(attControll);
											attLabels.put(attribute, nodes);
										}
										else {
											Label att = new Label();
											att.setText(attribute + ": " + node.getAttributes().get(attribute));
											att.setFont(Font.font("arial", FontPosture.ITALIC, 16));
											att.setTextFill(Color.BLUE);
											att.setPadding(new Insets(0, 0, 0, 5));
											elementInfo.getChildren().add(att);
											
											ArrayList<Node> nodes = new ArrayList<Node>();
											nodes.add(att);
											attLabels.put(attribute, nodes);
										}
						            }
						        });
								addAttributesItem.getItems().add(addItem);
			        		}
			        	}
			        }
					
					contextMenu.show(carrier, event.getScreenX(), event.getScreenY());
				}
			});
			further.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {

				@Override
				public void handle(ContextMenuEvent event) {
				}
			});
			elementInfo.getChildren().add(1, further);
		}
		
		int md = 10;
		try {
			md = Integer.parseInt(this.generalProperties.getProperty("expandNodesTo").toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		if(node.getItemChildCount() > 0) {
			VBox subBox = new VBox();
			subBox.setId("XMLCarrier");
			VBox.setMargin(subBox, new Insets(2, 2, 2, 12));
			carrier.getChildren().add(subBox);
			
			if(depth > md) {
				if(!openBranches.contains(node.getPathId())) {
					subBox.setVisible(false);
					subBox.setManaged(false);
					
					ImageView plus = new ImageView(plusImage);
					plus.setFitHeight(10);
					plus.setFitWidth(10);
					nodeNameLabel.setGraphic(plus);
				}
			}
			
			nodeNameLabel.setSubBox(subBox);
			nodeNameLabel.setNode(node);
//			nodeNameLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<Event>() {
//				@Override
//				public void handle(Event event) {
//					subBox.setVisible(!subBox.isVisible());
//					subBox.setManaged(!subBox.isManaged());
//					if(subBox.isVisible()) {
//						openBranches.add(node.getPathId());
//						
//						ImageView minus = new ImageView(minusImage);
//						minus.setFitHeight(10);
//						minus.setFitWidth(10);
//						nodeNameLabel.setGraphic(minus);
//					}
//					else {
//						openBranches.remove(node.getPathId());
//						
//						ImageView plus = new ImageView(plusImage);
//						plus.setFitHeight(10);
//						plus.setFitWidth(10);
//						nodeNameLabel.setGraphic(plus);
//					}
//				}
//			});
			
			for(int i=0;i<node.getItemChildCount();i++) {
				recurseXMLStructure((XNode) node.getItemChildAt(i), subBox, -1, editable, depth + 1, markers);
			}
		}
	}

	/**
	 * Called when a user clicks a codelist tree item
	 * @param item	The codelist tree item clicked
	 */
	private void handleCodelistClicked(TreeViewItem item) {
		XCodelist cl;
		if("XTA_GenericodeList".equals(item.getUserObject().getItemType())) {
			cl = (XCodelist) ((XTAGenericodeList) item.getUserObject()).getCodelist();
		}
		else {
			cl = (XCodelist) item.getUserObject();
		}
		
		if(cl != null) {
			this.workBenchTitleLabel.setText(cl.getName());
			this.workBench.setCenter(this.codelistPane);
			this.codelistPane.setVisible(true);
			for(int i=0;i<cl.getHeader().size();i++) {
				final int k = i;
				TableColumn<List<StringProperty>, String> coll = new TableColumn<>(cl.getHeader().get(k));
				coll.setCellValueFactory(data -> data.getValue().get(k));
				codelistTable.getColumns().add(coll);
			}
			
			this.codelistTable.getItems().clear();
			
			ObservableList<List<StringProperty>> data = FXCollections.observableArrayList();
			for(int i=0;i<cl.getValues().size();i++) {
				List<StringProperty> firstRow = new ArrayList<>();
				
				for(int j=0;j<cl.getValues().get(i).size();j++) {
			    	firstRow.add(new SimpleStringProperty(cl.getValues().get(i).get(j)));
				}
				data.add(firstRow);
			}
			codelistTable.setItems(data);
			codelistTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			codelistTable.setOnKeyPressed(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					final KeyCodeCombination keyCodeCopy = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
					if(keyCodeCopy.match((KeyEvent) event)) {
						copySelectionToClipboard(codelistTable);
					}
				}
			});
		}
	}
	
	/**
	 * Called when a user presses CTRL+C within a codelist in order to copy selected rows into the clipboard
	 * @param table	The codelist table
	 */
	public void copySelectionToClipboard(final TableView<?> table) {
	    final Set<Integer> rows = new TreeSet<>();
	    for (final TablePosition<Object, Object> tablePosition : table.getSelectionModel().getSelectedCells()) {
	        rows.add(tablePosition.getRow());
	    }
	    final StringBuilder strb = new StringBuilder();
	    boolean firstRow = true;
	    for (final Integer row : rows) {
	        if (!firstRow) {
	            strb.append('\n');
	        }
	        firstRow = false;
	        boolean firstCol = true;
	        for (final TableColumn<?, ?> column : table.getColumns()) {
	            if (!firstCol) {
	                strb.append('\t');
	            }
	            firstCol = false;
	            final Object cellData = column.getCellData(row);
	            strb.append(cellData == null ? "" : cellData.toString());
	        }
	    }
	    final ClipboardContent clipboardContent = new ClipboardContent();
	    clipboardContent.putString(strb.toString());
	    Clipboard.getSystemClipboard().setContent(clipboardContent);
	}
	
	@Override
	public void handle(ActionEvent event) {
		if(event.getSource() == this.newFolder) {
			DirectoryChooser fileChooser = new DirectoryChooser();
			fileChooser.setTitle("Bitte waehlen Sie einen Ordner");
			fileChooser.setInitialDirectory(new File("."));
			File file = fileChooser.showDialog(stage);
			
			if(file != null) {
				this.host.addFolderResource(file.getName(), file.getAbsolutePath());
			}
		}
		else if(event.getSource() == this.newXTAAccount) {
			DirectoryChooser fileChooser = new DirectoryChooser();
			fileChooser.setTitle("Bitte waehlen Sie einen Ordner");
			fileChooser.setInitialDirectory(new File("."));
			File file = fileChooser.showDialog(stage);
			
			if(file != null) {
				this.host.addXTAAccount(file.getName(), file.getAbsolutePath());
			}
		}
		else if(event.getSource() == this.newRESTAccount) {
			DirectoryChooser fileChooser = new DirectoryChooser();
			fileChooser.setTitle("Bitte waehlen Sie einen Ordner");
			fileChooser.setInitialDirectory(new File("."));
			File file = fileChooser.showDialog(stage);
			
			if(file != null) {
				this.host.addRESTAccount(file.getName(), file.getAbsolutePath());
			}
		}
		else if(event.getSource() == this.newMenuItem) {
			DirectoryChooser fileChooser = new DirectoryChooser();
			fileChooser.setTitle("Bitte waehlen Sie einen Ordner");
			fileChooser.setInitialDirectory(new File("."));
			File file = fileChooser.showDialog(stage);
			
			if(file != null) {
				this.host.addFolderResource(file.getName(), file.getAbsolutePath());
			}
		}
		else if(event.getSource() == this.exitMenuItem) {
			try {
				this.stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		else {
			TreeItem<TreeViewItem> selected = (TreeItem<TreeViewItem>) resourceTree.getSelectionModel().getSelectedItem();
			if(event.getSource() == this.createInstancesItem) {
			    TreeViewItem item = (TreeViewItem) selected.getValue();
				
			    DirectoryChooser fileChooser = new DirectoryChooser();
				fileChooser.setTitle("Bitte waehlen Sie einen Ordner");
				fileChooser.setInitialDirectory(new File("."));
				File file = fileChooser.showDialog(stage);
			    
			    this.createInstances(item, file.getAbsolutePath());
			}
			/*
			else if(event.getSource() == this.addToRepositoryItem) {
			    TreeViewItem item = (TreeViewItem) ((TreeItem)resourceTree.getSelectionModel().getSelectedItem()).getValue();
				this.host.addInstanceToRepository(item.getUserObject());
			}
			*/
			else if(event.getSource() == this.receiveMessagesItem) {
				ArrayList<XTAResource> res = this.host.getResources(XTAResourceType.XTA_ACCOUNT);
				for(XTAResource ressource : res) {
					((XTAPostOfficeBox) ressource).setCheck(true);
				}
				
				res = this.host.getResources(XTAResourceType.REST_ENDPOINT);
				for(XTAResource ressource : res) {
					((XTAPostOfficeBox) ressource).setCheck(true);
				}
			}
			else if(event.getSource() == this.proxyItem) {
				this.proxySettings.init();
				this.proxySettings.showDialog();
			}
			else if(event.getSource() == this.addressBookItem) {
				this.addressBook.show();
			}
			else if(event.getSource() == this.aboutItem) {
				this.dialog.showAndWait();
			}
			else if(event.getSource() == this.debugItem) {
				this.debugDialog.show();
			}
			else if(event.getSource() == this.showAddressBook) {
				this.addressBook.show();
			}
			else if(event.getSource() == this.removeButton) {
			    TreeViewItem item = (TreeViewItem) selected.getValue();
			    
			    this.host.removeResource(item.getId());
			}
			else if(event.getSource() == this.generalProperties.getApplyButton()) {
				this.host.setProperty("booleanDefault", (Serializable) this.generalProperties.getPropertyValue("booleanDefault"));
				this.host.setProperty("optionalElementPolicy", (Serializable) this.generalProperties.getPropertyValue("optionalElementPolicy"));
				this.host.setProperty("optionalAttributePolicy", (Serializable) this.generalProperties.getPropertyValue("optionalAttributePolicy"));
				this.host.setProperty("askUserForChoiceOption", (Serializable) this.generalProperties.getPropertyValue("askUserForChoiceOption"));
				this.host.setProperty("optionalAnyPolicy", (Serializable) this.generalProperties.getPropertyValue("optionalAnyPolicy"));
				this.host.setProperty("expandNodesTo", (Serializable) this.generalProperties.getPropertyValue("expandNodesTo"));
			}
			else if(event.getSource() == this.folderResourceProperties.getApplyButton()) {
				String itemID = this.folderResourceProperties.getItemID();
				XTAFolderResource folder = (XTAFolderResource) this.host.getResourcesForId(itemID);
				folder.setPath(this.folderResourceProperties.getPropertyValue("path").toString());
				String processMode = this.folderResourceProperties.getPropertyValue("processMode").toString();
				folder.setProcessMode(Integer.parseInt(processMode));
				
				TreeItem<TreeViewItem> item = this.nodesForId.get(itemID);
				
				for(int i=item.getChildren().size()-1;i>=0;i--) {
					this.removeRecursive(item.getChildren().get(i));
				}
				
				this.host.updateFolderResource(folder);
			}
			else if(event.getSource() == this.xtaResourceProperties.getApplyButton()) {
				String itemID = this.xtaResourceProperties.getItemID();
				XTAPostOfficeBox folder = (XTAPostOfficeBox) this.host.getResourcesForId(itemID);
				folder.setPath(this.xtaResourceProperties.getPropertyValue("path").toString());
				
				folder.setManagementServiceUrl(this.xtaResourceProperties.getPropertyValue("managementServiceUrl").toString());
				folder.setMsgBoxServiceUrl(this.xtaResourceProperties.getPropertyValue("msgBoxServiceUrl").toString());
				folder.setSendServiceUrl(this.xtaResourceProperties.getPropertyValue("sendServiceUrl").toString());
				
				folder.setIdentifier(this.xtaResourceProperties.getPropertyValue("identifierA1").toString());
				folder.setClientCertKeystoreA1(this.xtaResourceProperties.getPropertyValue("clientCertKeystoreA1").toString());
				folder.setClientCertKeystoreA1Password(this.xtaResourceProperties.getPropertyValue("clientCertKeystoreA1Password").toString());
				
				folder.setTrustCertKeystore(this.xtaResourceProperties.getPropertyValue("trustCertKeystore").toString());
				folder.setTrustCertKeystorePassword(this.xtaResourceProperties.getPropertyValue("trustCertKeystorePassword").toString());
				
				folder.setService(this.xtaResourceProperties.getPropertyValue("service").toString());
				folder.setBusinessScenario(this.xtaResourceProperties.getPropertyValue("businessScenario").toString());
				
				TreeItem<TreeViewItem> item = this.nodesForId.get(itemID);
				
				for(int i=item.getChildren().size()-1;i>=0;i--) {
					this.removeRecursive(item.getChildren().get(i));
				}
				
				this.host.updateFolderResource(folder);
			}
			else if(event.getSource() == this.restResourceProperties.getApplyButton()) {
				String itemID = this.restResourceProperties.getItemID();
				XTAFITConnectPostOfficeBox folder = (XTAFITConnectPostOfficeBox) this.host.getResourcesForId(itemID);
				folder.setPath(this.restResourceProperties.getPropertyValue("path").toString());
				
				folder.setSubscriberID(this.restResourceProperties.getPropertyValue("subscriberID").toString());
				folder.setSubscriberSecret(this.restResourceProperties.getPropertyValue("subscriberSecret").toString());
				folder.setSubscriberDestination(this.restResourceProperties.getPropertyValue("subscriberDestination").toString());
				folder.setPrivateKeyFile(this.restResourceProperties.getPropertyValue("privateKeyFile").toString());
				folder.setPrivateSignFile(this.restResourceProperties.getPropertyValue("privateSignFile").toString());
				folder.setPublicSignFile(this.restResourceProperties.getPropertyValue("publicSignFile").toString());
				
				folder.setSenderID(this.restResourceProperties.getPropertyValue("senderID").toString());
				folder.setSenderSecret(this.restResourceProperties.getPropertyValue("senderSecret").toString());
				folder.setService(this.restResourceProperties.getPropertyValue("service").toString());
				
				folder.setMessageOrganization(this.restResourceProperties.getPropertyValue("folder").toString());
				
				folder.init();
				
				TreeItem<TreeViewItem> item = this.nodesForId.get(itemID);
				
				for(int i=item.getChildren().size()-1;i>=0;i--) {
					this.removeRecursive(item.getChildren().get(i));
				}
				
				this.host.updateFolderResource(folder);
			}
			else if(event.getSource() == this.workBenchOpenAction || event.getSource() == this.workBenchOpenXMLAction) {
			    TreeViewItem item = (TreeViewItem) selected.getValue();
			    
			    if(item.getUserObject() instanceof XTAFileItem) {
			    	try {
						Desktop.getDesktop().open(new File(((XTAFileItem) item.getUserObject()).getPath()));
					} catch (IOException e) {
						e.printStackTrace();
					}
			    }
			}
			else if(event.getSource() == this.workBenchEmptyAction) {
			    for(int i=selected.getChildren().size() - 1;i >= 0;i--) {
			    	TreeItem<TreeViewItem> child = (resourceTree.getSelectionModel().getSelectedItem()).getChildren().get(i);
			    	TreeViewItem childItem = (TreeViewItem) child.getValue();
			    	XTATreeItemI toDel = childItem.getUserObject();
			    	if(toDel instanceof XTAFileItem) {
			    		String path = ((XTAFileItem) toDel).getPath();
			    		File del = new File(path);
			    		boolean suc = del.delete();
			    		
			    		if(suc) {
			        		this.itemRemoved(toDel);
			    		}
			    	}
			    }
			}
			else if(event.getSource() == this.workBenchSendAction) {
			    TreeViewItem item = (TreeViewItem) selected.getValue();
			    
			    if(item.getUserObject() instanceof XTAFileItem) {
			    	ArrayList<String> ids = this.host.getResourceIds(XTAResourceType.XTA_ACCOUNT);
			    	ids.addAll(this.host.getResourceIds(XTAResourceType.REST_ENDPOINT));
			    	
		    		this.xtaCaseComboBox.getItems().clear();
			    	ArrayList<String> openCases = new ArrayList<String>();
	    			openCases.add("Neuer Fall");
			    	if(ids.size() > 0) {
		    			XTAPostOfficeBox selectedXtaResource = (XTAPostOfficeBox) this.host.getResourcesForId(ids.get(0));
		    			openCases.addAll(selectedXtaResource.getCaseIds());
			    	}
		    		this.xtaCaseComboBox.getItems().addAll(openCases);
			    	
			    	if(ids.size() == 0) {
			    		showMessageDialog("Kein XTA-Konto gefunden", 
			    				"Zur Zeit ist kein XTA-Konto angelegt. Klicken Sie auf \"Neues XTA-Konto\" um eines anzulegen", 
			    				3);
			    	}
			    	else {
			    		int sPreChoice = this.xtaSenderAccountComboBox.getSelectionModel().getSelectedIndex();
			    		int rPreChoice = this.xtaReceiverAccountComboBox.getSelectionModel().getSelectedIndex();
			    		
			    		this.xtaSenderAccountComboBox.getItems().clear();
			    		this.xtaReceiverAccountComboBox.getItems().clear();
			    		
			    		for(int i=0;i<ids.size();i++) {
			    			XTAPostOfficeBox xtaResource = (XTAPostOfficeBox) this.host.getResourcesForId(ids.get(i));
							
			    			String label = xtaResource.getItemName() + " [" + xtaResource.getIdentifier() + "]";
							this.xtaSenderAccountComboBox.getItems().add(label);
			    			String rlabel = xtaResource.getReceiverIdentifier();
							this.xtaReceiverAccountComboBox.getItems().add(rlabel);
			    			
							// TODO
							if(xtaResource.isSender()) {
			    			}
							
							if(xtaResource.isReceiver()) {
							}
			    		}
			    		this.xtaSenderAccountComboBox.getSelectionModel().select(Math.max(0, sPreChoice));
			    		this.xtaReceiverAccountComboBox.getSelectionModel().select(Math.max(0, rPreChoice));
			    		
			    		this.workBenchTitle.setMinHeight(180);
			    		this.workBenchSendActions.setVisible(true);
			    		this.workBenchSendActions.setManaged(true);
//	            	this.xtaAccountChoiceDialog.show();
			    	}
			    }
			}
			else if(event.getSource() == this.xtaChoiceOKButton) {
				String receiver = this.xtaReceiverAccountComboBox.getSelectionModel().getSelectedItem();
				if(receiver != null && !"".equals(receiver)) {
					this.workBenchTitle.setMinHeight(20);
					this.workBenchSendActions.setVisible(false);
					this.workBenchSendActions.setManaged(false);
					
			        TreeViewItem item = (TreeViewItem) selected.getValue();
			        
			    	ArrayList<String> ids = this.host.getResourceIds(XTAResourceType.XTA_ACCOUNT);
			    	ids.addAll(this.host.getResourceIds(XTAResourceType.REST_ENDPOINT));
			    	
			    	int index = this.xtaSenderAccountComboBox.getSelectionModel().getSelectedIndex();
					String id = ids.get(index);
					
					XTAPostOfficeBox box = (XTAPostOfficeBox) this.host.getResourcesForId(id);
					String destinationFolder = box.getOutbox().getAbsolutePath();
					box.setReceiver(receiver);
					
					String caseId = null;
					if(this.xtaCaseComboBox.getSelectionModel().getSelectedIndex() > 0) {
						caseId = this.xtaCaseComboBox.getSelectionModel().getSelectedItem();
					}
					box.setSelectedCaseId(caseId);
					
					Platform.runLater(new Runnable() {
						
						@Override
						public void run() {
			    			host.copyItem(item.getUserObject().getItemId(), destinationFolder, false, 1, null, box, false);
						}
					});
				}
				else {
					showMessageDialog("Kein Empfaenger angegeben", 
							"Bitte geben Sie einen Empfaenger an", 
							3);
				}
			}
			else if(event.getSource() == this.xtaChoiceCancelButton) {
				this.workBenchTitle.setMinHeight(20);
				this.workBenchSendActions.setVisible(false);
				this.workBenchSendActions.setManaged(false);
			}
			else if(event.getSource() == this.workBenchEditAction || event.getSource() == this.editXMLItem) {
			    TreeViewItem item = (TreeViewItem) selected.getValue();
			    
			    if(item.getUserObject() instanceof XTAXMLFileItem) {
			    	this.editMode = 0;
			    	handleXMLFileClicked(item, true, false, true);
			    }
			}
			else if(event.getSource() == this.workBenchEditFreeAction || event.getSource() == this.editTextItem) {
			    TreeViewItem item = (TreeViewItem) selected.getValue();
			    
			    if(item.getUserObject() instanceof XTAXMLFileItem) {
			    	this.editMode = 1;
			    	handleXMLFileClicked(item, true, true, true);
			    }
			}
			else if(event.getSource() == this.workBenchAnalyseFreeAction) {
				xmlPane.setDisable(true);
				resourceTree.setDisable(true);
				resourceTree.setCursor(Cursor.WAIT);

			    TreeViewItem item = (TreeViewItem) selected.getValue();
			    XTATreeItemI source = item.getUserObject();
				
			    Platform.runLater(new Runnable() {
					
					@Override
					public void run() {
						host.analyzeItem(source.getItemId(), (XTAFolderResource) getRoot(source));
						XTAXMLFileItem newXtaxmlFileItem = (XTAXMLFileItem) host.getItemForId(source.getItemId());
						item.setUserObject(newXtaxmlFileItem);
						
						if(item.getId().equals(lastClickedID)) {
					    	handleXMLFileClicked(item, false, false, false);
							updateTitleIcon(newXtaxmlFileItem, (XTAFolderResource) getRoot(newXtaxmlFileItem));
						}
						
						xmlPane.setDisable(false);
						resourceTree.setDisable(false);
						resourceTree.setCursor(Cursor.DEFAULT);
					}
				});
			}
			else if(event.getSource() == this.workBenchCancelAction) {
				TreeViewItem item = (TreeViewItem) selected.getValue();
			    
				// Write back the origin values from backup node!
				if(this.backup != null) {
					XTAXMLFileItem xml = (XTAXMLFileItem) item.getUserObject();
					XTATreeItemI parent = xml.getParent();
					
					xml.clear();
					xml.clone(this.backup);
					xml.setParent(parent);
				}
				
			    if(item.getUserObject() instanceof XTAXMLFileItem) {
			    	XTAXMLFileItem xtaxmlFileItem = (XTAXMLFileItem) item.getUserObject();
			    	
			    	boolean plain = false;
					if(this.editMode == 1 || xtaxmlFileItem.getFatals().size() > 0) {
						xtaxmlFileItem.setPlainText(this.xmlPlaintext.getText());
						plain = true;
			    	}
					this.host.exportXMLFile(xtaxmlFileItem, plain);
					this.unsavedChanges = false;
			    	
			    	XTATreeItemI root = this.getRoot(item.getUserObject());
			    	this.host.validateXMLFile(root.getItemId(), item.getId(), (XTAXMLFileItem) item.getUserObject());
					
			    	this.processItem(resourceTree.getSelectionModel().getSelectedItem(), item.getUserObject());
			    	
			    	handleXMLFileClicked(item, false, false, true);
			    }
			    
			    this.showXMLActions();
			}
			else if(event.getSource() == this.workBenchStopEditAction) {
				xmlPane.setDisable(true);
				resourceTree.setDisable(true);
				resourceTree.setCursor(Cursor.WAIT);

			    TreeViewItem item = (TreeViewItem) selected.getValue();
			    XTATreeItemI source = item.getUserObject();
			    
			    if(item.getUserObject() instanceof XTAXMLFileItem) {
			    	if(currentEditor != null) {
			    		currentEditor.closeEditor();
			    	}
			    	
			    	XTAXMLFileItem xtaxmlFileItem = (XTAXMLFileItem) item.getUserObject();
			    	
			    	boolean plain = false;
					if(this.editMode == 1 || xtaxmlFileItem.getFatals().size() > 0) {
						xtaxmlFileItem.setPlainText(this.xmlPlaintext.getText());
						plain = true;
			    	}
					this.host.exportXMLFile(xtaxmlFileItem, plain);
					this.unsavedChanges = false;
					
				    Platform.runLater(new Runnable() {
						
						@Override
						public void run() {
							host.analyzeItem(source.getItemId(), (XTAFolderResource) getRoot(source));
							XTAXMLFileItem newXtaxmlFileItem = (XTAXMLFileItem) host.getItemForId(source.getItemId());
							item.setUserObject(newXtaxmlFileItem);
							
							if(item.getId().equals(lastClickedID)) {
						    	handleXMLFileClicked(item, false, false, true);
								updateTitleIcon(newXtaxmlFileItem, (XTAFolderResource) getRoot(newXtaxmlFileItem));
							}
							
							xmlPane.setDisable(false);
							resourceTree.setDisable(false);
							resourceTree.setCursor(Cursor.DEFAULT);
						}
					});
					
			    	this.editMode = 0;
			    }
			    
			    this.showXMLActions();
			}
			else if(event.getSource() == this.workBenchSaveAction) {
				handleSaveXML(selected);
			}
			else if(event.getSource() == this.workBenchSaveAsAction || event.getSource() == this.createCopyMenu) {
			    try {
					TreeViewItem item = (TreeViewItem) selected.getValue();
					
					XTAXMLFileItem xml = (XTAXMLFileItem) item.getUserObject();
					
					TextInputDialog diag = new TextInputDialog(item.getName());
					diag.setHeaderText("Geben Sie einen Dateinamen ein");
					Optional<String> result = diag.showAndWait();
					if(result.isPresent()) {
					    boolean suc = this.host.duplicateFile(xml.getItemId(), result.get());
					    
					    if(!suc) {
					    	this.showMessageDialog("Aktion fehlgeschlagen", "Das Dublizieren der Datei war nicht erfolgreich", 0);
					    }
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if(event.getSource() == this.workBenchInstanceAction) {
			    try {
					TreeViewItem item = (TreeViewItem) selected.getValue();
					this.host.createInstance(item.getUserObject().getItemId(), null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if(event.getSource() == this.workBenchDeleteRepoEntryAction) {
			}
			else if(event.getSource() == this.xtaSenderAccountComboBox) {
				int index = this.xtaSenderAccountComboBox.getSelectionModel().getSelectedIndex();
				
				if(index >= 0) {
			    	ArrayList<String> ids = this.host.getResourceIds(XTAResourceType.XTA_ACCOUNT);
			    	ids.addAll(this.host.getResourceIds(XTAResourceType.REST_ENDPOINT));
					
			    	XTAPostOfficeBox box = (XTAPostOfficeBox) host.getResourcesForId(ids.get(index));
			    	ArrayList<String> openCases = box.getCaseIds();
			    	
			    	this.xtaCaseComboBox.getItems().clear();
	    			openCases.add(0, "Neuer Fall");
		    		this.xtaCaseComboBox.getItems().addAll(openCases);
		    		this.xtaCaseComboBox.getSelectionModel().select(0);
				}
			}
			else if(event.getSource() == this.xtaAttachmentsAdd) {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("Bitte waehlen Sie eine oder mehrere Dateien");
				fileChooser.setInitialDirectory(new File("."));
				List<File> files = fileChooser.showOpenMultipleDialog(stage);
				
				this.openXMLFile.setAttachments(new ArrayList<String>());
				for(File fl : files) {
					this.openXMLFile.getAttachments().add(fl.getAbsolutePath());
					this.xtaAttachmentsList.getChildren().add(new AttachmentItem(fl.getPath(), xtaAttachmentsList));
				}
			}
		}
	}

	private void handleSaveXML(TreeItem<TreeViewItem> selected) {
		xmlPane.setDisable(true);
		resourceTree.setDisable(true);
		resourceTree.setCursor(Cursor.WAIT);

		TreeViewItem item = (TreeViewItem) selected.getValue();
		XTATreeItemI source = item.getUserObject();
		
		if(item.getUserObject() instanceof XTAXMLFileItem) {
			if(currentEditor != null) {
				currentEditor.closeEditor();
			}
			
			XTAXMLFileItem xtaxmlFileItem = (XTAXMLFileItem) item.getUserObject();
			
			boolean plain = false;
			if(this.editMode == 1 || xtaxmlFileItem.getFatals().size() > 0) {
				xtaxmlFileItem.setPlainText(this.xmlPlaintext.getText());
				plain = true;
			}
			this.host.exportXMLFile(xtaxmlFileItem, plain);
			this.unsavedChanges = false;
			
		    Platform.runLater(new Runnable() {
				
				@Override
				public void run() {
					host.analyzeItem(source.getItemId(), (XTAFolderResource) getRoot(source));
					XTAXMLFileItem newXtaxmlFileItem = (XTAXMLFileItem) host.getItemForId(source.getItemId());
					item.setUserObject(newXtaxmlFileItem);
					
					if(item.getId().equals(lastClickedID)) {
				    	handleXMLFileClicked(item, true, editMode == 1, false);
						updateTitleIcon(newXtaxmlFileItem, (XTAFolderResource) getRoot(newXtaxmlFileItem));
					}
					
					xmlPane.setDisable(false);
					resourceTree.setDisable(false);
					resourceTree.setCursor(Cursor.DEFAULT);
				}
			});
		}
	}
	
	/**
	 * Initiates XML instance creation of all root elements that are covered by the current tree selection
	 * @param item	The current tree item
	 * @param folder	The target folder to store XML instances to
	 */
	private void createInstances(TreeViewItem item, String folder) {
		host.checkPath(this.getRoot(item.getUserObject()).getItemId(), folder);
		host.createInstance(item.getUserObject().getItemId(), folder);
	}

	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#resourceAdded(de.init.xbauleitplanung.xmlxtatool.model.XTAResource)
	 */
	@Override
	public synchronized void resourceAdded(XTAResource resource) {
		if(XTAResourceType.FOLDER.equals(resource.getRessourceType())) {
			TreeItem<TreeViewItem> item = new TreeItem<TreeViewItem>(TreeViewItem.createByUserObject(resource), new ImageView(this.folderResourceImage));
			
			if(!"hidden".equals(resource.getAdvice())) {
				this.allFolderResources.getChildren().add(item);
			}
			this.nodesForId.put(resource.getItemId(), item);
		}
		else if(XTAResourceType.XTA_ACCOUNT.equals(resource.getRessourceType())) {
			System.err.println("XTA-Account kommt rein");
			TreeItem<TreeViewItem> item = new TreeItem<TreeViewItem>(TreeViewItem.createByUserObject(resource), new ImageView(this.xtaAccountImage));
			this.allPostboxResources.getChildren().add(item);
			this.nodesForId.put(resource.getItemId(), item);
		}
		else if(XTAResourceType.REST_ENDPOINT.equals(resource.getRessourceType())) {
			System.err.println("FIT-Connect-Account kommt rein");
			TreeItem<TreeViewItem> item = new TreeItem<TreeViewItem>(TreeViewItem.createByUserObject(resource), new ImageView(this.fitkoAccountsImage));
			this.allRESTResources.getChildren().add(item);
			this.nodesForId.put(resource.getItemId(), item);
		}
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#resourceRemoved(java.lang.String)
	 */
	@Override
	public void resourceRemoved(String id) {
		TreeItem<TreeViewItem> item = this.nodesForId.get(id);
		TreeItem<TreeViewItem> parent = item.getParent();
		
		if(item != null && parent != null) {
			this.removeRecursive(item);
		}
	}
	
	/**
	 * Recursively remove some tree item and all of its children
	 * @param item	The tree item to be removed
	 */
	private void removeRecursive(TreeItem<TreeViewItem> item) {
		if(item.getChildren().size() != 0) {
			for(int i=item.getChildren().size() - 1;i>=0;i--) {
				TreeItem<TreeViewItem> child = item.getChildren().get(i);
				this.removeRecursive(child);
			}
		}
		
		item.getParent().getChildren().remove(item);
		this.nodesForId.remove(item.getValue().getUserObject().getItemId());
	}
	
	/**
	 * This method sets the receiver for the next message to send. If the receiver is not yet 
	 * contained in the Combobox it will be added. 
	 * @param receiver	the receiver for the next message to send
	 */
	public void setReceiver(String receiver) {
		if(!this.xtaReceiverAccountComboBox.getItems().contains(receiver)) {
			this.xtaReceiverAccountComboBox.getItems().add(receiver);
		}
		
		this.xtaReceiverAccountComboBox.getSelectionModel().select(receiver);
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#resourceUpdated(java.lang.String)
	 */
	@Override
	public void resourceUpdated(String id) {
		XTAFolderResource resource = (XTAFolderResource) this.host.getResourcesForId(id);
		
		if(resource != null) {
			TreeItem<TreeViewItem> item = this.nodesForId.get(id);
			if(item == null) {
				this.resourceAdded((XTAResource) resource);
				item = this.nodesForId.get(id);
			}
			
			final TreeItem<TreeViewItem> tmp = item;
			Platform.runLater(new Runnable() {
				
				@Override
				public void run() {
					for(int i=0;i<resource.getItemChildCount();i++) {
						XTATreeItemI child = resource.getItemChildAt(i);
						recurseItem(tmp, child);
					}
				}
			});
		}
	}
//
//	protected void buildCaseStructure(TreeItem<TreeViewItem> alt, XTAFolderResource resource, XTAFolderResourceProxy target) {
//		HashMap<String, HashMap<Long, XTAXMLFileItemProxy>> buffer = new HashMap<String, HashMap<Long,XTAXMLFileItemProxy>>();
//		surveyResourceForCases(target, buffer);
//		
//		for(String id : buffer.keySet()) {
//			HashMap<Long,XTAXMLFileItemProxy> item = buffer.get(id);
//			
//			XTAFileItem virtFolder = new XTAFileItem();
////			virtFolder.setFolder(true);
//			virtFolder.setPath("virt#" + target.getPath() + "/" + id);
//			virtFolder.setItemId("virt#" + target.getPath() + "/" + id);
//			target.addChild(virtFolder);
//			TreeItem<TreeViewItem> virtTreeItem = processItem(alt, virtFolder);
//			
//			for(Long tm : item.keySet()) {
//				XTAXMLFileItemProxy it = item.get(tm);
//				virtFolder.addChild(it);
//				
//				TreeItem<TreeViewItem> treeItem = processItem(virtTreeItem, it);
////				treeItem = new TreeItem<TreeViewItem>(TreeViewItem.createByUserObject(child), folder);
////				item.getChildren().add(i, treeItem);
//			}
//			virtTreeItem.setGraphic(getTreeIcon(virtFolder, target));
//		}
////		for(int i=0;i<resource.getItemChildCount();i++) {
////			XTATreeItemI child = resource.getItemChildAt(i);
////			
////			if(child instanceof XTAFileItem) {
////				XTAFileItemProxy proxy = new XTAFileItemProxy((XTAFileItem) child);
////				target.addChild(proxy);
////				TreeItem<TreeViewItem> treeItem = processItem(alt, proxy);
////			}
////		}
////		if(child != null && child.getItemId() != null) {
////			TreeItem<TreeViewItem> treeItem = processItem(item, child);
////			
////			if(!child.getItemType().equals("XMLFile")) {
////				for(int i=0;i<child.getItemChildCount();i++) {
////					this.recurseItem(treeItem, child.getItemChildAt(i));
////				}
////			}
////		}
//	}
//	
//	protected void surveyResourceForCases(XTATreeItemI file, HashMap<String, HashMap<Long, XTAXMLFileItemProxy>> buffer) {
//		if(file instanceof XTAFileItem) {
//			if("XMLFile".equals(file.getItemType())) {
//				String id = UUID.randomUUID().toString();
//				
//				if(file.getMetaData() != null) {
//					if(file.getMetaData().containsKey("caseId")) {
//						try {
//							id = file.getMetaData().get("caseId").toString();
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//				}
//				else {
//					
//				}
//				
//				if(!buffer.containsKey(id)) {
//					buffer.put(id, new HashMap<Long, XTAXMLFileItemProxy>());
//				}
//				
//				File fl = new File(((XTAFileItem) file).getPath());
//				long date = fl.lastModified();
//				XTAXMLFileItemProxy proxy = new XTAXMLFileItemProxy((XTAFileItem) file);
//				buffer.get(id).put(date, proxy);
//			}
//			else {
//				for(int i=0;i<file.getItemChildCount();i++) {
//					XTATreeItemI child = file.getItemChildAt(i);
//					if(child.getItemName().endsWith("_Attachments")) {
//						continue;
//					}
//					
//					surveyResourceForCases(child, buffer);
//				}
//			}
//			
//		}
//	}

	@Override
	public void itemAdded(XTATreeItemI item, XTATreeItemI parent) {
		try {
			TreeItem<TreeViewItem> pItem = this.nodesForId.get(parent.getItemId());
			if(pItem != null) {
				this.processItem(pItem, item);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void itemUpdated(XTATreeItemI item) {
		try {
			TreeItem<TreeViewItem> tItem = this.nodesForId.get(item.getItemId());
			if(tItem != null) {
				tItem.getValue().setUserObject(item);
				this.processItem(tItem.getParent(), item);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void itemRestructured(XTATreeItemI item) {
		try {
			TreeItem<TreeViewItem> tItem = this.nodesForId.get(item.getItemId());
			if(tItem != null) {
				TreeItem<TreeViewItem> tParent = tItem.getParent();
				
				this.removeRecursive(tItem);
				
				this.processItem(tParent, item);
				
				final TreeItem<TreeViewItem> tmp = tItem.getParent();
				Platform.runLater(new Runnable() {
					
					@Override
					public void run() {
						recurseItem(tmp, item);
					}
				});
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void recurseItem(TreeItem<TreeViewItem> item, XTATreeItemI child) {
		if(child != null && child.getItemId() != null) {
			TreeItem<TreeViewItem> treeItem = processItem(item, child);
			
			if(!child.getItemType().equals("XMLFile")) {
				for(int i=0;i<child.getItemChildCount();i++) {
					this.recurseItem(treeItem, child.getItemChildAt(i));
				}
			}
		}
	}
//
//	private void recurseSpecific(XTAFolderResourceProxy root, TreeItem<TreeViewItem> item, XTATreeItemI child) {
//		if(child != null && child.getItemId() != null) {
//			TreeItem<TreeViewItem> treeItem = ((XTASpecificTreeModel) root.getOrigin()).foldIn(child, item, root, getTreeIcon(child, root), this);
//			
//			for(int i=0;i<child.getItemChildCount();i++) {
//				this.recurseSpecific(root, treeItem, child.getItemChildAt(i));
//			}
//		}
//	}

	/**
	 * Central dispatching and processing method, called some entity (XML file, schema, element, ...) is 
	 * added to the file tree for further processing by the user. 
	 * @param target	The parent tree item to which a new child shall be added
	 * @param child	The entity item that shall be represented as a new tree item
	 * @return	The new tree child created to represent child
	 */
	private synchronized TreeItem<TreeViewItem> processItem(TreeItem<TreeViewItem> target, XTATreeItemI child) {
		try {
			XTAFolderResource root = (XTAFolderResource) this.getRoot(child);
			
			Comparator<XTATreeItemI> comparator = root.getComparator();
			if(comparator == null) {
				comparator = new Comparator<XTATreeItemI>() {
					
					@Override
					public int compare(XTATreeItemI o1, XTATreeItemI o2) {
						if(o1 instanceof XTAFileItem && o2 instanceof XTAFileItem) {
							return ((XTAFileItem) o1).getPath().compareTo(((XTAFileItem) o2).getPath());
						}
						else {
							return o1.getItemId().compareTo(o2.getItemId());
						}
					}
				};
			}
			
			ImageView folder = getTreeIcon(child, root);
			
			TreeItem<TreeViewItem> treeItem = null;
			if(!this.nodesForId.containsKey(child.getItemId()) || child.changes()) {
				treeItem = new TreeItem<TreeViewItem>(TreeViewItem.createByUserObject(child), folder);
				
				this.nodesForId.put(child.getItemId(), treeItem);
				if(child instanceof XTAFileItem) {
					boolean done = false;
					
					if(!"hidden".equals(child.getAdvice())) {
						// Element an der richtigen Stelle einsortieren, falls möglich
						for(int i=0;i<target.getChildren().size();i++) {
							try {
								XTAFileItem comp = (XTAFileItem) target.getChildren().get(i).getValue().getUserObject();
//								if(comp.getPath().compareTo(((XTAFileItem) child).getPath()) == 0) {
								int cmp = comparator.compare(comp, child);
								if(cmp == 0) {
									target.getChildren().set(i, treeItem);
									done = true;
									break;
								}
								else if(cmp > 0) {
									target.getChildren().add(i, treeItem);
									done = true;
									break;
								}
							} catch (Exception e) {
								e.printStackTrace();
								target.getChildren().add(i, treeItem);
								done = true;
								break;
							}
						}
					}
					
					if(!done) {
						target.getChildren().add(treeItem);
					}
				}
				else {
					target.getChildren().add(treeItem);
				}
			}
			else {
				treeItem = nodesForId.get(child.getItemId());
				
				final ImageView f = folder;
				
				Platform.runLater(new Runnable() {
				    @Override
				    public void run() {
				    	TreeItem<TreeViewItem> treeItem = nodesForId.get(child.getItemId());
				    	if(treeItem != null) {
							treeItem.setGraphic(f);
							treeItem.setValue(TreeViewItem.createByUserObject(child));
				    	}
				    }
				});
			}
			return treeItem;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(target);
			System.err.println(child);
			System.err.println(child.getItemName());
			return null;
		}
	}

	/**
	 * Determines and returns an appropriate Icon for the tree view depending on the type of child and root
	 * @param child	The child object to be displayed in the tree view
	 * @param root	The root object which contains child
	 * @return	an appropriate Icon for the tree view
	 */
	private ImageView getTreeIcon(XTATreeItemI child, XTAFolderResource root) {
		ImageView folder = new ImageView(this.folderImage);
		try {
			if("XCodelist".equals(child.getItemType())) {
				folder = new ImageView(this.codelistImage);
			}
			else if("XComplexType".equals(child.getItemType())) {
				folder = new ImageView(this.complextypeImage);
			}
			else if("XSimpleType".equals(child.getItemType())) {
				folder = new ImageView(this.simpletypeImage);
			}
			else if("XElement".equals(child.getItemType())) {
				folder = new ImageView(this.elementImage);
			}
			else if("FOLDER_RESOURCE".equals(child.getItemType())) {
				folder = new ImageView(this.folderResourceImage);
			}
			else if("XMLFile".equals(child.getItemType())) {
				XTAXMLFileItem chld = (XTAXMLFileItem) child;
				
				MessageMetaData md = root.getMetaData(chld.getPath());
				if(md != null) {
					if(md.getVia() == MessageMetaData.RECEIVED) {
						folder = new ImageView(this.xmlReceivedImage);
					}
					else if(md.getVia() == MessageMetaData.SENT) {
						folder = new ImageView(this.xmlSentImage);
					}
					else {
						folder = new ImageView(this.xmlImage);
					}
				}
				else {
					if(!root.validationDesired()) {
						folder = new ImageView(this.xmlImage);
					}
					else if(chld.getFatals().size() > 0) {
						folder = new ImageView(this.xmlFatalImage);
					}
					else if(chld.getWarnings().size() > 0) {
						folder = new ImageView(this.xmlWarnImage);
					}
					else if(!chld.isValidated()) {
						folder = new ImageView(this.xmlUnvalidatedImage);
					}
					else {
						folder = new ImageView(this.xmlImage);
					}
				}
			}
			else if("XSD_FILE_ITEM".equals(child.getItemType())) {
				folder = new ImageView(this.xsdImage);
			}
			else if("XTA_GenericodeList".equals(child.getItemType())) {
				folder = new ImageView(this.gcImage);
			}
			else if("REPOSITORY".equals(child.getItemType())) {
				folder = new ImageView(this.gcImage);
			}
			else if("FILE_ITEM".equals(child.getItemType())) {
				File check = new File(((XTAFileItem) child).getPath());
				if(!check.isDirectory()) {
					if(child.getItemChildCount() == 0) {
						folder = new ImageView(this.fileImage);
					}
				}
			}
		} catch (Exception e) {
			folder = new ImageView(this.fileImage);
		}
		
		folder.setFitWidth(16);
		folder.setFitHeight(16);
		
		return folder;
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#setProgressVisible(boolean)
	 */
	@Override
	public void setProgressVisible(boolean vis) {
		this.progress.setVisible(vis);
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#setProgressValue(int)
	 */
	@Override
	public void setProgressValue(int progress) {
		double val = progress / this.progressMax;
		this.setProgress(val);
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#setProgressMax(int)
	 */
	@Override
	public void setProgressMax(int max) {
		this.progressMax = max;
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#setProgress(double)
	 */
	@Override
	public void setProgress(double prog) {
		Platform.runLater(new Runnable(){

			@Override
			public void run() {
				progress.setProgress(prog);
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#setStatusText(java.lang.String)
	 */
	@Override
	public void setStatusText(String text) {
		Platform.runLater(new Runnable(){

			@Override
			public void run() {
				statusLabel.setText(text);
				observer.resetCounter();
			}
		});
	}
	
	@Override
	public void changed(ObservableValue<? extends TreeView<String>> observable, TreeView<String> oldValue,
			TreeView<String> newValue) {
	}

	/**
	 * Returns the current window X value of the left border
	 * @return	the current window X value of the left border
	 */
	public int getX() {
		return (int) this.stage.getX();
	}

	/**
	 * Returns the current window y value of the top border
	 * @return	the current window y value of the top border
	 */
	public int getY() {
		return (int) this.stage.getY();
	}

	/**
	 * Returns the current window width
	 * @return	the current window width
	 */
	public int getWidth() {
		return (int) this.stage.getWidth();
	}

	/**
	 * Returns the current window height
	 * @return	the current window height
	 */
	public int getHeight() {
		return (int) this.stage.getHeight();
	}

	@Override
	public void stop() throws Exception {
	    // Accept clicks only on node cells, and not on empty spaces of the TreeView
	    TreeItem<TreeViewItem> selectedItem = (TreeItem<TreeViewItem>) resourceTree.getSelectionModel().getSelectedItem();
	    boolean cont = true;
	    if(selectedItem != null) {
	        TreeViewItem item = (TreeViewItem) selectedItem.getValue();
	        
	        if(item != null) {
		        handleNodeSelected(item);
				cont = this.checkForUnsavedChanges(item);
	        }
	    }
	    
	    if(cont) {
			String source = System.getProperty("user.home") + "/xtaviewer/ui.ini";
			IniFileWriter wt= new IniFileWriter(source);
			
			try {
				wt.setDoubleValue("x", this.stage.getX());
				wt.setDoubleValue("y", this.stage.getY());
				wt.setDoubleValue("width", this.stage.getWidth());
				wt.setDoubleValue("height", this.stage.getHeight());
				wt.setBooleanValue("maximized", this.stage.isMaximized());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			wt.write();
			wt.close();
			
	    	this.host.exit();
			
			super.stop();
			
			System.exit(0);
	    }
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI#showMessageDialog(java.lang.String, java.lang.String, int)
	 */
	@Override
	public void showMessageDialog(String title, String message, int type) {
		// TODO: Looks terrible! Must be improved! 
		if(Platform.isFxApplicationThread()) {
			Alert.AlertType tp = Alert.AlertType.INFORMATION;
			
			if(type == 1) {
				tp = AlertType.WARNING;
			}
			if(type == 2) {
				tp = AlertType.ERROR;
			}
			if(type == 3) {
				tp = AlertType.CONFIRMATION;
			}
			if(type == 4) {
				tp = AlertType.NONE;
			}
			
			Alert alert = new Alert(tp);
			alert.setTitle(title);
			alert.setHeaderText(message);
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			stage.getIcons().add(this.appImage);
			
			Optional<ButtonType> obj = alert.showAndWait();
			if(obj != null) {
				// TODO: Handle null value of obj
				System.out.println(obj);
			}
		}
		else {
			Platform.runLater(new Thread(new Runnable() {
				
				@Override
				public void run() {
					Alert.AlertType tp = Alert.AlertType.INFORMATION;
					
					if(type == 1) {
						tp = AlertType.WARNING;
					}
					if(type == 2) {
						tp = AlertType.ERROR;
					}
					if(type == 3) {
						tp = AlertType.CONFIRMATION;
					}
					if(type == 4) {
						tp = AlertType.NONE;
					}
					
					Alert alert = new Alert(tp);
					alert.setTitle(title);
					alert.setContentText(message);
					
					Optional<ButtonType> obj = alert.showAndWait();
					if(obj != null) {
						System.out.println(obj);
					}
				}
			}));
		}
	}
	
	@Override
	public synchronized int showSelectDialog(String title, String message, List<String> options) {
		int ret = -1;
	
		OptionDialog optionDialog = new OptionDialog(title, message, options);
		
		if(Platform.isFxApplicationThread()) {
			optionDialog.run();
		}
		else {
			Platform.runLater(optionDialog);
		}
		
		while(optionDialog.isRunning()) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ret = optionDialog.getSelection();
	
		return ret;
	}
	
	@Override
	public synchronized int showConfrmDialog(String title, String message) {
		int ret = 0;
		
		Alert alert = new Alert(AlertType.CONFIRMATION, message, ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
		alert.setTitle(title);
		alert.showAndWait();

		if (alert.getResult() == ButtonType.YES) {
			ret = 1;
		}
		else if (alert.getResult() == ButtonType.NO) {
			ret = 2;
		}
		
		return ret;
	}
	
	/**
	 * Helper methods which seeks for the root element of some XTATreeItem
	 * @param item	The item whose root element shall be found
	 * @return	the root element of some XTATreeItem
	 */
	private XTATreeItemI getRoot(XTATreeItemI item) {
		try {
			if(item.getParent() != null) {
				return getRoot(item.getParent());
			}
			else {
				return item;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}

	@Override
	public void enable(String function, boolean enable) {
		if("xta-newfolder".equals(function)) {
			this.newFolder.setDisable(!enable);
		}
		else if("xta-remove".equals(function)) {
			this.removeButton.setDisable(!enable);
		}
		else if("xta-newxtaacount".equals(function)) {
			this.newXTAAccount.setDisable(!enable);
		}
		else if("xta-restaccount".equals(function)) {
			this.newRESTAccount.setDisable(!enable);
		}
	}

	@Override
	public void itemRemoved(XTATreeItemI item) {
		try {
			TreeItem<TreeViewItem> tItem = this.nodesForId.get(item.getItemId());
			if(tItem != null) {
				if(tItem.getParent() != null) {
					tItem.getParent().getChildren().remove(tItem);
					this.nodesForId.remove(item.getItemId());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This class displays an option dialog which lets a user chose among a set of options. 
	 * 
	 * @author treichling
	 *
	 */
	class OptionDialog implements Runnable{
		private List<String> options;
		private String title;
		private String message;
		private int selection = -1;
		private boolean running = true;
		
		public OptionDialog(String title, String message, List<String> options) {
			super();
			
			this.options = options;
			this.title = title;
			this.message = message;
		}
		
		@Override
		public void run() {
			ChoiceDialog<String> dialog = new ChoiceDialog<String>(options.get(0), options);
			
			dialog.setTitle(title);
			dialog.setHeaderText(message);
			dialog.setContentText(null);
			
			Optional<String> result = dialog.showAndWait();
			if(result.isPresent()) {
				String res = result.get();
				
				for(int i=0;i<options.size();i++) {
					if(options.get(i).equals(res)) {
						selection = i;
						break;
					}
				}
			}
			running = false;
		}

		public int getSelection() {
			return selection;
		}

		public boolean isRunning() {
			return running;
		}
	}
	
	/**
	 * This class represents an edit component for some basic value of an open XML file. Different types 
	 * are represented by different editor widgets (boolean, string, integer, code choices, ...)
	 * 
	 * @author treichling
	 *
	 */
	class NodeValueEditor extends HBox implements EventHandler<Event>{
		// 0 = undefined
		// 1 = codelist
		// 2 = boolean
		// 3 = number
		// 4 = number with fraction digits
		private int type = 0;
		
		private String target = "";
		private XNode node;
		private String buffer;
		private Label label;
		private TextArea editor;
		private ComboBox<SelectionItem> selectorEditor;
		private CheckBox booleanEditor;
		private EnterSpinner intEditor;
		private XCodelist codelist = null;
		
		private Label marker;
		
		private int min = Integer.MIN_VALUE;
		private int max = Integer.MAX_VALUE;
//		private int fractionDigits = Integer.MAX_VALUE;
//		private int totalDigits = Integer.MAX_VALUE;
		
		private NodeValueEditor last = null;
		private NodeValueEditor next = null;
		private boolean inited = false;

		private boolean jumpOnEnter = false;
		
		public NodeValueEditor(XNode node, String target, NodeValueEditor last) {
			super();
			
			this.setBorder(new Border(new BorderStroke(Color.BLACK, 
		            BorderStrokeStyle.DOTTED, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
			this.node = node;
			this.target = target;
			
			if(node.getCodelistType() != null) {
				this.type = 1;
			}
			else{
				if(node.getTypeNode() != null) {
					try {
						String typeName = node.getTypeNode().getElementTypeName();
						if("xs:boolean".equals(typeName)) {
							this.type = 2;
						}
						else if("xs:integer".equals(typeName) || 
								"xs:int".equals(typeName) || 
								"xs:decimal".equals(typeName) || 
								"xs:short".equals(typeName) || 
								"xs:long".equals(typeName) || 
								"xs:positiveInteger".equals(typeName) || 
								"xs:nonNegativeInteger".equals(typeName) || 
								"xs:nonPositoveInteger".equals(typeName)) {
							this.type = 3;
						}
						else {
							XSimpleType anonymousSimpleType = node.getTypeNode().getAnonymousSimpleType();
							if(anonymousSimpleType != null) {
								if("xs:integer".equals(anonymousSimpleType.getRestriction()) || "xs:int".equals(anonymousSimpleType.getRestriction())) {
									this.type = 3;
								}
								else if("xs:decimal".equals(anonymousSimpleType.getRestriction())) {
									try {
										this.type = 4;
										
//										int fd = Integer.parseInt(anonymousSimpleType.getFractionDigits());
//										fractionDigits = fd;
										
//										int td = Integer.parseInt(anonymousSimpleType.getTotalDigits());
//										totalDigits = td;
									} catch (Exception e) {
									}
								}
								else if("xs:short".equals(anonymousSimpleType.getRestriction())) {
									this.type = 3;
								}
								else if("xs:long".equals(anonymousSimpleType.getRestriction())) {
									this.type = 3;
								}
								else if("xs:positiveInteger".equals(anonymousSimpleType.getRestriction())) {
									this.type = 3;
									this.min = 1;
								}
								else if("xs:nonNegativeInteger".equals(anonymousSimpleType.getRestriction())) {
									this.type = 3;
									this.min = 0;
								}
								else if("xs:nonPositoveInteger".equals(anonymousSimpleType.getRestriction())) {
									this.type = 3;
									this.max = 0;
								}
								
								if(anonymousSimpleType.getMinInclusive() != null) {
									try {
										this.min = Integer.parseInt(anonymousSimpleType.getMinInclusive());
									} catch (Exception e) {
									}
								}
								if(anonymousSimpleType.getMaxInclusive() != null) {
									try {
										this.max = Integer.parseInt(anonymousSimpleType.getMaxInclusive());
									} catch (Exception e) {
									}
								}
							}
						}
					} catch (Exception e) {
					}
				}
			}
			
			this.label = new Label();
			this.label.setText(this.getValue());
			this.label.setCursor(Cursor.HAND);
			this.label.setFont(Font.font("arial", FontWeight.EXTRA_LIGHT, 16));
			this.label.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
			this.getChildren().add(this.label);
			
			if(last != null) {
				this.setLast(last);
				last.setNext(this);
			}
		}

		private String getValue() {
			String ret;
			
			if("value".equals(target)) {
				if(node.getValue() != null) {
					ret = node.getValue().trim();
				}
				else {
					ret = "";
				}
			}
			else {
				StringTokenizer tok = new StringTokenizer(target, "#");
				String key = tok.nextToken();
				String val = tok.nextToken();
				
				if("attribute".equals(key)) {
					ret = this.node.getAttributes().get(val);
				}
				else {
					ret = "<undefiniert>";
				}
			}
			
			return ret;
		}
		
		private void setValue(String value) {
			if(value == null || value.equals(this.getValue())) {
				return;
			}
			if("value".equals(target)) {
				node.setValue(value);
				
				String resolved = null;
				if(this.codelist != null) {
					for(int i=0;i<this.codelist.getValues().size();i++) {
						if(this.codelist.getValues().get(i).get(0).equals(value)) {
							resolved = resolveCode(value);
							node.setResolved(resolved);
							break;
						}
					}
				}
			}
			else {
				StringTokenizer tok = new StringTokenizer(target, "#");
				String key = tok.nextToken();
				String val = tok.nextToken();
				
				if("attribute".equals(key)) {
					this.node.getAttributes().put(val, value);
					
					// In case listURI or listVersionID attributes were just changed, we will now check
					// whether the entries make sense by getting the according codelist from XRepository
					if("listURI".equals(val) || "listVersionID".equals(val)) {
						
						XNode code = this.node.getChildForName("code");
						if(code != null) {
							if(code.getXoevType() > 2) {
								Thread t = new Thread(new Runnable() {
									
									@Override
									public void run() {
										String urn = node.getAttributes().get("listURI");
										String lv = node.getAttributes().get("listVersionID");
										
										setStatusText("Prüfe Validität: " + urn);
										
										boolean valid = checkValidity(urn + "_" + lv);
										
										ElementInfoBox box = (ElementInfoBox) getParent();
										if(valid) {
											box.setUnMarked();
//											marker.setVisible(false);
//											marker.setManaged(false);
										}
										else {
											box.setMarked("Bitte Eingabe prüfen!", 1);
//											marker.setVisible(true);
//											marker.setManaged(true);
										}
									}
								});
								t.start();
							}
						}
					}
				}
			}
		}
		
		private HashMap<String, Integer> workingOn = new HashMap<>();
		public boolean checkValidity(
				String urn) {
			if(workingOn.containsKey(urn)) {
				// If urn is right now beeing checked...
				while(workingOn.get(urn) == 0) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				return workingOn.get(urn) == 2;
			}
			
			try {
				workingOn.put(urn, 0);
				URL schemaFileURL = new URL("https://www.xrepository.de/api/xrepository/" + urn + ":technischerBestandteilGenericode");
				
				try {
					// Teste, ob URL umgeleitet wird und folge ggf. 
					
					BufferedReader in = new BufferedReader(new InputStreamReader(schemaFileURL.openStream()));

					String lf = System.getProperty("line.separator");
					StringBuilder build = new StringBuilder();
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						build.append(inputLine + lf);
					}
					in.close();
			        
					workingOn.put(urn, 2);
			        return true;
				} catch (Exception e) {
					workingOn.put(urn, 1);
			        return false;
				}
		    } catch (IOException e) {
				workingOn.put(urn, 1);
		        return false;
			}
		}
		
		private String resolveCode(String codeKey) {
			String resolved = "";
			if(codelist != null) {
				ArrayList<String> resolution = codelist.getRowForKey(codeKey);
				if(resolution != null) {
					for(int i=1;i<resolution.size();i++) {
						resolved = resolved + resolution.get(i) + ", ";
					}
					if(resolved.endsWith(", ")) {
						resolved = resolved.substring(0, resolved.length() - 2);
					}
				}
				else {
					resolved = "Code nicht vorhanden " + codeKey;
				}
			}
			
			return resolved;
		}
		
		private void initEditor() {
			if(this.inited ) {
				return;
			}
			this.inited = true;
			
			this.buffer = this.getValue();
			
			createEditor();
			createSelector();
			createBooleanEditor();
			createIntegerEditor();
			
			this.marker = new Label();
			this.marker.setText("Bitte Eingabe prüfen!");
			this.marker.setFont(Font.font("arial", FontWeight.EXTRA_LIGHT, 16));
			this.marker.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
			this.marker.setVisible(false);
			this.marker.setManaged(false);
			this.getChildren().add(this.marker);
		}

		private void createIntegerEditor() {
			this.intEditor = new EnterSpinner();
			this.intEditor.setVisible(false);
			this.intEditor.setManaged(false);
			this.intEditor.setEditable(true);
			
			SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(this.min, this.max, 0);
			this.intEditor.setValueFactory(valueFactory);
			this.intEditor.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					if(event.getSource() == intEditor) {
						KeyEvent key = (KeyEvent) event;
						
						if(KeyCode.TAB.equals(key.getCode())) {
				            intEditor.doCommit();
				            event.consume();
							closeEditor();
							
							setValue(String.valueOf(intEditor.getValue()));
							label.setText(getValue());
							
							if(key.isShiftDown()) {
								if(next != null) {
									last.initEditor();
									last.openEditor();
								}
								
								if(lastEditor != null && lastEditor != last) {
									lastEditor.closeEditor();
								}
								lastEditor = last;
							}
							else {
								if(next != null) {
									next.initEditor();
									next.openEditor();
								}
								
								if(lastEditor != null && lastEditor != next) {
									lastEditor.closeEditor();
								}
								lastEditor = next;
							}
						}
					}
				}
			});
			this.intEditor.valueProperty().addListener(new ChangeListener<Integer>() {

				@Override
				public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
					unsavedChanges = true;
				}
			});
			this.getChildren().add(this.intEditor);
		}

		private void createBooleanEditor() {
			this.booleanEditor = new CheckBox();
			this.booleanEditor.setVisible(false);
			this.booleanEditor.setManaged(false);
			this.booleanEditor.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					if(event.getSource() == booleanEditor) {
						unsavedChanges = true;
						
						KeyEvent key = (KeyEvent) event;
						
						if("Tab".equals(key.getCode().getName())) {
							closeEditor();
							
							setValue(String.valueOf(booleanEditor.isSelected()));
							label.setText(getValue());
							
							if(key.isShiftDown()) {
								if(next != null) {
									last.initEditor();
									last.openEditor();
								}
								
								if(lastEditor != null && lastEditor != last) {
									lastEditor.closeEditor();
								}
								lastEditor = last;
							}
							else {
								if(next != null) {
									next.initEditor();
									next.openEditor();
								}
								
								if(lastEditor != null && lastEditor != next) {
									lastEditor.closeEditor();
								}
								lastEditor = next;
							}
						}
					}
				}
			});
			this.getChildren().add(this.booleanEditor);
		}

		private void createSelector() {
			this.selectorEditor = new ComboBox<>();
			this.selectorEditor.setVisible(false);
			this.selectorEditor.setManaged(false);
			this.selectorEditor.valueProperty().addListener(new ChangeListener<SelectionItem>() {

				@Override
				public void changed(ObservableValue<? extends SelectionItem> observable, SelectionItem oldValue,
						SelectionItem newValue) {
					if(newValue != null) {
						node.setValue(newValue.getKey());
						editor.setText(newValue.getKey());
						setValue(editor.getText());
						label.setText(getValue());
					}
				}
			});
			this.selectorEditor.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					if(event.getSource() == selectorEditor) {
						KeyEvent key = (KeyEvent) event;
						
						if("Tab".equals(key.getCode().getName())) {
							closeEditor();
							
							setValue(editor.getText());
							label.setText(getValue());
							
							if(key.isShiftDown()) {
								if(next != null) {
									last.initEditor();
									last.openEditor();
								}
								
								if(lastEditor != null && lastEditor != last) {
									lastEditor.closeEditor();
								}
								lastEditor = last;
							}
							else {
								if(next != null) {
									next.initEditor();
									next.openEditor();
								}
								
								if(lastEditor != null && lastEditor != next) {
									lastEditor.closeEditor();
								}
								lastEditor = next;
							}
						}
					}
				}
			});
			this.getChildren().add(this.selectorEditor);
		}
		private void createEditor() {
			this.editor = new TextArea(this.getValue());
			this.editor.setFont(Font.font("arial", FontWeight.NORMAL, 14));
			this.editor.setVisible(false);
			this.editor.setManaged(false);
			this.editor.setMaxHeight(50);
			this.editor.setMaxWidth(300);
			this.editor.addEventHandler(KeyEvent.KEY_TYPED, this);
			this.editor.addEventHandler(KeyEvent.KEY_RELEASED, this);
			this.editor.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					if(event.getSource() == editor) {
						unsavedChanges = true;
						
						KeyEvent key = (KeyEvent) event;
						
						if(KeyCode.TAB.equals(key.getCode())) {
							closeEditor();
							
							setValue(editor.getText());
							label.setText(getValue());
							
							if(key.isShiftDown()) {
								if(next != null) {
									last.initEditor();
									last.openEditor();
								}
								
								if(lastEditor != null && lastEditor != last) {
									lastEditor.closeEditor();
								}
								lastEditor = last;
							}
							else {
								if(next != null) {
									next.initEditor();
									next.openEditor();
								}
								
								if(lastEditor != null && lastEditor != next) {
									lastEditor.closeEditor();
								}
								lastEditor = next;
							}
							
							event.consume();
						}
					}
				}
			});
			this.editor.focusedProperty().addListener(new ChangeListener<Boolean>() {
			    @SuppressWarnings("rawtypes")
				@Override
			    public void changed(ObservableValue ov, Boolean t, Boolean t1) {

			        Platform.runLater(new Runnable() {
			            @Override
			            public void run() {
			                if (editor.isFocused() && !editor.getText().isEmpty()) {
			                	editor.selectAll();
			                }
			            }
			        });
			    }
			});			
			this.getChildren().add(this.editor);
		}

		public XNode getNode() {
			return node;
		}

		public void setNode(XNode node) {
			this.node = node;
		}

		public Label getLabel() {
			return label;
		}

		public void setLabel(Label label) {
			this.label = label;
		}

		public TextArea getEditor() {
			return editor;
		}

		public void setEditor(TextArea editor) {
			this.editor = editor;
		}
		
		public void openEditor() {
			currentEditor = this;
			
			if(this.type == 0) {
				this.openTextEditor();
			}
			else if(this.type == 1) {
				this.openSelector();
			}
			else if(this.type == 2) {
				this.openCheckbox();
			}
			else if(this.type == 3) {
				this.openSpinner();
			}
			else if(this.type == 4) {
				this.openTextEditor();
			}
		}
		
		public void closeEditor() {
        	try {
				unsavedChanges = true;
				
				if(this.type == 0) {
					this.closeTextEditor();
				}
				else if(this.type == 1) {
					this.closeSelector();
				}
				else if(this.type == 2) {
					this.closeCheckbox();
				}
				else if(this.type == 3) {
					this.closeSpinner();
				}
				else if(this.type == 4) {
					this.closeTextEditor();
				}
				
				currentEditor = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void openTextEditor() {
			this.label.setVisible(false);
			this.label.setManaged(false);
			this.editor.setVisible(true);
			this.editor.setManaged(true);
			this.editor.requestFocus();
		}
		
		public void closeTextEditor() {
			this.label.setVisible(true);
			this.label.setManaged(true);
			this.editor.setVisible(false);
			this.editor.setManaged(false);
			
			this.setValue(this.editor.getText());
			this.label.setText(this.editor.getText());
		}
		
		public void openCheckbox() {
			this.label.setVisible(false);
			this.label.setManaged(false);
			this.booleanEditor.setVisible(true);
			this.booleanEditor.setManaged(true);
			
			this.booleanEditor.setSelected(Boolean.parseBoolean(this.getValue()));
		}
		
		public void closeCheckbox() {
			this.label.setVisible(true);
			this.label.setManaged(true);
			this.booleanEditor.setVisible(false);
			this.booleanEditor.setManaged(false);
			
			this.setValue(String.valueOf(this.booleanEditor.isSelected()));
			this.label.setText(String.valueOf(this.booleanEditor.isSelected()));
		}
		
		public void openSpinner() {
			this.label.setVisible(false);
			this.label.setManaged(false);
			this.intEditor.setVisible(true);
			this.intEditor.setManaged(true);
			
			if(this.getValue() != null) {
				try {
					this.intEditor.getValueFactory().setValue((int) Math.round(Double.parseDouble(this.getValue())));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else {
				this.intEditor.getValueFactory().setValue(0);
			}
		}
		
		public void closeSpinner() {
			this.label.setVisible(true);
			this.label.setManaged(true);
			this.intEditor.setVisible(false);
			this.intEditor.setManaged(false);
			
			this.setValue(String.valueOf(this.intEditor.getValue()));
			this.label.setText(String.valueOf(this.intEditor.getValue()));
		}
		
		public void openSelector() {
			if(this.codelist == null) {
				codelist = host.getCodelistForId(node.getSchemaId(), node.getCodelistType());
			}
			
			this.selectorEditor.getItems().clear();
			
			if(codelist != null) {
				int index = 0;
				
				for(int i=0;i<codelist.getValues().size();i++) {
					String key = codelist.getValues().get(i).get(0);
					
					if(key.equalsIgnoreCase(this.getValue())) {
						index = i;
					}
					
					String val = key + " - ";
					for(int j=1;j<codelist.getValues().get(i).size();j++) {
						val = val + codelist.getValues().get(i).get(j) + "; ";
					}
					if(val.endsWith("; ")) {
						val = val.substring(0, val.length() - 2);
					}
					if(val.length() > 100) {
						val = val.substring(0, 100) + "...";
					}
					
					SelectionItem sel = new SelectionItem();
					sel.setKey(key);
					sel.setValue(val);
					this.selectorEditor.getItems().add(sel);
				}
				
				if(index >= 0) {
					this.selectorEditor.getSelectionModel().select(index);
				}
				
				this.label.setVisible(false);
				this.label.setManaged(false);
				this.selectorEditor.setVisible(true);
				this.selectorEditor.setManaged(true);
			}
			else {
				this.label.setVisible(false);
				this.label.setManaged(false);
				this.editor.setVisible(true);
				this.editor.setManaged(true);
			}
		}
		
		public void closeSelector() {
			this.label.setVisible(true);
			this.label.setManaged(true);
			this.selectorEditor.setVisible(false);
			this.selectorEditor.setManaged(false);
			this.editor.setVisible(false);
			this.editor.setManaged(false);
		}

		@Override
		public void handle(Event event) {
			if(event.getEventType() == MouseEvent.MOUSE_CLICKED) {
				initEditor();
				
				if(event.getSource() == this.label) {
					if(lastEditor != null && lastEditor != this) {
						lastEditor.closeEditor();
					}
					lastEditor = this;
					
					openEditor();
					
					this.buffer = this.label.getText();
				}
			}
			else if(event.getSource() == this.editor && event.getEventType() == KeyEvent.KEY_RELEASED) {
				if(event.getSource() == this.editor) {
					KeyEvent key = (KeyEvent) event;
					
					if(jumpOnEnter  && "Enter".equals(key.getCode().getName())) {
						closeEditor();
						
						this.setValue(this.editor.getText());
						this.label.setText(this.getValue());
					}
					else if("Esc".equals(key.getCode().getName())) {
						closeEditor();
						
						this.editor.setText(this.buffer);
						this.label.setText(this.buffer);
					}
				}
			}
			else if(event.getSource() == this.editor && event.getEventType() == KeyEvent.KEY_TYPED) {
				if(event.getSource() == this.editor) {
					KeyEvent key = (KeyEvent) event;
					
					if(type == 4) {
						if(key.getCharacter().compareTo("0") >= 0 && key.getCharacter().compareTo("9") <= 0) {
							// GUT!
						}
						else if(key.getCharacter().equals(".")) {
							// GUT!
						}
						else if(key.getCharacter().equals("+")) {
							// GUT!
						}
						else if(key.getCharacter().equals("-")) {
							// GUT!
						}
						else {
							event.consume();
						}
					}
				}
			}
		}

		public NodeValueEditor getLast() {
			return last;
		}

		public void setLast(NodeValueEditor last) {
			this.last = last;
		}

		public NodeValueEditor getNext() {
			return next;
		}

		public void setNext(NodeValueEditor next) {
			this.next = next;
		}
	}
	
	/**
	 * Helper class used in NodeValueEditor
	 * 
	 * @author treichling
	 *
	 */
	class SelectionItem{
		String key;
		String value;
		
		public SelectionItem() {
			super();
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}
	
	/**
	 * Helper class for displaying attachments of XML messages
	 * 
	 * @author treichling
	 *
	 */
    class AttachmentItem extends BorderPane{
    	private String path;
    	private String name;
    	
    	private Label nameLabel;
    	private Label iconLabel;
    	private Label removeLabel;
    	
    	private AttachmentItem(String path, FlowPane parent) {
    		Insets insets = new Insets(2);
    		Insets sht = new Insets(2, 2, 2, 2);
    		Insets lng = new Insets(2, 22, 2, 2);
    		
    		this.path = path.replace("\\", "/");
    		int sep = this.path.lastIndexOf("/");
    		if(sep > 0) {
    			this.name = this.path.substring(sep + 1);
    		}
    		else {
    			this.name = new String(this.path);
    		}
    		
			ImageView file = new ImageView(fileImage);
			ImageView remove = new ImageView(removeImage);
    		
			this.nameLabel = new Label(this.name);
			this.nameLabel.setTooltip(new Tooltip(this.path));
			BorderPane.setMargin(nameLabel, lng);
			this.setCenter(this.nameLabel);
			
			this.iconLabel = new Label();
			this.iconLabel.setGraphic(file);
			BorderPane.setMargin(iconLabel, insets);
			this.setLeft(this.iconLabel);
			
			this.removeLabel = new Label();
			this.removeLabel.setGraphic(remove);
			this.removeLabel.setVisible(false);
			this.removeLabel.setManaged(false);
			this.removeLabel.setCursor(Cursor.HAND);
			BorderPane.setMargin(removeLabel, insets);
			this.setRight(this.removeLabel);
			
			final AttachmentItem myself = this;
			
			this.removeLabel.setOnMouseClicked(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					parent.getChildren().remove(myself);
				}
			});
			
			this.setOnMouseEntered(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					removeLabel.setVisible(true);
					removeLabel.setManaged(true);
					BorderPane.setMargin(nameLabel, sht);
				}
			});
			
			this.setOnMouseExited(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					removeLabel.setVisible(false);
					removeLabel.setManaged(false);
					BorderPane.setMargin(nameLabel, lng);
				}
			});
    	}
    }
    
    /**
     * Helper class used in NodeValueEditor for integer input
     * 
     * @author treichling
     *
     */
    class EnterSpinner extends Spinner<Integer>{
    	public void doCommit(){
    	    String text = this.getEditor().getText();
    	    SpinnerValueFactory<Integer> valueFactory = this.getValueFactory();
    	    if (valueFactory != null) {
    	        StringConverter<Integer> converter = valueFactory.getConverter();
    	        if (converter != null) {
    	            try{
    	                Integer value = converter.fromString(text);
    	                valueFactory.setValue(value);
    	            } catch(NumberFormatException nfe){
    	                this.getEditor().setText(converter.toString(valueFactory.getValue()));
    	            }
    	        }
    	    }
    	}    
    }
    
    /**
     * Helper class which represents a deamon process observing status text in order to clear the status text after a while
     * 
     * @author treichling
     *
     */
    class Observer implements Runnable{

    	private int counter = 0;
    	
		@Override
		public void run() {
			while(true) {
				try {
					Thread.sleep(1000);
					
					if(counter > 0) {
						counter--;
						
						if(counter == 0) {
							Platform.runLater(new Runnable(){

								@Override
								public void run() {
									statusLabel.setText("");
								}
							});
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		public void resetCounter() {
			this.counter = 8;
		}
		
		public void resetCounter(int val) {
			if(val > 0) {
				if(val <= 120) {
					this.counter = val;
				}
				else {
					this.counter = 120;
				}
			}
			else {
				this.counter = 8;
			}
		}
    }
	
    /**
     * A stream implementation that displays contents on a text area
     * @author treichling
     *
     */
	private class TextAreaStream extends OutputStream{
		private TextArea dest;

		public TextAreaStream(TextArea dest) {
			super();
			this.dest = dest;
		}
		
		@Override
		public void write(int b) throws IOException {
			write (new byte [] {(byte)b}, 0, 1);
		}

		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			final String text = new String (b, off, len);
			
			Platform.runLater(new Runnable() {
				
				@Override
				public void run() {
					dest.appendText(text);
				}
			});
		}
	}
	
	/**
	 * TreeCell implementation tha supports renaming tree items
	 * @author treichling
	 *
	 */
    private final class TextFieldTreeCellImpl extends TreeCell<TreeViewItem> {
    	 
        private TextField textField;
        private TreeViewItem item;
 
        public TextFieldTreeCellImpl() {
        	super();
        }
 
        @Override
        public void startEdit() {
            super.startEdit();
 
            if (textField == null) {
                createTextField();
            }
            setText(null);
            setGraphic(textField);
            textField.selectAll();
        }
 
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            setText(getItem().getName());
            setGraphic(getTreeItem().getGraphic());
        }
 
        @Override
        public void updateItem(TreeViewItem item, boolean empty) {
            super.updateItem(item, empty);
            
            this.item = item;
            
            try {
				if(item != null) {
				    XTATreeItemI userItem = item.getUserObject();
				    if(userItem != null && userItem instanceof XTAFolderResource) {
				        this.setTooltip(new Tooltip(((XTAFolderResource) userItem).getPath()));
				    }
				}
 
				if (empty) {
				    setText(null);
				    setGraphic(null);
				} else {
				    if (isEditing()) {
				        if (textField != null) {
				            textField.setText(getString());
				        }
				        setText(null);
				        setGraphic(textField);
				    } else {
				        setText(getString());
				        setGraphic(getTreeItem().getGraphic());
				    }
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
 
        private void createTextField() {
            textField = new TextField(getString());
            textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
 
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                    	commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                    t.consume();
                }
            });
        }
 
        protected void commitEdit(String text) {
        	host.copyItem(this.item.getId(), null, true, XTAController.ITERATE, text, null, true);
            this.item.setName(text);
        	super.commitEdit(this.item);
            setText(getItem().getName());
            setGraphic(getTreeItem().getGraphic());
		}

		private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
    
    class ElementInfoBox extends HBox{
    	private Tooltip tooltip;
		private NodeNameLabel nodeNameLabel;

		public ElementInfoBox() {
    		super();
    		
    		this.tooltip = new Tooltip();
    	}
    	
    	public void setNodeNameLabel(NodeNameLabel nodeNameLabel) {
    		this.nodeNameLabel = nodeNameLabel;
		}
    	
    	public NodeNameLabel getNodeNameLabel() {
			return nodeNameLabel;
		}

		public void setMarked(String text) {
			setMarked(text, 0);
		}

		public void setMarked(String text, int type) {
    		this.setBorder(new Border(new BorderStroke(Color.BLACK, 
    	            BorderStrokeStyle.DOTTED, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
    		if(type == 1) {
        		this.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
    		}
    		else {
        		this.setBackground(new Background(new BackgroundFill(Color.ORANGE, CornerRadii.EMPTY, Insets.EMPTY)));
    		}
    		tooltip.setText(text);
    		Tooltip.install(this, tooltip);
    	}
    	
    	public void setUnMarked() {
    		this.setBorder(null);
    		this.setBackground(new Background(new BackgroundFill(Color.web("#FFFFDD"), CornerRadii.EMPTY, Insets.EMPTY)));
    		Tooltip.uninstall(this, this.tooltip);
//    		this.setBackground(new Background(new BackgroundFill(Color.web("#F4F4F4"), CornerRadii.EMPTY, Insets.EMPTY)));
    	}
    }
    
    class Marker {
    	private int lineNumber;
    	private String message;
    	private ElementInfoBox box = null;
		
    	public Marker(int lineNumber, String message) {
			super();
			this.lineNumber = lineNumber;
			this.message = message;
		}

		public Marker() {
			super();
		}
		
    	public int getLineNumber() {
			return lineNumber;
		}
		public void setLineNumber(int lineNumber) {
			this.lineNumber = lineNumber;
		}
		public ElementInfoBox getBox() {
			return box;
		}
		public void setBox(ElementInfoBox box) {
			this.box = box;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
    }
    
    class NodeNameLabel extends Label {
    	private ElementInfoBox box = null;
		private XNode node;
		private VBox subBox;

		public NodeNameLabel(String name) {
			super(name);
			
			this.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					subBox.setVisible(!subBox.isVisible());
					subBox.setManaged(!subBox.isManaged());
					if(subBox.isVisible()) {
						openBranches.add(node.getPathId());
						
						ImageView minus = new ImageView(minusImage);
						minus.setFitHeight(10);
						minus.setFitWidth(10);
						setGraphic(minus);
					}
					else {
						openBranches.remove(node.getPathId());
						
						ImageView plus = new ImageView(plusImage);
						plus.setFitHeight(10);
						plus.setFitWidth(10);
						setGraphic(plus);
					}
				}
			});
		}

		public void setNode(XNode node) {
			this.node = node;
		}

		public void setSubBox(VBox subBox) {
			this.subBox = subBox;
		}

		public ElementInfoBox getBox() {
			return box;
		}

		public void setBox(ElementInfoBox box) {
			this.box = box;
		}
		
		public void makeVisible() {
			Parent p = this.box;
			while(p != null) {
				if(p.getParent() != null) {
					if("XMLContent".equals(p.getParent().getId())){
						int index = p.getParent().getChildrenUnmodifiable().indexOf(p);
						if(index == 1) {
							ElementInfoBox be = (ElementInfoBox) p.getParent().getChildrenUnmodifiable().get(0);
							be.getNodeNameLabel().makeVisible();
						}
					}
				}
				p = p.getParent();
			}
			if(subBox != null) {
				if(!subBox.isVisible()) {
					subBox.setVisible(true);
					subBox.setManaged(true);
					openBranches.add(node.getPathId());
					
					ImageView minus = new ImageView(minusImage);
					minus.setFitHeight(10);
					minus.setFitWidth(10);
					this.setGraphic(minus);
				}
			}
		}
    }
    
    class ArrowFactory implements IntFunction<Node> {
        private final ObservableValue<Integer> shownLine;

        ArrowFactory(ObservableValue<Integer> shownLine) {
            this.shownLine = shownLine;
        }

        @Override
        public Node apply(int lineNumber) {
            Polygon triangle = new Polygon(0.0, 0.0, 10.0, 5.0, 0.0, 10.0);
            triangle.setFill(Color.GREEN);

            ObservableValue<Boolean> visible = Val.map(
                    shownLine,
                    sl -> sl == lineNumber);

            triangle.visibleProperty().bind(((Val<Boolean>) visible).conditionOnShowing(triangle));

            return triangle;
        }
    }
    
    public static final ObservableList<Integer> olistValue = FXCollections.observableArrayList();
    public static final ListProperty<Integer> listValue = new SimpleListProperty<Integer>(olistValue);
    class MultiBreakPointFactory implements IntFunction<Node> {

        private final ListProperty<Integer> shownLines;

        public MultiBreakPointFactory(ListProperty<Integer> shownLine) {
            this.shownLines = shownLine;
        }
        @Override
        public Node apply(int lineIndex) {
            StackPane stackPane = new StackPane();
            Circle circle = new Circle(10.0, 10.0, 6.0, Color.RED);
            Rectangle rectangle = new Rectangle(20,20);
            rectangle.setFill(Color.TRANSPARENT);
            rectangle.setCursor(Cursor.HAND);
            rectangle.setOnMouseClicked(me->{
                if (!olistValue.contains(lineIndex+1)){
                    olistValue.add(lineIndex+1);
                }
            });
            stackPane.getChildren().addAll(rectangle, circle);
            circle.setOnMouseClicked(me->{
                int index = olistValue.indexOf(lineIndex+1);
                if (index>-1)
                    olistValue.remove(index);
            });
            circle.setCursor(Cursor.HAND);
            ObservableValue<Boolean> visible = Val.map(shownLines, sl -> sl.contains(lineIndex+1));

            circle.visibleProperty().bind(
                    Val.flatMap(circle.sceneProperty(), scene -> {
                        return scene != null ? visible : Val.constant(false);
                    }));

            return stackPane;
        }
    }
}
