package de.init.xbauleitplanung.xmlxtatool.model.schema.plain;

import java.io.Serializable;
import java.util.ArrayList;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;

/**
 * This class represents an attribute group and all of its properties (name, prefix, attributes or nested attribute groups)
 * 
 * @author treichling
 *
 */
public class XAttributeGroup extends XTATreeItem implements Serializable, XTATreeItemI{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7244415882745131392L;
	
	private String prefix;
	private String localName;
	
	private ArrayList<XAttribute> attributes = new ArrayList<>();
	private ArrayList<String> attributeGroups = new ArrayList<>();
	
	public String getName() {
		return this.prefix + ":" + this.getLocalName();
	}
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public XAttributeGroup() {
		super();
	}

	@Override
	public String getItemName() {
		return this.getName();
	}

	@Override
	public String getItemType() {
		return "XElementGroup";
	}

	@Override
	public int getItemChildCount() {
		return 0;
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		return null;
	}

	public String getFullyQualifiedName() {
		String ret = null;
		
		if(this.prefix != null) {
			if(this.localName != null) {
				ret = this.prefix + ":" + this.localName;
			}
		}
		else {
			if(this.localName != null) {
				ret = this.localName;
			}
		}
		
		return ret;
	}

	@Override
	public String toString() {
		try {
			return this.prefix + ":" + this.localName;
		} catch (Exception e) {
			e.printStackTrace();
			
			return this.localName;
		}
	}

	public ArrayList<XAttribute> getAttributes() {
		return attributes;
	}

	public void addAttribute(XAttribute att) {
		this.attributes.add(att);
	}

	public ArrayList<String> getAttributeGroups() {
		return attributeGroups;
	}
}
