package de.init.xbauleitplanung.xmlxtatool.model.schema.plain;

import java.io.Serializable;

/**
 * This class represents an XML attribute and its properties
 * 
 * @author treichling
 *
 */
public class XAttribute implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 261692559398920826L;

	private String prefix;
	private String name;
	private String type;
	private String use;
	private String fixed;
	private String form;
	
	public XAttribute() {
		super();
	}

	public String getName() {
		return name;
	}

	public String getFullyQualifiedName() {
		return prefix + ":" + name;
	}

	public void setName(String prefix, String name) {
		this.prefix = prefix;
		
		int sep = name.lastIndexOf(":");
		if(sep > 0) {
			this.name = name.substring(sep + 1);
		}
		else {
			this.name = name;
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUse() {
		return use;
	}

	public void setUse(String use) {
		this.use = use;
	}

	public String getFixed() {
		return fixed;
	}

	public void setFixed(String fixed) {
		this.fixed = fixed;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getPrefix() {
		return prefix;
	}

	/**
	 * Returns the standard attribute name, depending in its form
	 * @return	the standard attribute name, depending in its form
	 */
	public String getStandardAttributeName() {
		if(this.getForm() != null) {
			if("qualified".equals(this.getForm())) {
				return this.getFullyQualifiedName();
			}
			else {
				return this.getName();
			}
		}
		else {
			return this.getName();
		}
	}
}
