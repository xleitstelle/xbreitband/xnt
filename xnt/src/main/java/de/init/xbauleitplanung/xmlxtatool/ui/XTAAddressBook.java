package de.init.xbauleitplanung.xmlxtatool.ui;

import java.util.HashMap;

import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectReader;
import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class XTAAddressBook extends BorderPane {
	
	private Scene carrierScene;
	private Stage carrierStage;
	private XTAMainPanel carrier;
	
	private BorderPane leftBox;
//	private TextField search;
	private ListView<String> entriesList;
	private ObservableList<String> entries;
	
	private BorderPane details;
	private Label detailsTitleLabel;
	private VBox centerBox;
	private HBox detailsControlls;
	private Button saveButton;
	
	private HBox mainControlls;
	private Button addButton;
	private Button removeButton;
	private Button useButton;
	
	private String[] labels = {"Anzeigename", "Organisation", "Ansprechpartner", "Telefonnummer", "E-Mail-Adresse", "FIT-Connect-Destination"};
	private HashMap<String, HashMap<String, String>> values = new HashMap<String, HashMap<String,String>>();
	private HashMap<String, TextField> valueFields = new HashMap<String, TextField>();

	public XTAAddressBook() {
		super();
		
		this.read();
		
		this.leftBox = new BorderPane();
		this.leftBox.setPrefWidth(200);
		this.setLeft(this.leftBox);
		
//		this.search = new TextField();
//		this.search.setPromptText("Suchbegriff eingeben...");
//		BorderPane.setMargin(this.search, new Insets(2, 2, 2, 2));
//		this.leftBox.setTop(this.search);
//		
		this.entriesList = new ListView<String>();
		this.entriesList.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				String name = entriesList.getSelectionModel().getSelectedItem();
				
				if(name != null) {
					HashMap<String, String> val = values.get(name);
					
					if(val != null) {
						detailsTitleLabel.setText("Eigenschaften von " + name);
						
						for(String prop : val.keySet()) {
							valueFields.get(prop).setText(val.get(prop));
						}
						
						removeButton.setDisable(false);
					}
				}
				
			}
		});
		BorderPane.setMargin(this.entriesList, new Insets(2, 2, 2, 2));
		this.leftBox.setCenter(this.entriesList);
		
		this.entries = FXCollections.observableArrayList();
		entriesList.setItems(entries);
		
		for(String name : this.values.keySet()) {
			this.entries.add(name);
		}
		
		this.details = new BorderPane();
		BorderPane.setMargin(this.details, new Insets(2, 2, 2, 2));
		this.setCenter(this.details);
		
		this.detailsTitleLabel = new Label("Eigenschaften");
		this.detailsTitleLabel.setFont(Font.font(this.detailsTitleLabel.getFont().getName(), FontWeight.BOLD, this.detailsTitleLabel.getFont().getSize() * 1.5));
		BorderPane.setMargin(this.detailsTitleLabel, new Insets(2, 2, 2, 2));
		this.details.setTop(this.detailsTitleLabel);
		
		this.centerBox = new VBox();
		BorderPane.setMargin(this.centerBox, new Insets(5, 5, 5, 5));
		this.details.setCenter(this.centerBox);
		
		this.detailsControlls = new HBox();
		this.details.setBottom(this.detailsControlls);
		
		this.saveButton = new Button("Speichern");
		this.saveButton.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				String name = valueFields.get("Anzeigename").getText();
				HashMap<String, String> newEntry = new HashMap<String, String>();
				for(int i=0;i<labels.length;i++) {
					newEntry.put(labels[i], valueFields.get(labels[i]).getText());
				}
				
				String selected = entriesList.getSelectionModel().getSelectedItem();
				if(!selected.equals(name)) {
					values.remove(selected);
					entries.remove(selected);
					entries.add(name);
				}
				values.put(name, newEntry);
				
				write();
			}
		});
		this.detailsControlls.getChildren().add(saveButton);
		
		this.mainControlls = new HBox();
		BorderPane.setMargin(this.mainControlls, new Insets(2, 2, 2, 2));
		this.setBottom(this.mainControlls);
		
		this.addButton = new Button("Neuer Eintrag");
		this.addButton.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				clearFields();
				String name = "Neuer Eintrag";
				String chosen = collision(name);
				
				entries.add(chosen);
				valueFields.get("Anzeigename").setText(chosen);
				
				entriesList.getSelectionModel().select(chosen);
			}
		});
		HBox.setMargin(this.addButton, new Insets(2, 2, 2, 2));
		this.mainControlls.getChildren().add(this.addButton);
		
		this.removeButton = new Button("Eintrag löschen");
		this.removeButton.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				String name = entriesList.getSelectionModel().getSelectedItem();
				
				if(name != null) {
					entries.remove(name);
					values.remove(name);
					
					write();
					
					if(entriesList.getSelectionModel().getSelectedItem() == null) {
						removeButton.setDisable(true);
					}
				}
			}
		});
		HBox.setMargin(this.removeButton, new Insets(2, 2, 2, 2));
		this.removeButton.setDisable(true);
		this.mainControlls.getChildren().add(this.removeButton);
		
		this.useButton = new Button("Eintrag verwenden");
		this.useButton.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				if(carrierStage != null) {
					String name = entriesList.getSelectionModel().getSelectedItem();
					
					if(name != null) {
						carrier.setReceiver(values.get(name).get("FIT-Connect-Destination"));
						carrierStage.hide();
					}
				}
			}
		});
		HBox.setMargin(this.useButton, new Insets(2, 2, 2, 2));
		this.mainControlls.getChildren().add(this.useButton);
		
		for(int i=0;i<labels.length;i++) {
			HBox carrier = new HBox();
			VBox.setMargin(carrier, new Insets(2,  2,  2,  2));
			this.centerBox.getChildren().add(carrier);
			
			Label title = new Label(labels[i]);
			title.setPrefWidth(150);
			carrier.getChildren().add(title);
			
			TextField valueField = new TextField();
			valueField.setPrefWidth(250);
			carrier.getChildren().add(valueField);
			
			this.valueFields.put(labels[i], valueField);
		}
	}
	
	public int addEntry(String name, String orga, String contact, String phone, String mail, String dest) {
		HashMap<String, String> data = new HashMap<String, String>();
		
		data.put("Anzeigename", name);
		data.put("Organisation", orga);
		data.put("Ansprechpartner", contact);
		data.put("Telefonnummer", phone);
		data.put("E-Mail-Adresse", mail);
		data.put("FIT-Connect-Destination", dest);
		
		return addEntry(data);
	}
	
	public int addEntry(HashMap<String, String> newEntry) {
		int ret = 2;
		String name = newEntry.get("Anzeigename");
		
		if(name != null) {
			ret = 1;
			String choosen = collision(name);
			
			if(name.equals(choosen)) {
				entries.add(name);
				values.put(name, newEntry);
				
				write();
				
				ret = 0;
			}
		}
		
		return ret;
	}
	
	private void read() {
		// let source be the actual home folder
		String source = System.getProperty("user.home") + "/xtaviewer";
		
		this.values = (HashMap<String, HashMap<String, String>>) ObjectReader.readObject(source + "/" + "addressBook.dat");
		
		if(this.values == null) {
			this.values = new HashMap<String, HashMap<String,String>>();
		}
	}
	
	private void write() {
		// let source be the actual home folder
		String source = System.getProperty("user.home") + "/xtaviewer";
		
		ObjectWriter.writeObject(this.values, source + "/" + "addressBook.dat");
	}
	
	private String collision(String name) {
		String chosen = new String(name);
		int ext = 1;
		
		while(true) {
			boolean collision = false;
			for(String nm : this.values.keySet()) {
				if(nm.contentEquals(chosen)) {
					chosen = name + "_" + ext;
					collision = true;
					break;
				}
			}
			if(!collision) {
				break;
			}
		}
		
		return chosen;
	}
	
	private void clearFields() {
		for(int i=0;i<labels.length;i++) {
			TextField valueField = this.valueFields.get(labels[i]);
			valueField.setText("");
		}
	}

	public Scene getCarrierScene() {
		return carrierScene;
	}

	public void setCarrierScene(Scene carrierScene) {
		this.carrierScene = carrierScene;
	}

	public Stage getCarrierStage() {
		return carrierStage;
	}

	public void setCarrierStage(Stage carrierStage) {
		this.carrierStage = carrierStage;
	}

	public XTAMainPanel getCarrier() {
		return carrier;
	}

	public void setCarrier(XTAMainPanel carrier) {
		this.carrier = carrier;
	}

}
