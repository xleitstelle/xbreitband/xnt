package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.Serializable;

public enum XTAResourceType implements Serializable {
	FOLDER, 
	XTA_ACCOUNT, 
	REST_ENDPOINT
}
