package de.init.xbauleitplanung.xmlxtatool.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.mifmif.common.regex.Generex;

import de.init.xbauleitplanung.xmlxtatool.model.XTASchemaAnalyzer;
import de.init.xbauleitplanung.xmlxtatool.model.schema.Import;
import de.init.xbauleitplanung.xmlxtatool.model.schema.Include;
import de.init.xbauleitplanung.xmlxtatool.model.schema.ListVersionEntry;
import de.init.xbauleitplanung.xmlxtatool.model.schema.NameSpace;
import de.init.xbauleitplanung.xmlxtatool.model.schema.ReferenceRegister;
import de.init.xbauleitplanung.xmlxtatool.model.schema.WorkLeft;
import de.init.xbauleitplanung.xmlxtatool.model.schema.hierarchical.XModelTreeNode;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XAttribute;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XCodelist;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XComplexType;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElement;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElementGroup;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElementProxy;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XSimpleType;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFITConnectPostOfficeBox;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFolderResource;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAGenericodeList;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAPostOfficeBox;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAResource;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAResourceType;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAViewerSettings;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAXMLFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.xml.XNode;
import de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI;
import de.init.xbauleitplanung.xmlxtatool.ui.XTAMainPanel;
import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectReader;
import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectWriter;

public class XTAController implements XTAServerI {
	// Reference to the front end instance
	private XTAClientI ui;
	// Home directory (will be read from system properties id not specified)
	private String home;

	// For debug purposes: set to true to get messages of many processes
	private boolean debug = false;
	
	// All relevant settings. Will be persisted on shutdown and loaded on startup
	private XTAViewerSettings settings;
	
	// File hashes: Are used to identify identical copies of files that were already analyzed. Copies will be 
	// analyzed only once
	private TreeMap<String, XTATreeItemI> hashesDone = new TreeMap<>();
	
	// Array containing all XML messages to be analyzed. All messages will be analyzed cumulative *after* all 
	// schemas are analyzed and codelists are loaded, cached and analyzed
	private ArrayList<MessageToDoItem> messagesToDo = new ArrayList<>();
	
	// Number of currently running analysis processes
	private int nodeThreads  = 0;
	// Maximal total number of parallel running analysis processes. Parallel processing shall speed up analysis 
	// (default is 1 - no parallel processing)
	private int maxNodeThreads = 1;
	
	// Number of already analyzed XML files
	private int currentCount = 0;
	// Expected total number of XML files
	private int targetCount = 0;
	
	// Register of all elements within the logical hierarchy of elements (resources, folder, XML files and -schemata, 
	// and contained constructs (elements, attributes, types, ...)
	private TreeMap<String, XTATreeItemI> itemsForId = new TreeMap<>();
	
	// Reference on an instance of SchemaAnalyzer (which does analys work)
	private XTASchemaAnalyzer schemaAnalyzer;
	
	// Set of listeners that will be triggered for debug purposes
	private TreeMap<String, ExpectationListener> expectaionListeners = new TreeMap<String, ExpectationListener>();
	
	// True when a creating processes is in progress
	private boolean creatingInProgress = false;
	
	// Indicates whether optional (sub) elements shall be created when an instance of an XML element is created
	private int optionalElementPolicy = 0;
	// Indicates whether optional attributes shall be created when an instance of an XML element is created
	private int optionalAttributePolicy = 0;
	// Indicates whether a user shall be asked for a choice in case a choice element is created
	private int askUserForChoiceOption = 0;
	// Indicates whether optional xs:any elements shall be created when an instance of an XML element is created
	private int optionalAnyPolicy = 0;
	
	// Counter for IDs. Guarantees for unique IDs since it gets incremented after each use
	private int idCount = 0;
	
	// Sets for used ids and idrefs within one XML file in order to make sure, for each id / idref there will be a counterpart
	private TreeSet<String> idStore = new TreeSet<String>();
	private TreeSet<String> idRefStore = new TreeSet<String>();
	
	// are unsaved changes present? 
	private boolean changes = false;
	
	/**
	 * Constructs a new XTAController (controller part of the MVC pattern)
	 * @param ui	the UI component (UI part of the MVC pattern)
	 */
	public XTAController(XTAMainPanel ui) {
		this(ui, null);
	}
	
	/**
	 * Constructs a new XTAController (controller part of the MVC pattern)
	 * @param ui	the UI component (UI part of the MVC pattern)
	 * @param hime	specifies the home folder (user directory) - if null it will be read from the system properties of the OS
	 */
	public XTAController(XTAClientI ui, String home) {
		try {
			System.setProperty("javax.xml.accessExternalDTD", "file,http");
			
			this.ui = ui;
			
			this.ui.enable("xta-newfolder", false);
			this.ui.enable("xta-newxtaacount", false);
			this.ui.enable("xta-restaccount", false);
			this.ui.enable("xta-remove", false);
			
			this.home = home;
			
			// let source be the actual home folder
			String source = System.getProperty("user.home") + "/xtaviewer";
			
			if(this.home != null) {
				source = this.home;
			}
			
			this.ui.setStatusText("Prüfe Ornderstruktur...");
			File check = new File(source);
			if(!check.exists()) {
				check.mkdirs();
			}
			
			// Create schema analyzer component
			this.schemaAnalyzer = new XTASchemaAnalyzer(ui, source);
			
			// Loading settings (existing folder resources or XTA accounts including all XML schemas and instances)
			this.ui.setStatusText("Lade Settings...");
			LogManager.getRootLogger().debug("Lade Settings...");
			this.settings = (XTAViewerSettings) ObjectReader.readObject(source + "/settings.dat");
			this.ui.setStatusText("Settings gelesen!");
			if(this.settings == null) {
				this.settings = new XTAViewerSettings();
			}
			
			// specify default settings in case of missing settings
			if(!this.settings.getProperties().containsKey("optionalElementPolicy")) {
				this.settings.getProperties().put("optionalElementPolicy", "0");
			}
			if(!this.settings.getProperties().containsKey("optionalAttributePolicy")) {
				this.settings.getProperties().put("optionalAttributePolicy", "0");
			}
			if(!this.settings.getProperties().containsKey("booleanDefault")) {
				this.settings.getProperties().put("booleanDefault", "0");
			}
			if(!this.settings.getProperties().containsKey("askUserForChoiceOption")) {
				this.settings.getProperties().put("askUserForChoiceOption", "0");
			}
			if(!this.settings.getProperties().containsKey("optionalAnyPolicy")) {
				this.settings.getProperties().put("optionalAnyPolicy", "1");
			}
			if(!this.settings.getProperties().containsKey("expandNodesTo")) {
				this.settings.getProperties().put("expandNodesTo", "0");
			}
			
			this.ui.setStatusText("fertig");
			LogManager.getRootLogger().debug("OK");
			
			// initialize all XTA accounts...
			this.ui.setStatusText("initialisiere Postfächer...");
			for(String rk : this.settings.getResources().keySet()) {
				XTAResource rec = this.settings.getResources().get(rk);
				
				if(rec instanceof XTAPostOfficeBox) {
					((XTAPostOfficeBox) rec).setHost(this);
					((XTAPostOfficeBox) rec).setStop(false);
				}
				
				rec.init();
			}
			this.ui.setStatusText("fertig");
			
			// initialization of the entire content of all resources will be done in a dedicated thread since it may take 
			// some time to reload and validate all XML files
			// The type model of the resources may become very large. So not all of its contents will be persisted since 
			// it would make the starting process take much longer. Instead, the type model of each resource will be rebuild 
			// from scratch at startup which turned out to be faster - will potentially be changed in the future when faster 
			// object persistence methods (the object serialization) will be applied. 
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					ui.setProgressVisible(true);
					ui.setWorkingStatus(true);
					
					ui.enable("xta-newfolder", true);
					ui.enable("xta-newxtaacount", true);
					ui.enable("xta-restaccount", true);
					
					for(String root : settings.getResources().keySet()) {
						XTAFolderResource newRec = (XTAFolderResource) settings.getResources().get(root);
						ui.setStatusText("initialisiere " + newRec.getItemName());
						
						// Create a root node which will keep the entire type hierarchy in its sub nodes (processed in 
						// the next step)
						newRec.setRootNode(new XModelTreeNode());
						
						// Create a complete hierarchical model (type tree) from the XSD files and codelists analyzed above
						for(XElement rootEl : newRec.getRootElements()) {
							ArrayList<String> typesInChain = new ArrayList<>();
							TreeMap<String, XModelTreeNode> typeRegister = new TreeMap<String, XModelTreeNode>();
							LogManager.getRootLogger().debug(rootEl);
							
							XTASchemaFileItem schema = rootEl.getRootSchema();
							XModelTreeNode child = schemaAnalyzer.recurseElement(newRec.getRootNode(), rootEl, 0, typesInChain, typeRegister, schema, false);
							child.setForm("qualified");
						}
						weighten(newRec, newRec);
					}
					
					ui.setWorkingStatus(false);
					ui.setProgressVisible(false);
					ui.setStatusText("Bereit");
					ui.enable("xta-remove", true);
				}
			});
			t.start();
			
			// Create an item index for all resources - the item index covers ALL XML files and XML constructs
			// contained (types, elements, groups, ...) which can be addressed by a unique identifier
			rescanItemIndex();
			
			// If debug is true output the contents of all resources
			if(this.debug) {
				// Zu Debug-Zwecken: Baumstruktur komplett ausgeben
				synchronized (this) {
					ArrayList<String> resourceIds = this.getResourceIds(XTAResourceType.FOLDER);
					for(int i=0;i<resourceIds.size();i++) {
						XTAFolderResource resource = (XTAFolderResource) this.getResourcesForId(resourceIds.get(i));
						LogManager.getRootLogger().debug("**** " + resource.getItemName());
						this.printXTATreeItem(resource, 0);
						
						LogManager.getRootLogger().debug("XXXX " + resource.getItemName());
						this.printXModelTreeNode(resource.getRootNode(), 0);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public XNode createInstance(XModelTreeNode template, XTAFolderResource res, XNode parent) {
		try {
			XTASchemaFileItem schema = (XTASchemaFileItem) res.getDescendantForPath(template.getSchemaLocation());
			
			// Buffer fuer die in der zu erzeugenden Nachricht verwendeten Namespaces. Diese muessen alle deklariert sein
			TreeSet<String> usedNamespaces = new TreeSet<>();
			TreeSet<NameSpace> namespaces = schema.recurseAllNamespaces(); //getNamespaces();
			for(NameSpace sp : namespaces) {
				usedNamespaces.add(sp.getPrefix() + ":" + sp.getNamespaceURL());
			}
			
			// Standard Namespace wird hinzugefuegt
			usedNamespaces.add("xsi");
			
			int depth = 0;
			XModelTreeNode cur = template;
			while(cur.getParent() != null) {
				depth++;
				cur = cur.getParent();
			}
			
			depth = depth - 1;
			if(depth < 0) {
				depth = 0;
			}
			
			XNode xDocument = new XNode();
			
			this.askUserForChoiceOption = Integer.parseInt(this.settings.getProperties().get("askUserForChoiceOption").toString());
			
			// ID und IDREF zurücksetzen
			this.idStore.clear();
			this.idRefStore.clear();
			
			// Hier findet die eigentliche Erzeugung von Model nach Instanz statt!
			this.recurseXSD(
					(XTAFolderResource) res, 
					template, 
					schema, 
					xDocument, 
					usedNamespaces, 
					schema.getDefaultElementForm(), 
					depth);
			
			XNode child = xDocument.getItemChildAt(0);
			child.setTypeNode(template);
			
			if(parent != null) {
				TreeMap<String, Integer> childReg = new TreeMap<String, Integer>();
				ArrayList<XModelTreeNode> childList = new ArrayList<XModelTreeNode>();
				
				TreeMap<String, Integer> positions = this.schemaAnalyzer.analyzeNode(parent, childReg, childList);
				
				parent.insertChild(child, positions);
			}
			
			// Suche nach dem obersten Eltern-Element (XML-Nachricht). Dort müssen ggf. Namespaces ergänzt werden, nachdem 
			// oben ein Element eingefügt wurde
			XNode doc = child;
			while(true) {
				if(doc.getParent() == null) {
					break;
				}
				else if(doc.getParent() instanceof XTAXMLFileItem) {
					break;
				}
				doc = (XNode) doc.getParent();
			}
			
			// Buffer table of namespaces to add
			TreeMap<String, String> namespaceBuffer = new TreeMap<>();
			
			// Table of reverse namespaces
			TreeMap<String, String> reverseNamespaces = new TreeMap<>();
			
			// Namespace / Prefix-Attribute setzen
			TreeSet<String> nsDone = new TreeSet<>();
			for(NameSpace namespace : namespaces) {
				if(nsDone.contains(namespace.getPrefix())) {
					continue;
				}
				
				String prefix = namespace.getPrefix();
				namespaceBuffer.put(prefix, namespace.getNamespaceURL());
				
				if(!reverseNamespaces.containsKey(namespace.getNamespaceURL())) {
					reverseNamespaces.put(namespace.getNamespaceURL(), prefix);
				}
				else {
					if(!isDummyPrefix(prefix)) {
						namespaceBuffer.remove(reverseNamespaces.get(namespace.getNamespaceURL()));
						reverseNamespaces.put(namespace.getNamespaceURL(), prefix);
					}
				}
				
				nsDone.add(namespace.getPrefix());
			}
			
			for(String ns : usedNamespaces) {
				String prefix = null;
				String namespaceURL = null;
				NameSpace namespace = null;
				
				int sep = ns.indexOf(":");
				if(sep > 0) {
					prefix = ns.substring(0, sep);
					namespaceURL = ns.substring(sep + 1);
					
					if(namespaceURL.length() > 0) {
						namespace = new NameSpace(prefix, namespaceURL);
					}
				}
				
				if(namespace != null) {
					if(nsDone.contains(prefix)) {
						continue;
					}
					
					namespaceBuffer.put(prefix, namespace.getNamespaceURL());
					
					if(!reverseNamespaces.containsKey(namespace.getNamespaceURL())) {
						reverseNamespaces.put(namespace.getNamespaceURL(), prefix);
					}
					else {
						if(!isDummyPrefix(prefix)) {
							namespaceBuffer.remove(reverseNamespaces.get(namespace.getNamespaceURL()));
							reverseNamespaces.put(namespace.getNamespaceURL(), prefix);
						}
					}
					
					nsDone.add(prefix);
				}
			}
			
			for(String prefix : namespaceBuffer.keySet()) {
				String url = namespaceBuffer.get(prefix);
				
				if(isDummyPrefix(prefix)) {
					xDocument.getAttributes().put("xmlns", url);
				}
				else {
					xDocument.getAttributes().put("xmlns:" + prefix, url);
				}
			}
			
			return child;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			
			return null;
		}
	}

	/**
	 * Clean the central item index and rescan and index all XML content of all resources 
	 */
	private void rescanItemIndex() {
		this.itemsForId.clear();
		
		ArrayList<String> folderResourceIds = this.getResourceIds(XTAResourceType.FOLDER);
		for(int i=0;i<folderResourceIds.size();i++) {
			XTATreeItemI item = this.getResourcesForId(folderResourceIds.get(i));
			recurseItem(item);
		}
		
		ArrayList<String> xtaAccountResourceIds = this.getResourceIds(XTAResourceType.XTA_ACCOUNT);
		for(int i=0;i<xtaAccountResourceIds.size();i++) {
			XTATreeItemI item = this.getResourcesForId(xtaAccountResourceIds.get(i));
			recurseItem(item);
		}
		
		ArrayList<String> restAccountResourceIds = this.getResourceIds(XTAResourceType.REST_ENDPOINT);
		for(int i=0;i<restAccountResourceIds.size();i++) {
			XTATreeItemI item = this.getResourcesForId(restAccountResourceIds.get(i));
			recurseItem(item);
		}
	}

	/**
	 * Rescan and index all XML content of all resources 
	 */
	private void updateItemIndex() {
		ArrayList<String> folderResourceIds = this.getResourceIds(XTAResourceType.FOLDER);
		for(int i=0;i<folderResourceIds.size();i++) {
			XTATreeItemI item = this.getResourcesForId(folderResourceIds.get(i));
			recurseItem(item);
		}
		
		ArrayList<String> xtaAccountResourceIds = this.getResourceIds(XTAResourceType.XTA_ACCOUNT);
		for(int i=0;i<xtaAccountResourceIds.size();i++) {
			XTATreeItemI item = this.getResourcesForId(xtaAccountResourceIds.get(i));
			recurseItem(item);
		}
		
		ArrayList<String> restAccountResourceIds = this.getResourceIds(XTAResourceType.REST_ENDPOINT);
		for(int i=0;i<restAccountResourceIds.size();i++) {
			XTATreeItemI item = this.getResourcesForId(restAccountResourceIds.get(i));
			recurseItem(item);
		}
	}
	
	/**
	 * Recursive method to create or update the central item index - called by rescanItemIndex() or updateItemIndex()
	 * @param item	The item to be scanned recursively
	 */
	private void recurseItem(XTATreeItemI item) {
		if(item != null) {
			if(item.getItemId() != null) {
				if(!this.itemsForId.containsKey(item.getItemId())) {
					this.itemsForId.put(item.getItemId(), item);
				}
			}
			
			for(int i=0;i<item.getItemChildCount();i++) {
				recurseItem(item.getItemChildAt(i));
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.controller.XTAServerI#getResourceNames(de.init.xbauleitplanung.xmlxtatool.model.XTAResourceType)
	 */
	@Override
	public ArrayList<String> getResourceNames(XTAResourceType type){
		ArrayList<String> ret = new ArrayList<>();
		
		for(String resourceId : this.settings.getResources().keySet()) {
			XTAResource resource = this.settings.getResources().get(resourceId);
			
			if(type == null || type.equals(resource.getRessourceType())) {
				ret.add(resource.getItemName());
			}
		}
		
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.controller.XTAServerI#getResourceIds(de.init.xbauleitplanung.xmlxtatool.model.XTAResourceType)
	 */
	@Override
	public ArrayList<String> getResourceIds(XTAResourceType type){
		ArrayList<String> ret = new ArrayList<>();
		
		for(String resourceId : this.settings.getResources().keySet()) {
			XTAResource resource = this.settings.getResources().get(resourceId);
			
			if(type == null || type.equals(resource.getRessourceType())) {
				ret.add(resource.getItemId());
			}
		}
		
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.controller.XTAServerI#getResources(de.init.xbauleitplanung.xmlxtatool.model.XTAResourceType)
	 */
	@Override
	public ArrayList<XTAResource> getResources(XTAResourceType type){
		ArrayList<XTAResource> ret = new ArrayList<>();
		
		for(String resourceId : this.settings.getResources().keySet()) {
			XTAResource resource = this.settings.getResources().get(resourceId);
			
			if(type == null || type.equals(resource.getRessourceType())) {
				ret.add(resource);
			}
		}
		
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.controller.XTAServerI#getResourcesForId(java.lang.String)
	 */
	@Override
	public XTATreeItemI getResourcesForId(String id){
		XTATreeItemI ret = this.settings.getResourceForId(id.replace("\\", "/"));
		
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.controller.XTAServerI#removeFolderResource(java.lang.String)
	 */
	@Override
	public void removeResource(String id) {
		try {
			XTAFolderResource newRec = (XTAFolderResource) this.settings.removeResourceById(id);
			this.settings.removeResourceById(id);
			
			newRec.exit();
			
			this.ui.resourceRemoved(id);
			
			this.removeItemsForIdRecursive(newRec);
			
			this.schemaAnalyzer.removeLocalSchemaCache(newRec.getItemId());
			this.schemaAnalyzer.removeReverseLocalSchemaCache(newRec.getItemId());
			
			this.changes = true;
			this.write();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Recursive worker method to remove an item and all of its sub components
	 * @param item	the item to be removed recursively
	 */
	private void removeItemsForIdRecursive(XTATreeItemI item) {
		try {
			if(item != null && item.getItemChildCount() != 0) {
				for(int i=item.getItemChildCount() - 1;i>=0;i--) {
					this.removeItemsForIdRecursive(item.getItemChildAt(i));
				}
			}
			if(item != null && item.getItemId() != null) {
				this.itemsForId.remove(item.getItemId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete the specified file or folder from disk recursively - in case no write operations are possible 
	 * (locked files), the deletion will be performed after termination
	 * @param dest	File or folder to be deleted
	 * @return	true if the deletion was successful - false otherwise
	 */
	private boolean deleteFolder(File dest){
	    boolean ret = true;
	    
		try {
	    	if(dest.isDirectory()) {
	    		File[] sub = dest.listFiles();
	    		for(File sb : sub) {
	    			ret = ret && deleteFolder(sb);
	    		}
	    		ret = ret && dest.delete();
	    	}
	    	else {
	    		boolean suc = dest.delete();
	    		
	    		if(!suc) {
	    			LogManager.getRootLogger().debug(dest + " wird beim beenden geloescht");
	    			dest.deleteOnExit();
	    		}
	    		ret = ret && suc;
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.controller.XTAServerI#addFolderResource(java.lang.String, java.lang.String)
	 */
	@Override
	public String addFolderResource(String name, String folder) {
		if(this.settings.containsFolderResource(folder)) {
			this.ui.showMessageDialog("Ordner bereits vorhanden", "Der Ordner " + folder + " ist bereits vorhanden. ", 0);
			
			return null;
		}
		
		// Erzeuge neue Folder-Ressource
		XTAFolderResource newRec = new XTAFolderResource(); //.create();
		newRec.setItemName(name);
		newRec.setPath(folder);
		newRec.setProcessMode(XTAFolderResource.ONLINE_CACHED);
		
		this.settings.addResource(newRec.getItemId(), newRec);
		
		ui.resourceAdded(newRec);
		
		this.changes = true;
		this.updateFolderResource(newRec);
//		this.write();
		
		return newRec.getItemId();
	}

	@Override
	public String addXTAAccount(String name, String folder) {
		if(this.settings.containsFolderResource(folder)) {
			this.ui.showMessageDialog("Ordner bereits vorhanden", "Der Ordner " + folder + " ist bereits vorhanden. ", 0);
			
			return null;
		}
		
		// Erzeuge neue Folder-Ressource
		XTAPostOfficeBox newRec = new XTAPostOfficeBox(); //.create();
		newRec.setItemName(name);
		newRec.setPath(folder);
		newRec.setProcessMode(XTAFolderResource.ONLINE_CACHED);
		
		this.settings.addResource(newRec.getItemId(), newRec);
		
		ui.resourceAdded(newRec);
		
		this.changes = true;
		this.updateFolderResource(newRec);
		
		newRec.setHost(this);
		newRec.init();
//		this.write();
		
		return newRec.getItemId();
	}

	@Override
	public String addRESTAccount(String name, String folder) {
		if(this.settings.containsFolderResource(folder)) {
			this.ui.showMessageDialog("Ordner bereits vorhanden", "Der Ordner " + folder + " ist bereits vorhanden. ", 0);
			
			return null;
		}
		
		// Erzeuge neue Folder-Ressource
		XTAFITConnectPostOfficeBox newRec = new XTAFITConnectPostOfficeBox(); //.create();
		newRec.setItemName(name);
		newRec.setPath(folder);
		newRec.setProcessMode(XTAFolderResource.ONLINE_CACHED);
		
		this.settings.addResource(newRec.getItemId(), newRec);
		
		ui.resourceAdded(newRec);
		
		this.changes = true;
		this.updateFolderResource(newRec);
		
		newRec.setHost(this);
		newRec.init();
		
//		this.write();
		return newRec.getItemId();
	}

	@Override
	public void updateFolderResource(XTAFolderResource resource) {
		try {
			XTAFolderResource newRec = resource;
			
			String folder = newRec.getPath();
			
			// Falls bereits offline-Inhalte im Verzeichnis sind, sollen diese geloescht werden
			File offline = new File(folder + "/offline");
			if(offline.exists()) {
				boolean suc = deleteFolder(offline);
				
				if(!suc) {
					LogManager.getRootLogger().debug("Ordner " + offline.getAbsolutePath() + " nicht geloescht. ");
				}
				else {
					XTAFileItem child = new XTAFileItem();
					child.setPath(folder + "/offline");
					newRec.removeChild(child.getItemId());
					ui.itemRemoved(child);
				}
			}
			
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					creatingInProgress = true;
					
					ui.setProgressVisible(true);
					ui.setWorkingStatus(true);
					
					ui.enable("xta-newfolder", false);
					ui.enable("xta-newxtaacount", false);
					ui.enable("xta-restaccount", false);
					ui.enable("xta-remove", false);
					
					// Aufraeumen, bevor alles nochmal neu analysiert wird
					removeItemsForIdRecursive(newRec);
					newRec.clear();
					
					schemaAnalyzer.removeLocalSchemaCache(newRec.getItemId());
					schemaAnalyzer.removeReverseLocalSchemaCache(newRec.getItemId());
					hashesDone.clear();
					
					try {
						newRec.getRootElements().clear();
						
						// in recurseFolder() wird bereits ueberprueft, ob eine XML-Datei syntaktisch korrekt ist
						// Analyse all local schema files for contained elements, element groups, attributes, 
						// attribute groups etc.
						// Note: considers only local files, not external references
						schemaAnalyzer.recurseDirForNamedElementsAndAttributes(newRec);
						
						// analyze entire folder structure: 
						// 1)	Analyze all XSD files for contained root elements, complex types and simple types
						// 2)	Register all XML files for later analysis (store paths in messagesToDo list). 
						// 		XML validation will be performed on the fly, validation results will be stored 
						//		within the according objects
						TreeSet<WorkLeft> workLeft = new TreeSet<>();
						recurseFolder(newRec, new File(folder), newRec, new TreeMap<String, XTASchemaFileItem>(), true, workLeft);
						
						// Load all external resources referred to in all schema files and store them within the 
						// offline folder for later analysis
						recurseDirToCacheRemainingXSDs(newRec, newRec);
						
						// Zaehler mit dem ueberwacht werden kann, wie weit die Bearbeitung der Nachrichten wirklich ist
						targetCount = messagesToDo.size();
						currentCount = 0;
						
						ui.setWorkingStatus(false);
						ui.setProgressMax(messagesToDo.size());
						
						// First, all XML files are consulted in order to load and cache referenced external schemas and 
						// genericode lists. Deep analysis of XML files takes place below. Each XML file is consulted in a 
						// separate process in order to be able to speed up by parallel processing
						int count = 0;
						for(int i=0;i<messagesToDo.size();i++) {
							MessageToDoItem todo = messagesToDo.get(i);
							ui.setStatusText("Lese (" + count + " / " + messagesToDo.size() + ") " + todo.getFile().getAbsolutePath());
							ui.setProgressValue(count++);
							
							if(todo.getItem().getFatals().size() > 0) {
								currentCount++;
								LogManager.getRootLogger().debug(currentCount + " / " + targetCount);
							}
							else {
								while(nodeThreads >= maxNodeThreads) {
									try {
										Thread.sleep(10);
									} catch (InterruptedException e) {
									}
								}
								
								Thread t = new Thread(new FetchProcess(newRec, todo));
								
								nodeThreads++;
								t.start();
							}
						}
					} catch (DOMException e) {
						e.printStackTrace();
					}
					
					// Wait until all XML files are consulted (all processes have terminated)
					while(currentCount < targetCount) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
						}
					}
					
					// In case of cached processing, the cached files shall be analysed as well in the same manner 
					// as given files (above). Comments are left out. 
					File offline = new File(folder + "/offline");
					if(offline.exists()) {
						XTAFileItem child = new XTAFileItem();
						child.setPath(folder + "/offline");
						newRec.addChild(child);
						
						schemaAnalyzer.recurseDirForNamedElementsAndAttributes(child);
						TreeSet<WorkLeft> workLeft = new TreeSet<>();
						recurseFolder(child, new File(folder + "/offline"), newRec, new TreeMap<String, XTASchemaFileItem>(), true, workLeft);
						
						System.out.println(workLeft);
						for(WorkLeft wl : workLeft) {
							if("elementGroup".equals(wl.todo)) {
								XElement typedTemplate = wl.schema.getNamedElement(wl.reference);
								
								XElementProxy typed = new XElementProxy(typedTemplate);
								((XElementGroup) wl.subject).addElement(typed);
								
								if(XElement.getPrefix(wl.reference) != null) {
									typed.setPrefix(XElement.getPrefix(wl.reference));
								}
							}
							else if("complexType".equals(wl.todo)) {
								XElement typedTemplate = wl.schema.getNamedElement(wl.reference);
								
								XElementProxy typed = new XElementProxy(typedTemplate);
								((XComplexType) wl.subject).addElement(typed);
								
								if(XElement.getPrefix(wl.reference) != null) {
									typed.setPrefix(XElement.getPrefix(wl.reference));
								}
							}
						}
					}
					
					// Resolve imports and includes to local files
					resolveImports(newRec, newRec, schemaAnalyzer.getLocalSchemaCache(newRec.getItemId()));
					
					// Create a root node which will keep the entire type hierarchy in its sub nodes (processed in 
					// the next step)
					newRec.setRootNode(new XModelTreeNode());
					
					// Create a complete hierarchical model (type tree) from the XSD files and codelists analyzed above
//					int counter = 0;
					for(XElement rootEl : newRec.getRootElements()) {
						ArrayList<String> typesInChain = new ArrayList<>();
						TreeMap<String, XModelTreeNode> typeRegister = new TreeMap<String, XModelTreeNode>();
						LogManager.getRootLogger().debug(rootEl);
						
						XTASchemaFileItem schema = rootEl.getRootSchema();
						schemaAnalyzer.reCount = 0;
						XModelTreeNode child = schemaAnalyzer.recurseElement(newRec.getRootNode(), rootEl, 0, typesInChain, typeRegister, schema, false);
						child.setForm("qualified");
					}
					
					// Suche nach abstrakten Datentypen und entsprechenden Implementoren
					newRec.registerImplementors();
					
					// Now tha all schema and codelist files have been loaded, stored and analyzed, create a hierarchical model 
					// for each XML file
					try {
						// Counter to monitor the progress of XML processing
						targetCount = messagesToDo.size();
						currentCount = 0;
						
						ui.setWorkingStatus(false);
						ui.setProgressMax(messagesToDo.size());
						
						int count = 0;
						int total = messagesToDo.size();
						while(messagesToDo.size() > 0) {
							MessageToDoItem todo = messagesToDo.remove(0);
							ui.setStatusText("Analysiere (" + count + " / " + total + ") " + todo.getFile().getAbsolutePath());
							ui.setProgressValue(count++);
							
							// in case the XML structure is violated (no XML processing is possible)...
							if(todo.getItem().getFatals().size() > 0) {
								ObjectReader read = new ObjectReader(todo.getFile().getAbsolutePath());
								StringBuilder build = new StringBuilder();
								String line = read.getString();
								while(line != null) {
									build.append(line + "\n");
									line = read.getString();
								}
								todo.getItem().setPlainText(build.toString());
								
								currentCount++;
							}
							// XML structure is valid
							else {
								// Wait until a processing slot is available...
								while(nodeThreads >= maxNodeThreads) {
									try {
										Thread.sleep(10);
									} catch (InterruptedException e) {
									}
								}
								
								// Start XML processing
								Thread t = new Thread(new Runnable() {
									
									@Override
									public void run() {
										try {
											// Method getNodeFromXML() will recursively analyze the XML structure of the given 
											// XML file after validating the file against the referenced schema. Also codes 
											// from registered code lists will be resolved if possible
											getNodeFromXML(todo.getFile(), todo.getItem(), newRec, true, false, true);
											
											nodeThreads--;
											LogManager.getRootLogger().debug(currentCount + " / " + targetCount + " / " + nodeThreads);
										} catch (Throwable e) {
											e.printStackTrace();
										}
										finally {
											currentCount++;
											System.out.println(targetCount + " - " + currentCount);
										}
									}
								});
								
								nodeThreads++;
								t.start();
							}
						}
					} catch (Error e) {
						e.printStackTrace();
					}
					
					messagesToDo.clear();
					
					// Wait for all processes to be terminated
					while(currentCount < targetCount) {
						try {
							System.out.println(targetCount + " x " + currentCount);
							Thread.sleep(100);
						} catch (InterruptedException e) {
						}
					}
					
					ui.enable("xta-newfolder", true);
					ui.enable("xta-newxtaacount", true);
					ui.enable("xta-restaccount", true);
					ui.enable("xta-remove", true);
					
					ui.resourceUpdated(newRec.getItemId());
					ui.setProgressVisible(false);
					
					rescanItemIndex();
					
					// Notify update is finished
					resource.resourceUpdated();
					
					// Save changes!
					changes = true;
					write();
					
					creatingInProgress = false;
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Analyze referenced XML schemas in imports and includes of XML schemas and point to the corresponding local 
	 * copies in the localFile field of the import or include objects 
	 * @param resource	The containing folder resource
	 * @param item	the XML schema file item to be analyzed
	 * @param cache	A map which links all remote XML schemas within a given folder resource to their corresponding 
	 * local copies
	 */
	private void resolveImports(XTAFolderResource resource, XTAFileItem item, TreeMap<String, String> cache) {
		if(item instanceof XTASchemaFileItem) {
			for(Import imp : ((XTASchemaFileItem) item).getImports()) {
				try {
					String localFile = null;
					String loc = imp.getSchemaLocation();
					if(loc.startsWith("http://") || loc.startsWith("https://")) {
						localFile = cache.get(loc);
					}
					else {
						localFile = loc.replace("\\", "/");
					}
					
					if(localFile != null) {
						File check = new File(localFile);
						if(check.isAbsolute()) {
							XTAFileItem ref = resource.getDescendantForPath(localFile);
							
							if(ref != null) {
								imp.setLocalFile((XTASchemaFileItem) ref);
							}
							else {
								println("Descendant nicht gefunden: " + item.getPath() + " --> " + check.getAbsolutePath());
							}
						}
						else {
							File root = new File(item.getPath()).getParentFile();
							while(localFile.startsWith("../")) {
								localFile = localFile.substring(3);
								root = root.getParentFile();
							}
							check = new File(root.getAbsolutePath() + "/" + localFile);
							
							XTAFileItem ref = resource.getDescendantForPath(check.getAbsolutePath());
							if(ref != null) {
								imp.setLocalFile((XTASchemaFileItem) ref);
							}
							else {
								println("Descendant nicht gefunden: " + item.getPath() + " --> " + check.getAbsolutePath());
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			for(Include inc : ((XTASchemaFileItem) item).getIncludes()) {
				try {
					String localFile = null;
					String loc = inc.getSchemaLocation();
					if(loc.startsWith("http://") || loc.startsWith("https://")) {
						localFile = cache.get(loc);
					}
					else {
						localFile = loc.replace("\\", "/");
					}
					
					if(localFile != null) {
						File check = new File(localFile);
						if(check.isAbsolute()) {
							XTAFileItem ref = resource.getDescendantForPath(localFile);
							
							if(ref != null) {
								inc.setLocalFile((XTASchemaFileItem) ref);
							}
							else {
								println("Descendant nicht gefunden: " + item.getPath() + " --> " + check.getAbsolutePath());
							}
						}
						else {
							File root = new File(item.getPath()).getParentFile();
							while(localFile.startsWith("../")) {
								localFile = localFile.substring(3);
								root = root.getParentFile();
							}
							check = new File(root.getAbsolutePath() + "/" + localFile);
							
							XTAFileItem ref = resource.getDescendantForPath(check.getAbsolutePath());
							if(ref != null) {
								inc.setLocalFile((XTASchemaFileItem) ref);
							}
							else {
								println("Descendant nicht gefunden: " + item.getPath() + " --> " + check.getAbsolutePath());
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		else if(item instanceof XTAFileItem){
			for(int i=0;i<item.getItemChildCount();i++) {
				try {
					resolveImports(resource, (XTAFileItem) item.getItemChildAt(i), cache);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public XCodelist getCodelistForId(String schemaId, String codelistName) {
		XCodelist ret = null;
		
		try {
			LogManager.getRootLogger().debug(schemaId);
			
			if(schemaId != null) {
				XTASchemaFileItem schema = (XTASchemaFileItem) this.getItemForId(schemaId);
				ret = schema.getCodelist(codelistName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		LogManager.getRootLogger().debug(ret);
		return ret;
	}
	
	@Override
	public XTATreeItemI getItemForId(String id) {
		XTATreeItemI ret = this.itemsForId.get(id);
		
		return ret;
	}
	
	@Override
	public void putItemForId(String id, XTATreeItemI item) {
		this.itemsForId.put(id, item);
	}
	
	@Override
	public void validateAndStructXMLFile(String resourceId, String id) {
		XTAFolderResource root = (XTAFolderResource) this.settings.getResources().get(resourceId);
		XTAXMLFileItem child = (XTAXMLFileItem) this.getItemForId(id);
		
		this.validateAndStructXMLFile(child, root, true);
	}
	
	@Override
	public void validateXMLFile(String resourceId, String id, XTAXMLFileItem file) {
		XTAFolderResource root = (XTAFolderResource) this.settings.getResources().get(resourceId);
		XTAXMLFileItem child = (XTAXMLFileItem) this.getItemForId(id);
		
		root.validationDesired();
		XTAXMLFileItem proxy = child.clone();
		
		child.getWarnings().clear();
		child.getFatals().clear();
		
		proxy.getWarnings().clear();
		proxy.getFatals().clear();
		proxy.setValidated(false);
		
		File cur = new File(((XTAXMLFileItem) proxy).getPath());
		try {
			String type = this.getXMLTypeUncatched(cur);
			
			if("gc:CodeList".equals(type)) {
			}
			else {
				getNodeFromXML(new File(proxy.getPath()), proxy, root, true, false, true);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			
			// Bereits beim Parsen wurde eine Exception geworfen...
			
			proxy.getFatals().add(e.getMessage());
		}
		
		child.getWarnings().addAll(proxy.getWarnings());
		child.getFatals().addAll(proxy.getFatals());
		child.setValidated(true);
	}
	
	@Override
	public void validateAndStructXMLFile(XTAXMLFileItem child, XTAFolderResource root, boolean cacheReferencedResources) {
		File cur = new File(child.getPath());
		
		child.clear();
		
		try {
			String type = this.getXMLTypeUncatched(cur);
			
			// Bereinige alle Kindknoten und Meldungen (Warnings, Fatals). Diese werden im naechsten Gang neu 
			// analysiert und hinzugefuege
			child.clear();
			
			if("gc:CodeList".equals(type)) {
			}
			else {
				getNodeFromXML(new File(child.getPath()), child, root, true, true, cacheReferencedResources);
			}
		} catch (Throwable e) {
			
			child.getFatals().add(e.getMessage());
		}
	}
	
	// TODO: MVC-Pattern umsetzen!
	/* (non-Javadoc)
	 * @see de.init.xbauleitplanung.xmlxtatool.controller.XTAServerI#exportXMLFile(java.lang.String, de.init.xbauleitplanung.xmlxtatool.model.XTAXMLFileItem)
	 */
	@Override
	public void exportXMLFile(XTAXMLFileItem item, boolean plain) {
		try {
			item.export(plain);
		} catch (Throwable e) {
			e.printStackTrace();
			
			this.ui.showMessageDialog("Warnung", "Beim Speichern von " + item.getPath() + " ist es zu einem Fehler gekommen. ", 0);
		}
	}
	
	/**
	 * This method loads resources referred to in schema files within directories (which themselves are code lists or 
	 * schema files) and stores them within the offline folder of the given resource. 
	 * @param parent	directory to be analyzed
	 * @param root	resource to which the directory (parent) belongs
	 */
	private void recurseDirToCacheRemainingXSDs(XTAFileItem parent, XTAFolderResource root) {
		try {
			File dir = new File(parent.getPath());
			
			if(dir.exists()) {
				File[] schemas = dir.listFiles();
				
				for(File sub : schemas) {
					if(sub.isFile()) {
						if(sub.getName().toLowerCase().endsWith(".xsd")) {
							try {
								TreeMap<String, String> schemaCache = this.schemaAnalyzer.getLocalSchemaCache(root.getItemId());
								TreeMap<String, String> reverseSchemaCache = this.schemaAnalyzer.getReverseLocalSchemaCache(root.getItemId());
								ReferenceRegister references = this.schemaAnalyzer.getReferenceCache(root.getItemId());
								this.schemaAnalyzer.getListURIReferencesFromMessage(sub, references);
								processURNs(root, schemaCache, reverseSchemaCache, references, false);
								
								if(sub.isAbsolute()) {
									this.schemaAnalyzer.cacheRecursiveFile(root, sub, root.getPath() + "/offline", schemaCache, reverseSchemaCache, references);
								}
							} catch (XPathExpressionException e) {
								e.printStackTrace();
							} catch (ParserConfigurationException e) {
								e.printStackTrace();
							} catch (SAXException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					else if(sub.isDirectory()) {
						// TODO: Hier ggf. Attachments ausschließen: if(sub.getName().startsWith(".") && sub.getName().endsWith("_Attachments")) {...
						
						XTAFileItem item = parent.getChildForPath(sub.getAbsolutePath());
						recurseDirToCacheRemainingXSDs(item, root);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This method makes a complete recursive survey of all XML and related files (schemas, codelists). Thereby, schema files 
	 * and codelists will be analyzed instantly whereas XML files will be registered for later (bulk) analysis. This guarantees
	 * that all schemas have been analyzed before the XML files become analyzed. 
	 * @param fileItem	The target file item to which the new file item object will be added
	 * @param file	The file or folder to be analyzed
	 * @param root	The folder resource which contains the file
	 * @param cache	The local index for all file items of a folder resource
	 * @param checkMessages	Indicates whether XML files shall be analyzed or not
	 */
	private void recurseFolder(
			XTAFileItem fileItem, 
			File file, 
			XTAFolderResource root, 
			Map<String, XTASchemaFileItem> cache, 
			boolean checkMessages, 
			TreeSet<WorkLeft> workLeft) {
		fileItem.setPath(file.getAbsolutePath());
		
		if(file.isDirectory()) {
			File[] sub = file.listFiles();
			
			for(File cur : sub) {
				if(cur.isDirectory()) {
					// TODO: Hier ggf. Attachments ausschließen: if(sub.getName().startsWith(".") && sub.getName().endsWith("_Attachments")) {...
//					
					XTAFileItem child = null;
					try {
						child = fileItem.getChildForPath(cur.getAbsolutePath());
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(child == null) {
						child = new XTAFileItem();
						child.setPath(cur.getAbsolutePath());
						fileItem.addChild(child);
					}
					recurseFolder(child, cur, root, cache, checkMessages, workLeft);
				}
				else if(cur.isFile()) {
					String hash = getHashForFile(cur.getAbsolutePath());
					boolean done = false;
					
					this.ui.setStatusText("Analysiere " + cur.getAbsolutePath());
					
					// Wenn identische Dateien gefunden werden, muss jede weitere nicht nocheinmal analysiert werden. 
					// Die Informationen werden aus der ersten Instanz kopiert
					// TODO: Die Clone-Funktion ueebrarbeiten - sie muss ALLE Felder clonen und korrekte Vater-Beziehungen herstellen
//					if(this.hashesDone.containsKey(hash)) {
//						XTATreeItemI child = this.hashesDone.get(hash);
//						
//						if(child instanceof XTASchemaFileItem) {
//							XTASchemaFileItem copy = new XTASchemaFileItem();
//							
//							copy.clone((XTASchemaFileItem) child);
//							copy.setPath(cur.getAbsolutePath());
//							
//							resource.addChild(copy);
//							done = true;
//						}
//					}
					
					if(done) {
						continue;
					}
					
					if(cur.getName().toLowerCase().endsWith(".xsd")) {
						// XSDs werden schon vorher in recurseDirForTypedElements() gescannt
						String type = this.getXMLType(cur);
						LogManager.getRootLogger().debug(cur);
						if("xs:schema".equals(type) || "schema".equals(type)) {
							XTASchemaFileItem child = null;
							try {
								child = (XTASchemaFileItem) fileItem.getChildForPath(cur.getAbsolutePath());
							} catch (Exception e) {
								e.printStackTrace();
							}
							if(child == null) {
								child = new XTASchemaFileItem();
								child.setPath(cur.getAbsolutePath());
								fileItem.addChild(child);
							}
							
							// Neu: HIer wird erst abgefragt, ob die Schema-Analyse schon durchgelaufen ist, 
							// damit dies nicht nocheinmal passiert
							if(!child.isAnalysisDone()) {
								this.schemaAnalyzer.scanSchema(cur, child, root, cache, workLeft);
							}
							
							this.hashesDone.put(hash, child);
						}
					}
					else if(cur.getName().toLowerCase().endsWith(".gc")) {
						this.ui.setStatusText("Analysiere " + cur.getAbsolutePath());
						String type = this.getXMLType(cur);
						if("gc:CodeList".equals(type)) {
							XTAGenericodeList child = new XTAGenericodeList();
							child.setPath(cur.getAbsolutePath());
							
							boolean suc = this.schemaAnalyzer.scanGenericode(cur, child, root);
							schemaAnalyzer.getReferenceCache(root.getItemId()).set(child.getUrn(), child.getVersion(), suc); //   .getReferences(root.getItemId(), child.getUrn()).addListVersionID(child.getVersion(), suc);
							
							if(suc) {
								fileItem.addChild(child);
							}
							
							this.hashesDone.put(hash, child);
						}
					}
					else if(cur.getName().toLowerCase().endsWith(".xml")) {
						this.ui.setStatusText("Analysiere " + cur.getAbsolutePath());
						try {
							String type = this.getXMLTypeUncatched(cur);
							
							if("gc:CodeList".equals(type)) {
								XTAGenericodeList child = new XTAGenericodeList();
								child.setPath(cur.getAbsolutePath());
								
								boolean suc = this.schemaAnalyzer.scanGenericode(cur, child, root);
								schemaAnalyzer.getReferenceCache(root.getItemId()).set(child.getUrn(), child.getVersion(), suc);
								
								if(suc) {
									fileItem.addChild(child);
								}
								
								this.hashesDone.put(hash, child);
							}
							else {
								if(checkMessages) {
									XTAXMLFileItem child = new XTAXMLFileItem();
									MessageToDoItem todo = new MessageToDoItem();
									todo.setFile(cur);
									todo.setItem(child);
									child.setItemName(cur.getName());
									child.setItemId(cur.getAbsolutePath());
									child.setPath(cur.getAbsolutePath());
									fileItem.addChild(child);
									
									this.messagesToDo.add(todo);
								}
							}
						} catch (Throwable e) {
							e.printStackTrace();
							
							XTAXMLFileItem child = new XTAXMLFileItem();
							MessageToDoItem todo = new MessageToDoItem();
							todo.setFile(cur);
							todo.setItem(child);
							child.setItemName(cur.getName());
							child.setItemId(cur.getAbsolutePath());
							child.setPath(cur.getAbsolutePath());
							child.getFatals().add(e.getMessage());
							fileItem.addChild(child);
							
							this.messagesToDo.add(todo);
						}
					}
					else {
						try {
							String type = this.getXMLTypeUncatched(cur);
							
							if(type != null) {
								XTAXMLFileItem child = new XTAXMLFileItem();
								MessageToDoItem todo = new MessageToDoItem();
								todo.setFile(cur);
								todo.setItem(child);
								child.setItemName(cur.getName());
								child.setItemId(cur.getAbsolutePath());
								child.setPath(cur.getAbsolutePath());
								fileItem.addChild(child);
								
								this.messagesToDo.add(todo);
							}
						} catch(Exception e) {
							System.err.println("Fehler beim Analysieren von " + cur.getAbsolutePath());
							e.printStackTrace();
						}
//						XTAFileItem child = new XTAFileItem();
//						child.setItemName(cur.getName());
//						child.setItemId(cur.getAbsolutePath());
//						child.setPath(cur.getAbsolutePath());
//						fileItem.addChild(child);
					}
				}
			}
		}
	}

	@Override
	public String getPlainText(String itemId) {
		XTAXMLFileItem child = (XTAXMLFileItem) this.getItemForId(itemId);
		ObjectReader read = new ObjectReader(child.getPath());
		StringBuilder build = new StringBuilder();
		String line = read.getString();
		while(line != null) {
			build.append(line + "\n");
			line = read.getString();
		}
		
		return build.toString();
	}
	
	/**
	 * This method validates, analyzes and processes an input XML file and generates the corresponding XTAXMLFileItem object 
	 * which represents the entire structure of the XML file and carries references ro the according codelists, complex types 
	 * or simple types that are used in the XML
	 * @param file	The XML file
	 * @param item	The target XTAXMLFileItem object to which all information is written
	 * @param root	The folder resource that contains the XML file
	 * @param transformer	The transformer is used to carry out XML validation against the referred schema
	 * @param rebuild	rebuild indicates whether new instances of XNode objects will be created or existing ones will be used
	 * @param directPaint	directPaint indicates whether the resulting XTAXMLFileItem will appear instantly on the UI
	 * @param cacheReferencedResources	Indicates whether referenced resources (schema files or codelists) shall be cached for later use
	 */
	private void getNodeFromXML(
			File file, 
			XTAXMLFileItem item, 
			XTAFolderResource root, 
			boolean rebuild, 
			boolean directPaint, 
			boolean cacheReferencedResources) {
		try {
			ArrayList<String> warnings = new ArrayList<>();
			
			// Schema-Item, das durch die XML-Nachricht referenziert wird
			XTASchemaFileItem rootSchema = null;
			
			// Validierung des XML-Dokumentes...
			// Warnungen und Fehler werden in item gespeichert
			if(root.validationDesired() && file.getName().toLowerCase().endsWith(".xml")) {
				try {
					TreeMap<String, String> schemaCache = this.schemaAnalyzer.getLocalSchemaCache(root.getItemId());
					TreeMap<String, String> reverseSchemaCache = this.schemaAnalyzer.getReverseLocalSchemaCache(root.getItemId());
					
					// Referenzen auf Genericode-Listen
					ReferenceRegister references = this.schemaAnalyzer.getReferenceCache(root.getItemId());
					
					this.schemaAnalyzer.getListURIReferencesFromMessage(file, references);
					
					String schemaLocation = this.schemaAnalyzer.getSchemaLocationFromMessage(file);
					schemaLocation = schemaLocation.replace("%20", " ");

					String localSchemaPath = file.getParent().replace("\\", "/") + "/" + schemaLocation;
					
					URL schemaLocationEffective = new File(localSchemaPath).toURI().toURL();
					
					// Schema Location wird ermittelt und ggf. Schemata zwischengespeichert (OFFLINE_CACHED), 
					// damit die Bearbeitung im Falle vieler Dateien schneller geht
					if(root.getProcessMode() == XTAFolderResource.OFFLINE) {
						// Es werden relevante und wahrscheinliche Quellen gesucht,an denen sich das Schema
						// offline befinden koennte. Zwischenergebisse werden gecached, damit nicht immer 
						// wieder von neuem gesucht werden muss
						
						if(schemaLocation.toLowerCase().trim().startsWith("http://") || 
								schemaLocation.toLowerCase().trim().startsWith("https://") || 
								schemaLocation.toLowerCase().trim().startsWith("file:/")) {
							if(schemaCache.containsKey(schemaLocation)) { //containedInSchemaCache(schemaCache, schemaLocation)) {
								File schemaCheck = new File(this.schemaAnalyzer.getFromSchemaCache(schemaCache, schemaLocation));
								schemaLocationEffective = schemaCheck.toURI().toURL();
							}
							else {
								localSchemaPath = schemaLocation;
								URL check = new URL(localSchemaPath);
								String path = check.getPath();
								File schemaCheck = new File(root.getPath() + "/" + path);
								if(!schemaCheck.exists()) {
									schemaCheck = new File(root.getPath() + "/" + schemaCheck.getName());
								}
								if(!schemaCheck.exists()) {
									schemaCheck = new File(file.getParentFile().getAbsolutePath() + "/" + schemaCheck.getName());
								}
								
								this.schemaAnalyzer.putToSchemaCache(schemaCache, reverseSchemaCache, schemaLocation, schemaCheck.getAbsolutePath());
								
								schemaLocationEffective = schemaCheck.toURI().toURL();
							}
						}
					}
					else if(root.getProcessMode() == XTAFolderResource.ONLINE) {
						if(schemaLocation.toLowerCase().trim().startsWith("http://") || schemaLocation.toLowerCase().trim().startsWith("https://")) {
							localSchemaPath = schemaLocation;
							schemaLocationEffective = new URL(localSchemaPath).toURI().toURL();
						}
					}
					else if(root.getProcessMode() == XTAFolderResource.ONLINE_CACHED) {
						if(schemaLocation.toLowerCase().trim().startsWith("http://") || 
								schemaLocation.toLowerCase().trim().startsWith("https://") || 
								schemaLocation.toLowerCase().trim().startsWith("file:/")) {
							localSchemaPath = schemaLocation;
							
							if(schemaLocation.toLowerCase().trim().startsWith("file:/")) {
								schemaLocationEffective = new URL(schemaLocation);
							}
							else {
								if(cacheReferencedResources) {
									this.schemaAnalyzer.cacheRecursiveURL(new URL(localSchemaPath).toURI().toURL(), root.getPath() + "/offline", schemaCache, reverseSchemaCache, references);
								}
								
								try {
									schemaLocationEffective = new File(this.schemaAnalyzer.getFromSchemaCache(schemaCache, localSchemaPath)).toURI().toURL();
								} catch (Exception e) {
									schemaLocationEffective = new File(localSchemaPath).toURI().toURL();
								}
							}
						}
						else {
							localSchemaPath = schemaLocation;
							File check = new File(localSchemaPath);
							if(check.isAbsolute()) {
								if(cacheReferencedResources) {
									this.schemaAnalyzer.cacheRecursiveFile(root, check, root.getPath() + "/offline", schemaCache, reverseSchemaCache, references);
								}
								
								try {
									schemaLocationEffective = new File(this.schemaAnalyzer.getFromSchemaCache(schemaCache, localSchemaPath)).toURI().toURL();
								} catch (Exception e) {
									schemaLocationEffective = new File(localSchemaPath).toURI().toURL();
								}
							}
							else {
								check = file.getParentFile();
								while(schemaLocation.startsWith(".")) {
									if(schemaLocation.startsWith("./")) {
										schemaLocation = schemaLocation.substring(2);
									}
									else if(schemaLocation.startsWith("../")) {
										check = check.getParentFile();
										schemaLocation = schemaLocation.substring(3);
									}
									else {
										break;
									}
								}
								localSchemaPath = check.getAbsolutePath() + "/" + schemaLocation;
								
								if(localSchemaPath.replace("\\", "/").startsWith(root.getPath().replace("\\", "/") + "/offline/")) {
									schemaLocationEffective = new File(localSchemaPath).toURI().toURL();
								}
								else {
									if(cacheReferencedResources) {
										this.schemaAnalyzer.cacheRecursiveFile(root, new File(localSchemaPath), root.getPath() + "/offline", schemaCache, reverseSchemaCache, references);
									}
									
									try {
										schemaLocationEffective = new File(this.schemaAnalyzer.getFromSchemaCache(schemaCache, localSchemaPath)).toURI().toURL();
									} catch (Exception e) {
										schemaLocationEffective = new File(localSchemaPath).toURI().toURL();
									}
								}
							}
						}
						
						if(cacheReferencedResources) {
							processURNs(root, schemaCache, reverseSchemaCache, references, true);
						}
					}
					
					rootSchema = (XTASchemaFileItem) root.getDescendantForPath(schemaLocationEffective.toString().substring("file:/".length()));
					
					Source xmlFile = new StreamSource(new File(file.getAbsolutePath()));
					SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
					schemaFactory.setFeature("http://apache.org/xml/features/honour-all-schemaLocations", true);
					
					Schema schema;
					
					if(schemaLocationEffective.toString().startsWith("file:/")) {
						File check = new File(schemaLocationEffective.toString().replace("%20", " ").substring("file:/".length()));
						schema = schemaFactory.newSchema(check);
					}
					else {
						schema = schemaFactory.newSchema(schemaLocationEffective);
					}
					
					TransformListener listener = new TransformListener();
					
					Validator validator = schema.newValidator();
					validator.setErrorHandler(listener);
					try {
						validator.validate(xmlFile);
					} catch (Throwable e) {
						e.printStackTrace();
					}
					item.setValidated(true);
					
					for(String warning : listener.warnings) {
						if(warning != null) {
							item.addWarning(warning);
						}
					} 
					
					for(String error : listener.errors) {
						if(error != null) {
							item.addWarning(error);
						}
					} 
					
					for(String fatal : listener.fatals) {
						if(fatal != null) {
							item.addFatal(fatal);
						}
					}
				} catch (Exception e) {
					warnings.add(e.getMessage());
				}
			}
			
			// XML-Struktur wird analysiert und Knotenstruktur fuer UI-Darstellung (XNode) aufgebaut
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				factory.setFeature("http://apache.org/xml/features/honour-all-schemaLocations", true);
				
//				DocumentBuilder builder = factory.newDocumentBuilder();
//				Document doc = builder.parse("file:///" + file.getAbsolutePath().replace("\\", "/"));
				Document doc = XTASchemaAnalyzer.readXML(new URL("file:///" + file.getAbsolutePath().replace("\\", "/")).openStream());
				
				NodeList result = doc.getChildNodes();
				
				for(int i=0;i<result.getLength();i++) {
					if(rebuild) {
						XNode child = new XNode();
						item.addNode(child);
						recurseNode(result.item(i), child, root, rootSchema, null, null, rebuild, directPaint);
					}
					else {
						XNode child = item.getNodes().get(i);
						recurseNode(result.item(i), child, root, rootSchema, null, null, rebuild, directPaint);
					}
				}
				
				if(root.validationDesired()) {
					for(String warning : warnings) {
						if(warning != null) {
							item.addWarning(warning);
						}
					}
				}
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
//	
//	private String cleanURL(String url) {
//		return url.replace("Ä", "%C4")
//				.replace("Ö", "%D6")
//				.replace("Ü", "%DC")
//				.replace("ä", "%E4")
//				.replace("ö", "%F6")
//				.replace("ü", "%FC")
//				.replace("ß", "%DF");
//	}
	
	// Analysiere rekursiv die Struktur von XML-Dateien, Knoten fuer Knoten (Node) und schreibe die 
	// Ergebnisse in item (XNode)
	// org.w3c.dom.Node --> de.init.xbauleitplanung.xmlxtatool.model.XNode
	/**
	 * Recursively analyze the structure of XML files node by node using the Java XML API. Results are written into 
	 * an XNode object which represents the XML structure in the context of this application. Thi method also recognized 
	 * different types codelists in the context of XOEV standards
	 * @param node	The input w3c node to be analyzed
	 * @param item	The target item to write the results to
	 * @param root	the containing folder resource
	 * @param schemaRoot	The schema file (schemaLocation) of the root XML file 
	 * @param parentListURI	optional identifier of the listURI of the parent node; given in order to identify potential code list in case the 
	 * present element node represents an XOEV code
	 * @param parentListVersionId	optional identifier of the listVersion of the parent node; given in order to identify potential code list in case the 
	 * present element node represents an XOEV code
	 * @param rebuild	Indicates whether child nodes shall be created (rebuild) or are already present an just need to be referenced
	 * @param directPaint	Indicates whether new child nodes shal be displayed on the UI instanty or not
	 */
	private void recurseNode(
			Node node, 
			XNode item, 
			XTAFolderResource root, 
			XTASchemaFileItem schemaRoot, 
			String parentListURI, 
			String parentListVersionId, 
			boolean rebuild, 
			boolean directPaint) {
		try {
			if(node.getAttributes() != null) {
				for(int i=0;i<node.getAttributes().getLength();i++) {
					Node att = node.getAttributes().item(i);
					String attName = att.getNodeName();
					String attVal = att.getNodeValue();
					
					item.getAttributes().put(attName, attVal);
				}
			}
			item.setName(node.getNodeName());
			Object lnO = node.getUserData(XTASchemaAnalyzer.LINE_NUMBER_KEY_NAME);
			
			try {
				item.setLineNumber(Integer.parseInt(lnO.toString()));
			} catch (Exception e1) {
			}
			
			NodeList result = node.getChildNodes(); //(NodeList) expr.evaluate(node, XPathConstants.NODESET);
			
			if("#text".equals(node.getNodeName())) {
				if(result.getLength() == 0) {
					item.setValue(node.getTextContent());
					item.setType(XNode.TEXT);
				}
				else {
					item.setValue(node.getTextContent()); //.trim());
					item.setType(XNode.ELEMENT);
				}
			}
			else if("#comment".equals(node.getNodeName())) {
				item.setValue(node.getTextContent()); //.trim());
				item.setType(XNode.COMMENT);
			}
			else {
				item.setType(XNode.ELEMENT);
				
				if(result.getLength() == 1) {
					if("#text".equals(result.item(0).getNodeName())) {
						item.setValue(result.item(0).getTextContent()); //.trim());
					}
				}
				
				// Ermittele Eltern-Element des code-Elements...
				// Dazu ermittele den kompletten Code-Pfad und laufe durch bis das Eltern-Element erreicht wird
				// Code-Pfad ermitteln
				ArrayList<String> codePath = new ArrayList<>();
				Node nd = node;
				while(nd != null) {
					if("#document".equals(nd.getNodeName())) {
						break;
					}
					
					codePath.add(nd.getNodeName());
					nd = nd.getParentNode();
				}
				
				// Hier werden nach Moeglichkeit Codes aufgeloest...
				if("code".equals(node.getNodeName())) {
					// Eltern-Element ermitteln...
					String key = codePath.remove(codePath.size() - 1);
					String localName = localName(key);
					
					item.setValue(node.getTextContent().trim());
					
					try {
						XModelTreeNode cur = root.getRootNode().getChildForName(localName);
						while(cur != null && codePath.size() > 1) {
							key = codePath.remove(codePath.size() - 1);
							cur = cur.getChildForName(localName(key));
						}
						
						// ok, cur ist Eltern-Element des code-Elementes
						if(cur != null && schemaRoot != null) {
							String typeName = cur.getElementTypeName();
							item.setCodeType(typeName);
							
							XComplexType xComplexType = schemaRoot.findRootType(typeName);
							
							if(xComplexType != null) {
								XElement cdEl = xComplexType.getElementByLocalName("code");
								
								if(cdEl != null) {
									String clType = cdEl.getElementType();
									
									if("xs:token".equals(clType)) {
										
										// No codelist type 1 specified. Try to read listURI from schema OR element...
										
										TreeMap<String, String> schemaCache = this.schemaAnalyzer.getLocalSchemaCache(root.getItemId());
										TreeMap<String, String> reverseSchemaCache = this.schemaAnalyzer.getReverseLocalSchemaCache(root.getItemId());
										
										if(xComplexType.getListURI() != null) {
											
											// Try to detect listURI and listVersionId...
											String listVersionId = null;
											
											// XOEV type must be 2 or 3, since listURI is specified within schema
											if(xComplexType.getListVersionID() != null) {
												// listVersionID is also specified in schema --> Type 2
												item.setXoevType(2);
												listVersionId = xComplexType.getListVersionID();
											}
											else {
												// listVersionID is NOT specified in schema  --> Type 3
												item.setXoevType(3);
												listVersionId = parentListVersionId;
											}
											
											// is parentListURI URL or URN? 
											if(listVersionId.startsWith("http://") || listVersionId.startsWith("https://")) {
												clType = parentListURI;
												
												processOtherCode(root, parentListURI, directPaint, clType, schemaCache,
														reverseSchemaCache);
											}
											else if(listVersionId != null) {
												clType = xComplexType.getListURI() + "_" + listVersionId;
												
												processGenericode(root, directPaint, clType, schemaCache,
														reverseSchemaCache);
											}
											
											enterResolvedCode(item, root, schemaRoot, clType);
										}
										else if(parentListURI != null) {
											// XOEV type must be 4, since listURI is indicated within XML
											item.setXoevType(4);
											
											// is parentListURI URL or URN? 
											if(parentListURI.startsWith("http://") || parentListURI.startsWith("https://")) {
												clType = parentListURI;
												
												processOtherCode(root, parentListURI, directPaint, clType, schemaCache,
														reverseSchemaCache);
											}
											else if(parentListVersionId != null) {
												clType = parentListURI + "_" + parentListVersionId;
												
												processGenericode(root, directPaint, clType, schemaCache,
														reverseSchemaCache);
											}
											
											enterResolvedCode(item, root, schemaRoot, clType);
										}
										else if(item.getAttributes().get("listURI") != null) {
											
											// Versuche listURI zu ermitteln...
											String listVersionId = parentListVersionId;
											
											// XOEV-Typ muss 4 sein, da listURI in der Nachricht angegeben ist
											item.setXoevType(4);
											
											// is parentListURI URL or URN? 
											if(listVersionId.startsWith("http://") || listVersionId.startsWith("https://")) {
												clType = parentListURI;
												
												processOtherCode(root, parentListURI, directPaint, clType, schemaCache,
														reverseSchemaCache);
											}
											else if(listVersionId != null) {
												clType = item.getAttributes().get("listURI") + "_" + listVersionId;
												
												processGenericode(root, directPaint, clType, schemaCache,
														reverseSchemaCache);
											}
											
											enterResolvedCode(item, root, schemaRoot, clType);
										}
										else {
											item.setResolved("Verweis auf Codeliste nicht vorhanden");
										}
									}
									else if(clType != null){
										
										// Typ-1 Codelistentyp angegeben. Versuche Code aus Schema direkt zu ermitteln...
										
										// XOEV-Typ muss 1 sein, da Codeliet im Schema angegeben ist
										item.setXoevType(1);
										
										XTASchemaFileItem schemaFile = schemaRoot.findSchemaForType(typeName);
										if(schemaFile == null) {
											schemaFile = root.getGenericodeList(typeName);
										}
										
										if(schemaFile != null) {
											String resolved = resolveCode(clType, schemaFile, item.getValue());
											item.setResolved(resolved);
											item.setSchemaId(schemaFile.getItemId());
										}
										else {
											item.setResolved("Codeliste nicht vorhanden " + clType);
										}
									}
									else {
										
										// Typ-1 Codelistentyp auf andere Weise angegeben (XOEV-Codierung mittels "wertErsteBEschreibunsSpalte" usw.). 
										// Versuche Code aus Schema direkt zu ermitteln...
										
										// XOEV-Typ muss 1 sein, da Codeliet im Schema angegeben ist
										item.setXoevType(1);
										
										clType = typeName;
										
										XTASchemaFileItem schemaFile = schemaRoot.findSchemaForType(clType);
										if(schemaFile == null) {
											schemaFile = root.getGenericodeList(clType);
										}
//										XTASchemaFileItem schemaFile = root.getSchemaForType(clType);
										
										if(schemaFile != null) {
											String resolved = resolveCode(clType, schemaFile, item.getValue());
											item.setResolved(resolved);
											item.setSchemaId(schemaFile.getItemId());
										}
										else {
											item.setResolved("Codelistentyp nicht vorhanden (2) " + clType);
										}
									}
									
									// Setze Code-Typ
									item.setCodelistType(clType);
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else {
					try {
						String key = codePath.remove(codePath.size() - 1);
						String localName = localName(key);
						XModelTreeNode cur = root.getRootNode().getChildForName(localName);
						if(cur != null) {
							while(cur != null && codePath.size() > 0) {
								key = codePath.remove(codePath.size() - 1);
								localName = localName(key);
								cur = cur.getChildForName(localName);
							}
						}
						if(cur != null) {
							item.setTypeNode(cur);
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				// Hier wird entweder der neue Knoten in die Struktur eingefuegt, falls er einen Wert hat oder 
				// rekursiv alle Kindknoten bearbeitet
				if(result.getLength() == 1) {
					if("#text".equals(result.item(0).getNodeName())) {
						item.setValue(result.item(0).getTextContent()); //.trim());
					}
					else {
						for(int i=0;i<result.getLength();i++) {
							if(rebuild) {
								XNode child = new XNode();
								item.addChild(child);
								recurseNode(result.item(i), child, root, schemaRoot, item.getAttributes().get("listURI"), item.getAttributes().get("listVersionID"), rebuild, directPaint);
							}
							else {
								XNode child = (XNode) item.getItemChildAt(i);
								recurseNode(result.item(i), child, root, schemaRoot, item.getAttributes().get("listURI"), item.getAttributes().get("listVersionID"), rebuild, directPaint);
							}
						}
					}
				}
				else {
					for(int i=0;i<result.getLength();i++) {
						if(rebuild) {
							XNode child = new XNode();
							item.addChild(child);
							recurseNode(result.item(i), child, root, schemaRoot, item.getAttributes().get("listURI"), item.getAttributes().get("listVersionID"), rebuild, directPaint);
						}
						else {
							XNode child = (XNode) item.getItemChildAt(i);
							recurseNode(result.item(i), child, root, schemaRoot, item.getAttributes().get("listURI"), item.getAttributes().get("listVersionID"), rebuild, directPaint);
						}
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void processOtherCode(XTAFolderResource root, String parentListURI, boolean directPaint, String clType,
			TreeMap<String, String> schemaCache, TreeMap<String, String> reverseSchemaCache) {
		if(!schemaCache.containsKey(clType)) {
			String schemaFilePath = root.getPath() + "/offline/" + clType.replace(":", "_").replace("/", "_") + ".gc";
			File check = new File(schemaFilePath);
			
			if(!check.exists()) {
				boolean suc = this.schemaAnalyzer.cacheURL(clType, root.getPath() + "/offline", schemaCache, reverseSchemaCache);
				
				if(suc) {
					XTAGenericodeList child = new XTAGenericodeList();
					child.setPath(schemaFilePath);
					
					boolean exists = this.schemaAnalyzer.scanCode(new File(schemaFilePath), child, root, parentListURI);
					schemaAnalyzer.getReferenceCache(root.getItemId()).set(child.getUrn(), child.getVersion(), exists);
					
					XTAFileItem resource = root.getDescendantForPath(root.getPath() + "/offline");
					
					if(resource != null) {
						resource.addChild(child);
						
						String hash = getHashForFile(schemaFilePath);
						this.hashesDone.put(hash, child);
						
						if(directPaint) {
							this.itemsForId.put(child.getItemId(), child);
							this.ui.itemAdded(child, child.getParent());
						}
					}
				}
			}
		}
	}

	private void processGenericode(XTAFolderResource root, boolean directPaint, String clType,
			TreeMap<String, String> schemaCache, TreeMap<String, String> reverseSchemaCache) {
		if(!schemaCache.containsKey(clType)) {
			String schemaFilePath = root.getPath() + "/offline/" + clType.replace(":", "_") + ".gc";
			File check = new File(schemaFilePath);
			
			if(!check.exists()) {
				boolean suc = this.schemaAnalyzer.cacheURN(clType, root.getPath() + "/offline", schemaCache, reverseSchemaCache);
				
				if(suc) {
					XTAGenericodeList child = new XTAGenericodeList();
					child.setPath(schemaFilePath);
					
					boolean exists = this.schemaAnalyzer.scanGenericode(new File(schemaFilePath), child, root);
					schemaAnalyzer.getReferenceCache(root.getItemId()).set(child.getUrn(), child.getVersion(), exists);     //, child.getUrn()).addListVersionID(child.getVersion(), exists);
					
					XTAFileItem resource = root.getDescendantForPath(root.getPath() + "/offline");
					
					if(resource != null) {
						resource.addChild(child);
						
						String hash = getHashForFile(schemaFilePath);
						this.hashesDone.put(hash, child);
						
						if(directPaint) {
							this.itemsForId.put(child.getItemId(), child);
							this.ui.itemAdded(child, child.getParent());
						}
					}
				}
			}
		}
	}

	private void enterResolvedCode(XNode item, XTAFolderResource root, XTASchemaFileItem schemaRoot, String clType) {
		XTASchemaFileItem schemaFile = schemaRoot.findSchemaForType(clType);
		if(schemaFile == null) {
			schemaFile = root.getGenericodeList(clType);
		}
		
		if(schemaFile != null) {
			String resolved = resolveCode(clType, schemaFile, item.getValue());
			item.setResolved(resolved);
			item.setSchemaId(schemaFile.getItemId());
		}
		else {
			item.setResolved("Codelistentyp nicht vorhanden " + clType);
		}
	}

	/**
	 * This method resolves the code value from a given codelist specified ba a code key
	 * @param clType	Identifier of the codelist
	 * @param schemaFile	Schema file which contains the code list
	 * @param codeKey	The key of the code
	 * @return	The value in the codelist that corresponds to codeKey
	 */
	private String resolveCode(String clType, XTASchemaFileItem schemaFile, String codeKey) {
		String resolved = "";
		XCodelist codelist = schemaFile.getCodelist(clType);
		if(codelist != null) {
			ArrayList<String> resolution = codelist.getRowForKey(codeKey);
			if(resolution != null) {
				for(int i=1;i<resolution.size();i++) {
					resolved = resolved + resolution.get(i) + ", ";
				}
				if(resolved.endsWith(", ")) {
					resolved = resolved.substring(0, resolved.length() - 2);
				}
			}
			else {
				resolved = "Code nicht vorhanden " + codeKey;
			}
		}
		else {
			resolved = "Codeliste nicht vorhanden " + clType;
		}
		
		return resolved;
	}
	
	/**
	 * this method is called in prior of analyzing an xml file in order to pre-fetch all referenced schema or code list files
	 * and store them in the offline folder of the given root (resource). The files that are pre-fetched will themselves be analyzed 
	 * for references recursively. Note: This method does pre-fetching only in order to prepare a proper analysis. Analysis of the 
	 * according xml files takes place later explicitly. 
	 * @param file	the file to check references in
	 * @param root	the root to which the file belongs
	 */
	public void fetchReferredFiles(File file, XTAFolderResource root) {
		try {
			if(file.getName().endsWith(".xml")) {
				try {
					TreeMap<String, String> schemaCache = this.schemaAnalyzer.getLocalSchemaCache(root.getItemId());
					TreeMap<String, String> reverseSchemaCache = this.schemaAnalyzer.getReverseLocalSchemaCache(root.getItemId());
					
					// Referenzen auf Genericode-Listen
					ReferenceRegister references = this.schemaAnalyzer.getReferenceCache(root.getItemId());
					this.schemaAnalyzer.getListURIReferencesFromMessage(file, references);
					
					ArrayList<String> schemaLocations = this.schemaAnalyzer.getSchemaLocationsFromMessage(file);
					for(String schemaLocation : schemaLocations) {
						String localSchemaPath = file.getParent().replace("\\", "/") + "/" + schemaLocation;
						
						// Schema Location wird ermittelt und ggf. Schemata zwischengespeichert (OFFLINE_CACHED), 
						// damit die Bearbeitung im Falle vieler Dateien schneller geht
						if(root.getProcessMode() == XTAFolderResource.OFFLINE) {
							// Es werden relevante und wahrscheinliche Quellen gesucht,an denen sich das Schema
							// offline befinden koennte. Zwischenergebisse werden gecached, damit nicht immer 
							// wieder von neuem gesucht werden muss
							
							if(schemaLocation.toLowerCase().trim().startsWith("http://") || schemaLocation.toLowerCase().trim().startsWith("https://")) {
								if(schemaCache.containsKey(schemaLocation)) { //containedInSchemaCache(schemaCache, schemaLocation)) {
								}
								else {
									localSchemaPath = schemaLocation;
									URL check = new URL(localSchemaPath);
									String path = check.getPath();
									File schemaCheck = new File(root.getPath() + "/" + path);
									if(!schemaCheck.exists()) {
										schemaCheck = new File(root.getPath() + "/" + schemaCheck.getName());
									}
									if(!schemaCheck.exists()) {
										schemaCheck = new File(file.getParentFile().getAbsolutePath() + "/" + schemaCheck.getName());
									}
									
									this.schemaAnalyzer.putToSchemaCache(schemaCache, reverseSchemaCache, schemaLocation, schemaCheck.getAbsolutePath());
								}
							}
						}
						else if(root.getProcessMode() == XTAFolderResource.ONLINE) {
							if(schemaLocation.toLowerCase().trim().startsWith("http://") || schemaLocation.toLowerCase().trim().startsWith("https://")) {
								localSchemaPath = schemaLocation;
							}
						}
						else if(root.getProcessMode() == XTAFolderResource.ONLINE_CACHED) {
							if(schemaLocation.toLowerCase().trim().startsWith("http://") || schemaLocation.toLowerCase().trim().startsWith("https://")) {
								localSchemaPath = schemaLocation;
								this.schemaAnalyzer.cacheRecursiveURL(new URL(localSchemaPath).toURI().toURL(), root.getPath() + "/offline", schemaCache, reverseSchemaCache, references);
							}
							else {
								localSchemaPath = schemaLocation;
								File check = new File(localSchemaPath);
								if(check.isAbsolute()) {
									this.schemaAnalyzer.cacheRecursiveFile(root, check, root.getPath() + "/offline", schemaCache, reverseSchemaCache, references);
								}
								else {
									check = file.getParentFile();
									while(schemaLocation.startsWith(".")) {
										if(schemaLocation.startsWith("./")) {
											schemaLocation = schemaLocation.substring(2);
										}
										else if(schemaLocation.startsWith("../")) {
											check = check.getParentFile();
											schemaLocation = schemaLocation.substring(3);
										}
										else {
											break;
										}
									}
									localSchemaPath = check.getAbsolutePath() + "/" + schemaLocation;
									
									File schemaCheck = new File(localSchemaPath);
									if(schemaCheck.exists()) {
										this.schemaAnalyzer.cacheRecursiveFile(root, schemaCheck, root.getPath() + "/offline", schemaCache, reverseSchemaCache, references);
									}
								}
							}
							
							processURNs(root, schemaCache, reverseSchemaCache, references, false);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e2) {
		}
	}

	/**
	 * This method processes the content of a given ReferenceRegister object. It loads and caches 
	 * the contents of the referenced resources, processes their contents and stores them within the 
	 * offline folder of the given folder resource 
	 * @param root	The folder resource covering the resources to be analyzed
	 * @param schemaCache	Maps a remote resource to its local counterpart (within the offline folder)
	 * @param reverseSchemaCache	Maps a local resource (within the offline folder) to its remote 
	 * counterpart - this object is the counterpart to schemaCache
	 * @param references	The remote references (URNs) to be processed
	 * @param directPaint	Indicates whether changes shall be displayed on the UI instantly
	 */
	private void processURNs(XTAFolderResource root, TreeMap<String, String> schemaCache,
			TreeMap<String, String> reverseSchemaCache, ReferenceRegister references, boolean directPaint) {
		try {
			for(String listURI : references.listURIs()) {
				TreeMap<String, ListVersionEntry> ref = references.listVersionIds(listURI);
				
				// Ohne Angabe von listVersionIds geht es nicht. Falls keine angegeben ist, wird geraten 
				if(ref.size() > 0) {
					for(String listVersionID : ref.keySet()) {
						// Falls dieser URN bereits untersucht wurde kann er übersprungen werden
						if(references.isChecked(listURI, listVersionID)) {
							continue;
						}
						
						String urn = listURI + "_" + listVersionID;
						
						// Try to load and cache the codelist given by urn from XRepository. Thereby make according entries
						// in schemaCache and reverseSchemaCache
						this.schemaAnalyzer.cacheURN(urn, root.getPath() + "/offline", schemaCache, reverseSchemaCache);
						
						// Store the cached codelist in a coldelist file within the offline folder of the folder resource
						String codelistFilePath = root.getPath() + "/offline/" + urn.replace(":", "_") + ".gc";
						XTAGenericodeList child = new XTAGenericodeList();
						child.setPath(codelistFilePath);
						
						// Analyze the content of the codelist and switch the exists property to true
						boolean exists = this.schemaAnalyzer.scanGenericode(new File(codelistFilePath), child, root);
						references.set(listURI, listVersionID, exists);
						
						// Get a handle for offline folder
						XTAFileItem resource = root.getDescendantForPath(root.getPath() + "/offline");
						
						// Add entries for the new codelist to the offline folder
						if(resource != null) {
							resource.addChild(child);
							
							String hash = getHashForFile(codelistFilePath);
							if(hash != null) {
								this.hashesDone.put(hash, child);
							}
							
							if(directPaint) {
								this.itemsForId.put(child.getItemId(), child);
								this.ui.itemAdded(child, child.getParent());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Returns the local name of a given XML element name. In case the element is fully qualified, the prefix will be cut
	 * @param name	the element name
	 * @return	the local name (part) of the element name
	 */
	public static String localName(String name) {
		try {
			int sep = name.indexOf(":");
			if(sep >= 0) {
				return name.substring(sep + 1);
			}
			else {
				return name;
			}
		} catch (Exception e) {
			return name;
		}
	}
	
	/**
	 * Read a value from a given XML element specified by an XPath expression
	 * @param base	the element node to be evaluated
	 * @param expression	The XPath expression to be used for evaluation
	 * @return	The value read from the XPath evaluation
	 */
	public static String getNodeValue(Node base, String expression) {
		try {
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile(expression);
			String element = (String) expr.evaluate(base, XPathConstants.STRING);
			
			return element;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	/**
	 * Analyzes and returns the type of XML content of a given XML file
	 * @param file	The XML file to be processed
	 * @return	The content type of the XML file (e.g. xs:schema or gc:CodeList)
	 */
	public String getXMLType(File file) {
		String type = null;
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setFeature("http://apache.org/xml/features/honour-all-schemaLocations", true);
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			String filePath = "file:///" + file.getAbsolutePath().replace("\\", "/");
			Document doc = builder.parse(filePath);
			
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			
			XPathExpression expr = xpath.compile("/*[1]");
			Node result = (Node) expr.evaluate(doc, XPathConstants.NODE);
			type = result.getNodeName();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		return type;
	}
	
	/**
	 * Analyzes and returns the type of XML content of a given XML file. No exceptions will be caught but 
	 * thrown for external handling
	 * @param file	The XML file to be processed
	 * @return	The content type of the XML file (e.g. xs:schema or gc:CodeList)
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	public String getXMLTypeUncatched(File file) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		String type = null;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setFeature("http://apache.org/xml/features/honour-all-schemaLocations", true);
		
		DocumentBuilder builder = factory.newDocumentBuilder();
		String filePath = "file:///" + file.getAbsolutePath().replace("\\", "/");
		Document doc = builder.parse(filePath);
		
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		
		XPathExpression expr = xpath.compile("/*[1]");
		Node result = (Node) expr.evaluate(doc, XPathConstants.NODE);
		type = result.getNodeName();
		
		return type;
	}
	
	/**
	 * Calculates a hash value based on the contents of a given file
	 * @param filePath	The path of the input file 
	 * @return	The hash value for the input file
	 */
	public static String getHashForFile(String filePath){
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			// calculating from the given file running its inside
			// while calculating the digest formula

			InputStream input;
			input = new FileInputStream(filePath);
			
			byte[] buffer = new byte[8192];
			int length;
			while( (length = input.read(buffer)) != -1 ) {
			  messageDigest.update(buffer, 0, length);
			}
			byte[] raw = messageDigest.digest();
			input.close();

			//printout in 64 base
			Encoder encoder = Base64.getEncoder();
			String base64 = encoder.encodeToString(raw);
			return base64;
		} catch (NoSuchAlgorithmException e) {
			return null;
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Calculates a hash value based on the characters of a given input string
	 * @param input	The input string
	 * @return	The hash value for the input string
	 */
	public static String getHash(String input){
		try {
			return "ppp-" + input.hashCode();
//			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
//			// calculating from the given file running its inside
//			// while calculating the digest formula
//
//			byte[] buffer = input.getBytes();
//			messageDigest.update(buffer, 0, buffer.length);
//			byte[] raw = messageDigest.digest();
//
//			//printout in 64 base
//			Encoder encoder = Base64.getEncoder();
//			String base64 = encoder.encodeToString(raw);
//			
//			StringBuilder ret = new StringBuilder();
//			for(int i=0;i<base64.length();i++) {
//				char c = base64.charAt(i);
//				if(c >= 'a' && c <= 'z') {
//					ret.append(c);
//				}
//				else if(c >= 'A' && c <= 'Z') {
//					ret.append(c);
//				}
//				else if(c >= '0' && c <= '9') {
//					ret.append(c);
//				}
//				else {
//				}
//			}
//			return ret.toString();
//		} catch (NoSuchAlgorithmException e) {
//			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Exit handling, called when the application terminates to persist the settings and contents
	 */
	public void exit() {
		for(String rk : this.settings.getResources().keySet()) {
			XTAResource rec = this.settings.getResources().get(rk);
			rec.exit();
		}
		
		// Catches all unsaved changes
		this.changes = true;
		this.write();
	}
//	
//	/**
//	 * Recursively remove (clear) all xml information which does not need to be persisted on program quit. 
//	 * Those information will be recovered automatically at the next start. 
//	 * @param item	item to be cleared recursively
//	 */
//	private void lighten(XTATreeItemI item) {
//		if(item instanceof XTAXMLFileItem) {
//			XTAXMLFileItem xml = (XTAXMLFileItem) item;
//			xml.clear();
//		}
//		
//		for(int i=0;i<item.getItemChildCount();i++) {
//			lighten(item.getItemChildAt(i));
//		}
//	}
	
	// Diese Funktion wird zum Programmstart aufgerufen um die XML-Dateien erneut zu validieren. Die XML-Dateien 
	// werden NICHT persistiert, da dies das Einlesen zu langwierig macht
	/**
	 * Recursively process the contents of XML files and folders. This method is called at program start
	 * in order to read XML contents. Those will NOT be persisted since they take a lot of space and slow 
	 * down starting process - this might be changed in the future when more efficient ways are used to 
	 * persist XML files 
	 * @param root	The folder resource containing the XML files and folders
	 * @param item	The current item (XML file or folder) to be analyzed
	 */
	private void weighten(XTAFolderResource root, XTATreeItemI item) {
		try {
			if(item instanceof XTAFileItem) {
				XTAFileItem xml = (XTAFileItem) item;
				File check = new File(xml.getPath());
				
				if(check.exists()) {
					if(item instanceof XTAXMLFileItem && "XMLFile".equals(item.getItemType())) {
						while(nodeThreads >= maxNodeThreads) {
							try {
								Thread.sleep(10);
							} catch (InterruptedException e) {
							}
						}
						ui.setStatusText("Verarbeite: " + check.getAbsolutePath());
						
						Thread t = new Thread(new Runnable() {
							
							@Override
							public void run() {
								try {
									validateAndStructXMLFile((XTAXMLFileItem) xml, root, true);
									ui.itemUpdated(xml);
									
									nodeThreads--;
								} catch (Throwable e) {
									e.printStackTrace();
								}
								finally {
									currentCount++;
								}
							}
						});
						
						nodeThreads++;
						t.start();
					}
				}
				else {
					XTAFileItem parent = (XTAFileItem) item.getParent();
					if(parent != null) {
						parent.removeChild(item.getItemId());
					}
					ui.itemRemoved(item);
				}
				
				for(int i=0;i<item.getItemChildCount();i++) {
					weighten(root, item.getItemChildAt(i));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//	
//	@Override
//	public void write(boolean force) {
//		this.changes = true;
//		this.write();
//	}
	
	/**
	 * Persists the state of this application
	 */
	private synchronized void write() {
		if(changes) {
			long tm = System.currentTimeMillis();
			System.out.println("write started");
//			for(String root : this.settings.getResources().keySet()) {
//				XTAResource item = this.settings.getResources().get(root);
//				this.ui.setStatusText("Schreibe " + item.getItemName());
//				this.lighten(item);
//			}
			
			String source = System.getProperty("user.home") + "/xtaviewer";
			
			if(this.home != null) {
				source = this.home;
			}
			
			this.ui.setStatusText("Schreibe Settings");
			ObjectWriter.writeObject(this.settings, source + "/settings.dat");
			
			this.ui.setStatusText("Schreibe Cache...");
			this.schemaAnalyzer.write();
			this.ui.setStatusText("fertig");
			
			changes = false;
			
			System.out.println("write fertig: " + (System.currentTimeMillis() - tm));
		}
	}
	
	/**
	 * This method returns the the root item (always a folder resource object) of a given XTATreeItemI object
	 * @param item	The XTATreeItemI object
	 * @return	The root of item
	 */
	private XTATreeItemI getRoot(XTATreeItemI item) {
		try {
			if(item.getParent() != null) {
				return getRoot(item.getParent());
			}
			else {
				return item;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}

	/**
	 * This method reads textual contents of a given file
	 * @param filePath	The path of the file to be read
	 * @return	The textual contents of the file
	 * @throws IOException
	 */
	private String readTextFile(String filePath) throws IOException {
		Path path = Paths.get(filePath);
		StringBuilder build = new StringBuilder();
		
		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				build.append(line + "\n");
			}
		}
		
		return build.toString();
	}

	@Override
	public boolean duplicateFile(String itemId, String name) {
		try {
			XTATreeItemI item = this.getItemForId(itemId);
			if(item instanceof XTAXMLFileItem) {
				if(item.getItemName().equals(name)) {
					return false;
				}
				
				File cur = new File(((XTAXMLFileItem) item).getPath());
				File parent = cur.getParentFile();
				File target = new File(parent.getAbsolutePath() + "/" + name);
				
				String content = readTextFile(cur.getAbsolutePath());
				
				BufferedWriter writer = new BufferedWriter(new FileWriter(target));
			    writer.write(content);
			    writer.close();
				
			    XTAFolderResource resource = (XTAFolderResource) this.getRoot(item);
				XTAFileItem parentItem = (XTAFileItem) item.getParent();
				
				XTAXMLFileItem child = new XTAXMLFileItem();
				child.setItemName(target.getName());
				child.setItemId(target.getAbsolutePath());
				child.setPath(target.getAbsolutePath());
				parentItem.addChild(child);
				this.itemsForId.put(child.getItemId(), child);
			    
				Thread t = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							validateAndStructXMLFile(child, resource, true);
							
							ui.itemAdded(child, parentItem);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}
				});
				
				t.start();
				
				return true;
			}
			else {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			
			return false;
		}
	}
	
	/**
	 * This method creates a copy of a given input file or entire folder and stores it into dest
	 * @param src	The sources file or folder to be copied
	 * @param dest	The destination file or folder
	 * @param overwrite	Indicates whether the source file shall be deleted after the copy action
	 */
	private void copyFolder(File src, File dest, boolean overwrite){
	    try {
			if(src.isDirectory()){
			    if(!dest.exists()){
			        dest.mkdir();
			    }

			    String files[] = src.list();

			    for (String file : files) {
			        File srcFile = new File(src, file);
			        File destFile = new File(dest, file);

			        copyFolder(srcFile,destFile, overwrite);
			    }

			}
			else {
			    if(overwrite || !dest.exists()) {
					InputStream in = new FileInputStream(src);
				    OutputStream out = new FileOutputStream(dest); 

				    byte[] buffer = new byte[1024];

				    int length;
				    while ((length = in.read(buffer)) > 0){
				        out.write(buffer, 0, length);
				    }

				    in.close();
				    out.close();
			    }
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public XTAFileItem copyItem(
			String itemId, 
			String destinationFolder, 
			boolean move, 
			int overwritePolicy, 
			String newFileName, 
			XTAFolderResource targetResource, 
			boolean analyze) {
		try {
			XTAFileItem item = (XTAFileItem) this.getItemForId(itemId);
			
			ArrayList<String> attachments = null;
			if(item instanceof XTAXMLFileItem) {
				attachments = ((XTAXMLFileItem) item).getAttachments();
			}
			
			File src = new File(item.getPath());
			
			if(destinationFolder == null) {
				destinationFolder = src.getParent();
			}
			
			File target = new File(destinationFolder + "/" + src.getName());
			
			if(newFileName != null) {
				target = new File(destinationFolder + "/" + newFileName);
			}
			
			// No overwrite
			if(overwritePolicy == OVERWRITE_NO) {
				if(!target.exists()) {
					this.copyFolder(src, target, false);
				}
				else {
					return null;
				}
			}
			// overwrite
			else if(overwritePolicy == OVERWRITE_YES) {
				this.copyFolder(src, target, true);
			}
			// rename if file exists
			else if(overwritePolicy == ITERATE) {
				int count = 1;
				int sep = src.getName().lastIndexOf(".");
				String ext = "";
				String stump = src.getName();
				
				if(sep > 0) {
					ext = stump.substring(sep + 1);
					stump = stump.substring(0, sep);
				}
				
				while(target.exists()) {
					target = new File(destinationFolder + "/" + stump + "_" + count + "." + ext);
					count++;
				}
				
				this.copyFolder(src, target, true);
			}
			// rename if file exists
			else if(overwritePolicy == TIMESTAMP) {
				int sep = src.getName().lastIndexOf(".");
				String ext = "";
				String stump = src.getName();
				
				if(sep > 0) {
					ext = stump.substring(sep + 1);
					stump = stump.substring(0, sep);
				}
				
				while(target.exists()) {
					target = new File(destinationFolder + "/" + stump + "_" + (System.currentTimeMillis() / 1000l) + "." + ext);
				}
				
				this.copyFolder(src, target, true);
			}
			
			final XTAFolderResource resource;
			
			// Falls die Datei von einer anderen Folder-Ressource stammt (also potenziell auch auf eine andere 
			// Schema-Hierarchie zugreift), wird scanOfflineFolder auf true gesetzt
			final boolean crossResourceInsert = (targetResource != null);
			
			if(targetResource != null) {
				resource = targetResource;
			}
			else {
				resource = (XTAFolderResource) this.getRoot(item);
			}
			
			XTAFileItem parentItem = (XTAFileItem) item.getParent();
			
			if(move) {
				this.deleteFolder(src);
				
				XTAFileItem child = new XTAFileItem();
				child.setPath(src.getAbsolutePath());
				parentItem.removeChild(child.getItemId());
				ui.itemRemoved(child);
			}
			
			XTAFileItem result = null;
			XTAFileItem targetParentItem = resource.getDescendantForPath(destinationFolder);
//			if(targetParentItem != null) {
//				final File tg = target;
//				
//				Thread t = new Thread(new NewFileHandler(crossResourceInsert, analyze, tg, targetParentItem, resource, attachments));
//				t.start();
//			}
			if(targetParentItem != null) {
				NewFileHandler handler = new NewFileHandler(crossResourceInsert, analyze, target, targetParentItem, resource, attachments);
				handler.run();
				result = (XTAFileItem) handler.getResult();
			}
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(itemId);
			System.err.println(destinationFolder);
			return null;
		}
	}

	@Override
	public boolean analyzeItem(
			String itemId, 
			XTAFolderResource targetResource) {
		XTAFileItem item = (XTAFileItem) this.getItemForId(itemId);
		
		File src = new File(item.getPath());
//		Thread t = new Thread(new NewFileHandler(true, true, src, (XTAFileItem) item.getParent(), targetResource, null));
//		t.start();
		new NewFileHandler(true, true, src, (XTAFileItem) item.getParent(), targetResource, null).run();
		
		return true;
	}


	@Override
	public void checkPath(String elementId, String targetFolder) {
		LogManager.getRootLogger().debug("Element-ID: " + elementId);
		XTATreeItemI element = this.getItemForId(elementId);
		XTAFolderResource res = (XTAFolderResource) getRoot(element);
		
		XTAFileItem parentItem = null;
		if(targetFolder.replace("\\", "/").startsWith(res.getPath() + "/")) {
			ArrayList<String> pathWay = new ArrayList<>();
			File check = new File(targetFolder);
			check.mkdirs();
			
			while(check.getAbsolutePath().length() > res.getPath().length()) {
				pathWay.add(0, check.getAbsolutePath());
				check = check.getParentFile();
				
				if(check == null) {
					break;
				}
			}
			
			parentItem = res;
			for(String path : pathWay) {
				if(!parentItem.coversPath(path)) {
					XTAFileItem child = new XTAFileItem();
					child.setPath(path);
					parentItem.addChild(child);
//					
//					File chk = new File(path);
					
					ui.itemAdded(child, parentItem);
					
					parentItem = child;
				}
				else {
					parentItem = parentItem.getChildForPath(path);
				}
			}
		}
	}

	@Override
	public void createInstance(String elementId, String targetFolder) {
		if(!creatingInProgress) {
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					createInstanceWorker(elementId, targetFolder);
				}
			});
			t.start();
		}
	}

	/**
	 * This method is the entry point for bulk instantiation of elements contained in a folder structure specified by elementID. 
	 * The actual recursive instantiation process starts from here
	 * @param elementId	Specifies either an element or any kind of parent element of element definitions (folder or schema file). 
	 * In either case the element itself or all sub elements will be created recursively
	 * @param targetFolder	The optional target folder to store the XML instance to. If not specified, the folder of the containing 
	 * schema file will be used instaed
	 */
	private void createInstanceWorker(String elementId, String targetFolder) {
		try {
			this.creatingInProgress = true;
			
			ui.setProgressVisible(true);
			ui.setWorkingStatus(true);
			
			this.optionalElementPolicy = Integer.parseInt(this.settings.getProperties().get("optionalElementPolicy").toString());
			this.optionalAttributePolicy = Integer.parseInt(this.settings.getProperties().get("optionalAttributePolicy").toString());
			this.askUserForChoiceOption = Integer.parseInt(this.settings.getProperties().get("askUserForChoiceOption").toString());
			this.optionalAnyPolicy = Integer.parseInt(this.settings.getProperties().get("optionalAnyPolicy").toString());
			
			LogManager.getRootLogger().debug("Element-ID: " + elementId);
			XTATreeItemI element = this.getItemForId(elementId);
			XTAFolderResource res = (XTAFolderResource) getRoot(element);
			
			XTAFileItem parentItem = null;
			if(targetFolder != null) {
				parentItem = res.getDescendantForPath(targetFolder);
			}
			else {
				parentItem = (XTAFileItem) element.getParent().getParent();
			}
			
			this.createInstanceRecursive(element, targetFolder, parentItem, res, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			ui.setProgressVisible(false);
			ui.setWorkingStatus(false);
			
			this.creatingInProgress = false;
		}
	}

	/**
	 * Returns the working status of the creation process
	 * @return	the working status of the creation process
	 */
	public boolean isWorking() {
		return creatingInProgress;
	}

	/**
	 * This is the actual recursive method which instantiates XML elements
	 * @param element	Specifies the element definition itself or any kind of parent element of element definitions 
	 * (folder or schema file)
	 * @param targetFolder	The target folder to store created XML instances to
	 * @param parentItem	The parent item to which the newly created element will be added
	 * @param res	The folder resource which contains the schema file which defines the element to be created
	 * @param force	Indicates whether offline resources (local copies of remote resources) shall be created
	 */
	private void createInstanceRecursive(XTATreeItemI element, String targetFolder, XTAFileItem parentItem, XTAFolderResource res, boolean force) {
		if("XElement".equals(element.getItemType())) {
			this.doCreateXMLInstance(element, targetFolder, null, parentItem, (XTAFolderResource) res, true);
		}
		else if(element instanceof XTASchemaFileItem){
			TreeMap<String, XElement> rootElements = ((XTASchemaFileItem) element).getRootElements();
			for(String el : rootElements.keySet()) {
				this.doCreateXMLInstance(rootElements.get(el), targetFolder, null, parentItem, (XTAFolderResource) res, true);
			}
		}
		else {
			if(element instanceof XTAFileItem) {
				if(force || !((XTAFileItem) element).getPath().endsWith("/offline")) {
					for(int i=0;i<element.getItemChildCount();i++) {
						createInstanceRecursive(element.getItemChildAt(i), targetFolder, parentItem, res, false);
					}
				}
			}
		}
	}
	
	/**
	 * Creates an XML instance of the specified element
	 * @param element	The element to be created
	 * @param targetFolder	the target folder to store the XML file in
	 * @param targetFileName	The target file name
	 * @param parentItem	The parent item to which the newly created element will be added
	 * @param res	The folder resource which contains the schema file which defines the element to be created
	 * @param addItem	Indicates whether the created element shall be added to its parent element or not
	 * @return	The created XML file
	 */
	private XTAXMLFileItem doCreateXMLInstance(
			XTATreeItemI element, 
			String targetFolder, 
			String targetFileName, 
			XTAFileItem parentItem, 
			XTAFolderResource res, 
			boolean addItem) {
		boolean writeSuccess = false;
		XTAXMLFileItem ret = null;
		
		ExpectationListener listener = this.expectaionListeners.get("createInstance");
		String rid = UUID.randomUUID().toString() + "#" + element.getItemName();
		if(listener != null) {
			listener.raise(rid);
		}
		
		ui.setStatusText("Erzeuge Nachricht: " + element.getItemName() + " (" + element.getParent().toString() + ")");
		
		XTASchemaFileItem schema = (XTASchemaFileItem) element.getParent();
		File schemaFile = new File(((XTAFileItem) schema).getPath());
		File parentFile = schemaFile.getParentFile();
		
		try {
			XModelTreeNode mnode = res.getRootNode();
			XModelTreeNode elementNode = mnode.getChildForName(element.getItemName(), schema.getPath());
			
			TreeMap<String, String> cache = this.schemaAnalyzer.getLocalSchemaCache(res.getItemId());
			TreeMap<String, String> reverseCache = this.schemaAnalyzer.getReverseLocalSchemaCache(res.getItemId());
			
			
			// Buffer fuer die in der zu erzeugenden Nachricht verwendeten Namespaces. Diese muessen alle deklariert sein
			TreeSet<String> usedNamespaces = new TreeSet<>();
			TreeSet<NameSpace> namespaces = schema.recurseAllNamespaces(); //getNamespaces();
			for(NameSpace sp : namespaces) {
				usedNamespaces.add(sp.getPrefix() + ":" + sp.getNamespaceURL());
			}
			
			// Standard Namespace wird hinzugefuegt
			usedNamespaces.add("xsi:http://www.w3.org/2001/XMLSchema-instance");
			
			if(targetFolder == null) {
				targetFolder = parentFile.getAbsolutePath();
			}
			if(targetFileName == null) {
				targetFileName = elementNode.getElementName();
			}
			
			File export = new File(targetFolder + "/" + targetFileName + ".xml");
			
			int count = 1;
			while(export.exists()) {
				targetFileName = elementNode.getElementName() + "_" + count++;
				export = new File(targetFolder + "/" + targetFileName + ".xml");
			}
			
			// Carrier object for recursive creation of all sub elements
			XNode xDocument = new XNode();
			xDocument.setType(XNode.ELEMENT);
			
			// ID und IDREF zurücksetzen
			this.idStore.clear();
			this.idRefStore.clear();
			
			// Hier findet die eigentliche Erzeugung von Model nach Instanz statt!
			this.recurseXSD(
					(XTAFolderResource) res, 
					elementNode, 
					schema, 
					xDocument, 
					usedNamespaces, 
					schema.getDefaultElementForm(), 
					0);
			
			xDocument = xDocument.getItemChildAt(0);
			
			String schemaPath = "";
			if(((XTAFolderResource) res).getProcessMode() == XTAFolderResource.OFFLINE) {
				if(schemaFile.getParent().replace("\\", "/").equals(targetFolder)) {
					schemaPath = schemaFile.getName();
				}
				else {
					schemaPath = Paths.get(export.getAbsolutePath()).relativize(Paths.get(schemaFile.getAbsolutePath())).toString();
				}
			}
			else {
				// Suche die Schema-Referenz im Chache
				schemaPath = schemaFile.getAbsolutePath();
				schemaPath = schemaPath.replace("\\", "/");
				if(cache.containsKey(schemaPath)) {
					schemaPath = cache.get(schemaPath);
				}
				if(reverseCache.containsKey(schemaPath)) {
					schemaPath = reverseCache.get(schemaPath);
				}
				
				if(schemaPath.startsWith("http://") || schemaPath.startsWith("https://")) {
					// TODO: Hier Abfrage einbauen, ob Schema-URL gewollt ist oder lokales Schema referenziert werden soll
				}
				else {
					if(schemaFile.getParent().replace("\\", "/").equals(targetFolder)) {
						schemaPath = schemaFile.getName();
					}
					else {
						schemaPath = Paths.get(export.getParentFile().getAbsolutePath()).relativize(Paths.get(schemaFile.getAbsolutePath())).toString().replace("\\", "/");
					}
				}
			}
			
			// Neu
			if(schema.getTargetNamespace() != null && schema.getTargetNamespace().getNamespaceURL() != null) {
				xDocument.getAttributes().put("xsi:schemaLocation", schema.getTargetNamespace().getNamespaceURL() + " " + schemaPath); // .replace(" ", "%20")
			}
			else {
				xDocument.getAttributes().put("xsi:schemaLocation", schemaPath); // .replace(" ", "%20")
			}
			
			// Buffer table of namespaces to add
			TreeMap<String, String> namespaceBuffer = new TreeMap<>();
			
			// Table of reverse namespaces
			TreeMap<String, String> reverseNamespaces = new TreeMap<>();
			
			// Namespace / Prefix-Attribute setzen
			TreeSet<String> nsDone = new TreeSet<>();
			for(NameSpace namespace : namespaces) {
				if(nsDone.contains(namespace.getPrefix())) {
					continue;
				}
				
				String prefix = namespace.getPrefix();
				namespaceBuffer.put(prefix, namespace.getNamespaceURL());
				
				if(!reverseNamespaces.containsKey(namespace.getNamespaceURL())) {
					reverseNamespaces.put(namespace.getNamespaceURL(), prefix);
				}
				else {
					if(!isDummyPrefix(prefix)) {
						namespaceBuffer.remove(reverseNamespaces.get(namespace.getNamespaceURL()));
						reverseNamespaces.put(namespace.getNamespaceURL(), prefix);
					}
				}
				
				nsDone.add(namespace.getPrefix());
			}
			
			for(String ns : usedNamespaces) {
				String prefix = null;
				String namespaceURL = null;
				NameSpace namespace = null;
				
				int sep = ns.indexOf(":");
				if(sep > 0) {
					prefix = ns.substring(0, sep);
					namespaceURL = ns.substring(sep + 1);
					
					if(namespaceURL.length() > 0) {
						namespace = new NameSpace(prefix, namespaceURL);
					}
				}
				
				if(namespace != null) {
					if(nsDone.contains(prefix)) {
						continue;
					}
					
					namespaceBuffer.put(prefix, namespace.getNamespaceURL());
					
					if(!reverseNamespaces.containsKey(namespace.getNamespaceURL())) {
						reverseNamespaces.put(namespace.getNamespaceURL(), prefix);
					}
					else {
						if(!isDummyPrefix(prefix)) {
							namespaceBuffer.remove(reverseNamespaces.get(namespace.getNamespaceURL()));
							reverseNamespaces.put(namespace.getNamespaceURL(), prefix);
						}
					}
					
					nsDone.add(prefix);
				}
			}
			
			for(String prefix : namespaceBuffer.keySet()) {
				String url = namespaceBuffer.get(prefix);
				
				if(isDummyPrefix(prefix)) {
					xDocument.getAttributes().put("xmlns", url);
				}
				else {
					xDocument.getAttributes().put("xmlns:" + prefix, url);
				}
			}
			
		    XTAFolderResource resource = (XTAFolderResource) this.getRoot(schema);
			
		    XTAXMLFileItem child = new XTAXMLFileItem();
			child.setItemName(export.getName());
			child.setItemId(export.getAbsolutePath());
			child.setPath(export.getAbsolutePath());
			child.addNode(xDocument);
			child.export(false);
			writeSuccess = true;
			this.changes = true;
			
			// Neu erzeugte XML-Datei registrieren und validieren
			if(parentItem != null && addItem) {
				parentItem.addChild(child);
				this.itemsForId.put(child.getItemId(), child);
			    
				ui.itemAdded(child, parentItem);
				validateAndStructXMLFile(child, resource, true);
				
				ui.itemUpdated(child);
		    }
			else {
				validateAndStructXMLFile(child, resource, true);
			}
			
			ret = child;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		finally {
			if(listener != null && writeSuccess) {
				listener.fulfill(rid.toString());
			}
		}
		
		return ret;
	}
	
	public static boolean isDummyPrefix(String prefix) {
		boolean ret = false;
//		
//		if(prefix.startsWith("ppp-")) {
//			try {
//				Integer.parseInt(prefix.substring(4));
//				ret = true;
//			} catch (Exception e) {
//			}
//		}
		
		return ret;
	}
	/**
	 * Print the node hierarchy of a org.w3c.dom.Node to the console for debugging purposes
	 * @param node	The element node to be printed
	 * @param depth	The depth of the node within the hierarchy (for line indent)
	 */
	public void info(Node node, int depth) {
		for(int i=0;i<depth;i++) {
			System.out.print("  ");
		}
		LogManager.getRootLogger().debug(node.getNodeName());
		
		if(node.getChildNodes() != null) {
			NodeList children = node.getChildNodes();
			
			for(int i=0;i<children.getLength();i++) {
				info(children.item(i), depth + 1);
			}
		}
		else {
			for(int i=0;i<depth;i++) {
				System.out.print("  ");
			}
			LogManager.getRootLogger().debug(node.getTextContent());
		}
	}
	
	/**
	 * Print the node hierarchy of an XModelTreeNode to the console for debugging purposes
	 * @param node	The element node to be printed
	 * @param depth	The depth of the node within the hierarchy (for line indent)
	 */
	public void infoModel(XModelTreeNode node, int depth) {
		for(int i=0;i<depth;i++) {
			System.out.print("  ");
		}
		System.out.println(node.toString());
		
		for(int i=0;i<node.getChildCount();i++) {
			infoModel(node.getChildAt(i), depth + 1);
		}
	}
	
	/**
	 * The basic recursive method that recursively processes the element structure of an XML schema element definition
	 * including all its sub constructs
	 * @param root	The folder resource which contains the schema file which defines the element to be created
	 * @param template	The element definition of the element to be created
	 * @param rootSchema	The schema file which contains the element definition
	 * @param xparent	The parent element node to which the new element node shall be added
	 * @param usedNamespacesPrefixes	Buffer object for all used namespace prefixes within the created element
	 * @param defaultForm	The default element for of the XML schema file
	 * @param depth	The depth of element within the element structure (used for debugging purposes and to prevent from 
	 * infinite loops caused by recursive schema definitions
	 * @return	The created element as XNode object
	 */
	private XNode recurseXSD(
			XTAFolderResource root, 
			XModelTreeNode template, 
			XTASchemaFileItem rootSchema, 
			XNode xparent, 
			TreeSet<String> usedNamespacesPrefixes, 
			String defaultForm, 
			int depth) {
		if(debug) {
			for(int i=0;i<depth;i++) {
				System.out.print("  ");
			}
			System.out.println(template.getFullyQualifiedElementName() + " (" + depth + ")");
		}
		String tp = template.getElementTypeName();
		
		if(depth > 50) {
			return null;
		}
		
		XModelTreeNode parent2 = template.getParent();
		
		// Check, ob Element qualifiziert oder unqualifiziert eingebunden werden soll
		// Root-Elemente (depth == 0) werden qualifiziert erzeugt, es sei denn, es ist kein Target-Namespace angegeben
		// Ansonsten werden zunächste template.getForm() und danach das Attribut defaultForm ausgewertet
		XNode xtarget;
		if(depth == 0) {
			XTASchemaFileItem schemaRoot = (XTASchemaFileItem) root.getDescendantForPath(template.getSchemaLocation());
			if(schemaRoot != null) {
				if(schemaRoot.isTargetNamespaceInherited()) {
					xtarget = new XNode(template.getElementName());
				}
				else {
					xtarget = new XNode(template.getFullyQualifiedElementName());
				}
			}
			else {
				xtarget = new XNode(template.getFullyQualifiedElementName());
			}
			
			if(template.getNamespacePrefix() != null && !usedNamespacesPrefixes.contains(template.getNamespacePrefix())) {
				usedNamespacesPrefixes.add(template.getNamespacePrefix());
			}
			else {
				System.out.println(template.getNamespacePrefix());
			}
		}
		else if("".equals(template.getForm())) {
			if("".equals(defaultForm)) {
				xtarget = new XNode(template.getFullyQualifiedElementName());
				
				if(template.getNamespacePrefix() != null && !usedNamespacesPrefixes.contains(template.getNamespacePrefix())) {
					usedNamespacesPrefixes.add(template.getNamespacePrefix());
				}
			}
			else{
				if("unqualified".equals(defaultForm)) {
					xtarget = new XNode(template.getElementName());
				}
				else {
					xtarget = new XNode(template.getFullyQualifiedElementName());
					if(template.getNamespacePrefix() != null) {
						if(!usedNamespacesPrefixes.contains(template.getNamespacePrefix())) {
							usedNamespacesPrefixes.add(template.getNamespacePrefix());
						}
					}
					else {
						LogManager.getRootLogger().debug(template);
					}
				}
			}
		}
		else{
			if("unqualified".equals(template.getForm())) {
				xtarget = new XNode(template.getElementName());
			}
			else {
				xtarget = new XNode(template.getFullyQualifiedElementName());
				if(template.getNamespacePrefix() != null && !usedNamespacesPrefixes.contains(template.getNamespacePrefix())) {
					usedNamespacesPrefixes.add(template.getNamespacePrefix());
				}
			}
		}
		
		// NEU
		xtarget.setTypeNode(template);
		
		// ELEMENT-Eigenschaft setzen
		xtarget.setType(XNode.ELEMENT);
		
		// Knoten anfuegen
		if(!template.isAbstr() && !template.isAny() && !template.isTransparent()) {
			xparent.addChild(xtarget);
		}
		
		if(template.isTransparent()) {
			xtarget = xparent;
		}
		
		// Attribute setzen. Falls listURI bzw. listVersionID gefragt sind, sollen bekannte Listen referenziert werden
		ReferenceRegister references = this.schemaAnalyzer.getReferenceCache(root.getItemId());
		
		// Falls eine listURI spezifiziert wird, soll diese hier eingetragen werden um später ausgewertet zu werden
		// wenn eine listVersionID gefragt ist, die zu dieser listUri passt
		String listURI = null;
		
		for(int i=0;i<template.getAttributes().size();i++) {
			XAttribute att = template.getAttributes().get(i);
			
			// Falls nach Zufall bestmmt werden soll, ob optionale Attribute verwendet werden sollen, wird createAtt zufällig gesetzt
			boolean createAtt = this.optionalAttributePolicy == 0 || (this.optionalAttributePolicy == 2 ? (Math.random() > 0.5) : false);
			
			if(att.getName() != null) {
				if(createAtt || "required".equals(att.getUse())) {
					try {
						String attName = "";
						String attValue = "";
						
						if("qualified".equals(att.getForm())) {
							attName = att.getFullyQualifiedName();
						}
						else {
							attName = att.getName();
						}
						
						if(att.getFixed() != null) {
							attValue = att.getFixed();
							
							if("listURI".equals(att.getName())) {
								listURI = att.getFixed();
							}
						}
						else {
							if("listVersionID".equals(att.getName())) {
								if(listURI != null) {
									if(listURI.startsWith("http://") || listURI.startsWith("https://")) {
										attValue = "";
									}
									else {
										int sep = listURI.lastIndexOf("_");
										if(sep > 0) {
											attValue = listURI.substring(sep + 1);
										}
										else {
											TreeSet<String> ref = references.listExistingVersionIds(listURI); //.get(listURI);
											
											if(ref.size() > 0) {
												int index = (int) (Math.random() * ref.size());
												String listVersionID = ref.toArray()[index].toString();
												attValue = listVersionID;
											}
											else {
												LogManager.getRootLogger().debug(listURI);
												attValue = "1.0";
											}
										}
									}
								}
							}
							else if("listURI".equals(att.getName())) {
								if(references.size() > 0) {
									// Falls hier eine listURI referenziert wird und die Auswahl frei ist (Typ 4), soll versucht 
									// werden, eine bereits bekannte und geladene Genericode-Liste auszuwählen. Es wird dann in den 
									// bekannten GC-Listen eine per Zufall ausgewählt
									if(root.getGenericodeLists().size() > 0) {
										int index = (int) (Math.random() * root.getGenericodeLists().size());
										listURI = root.getGenericodeLists().keySet().toArray()[index].toString();
										
										if(listURI.startsWith("http://") || listURI.startsWith("https://")) {
											attValue = listURI;
										}
										else {
											int sep = listURI.lastIndexOf("_");
											if(sep > 0) {
												attValue = listURI.substring(0, sep);
											}
											else {
												attValue = listURI;
											}
										}
									}
									else {
										int index = (int) (Math.random() * references.size());
										listURI = references.listURIs().toArray()[index].toString();
										attValue = listURI;
									}
								}
								else {
									attValue = "urn:sample.listuri";
								}
							}
							else {
								if(att.getType() != null) {
									// TODO: Hier koennte man eine Methode erzeugen. Der Code ist genau wie der Code unten beim naechsten
									String attType = att.getType();
									XTASchemaFileItem schemaRoot = (XTASchemaFileItem) root.getDescendantForPath(template.getSchemaLocation());
									XTASchemaFileItem schema = schemaRoot.findSchemaForType(attType);
									
									if(schema != null) {
										XSimpleType st = schema.getRootSimpleType(att.getType());
										XCodelist cl = schema.getCodelist(att.getType());
										if(cl != null) {
											int cd = (int) (Math.random() * cl.getValues().size());
											String val = cl.getValues().get(cd).get(0);
											attValue = val;
										}
										else if(st != null) {
											try {
												String val = this.instaniateSimpleType(root, att.getType(), schema);
												attValue = val;
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}
									else {
										attValue = this.suggestAttribute(att);
									}
								}
								else {
									attValue = this.suggestAttribute(att);
								}
							}
						}
						xtarget.getAttributes().put(attName, attValue);
					} catch (DOMException e) {
						e.printStackTrace();
						LogManager.getRootLogger().debug(att.getName());
					}
				}
			}
		}
		
		// Falls Element keine Kindknoten hat...
		if(template.getChildCount() == 0) {
			// TODO: Dies hier ist eine Kruecke! Dass der Code aus dem Elternelement geholt werden muss, liegt daran, dass
			// inline-Codelisten (als anonyme Typen innerhalb der ComplexTypes definiert) noch nicht richtig aufgeloest werden
			if((tp == null || tp.equals("xs:token") || tp.equals("token")) && "code".equals(template.getElementName())) {
				if(parent2 != null) {
					tp = parent2.getElementTypeName();
					
					XTASchemaFileItem schemaRoot = (XTASchemaFileItem) root.getDescendantForPath(template.getSchemaLocation());
					XTASchemaFileItem schema = schemaRoot.findSchemaForType(tp);
					
					if(schema != null) {
						XSimpleType st = schema.getRootSimpleType(tp);
						XCodelist cl = schema.getCodelist(tp);
						if(cl != null) {
							int cd = (int) (Math.random() * cl.getValues().size());
							String val = cl.getValues().get(cd).get(0);
							xtarget.setValue(val);
						}
						else if(st != null) {
							try {
								String val = this.instaniateSimpleType(root, tp, schema);
								xtarget.setValue(val);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						else {
							// TODO: Can be replaced by XComplexType ct = template.getComplexType();
							XComplexType ct = schema.getRootType(tp);
							if(ct != null) {
								if(ct.getSimpleBase() != null) {
									// Suche nach Codeliste
									
									schema = schema.findSchemaForType(ct.getSimpleBase());
									if(schema != null) {
										st = schema.getRootSimpleType(ct.getSimpleBase());
										cl = schema.getCodelist(ct.getSimpleBase());
										
										if(cl != null) {
											int cd = (int) (Math.random() * cl.getValues().size());
											String val = cl.getValues().get(cd).get(0);
											xtarget.setValue(val);
										}
										else if(st != null) {
											try {
												String val = this.instaniateSimpleType(root, st, schema);
												xtarget.setValue(val);
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}
								}
								else {
									// Versuche Genericodelist zu laden und zu finden und dann einen Code daraus zu instanziieren
									
									try {
										String listUri = xparent.getAttributes().get("listURI");
										String listVersionId = xparent.getAttributes().get("listVersionID");
										String gcTarget = listUri + "_" + listVersionId;
										
										if(listUri.startsWith("http://") || listUri.startsWith("https://")) {
											gcTarget = xparent.getAttributes().get("listURI");
										}
										
										XTAGenericodeList gc = root.getGenericodeList(gcTarget);
										cl = gc.getCodelist();
										
										int cd = (int) (Math.random() * cl.getValues().size());
										String val = cl.getValues().get(cd).get(0);
										xtarget.setValue(val);
										xtarget.setXoevType(ct.getListURI() == null ? 4 : (ct.getListVersionID() == null ? 3 : 2));
										xtarget.setCodeType(tp);
										xtarget.setCodelistType(gcTarget);
										xtarget.setSchemaId(gc.getItemId());
									} catch (Exception e) {
										xtarget.setValue("ComplexType (1): " + ct);
									}
								}
							}
							else {
								xtarget.setValue("Wert - nicht CL, CT, ST");
							}
						}
					}
					else {
						xtarget.setValue(generateSampleContent(tp));
					}
				}
			}
			else {
				// Wenn folgende Bedingung erfuellt ist (d.h. template definiert einen anonymen simplen Typ), dann MUSS tp == null sein, 
				// denn ein Element kann nur entweder von einem (komplexen oder simplen) Typ sein ODER einen anonymen Typ definieren.  
				if(template.getFixed() != null) {
					xtarget.setValue(template.getFixed());
				}
				else if(template.getAnonymousSimpleType() != null) {
					try {
						String val = this.instaniateSimpleType(root, template.getAnonymousSimpleType(), null);
						xtarget.setValue(val);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else if(tp != null) {
					XTASchemaFileItem schemaRoot = (XTASchemaFileItem) root.getDescendantForPath(template.getSchemaLocation());
					XTASchemaFileItem schema = schemaRoot.findSchemaForType(tp);
					
					if(schema != null) {
						XSimpleType st = schema.getRootSimpleType(tp);
						XCodelist cl = schema.getCodelist(tp);
						if(cl != null) {
							int cd = (int) (Math.random() * cl.getValues().size());
							String val = cl.getValues().get(cd).get(0);
							xtarget.setValue(val);
							xtarget.setCodelistType(tp);
							xtarget.setSchemaId(schema.getItemId());
						}
						else if(st != null) {
							try {
								String val = this.instaniateSimpleType(root, tp, schema);
								xtarget.setValue(val);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						else {
							XComplexType ct = schema.getRootType(tp);
							
							if(ct != null) {
								if(ct.getSimpleBase() != null) {
									schema = schema.findSchemaForType(ct.getSimpleBase());
									
									if(schema != null) {
										st = schema.getRootSimpleType(ct.getSimpleBase());
										cl = schema.getCodelist(ct.getSimpleBase());
										
										if(cl != null) {
											int cd = (int) (Math.random() * cl.getValues().size());
											String val = cl.getValues().get(cd).get(0);
											xtarget.setValue(val);
										}
										else if(st != null) {
											try {
												String val = this.instaniateSimpleType(root, st, schema);
												xtarget.setValue(val);
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}
									else {
										try {
											String val = this.instaniateSimpleType(root, ct.getSimpleBase(), schema);
											xtarget.setValue(val);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}
							}
						}
					}
					else {
						xtarget.setValue(generateSampleContent(tp));
					}
				}
				// process xs:any element
				else if(template.isAny()) {
					int minOcc = 1;
					try {
						minOcc = Integer.parseInt(template.getMinOccurs());
					} catch (Exception e) {
					}
					
					if(this.optionalAnyPolicy != 1 || minOcc > 0) {
						XModelTreeNode t = template;
						while(t != null) {
							t = t.getParent();
						}
						String namespace = template.getAnyNamespacePrefix();
						
						String suggestedNamespace = null;
						try {
							suggestedNamespace = template.getParent().getNamespacePrefix();
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						ArrayList<XElement> matchingElements = null;
						
						if("##other".equals(namespace)) {
							matchingElements = rootSchema.findByPrefix("!" + template.getSchema().getTargetNamespace().getPrefix());
						}
						else if("##any".equals(namespace)) {
							matchingElements = rootSchema.findByPrefix(null);
						}
						else {
							matchingElements = rootSchema.findByNamespace(namespace);
						}
						
						// prefixes enthält alle bekannten Präfixe
						TreeSet<String> prefixes = rootSchema.getAllPrefixes();
						
						// es sollen nur Typen aus bekannten Namespaces verwendet werden, deswegen werden alle 
						// aussortiert, die nicht bekannt sind
						for(int k=matchingElements.size() - 1;k>=0;k--) {
							if(!prefixes.contains(matchingElements.get(k).getPrefix())) {
								matchingElements.remove(k);
							}
						}
						
						for(int o=0;o<minOcc;o++) {
							if(matchingElements.size() > 0) {
								int choice = 0;
								
								if(this.askUserForChoiceOption == 1 || this.askUserForChoiceOption == 2) {
									ArrayList<String> options = new ArrayList<String>();
									TreeMap<Integer, Integer> optionaMap = new TreeMap<Integer, Integer>();
									
									int opCount = 0;
									for(int j=0;j<matchingElements.size();j++) {
										options.add(matchingElements.get(j).getFullyQualifiedName());
										optionaMap.put(opCount, j);
										opCount++;
									}
																
									choice = getOption(options, optionaMap, opCount);
								}
								else {
									if(suggestedNamespace != null) {
										for(int i=0;i<matchingElements.size();i++) {
											if(suggestedNamespace.equals(matchingElements.get(i).getPrefix())) {
												choice = i;
												break;
											}
										}
									}
								}
								
								XElement el = matchingElements.get(choice);
								
								XModelTreeNode temp = this.schemaAnalyzer.recurseElement(null, el, 0, new ArrayList<String>(), new TreeMap<String, XModelTreeNode>(), el.getSchema(), false);
								
								// xs:any requires the XModelTreeNode to be qualified since it is considered a root element
								temp.setForm("qualified");
								
								usedNamespacesPrefixes.add(temp.getSchema().getTargetNamespace().getPrefix() + ":" + temp.getSchema().getTargetNamespace().getNamespaceURL());
								XNode nd = recurseXSD(root, temp, rootSchema, xparent, usedNamespacesPrefixes, defaultForm, depth + 1);
								nd.setOffspring(template);
							}
						}
					}
				}
			}
		}
		else {
			if(template.isAbstr()) {
				// Process an abstract element
				
				int sTotal = template.getSubstitutionCount();
				int sChoice = (int) (sTotal * Math.random());
				int sCount = 0;
				
				for(int i=0;i<template.getChildCount();i++) {
					XModelTreeNode child = template.getChildAt(i);
					
					// Falls nach Zufall bestmmt werden soll, ob optionale Elemente verwendet werden sollen, wird createElement zufällig gesetzt
					boolean createElement = this.optionalElementPolicy == 0 || (this.optionalElementPolicy == 2 ? (Math.random() > 0.5) : false);
					
					int minOcc = 1;
					try {
						minOcc = Integer.parseInt(child.getMinOccurs());
					} catch (Exception e) {
					}
					
					// TODO: Consider maxOccurs too: maxOcc = Integer.parseInt(child.getMaxOccurs());
					
					if(child.isSubstitution()) {
						if(sCount++ != sChoice) {
							continue;
						}
						
						// TODO: Hier ermoeglichen, dass eine beliebige Anzahl von Elementen erzeugt werden
						if(minOcc > 0) {
							for(int j=0;j<minOcc;j++) {
								recurseXSD(root, child, rootSchema, xparent, usedNamespacesPrefixes, defaultForm, depth + 1);
							}
						}
						else if(createElement){
							// TODO: hier Steuerung einbauen, so dass man 10 +/- setzen kann
							if(depth < 10) {
								recurseXSD(root, child, rootSchema, xparent, usedNamespacesPrefixes, defaultForm, depth + 1);
							}
						}
						
						sCount++;
					}
				}
			}
			else if(template.getComplexType() != null && template.getComplexType().isAbstr()) {
				// Process an abstract data type
				
				ArrayList<String> implementors = template.getComplexType().getImplementors(rootSchema);
				
				if(implementors.size() > 0) {
					int choice = 0;
					
					if(this.askUserForChoiceOption == 1 || this.askUserForChoiceOption == 2) {
						TreeMap<Integer, Integer> optionaMap = new TreeMap<Integer, Integer>();
						
						int opCount = 0;
						for(int j=0;j<implementors.size();j++) {
							optionaMap.put(opCount, j);
							opCount++;
						}
													
						choice = getOption(implementors, optionaMap, opCount);
					}
					
					
					String chosen = implementors.get(choice);
					XComplexType ty = rootSchema.findRootType(chosen);
					
					XElement el = new XElement();
					el.setElementType(chosen);
					el.setSchema(ty.getSchema());
					el.setElementName(chosen + "_Sample");
					el.setParent(ty.getSchema());
					
					XModelTreeNode temp = new XModelTreeNode(ty, el, ty.getSchema().getPath());
					temp = this.schemaAnalyzer.recurseElement(null, el, 0, new ArrayList<String>(), new TreeMap<String, XModelTreeNode>(), ty.getSchema(), false);
					temp.setTransparent(true);
					
					if(!temp.getComplexType().isAbstr()) {
						xtarget.getAttributes().put("xsi:type", temp.getComplexType().getFullyQualifiedName());
						usedNamespacesPrefixes.add(temp.getComplexType().getSchema().getTargetNamespace().getPrefix() + ":" + temp.getComplexType().getSchema().getTargetNamespace().getNamespaceURL());
					}
					
					recurseXSD(root, temp, rootSchema, xtarget, usedNamespacesPrefixes, defaultForm, depth + 1);
				}
			}
			else {
				HashSet<String> choicesDone = new HashSet<String>();
				for(int i=0;i<template.getChildCount();i++) {
					XModelTreeNode child = template.getChildAt(i);
					
					// Falls nach Zufall bestmmt werden soll, ob optionale Elemente verwendet werden sollen, wird createElement zufällig gesetzt
					boolean createElement = this.optionalElementPolicy == 0 || (this.optionalElementPolicy == 2 ? (Math.random() > 0.5) : false);
					
					int minOcc = 1;
					try {
						minOcc = Integer.parseInt(child.getMinOccurs());
					} catch (Exception e) {
					}
					
					// TODO: Hier ermoeglichen, dass eine beliebige Anzahl von Elementen erzeugt werden
					if(child.isChoiceChild()) {
						if(choicesDone.contains(template.getChildAt(i).getChoiceBase())){
							continue;
						}
						
						int choice = i; //(int) (template.getChildCount() * Math.random());
						
						if(this.askUserForChoiceOption == 1 || this.askUserForChoiceOption == 2) {
							ArrayList<String> options = new ArrayList<String>();
							TreeMap<Integer, Integer> optionaMap = new TreeMap<Integer, Integer>();
							
							int opCount = 0;
							for(int j=0;j<template.getChildCount();j++) {
								if(template.getChildAt(i).getChoiceBase().equals(template.getChildAt(j).getChoiceBase())) {
									options.add(template.getChildAt(j).getFullyQualifiedElementName());
									optionaMap.put(opCount, j);
									opCount++;
								}
							}
														
							choice = getOption(options, optionaMap, opCount);
						}
						
						recurseXSD(root, template.getChildAt(choice), rootSchema, xtarget, usedNamespacesPrefixes, defaultForm, depth + 1);
						choicesDone.add(template.getChildAt(i).getChoiceBase());
						
						continue;
					}
					else{
						// TODO: Hier ermoeglichen, dass eine beliebige Anzahl von Elementen erzeugt werden
						if(minOcc > 0) {
							for(int j=0;j<minOcc;j++) {
								recurseXSD(root, child, rootSchema, xtarget, usedNamespacesPrefixes, defaultForm, depth + 1);
							}
						}
						else if(createElement){
							if(depth < 10) {
								recurseXSD(root, child, rootSchema, xtarget, usedNamespacesPrefixes, defaultForm, depth + 1);
							}
						}
					}
				}
			}
		}
		
		return xtarget;
	}

	/**
	 * Returns a random choice (index) among given element options in case of a choice element. In case the 
	 * settings require the user to chose, a choice dialog is displayed. The dialog aoso allows to disable 
	 * this option in order to prevent for large numbers of requests to the user. 
	 * @param options	The possible options as ArrayList of strings ordered lexicographically 
	 * @param optionaMap	A map of indices between the lexicographically ordered options and their position 
	 * within the element definition
	 * @param opCount	The total number of options
	 * @return	The index of the chosen element
	 */
	private int getOption(ArrayList<String> options, TreeMap<Integer, Integer> optionaMap, int opCount) {
		int choice;
		if(this.askUserForChoiceOption == 2) {
			// Diese Option verhindert, dass man allzu oft gefragt wird
			options.add("Nicht mehr fragen");

			int sel = ui.showSelectDialog("Auswahl-Option", "Bitte wählen Sie ein optionales Element", options);
			
			if(sel == options.size() - 1) {
				this.askUserForChoiceOption = 0;
				sel = 0;
			}
			choice = optionaMap.get(sel);
		}
		else {
			int sel = (int) (Math.random() * opCount);
			
			choice = optionaMap.get(sel);
		}
		return choice;
	}
	
	/**
	 * This method creates an instance of a simple type (specified by its fully qualified name which is 
	 * resolved to a type definition first) considering the type definition and returns the created value
	 * @param root	The folder resource containing the simple type definition
	 * @param tp	The simple type name
	 * @param schema	The schema file covering the simple tape
	 * @return	The simple type instance
	 */
	private String instaniateSimpleType(XTAFolderResource root, String tp, XTASchemaFileItem schema) {
		String ret = "";
		
		if("xink:UUID".equals(tp)) {
			ret = this.generateSampleContent(tp);
		}
		else if(tp.startsWith("xs:")) {
			ret = this.generateSampleContent(tp);
		}
		else {
			if(schema != null) {
				schema = schema.findSchemaForType(tp);
				XSimpleType st = schema.getRootSimpleType(tp);
				if(st != null) {
					ret = instaniateSimpleType(root, st, schema);
				}
			}
		}
		
		return ret;
	}

	/**
	 * This method creates an instance of a simple type considering the type definition
	 * @param root	The folder resource containing the simple type definition
	 * @param tp	The simple type
	 * @param schema	The schema file covering the simple tape
	 * @return	The simple type instance
	 */
	private String instaniateSimpleType(XTAFolderResource root, XSimpleType st, XTASchemaFileItem schema) {
		if(schema == null) {
			if(st.getParent() != null && st.getParent() instanceof XTASchemaFileItem) {
				schema = (XTASchemaFileItem) st.getParent();
			}
		}
		String ret = "";
		
		if(st.getRestriction() != null) {
			ret = instaniateSimpleType(root, st.getRestriction(), schema);
		}
		
		// generate random string according to pattern
		if(st.getPattern() != null) {
			if(!Pattern.matches(st.getPattern(), ret)) {
				Generex gen = new Generex(st.getPattern());
				ret = gen.random();
				
				try {
					int minL = Integer.parseInt(st.getMinLenght());
					int count = 0;
					while(ret.length() == 0 && minL > 0) {
						ret = gen.random();
						if(count++ > 1000) {
							System.out.println("Hier Endlosschleife");
							break;
						}
					}
				} catch (Exception e) {
				}
			}
		}
		
		// Make sure the generated string is conform to all restrictions defined in the simple type definition
		ret = adjustToRestrictions(ret, st);
		
		return ret;
	}

	/**
	 * This method tries to fit a given input string to match conditions defined by a simple type definition (including
	 * potential patterns, lengths or limits)
	 * @param ret	The input value to be adjusted
	 * @param st	The simple type definition
	 * @return	The adjusted value
	 */
	private String adjustToRestrictions(String ret, XSimpleType st) {
		// String restrictions...
		try {
			int minLength = Integer.parseInt(st.getMinLenght());
			
			while(ret.length() < minLength) {
				ret = ret + ret;
			}
		} catch (Exception e) {
		}
		try {
			int maxLength = Integer.parseInt(st.getMaxLenght());
			
			if(ret.length() > maxLength) {
				ret = ret.substring(0, maxLength);
			}
		} catch (Exception e) {
		}
		try {
			int length = Integer.parseInt(st.getLenght());
			
			while(ret.length() < length) {
				ret = ret + ret;
			}
			if(ret.length() > length) {
				ret = ret.substring(0, length - 1);
			}
		} catch (Exception e) {
		}
		
		// Number restrictions...
		// Stratgie: Moeglichkeiten abklappern aus min und max 
		try {
			long min = Long.parseLong(st.getMinInclusive());
			long vl = Long.parseLong(ret);
			
			if(vl < min) {
				ret = String.valueOf(min);
			}
		} catch (Exception e) {
			try {
				double min = Long.parseLong(st.getMinInclusive());
				ret = String.valueOf(Math.round(min));
			} catch (Exception e2) {
			}
		}
		try {
			long max = Long.parseLong(st.getMaxInclusive());
			long vl = Long.parseLong(ret);
			
			if(vl > max) {
				ret = String.valueOf(Math.round(max));
			}
		} catch (Exception e) {
			try {
				double max = Double.parseDouble(st.getMaxInclusive());
				ret = String.valueOf(max);
			} catch (Exception e2) {
			}
		}
		
		// Zwischenwertberechnung aus min und max
		try {
			long min = Long.parseLong(st.getMinExclusive());
			try {
				long max = Long.parseLong(st.getMaxExclusive());
				long vl = Long.parseLong(ret);
				
				if(vl >= max || vl <= min) {
					ret = String.valueOf(Math.round(min + (max - min) / 2));
				}
			} catch (Exception e) {
				try {
					double max = Double.parseDouble(st.getMaxExclusive());
					ret = String.valueOf(Math.round(min + (max - min) / 2));
				} catch (Exception e2) {
				}
			}
			ret = String.valueOf(min + 1);
		} catch (Exception e) {
			try {
				double min = Double.parseDouble(st.getMinExclusive());
				try {
					double max = Double.parseDouble(st.getMaxExclusive());
					ret = String.valueOf(min + (max - min) / 2d);
				} catch (Exception e2) {
				}
			} catch (Exception e2) {
			}
		}
		return ret;
	}

	/**
	 * This method generates sample content for given standard simple types
	 * @param tp	The standard simple types
	 * @return	The generated value
	 */
	private String generateSampleContent(String tp) {
		// TODO: should be either qualified OR unqualified?
		String ret = "";
		
		if("xink:UUID".equals(tp) || "UUID".equals(tp)) {
			String uuid = UUID.randomUUID().toString();
			ret = uuid;
		}
		else if("xs:token".equals(tp) || "token".equals(tp)) {
			ret = "sampletoken";
		}
		else if("xs:string".equals(tp) || "string".equals(tp)) {
			ret = "ABCabcXYZxyz";
		}
		else if("xs:normalizedString".equals(tp) || "normalizedString".equals(tp)) {
			ret = "ABCabcXYZxyz";
		}
		else if("xs:int".equals(tp) || "int".equals(tp)) {
			ret = "1";
		}
		else if("xs:integer".equals(tp) || "integer".equals(tp)) {
			ret = "1";
		}
		else if("xs:short".equals(tp) || "short".equals(tp)) {
			ret = "1";
		}
		else if("xs:float".equals(tp) || "float".equals(tp)) {
			ret = "1.0";
		}
		else if("xs:double".equals(tp) || "double".equals(tp)) {
			ret = "1.0";
		}
		else if("xs:long".equals(tp) || "long".equals(tp)) {
			ret = "1000000";
		}
		else if("xs:unsignedLong".equals(tp) || "unsignedLong".equals(tp)) {
			ret = "1000000";
		}
		else if("xs:positiveInteger".equals(tp) || "positiveInteger".equals(tp)) {
			ret = "1";
		}
		else if("xs:negativeInteger".equals(tp) || "negativeInteger".equals(tp)) {
			ret = "-1";
		}
		else if("xs:nonNegativeInteger".equals(tp) || "nonNegativeInteger".equals(tp)) {
			ret = "1";
		}
		else if("xs:nonPositiveInteger".equals(tp) || "nonPositiveInteger".equals(tp)) {
			ret = "1";
		}
		else if("xs:decimal".equals(tp) || "decimal".equals(tp)) {
			ret = "1";
		}
		else if("xs:ID".equals(tp) || "ID".equals(tp)) {
			if(this.idRefStore.size() > 0) {
				ret = this.idRefStore.first();
			}
			else {
				ret = "id" + idCount++;
				this.idStore.add(ret);
			}
		}
		else if("xs:IDREF".equals(tp) || "IDREF".equals(tp)) {
			if(this.idStore.size() > 0) {
				ret = this.idStore.first();
			}
			else {
				ret = "id" + idCount++;
				this.idRefStore.add(ret);
			}
		}
		else if("xs:boolean".equals(tp) || "boolean".equals(tp)) {
			try {
				boolean predef = "2".equals(this.settings.getProperties().get("booleanDefault").toString());
				
				if("0".equals(this.settings.getProperties().get("booleanDefault").toString())) {
					predef = Math.random() > 0.5;
				}
				
				ret = String.valueOf(predef);
			} catch (Exception e) {
				e.printStackTrace();
				ret = "true";
			}
		}
		else if("xs:date".equals(tp) || "date".equals(tp)) {
			ret = "2002-09-24";
		}
		else if("xs:time".equals(tp) || "time".equals(tp)) {
			ret = "09:30:10.5";
		}
		else if("xs:dateTime".equals(tp) || "dateTime".equals(tp)) {
			Calendar now = new GregorianCalendar();
			String yr = String.valueOf(now.get(Calendar.YEAR));
			String mt = setToLen(String.valueOf(now.get(Calendar.MONTH) + 1), 2);
			String dt = setToLen(String.valueOf(now.get(Calendar.DAY_OF_MONTH)), 2);
			String hr = setToLen(String.valueOf(now.get(Calendar.HOUR_OF_DAY)), 2);
			String mn = setToLen(String.valueOf(now.get(Calendar.MINUTE)), 2);
			String sc = setToLen(String.valueOf(now.get(Calendar.SECOND)), 2);
			ret = yr + "-" + mt + "-" + dt + "T" + hr + ":" + mn + ":" + sc;
		}
		else if("xs:duration".equals(tp) || "duration".equals(tp)) {
			ret = "P5Y2M10D";
		}
		else if("xs:gDay".equals(tp) || "gDay".equals(tp)) {
			ret = "---01";
		}
		else if("xs:gMonth".equals(tp) || "gMonth".equals(tp)) {
			ret = "--03";
		}
		else if("xs:gYearMonth".equals(tp) || "gYearMonth".equals(tp)) {
			ret = "2001-10";
		}
		else if("xs:gMonthDay".equals(tp) || "gMonthDay".equals(tp)) {
			ret = "03-02";
		}
		else if("xs:gYear".equals(tp) || "gYear".equals(tp)) {
			ret = "2019";
		}
		else if("xs:anyURI".equals(tp) || "anyURI".equals(tp)) {
			ret = "urn:sample:name";
		}
		else if("xs:base64Binary".equals(tp) || "base64Binary".equals(tp)) {
			ret = "U2FucGxlIFRleHQ=";
		}
		else if("xs:language".equals(tp)) {
			ret = "de-DE";
		}
		
		else if ("xs:anyURI".equals(tp)) {
			ret = "https://example.com";
		} else if ("xs:byte".equals(tp)) {
			ret = "27";
		} else if ("xs:ENTITIES".equals(tp)) {
			// TODO
			ret = "";
		} else if ("xs:ENTITY".equals(tp)) {
			// TODO
			ret = "";
		} else if ("xs:hexBinary".equals(tp)) {
			ret = "3c3f786d6c2076657273696f6e3d22312e302220656e636f64696e673d225554462d22223f3e";
		} else if ("xs:IDREFS".equals(tp)) {
			// TODO
			ret = "";
		} else if ("xs:Name".equals(tp)) {
			ret = "Sample:Name";
		} else if ("xs:NCName".equals(tp)) {
			ret = "SampleName";
		} else if ("xs:NMTOKEN".equals(tp)) {
			ret = "SampleNMTOKEN";
		} else if ("xs:NMTOKENS".equals(tp)) {
			// TODO
			ret = "";
		} else if ("xs:NOTATION".equals(tp)) {
			// TODO
			ret = "";
		} else if ("xs:QName".equals(tp)) {
			ret = "xs:example";
		} else if ("xs:unsignedByte".equals(tp)) {
			ret = "27";
		} else if ("xs:unsignedInt".equals(tp)) {
			ret = "27";
		} else if ("xs:unsignedShort".equals(tp)) {
			ret = "27";
		}		
		
		else {
			ret = "Unknown Type " + tp;
		}
		
		/*
    */
		return ret;
	}
	
	/**
	 * Makes some numeric input string fit a given minimum length by filling up with leading zeros
	 * @param num	The numeric input string
	 * @param len	the minimum length of the input string
	 * @return	The adjusted string
	 */
	private String setToLen(String num, int len) {
		while(num.length() < len) {
			num = "0" + num;
		}
		
		return num;
	}
	
	/**
	 * Suggests sample content for a given attribute
	 * @param att	The attribute definition
	 * @return	The generated attribute value
	 */
	private String suggestAttribute(XAttribute att) {
		String suggestion = generateSampleContent(att.getType());
		
		if(suggestion.length() == 0) {
			suggestion = "Sample Attribute - " + att.getName();
		}
		
		return suggestion;
	}

	public XTAClientI getUi() {
		return ui;
	}

	@Override
	public Serializable getProperty(String string) {
		return this.settings.getProperties().get(string);
	}

	@Override
	public void setProperty(String string, Serializable value) {
		this.settings.getProperties().put(string, value);
		changes = true;
	}
	
	public String getHomeDirectory() {
		return home;
	}
	
	public void addExpectationListener(String domain, ExpectationListener listener) {
		this.expectaionListeners.put(domain, listener);
	}

	@Override
	public boolean deleteItem(String itemId) {
		try {
			XTAFileItem item = (XTAFileItem) this.getItemForId(itemId);
			File src = new File(item.getPath());
			
			XTAFileItem parentItem = (XTAFileItem) item.getParent();
			
			this.deleteFolder(src);
			
			XTAFileItem child = new XTAFileItem();
			child.setPath(src.getAbsolutePath());
			parentItem.removeChild(child.getItemId());
			ui.itemRemoved(child);
			
			this.changes = true;
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			
			return false;
		}
	}

	@Override
	public XTASchemaAnalyzer getAnalyzer() {
		return this.schemaAnalyzer;
	}
	
	/**
	 * Facade method for System.out.println() which only prints when debug == true 
	 * @param ln	The String to be printed
	 */
	public void println(Object ln) {
		if(this.debug) {
			System.out.println(ln);
		}
	}
	
	/**
	 * Output a given XTATreeItem on the console for debug purposes
	 * @param item	the item to be displayed
	 * @param depth	depth indicator for line indent
	 */
	private void printXTATreeItem(XTATreeItemI item, int depth) {
		if(debug) {
			for(int i=0;i<depth;i++) {
				System.out.print("\t");
			}
			
			System.out.println(item.getItemName() + " [" + item.getItemType() + "]");
			
			for(int i=0;i<item.getItemChildCount();i++) {
				printXTATreeItem(item.getItemChildAt(i), depth + 1);
			}
		}
	}
	
	/**
	 * Output a given XModelTreeNode on the console for debug purposes
	 * @param item	the item to be displayed
	 * @param depth	depth indicator for line indent
	 */
	private void printXModelTreeNode(XModelTreeNode item, int depth) {
		if(debug) {
			for(int i=0;i<depth;i++) {
				System.out.print("\t");
			}
			
			if(item != null) {
				if(item.getLabel() != null && item.getElementTypeName() != null) {
					System.out.println(item.getLabel() + " [" + item.getElementTypeName() + "]");
				}
				else if(item.getLabel() != null) {
					System.out.println(item.getLabel());
				}
				else{
					System.out.println(item);
				}
			}
			
			for(int i=0;i<item.getChildCount();i++) {
				printXModelTreeNode(item.getChildAt(i), depth + 1);
			}
		}
	}
	
	/**
	 * 
	 * @author treichling
	 *	Thi class represents a handling process for a new file to be handeled
	 */
	private class NewFileHandler implements Runnable {
		private boolean crossResourceFile;
		private boolean analyzeXMLFile;
		private File tg;
		private XTAFileItem targetParentItem;
		private XTAFolderResource resource;
		private ArrayList<String> attachments;
		
		private XTATreeItemI result = null;

		public NewFileHandler(
				boolean crossResourceFile, 
				boolean analyzeXMLFile, 
				File targetFile, 
				XTAFileItem targetParentItem,
				XTAFolderResource resource, 
				ArrayList<String> attachments) {
			this.crossResourceFile = crossResourceFile;
			this.analyzeXMLFile = analyzeXMLFile;
			this.tg = targetFile;
			this.targetParentItem = targetParentItem;
			this.resource = resource;
			this.attachments = attachments;
		}

		@Override
		public void run() {
			ui.setProgressVisible(true);
			ui.setWorkingStatus(true);
			
			try {
				String type = getXMLType(tg);
				
				if(tg.getName().toLowerCase().endsWith(".xml")) {
					if("gc:CodeList".equals(type)) {
						handleNewGCList();
					}
					else{
						handleNewXMLFile();
					}
				}
				else if(tg.getName().toLowerCase().endsWith(".gc")) {
					if("gc:CodeList".equals(type)) {
						handleNewGCList();
					}
				}
				else if(tg.getName().toLowerCase().endsWith(".xsd")) {
					if("xs:schema".equals(type) || "schema".equals(type)) {
						handleNewXSDFile();
					}
				}
				else {
					// Neuen Eintrag im Datei-Modell erzeugen
					XTAFileItem child = new XTAFileItem();
					child.setPath(tg.getAbsolutePath());
					targetParentItem.addChild(child);
					itemsForId.put(child.getItemId(), child);
					
					this.result = child;
					
					// Neuen Knoten am UI anzeigen
					ui.itemAdded(child, targetParentItem);
					
					// Indexiere alle FolderResources
					updateItemIndex();
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
			
			ui.setWorkingStatus(false);
			ui.setProgressVisible(false);
		}

		/**
		 * Process a new XSD (schema) file according to the rules for schema file handling
		 * @throws ParserConfigurationException
		 * @throws SAXException
		 * @throws IOException
		 * @throws XPathExpressionException
		 */
		public void handleNewXSDFile() throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
			// Erzeuge Offline-Ordner, falls dieser noch nicht existiert
			createOfflineFolder();
			
			// Neuen Eintrag im Datei-Modell erzeugen
			XTASchemaFileItem schem = new XTASchemaFileItem();
			schem.setPath(tg.getAbsolutePath());
			targetParentItem.addChild(schem);
			itemsForId.put(schem.getItemId(), schem);
			
			this.result = schem;
			
			schemaAnalyzer.scanSchemaForNamedElementsAndAttributes(schem);
			
			TreeSet<WorkLeft> workLeft = new TreeSet<>();
			schemaAnalyzer.scanSchema(tg, schem, resource, new TreeMap<String, XTASchemaFileItem>(), workLeft);
			
			TreeMap<String, String> schemaCache = schemaAnalyzer.getLocalSchemaCache(resource.getItemId());
			TreeMap<String, String> reverseSchemaCache = schemaAnalyzer.getReverseLocalSchemaCache(resource.getItemId());
			ReferenceRegister references = schemaAnalyzer.getReferenceCache(resource.getItemId());
			schemaAnalyzer.getListURIReferencesFromMessage(tg, references);
			processURNs(resource, schemaCache, reverseSchemaCache, references, false);
			
			if(tg.isAbsolute()) {
				schemaAnalyzer.cacheRecursiveFile(resource, tg, resource.getPath() + "/offline", schemaCache, reverseSchemaCache, references);
			}
			
			// Create a root node which will keep the entire type hierarchy in its sub nodes (processed in 
			// the next step)
			resource.setRootNode(new XModelTreeNode());
			
			// Create a complete hierarchical model (type tree) from the XSD files and codelists analyzed above
			for(XElement rootEl : resource.getRootElements()) {
				ArrayList<String> typesInChain = new ArrayList<>();
				TreeMap<String, XModelTreeNode> typeRegister = new TreeMap<String, XModelTreeNode>();
				LogManager.getRootLogger().debug(rootEl);
				
				XTASchemaFileItem schema = rootEl.getRootSchema();
				XModelTreeNode child = schemaAnalyzer.recurseElement(resource.getRootNode(), rootEl, 0, typesInChain, typeRegister, schema, false);
				child.setForm("qualified");
			}
			
			// Indexiere alle FolderResources
			updateItemIndex();
			
			ui.itemAdded(schem, targetParentItem);
			ui.itemRestructured(schem);
		}

		/**
		 * Process a new XML file according to the rules for XML handling
		 */
		public void handleNewXMLFile() {
			// Neuen Eintrag im Datei-Modell erzeugen
			XTAXMLFileItem child = new XTAXMLFileItem();
			child.setPath(tg.getAbsolutePath());
			child.setAttachments(this.attachments);
			targetParentItem.addChild(child);
			itemsForId.put(child.getItemId(), child);
			
			this.result = child;
			
			if(crossResourceFile) {
				if(this.analyzeXMLFile) {
					// Erzeuge Offline-Ordner, falls dieser noch nicht existiert
					createOfflineFolder();
					
					fetchReferredFiles(tg, resource);
					
					String offlinePath = resource.getPath() + "/offline";
					XTAFileItem offlineFolder = resource.getDescendantForPath(offlinePath);
					
					if(offlineFolder != null) {
						TreeSet<WorkLeft> workLeft = new TreeSet<>();
						schemaAnalyzer.recurseDirForNamedElementsAndAttributes(offlineFolder);
						recurseFolder(offlineFolder, new File(offlinePath), resource, new TreeMap<String, XTASchemaFileItem>(), false, workLeft);
					}
					
					// Resolve imports and includes to local files
					resolveImports(resource, offlineFolder, schemaAnalyzer.getLocalSchemaCache(resource.getItemId()));
					
					// Create a root node which will keep the entire type hierarchy in its sub nodes (processed in 
					// the next step)
					resource.setRootNode(new XModelTreeNode());
					
					// Create a complete hierarchical model (type tree) from the XSD files and codelists analyzed above
					for(XElement rootEl : resource.getRootElements()) {
						ArrayList<String> typesInChain = new ArrayList<>();
						TreeMap<String, XModelTreeNode> typeRegister = new TreeMap<String, XModelTreeNode>();
						LogManager.getRootLogger().debug(rootEl);
						
						XTASchemaFileItem schema = rootEl.getRootSchema();
						XModelTreeNode node = schemaAnalyzer.recurseElement(resource.getRootNode(), rootEl, 0, typesInChain, typeRegister, schema, false);
						node.setForm("qualified");
					}
					
					ui.resourceUpdated(resource.getItemId());
					
					// Erfasse XML-Struktur und lade ggf. direkt referenzierte Dateien und lege sie im Offline-Ordner ab 
					validateAndStructXMLFile(child, resource, true);
				}
				else {
					// Erfasse XML-Struktur, ohne referenzierte Dateien zu laden
					validateAndStructXMLFile(child, resource, false);
				}
			}
			else {
				// Erfasse XML-Struktur und lade ggf. direkt referenzierte Dateien und lege sie im Offline-Ordner ab 
				validateAndStructXMLFile(child, resource, true);
			}
			
			// Neuen Knoten am UI anzeigen
			ui.itemAdded(child, targetParentItem);
			
			// Indexiere alle FolderResources
			updateItemIndex();
		}

		/**
		 * Creates a new folder offline within the folder resource (if not existing) 
		 */
		public void createOfflineFolder() {
			// Offline folder anlegen falls nötig...
			String folder = resource.getPath();
			XTAFileItem offlineItem = resource.getDescendantForPath(folder + "/offline");
			if(offlineItem == null) {
				// In case of cached processing, the cached files shall be analysed as well in the same manner 
				// as given files (above). Comments are left out. 
				File offline = new File(folder + "/offline");
				if(!offline.exists()) {
					offline.mkdirs();
				}
				XTAFileItem child = new XTAFileItem();
				child.setPath(folder + "/offline");
				resource.addChild(child);
				
				ui.itemAdded(child, resource);
			}
		}

		/**
		 * Process a new Genericode file
		 */
		public void handleNewGCList() {
			XTAGenericodeList gen = new XTAGenericodeList();
			gen.setPath(tg.getAbsolutePath());
			targetParentItem.addChild(gen);
			itemsForId.put(gen.getItemId(), gen);
			
			this.result = gen;
			
			boolean suc = schemaAnalyzer.scanGenericode(tg, gen, resource);
			
			schemaAnalyzer.getReferenceCache(resource.getItemId()).set(gen.getUrn(), gen.getVersion(), suc); // .get(gen.getUrn()).addListVersionID(gen.getVersion(), suc);
			
			if(suc) {
				ui.itemAdded(gen, targetParentItem);
				
				// Indexiere alle FolderResources
				updateItemIndex();
			}
		}

		public XTATreeItemI getResult() {
			return result;
		}
	}

	/**
	 * Fetch process for asynchronous processing of caching referred files from XML files
	 * @author treichling
	 *
	 */
	private final class FetchProcess implements Runnable {
		private final XTAFolderResource newRec;
		private final MessageToDoItem todo;

		private FetchProcess(XTAFolderResource newRec, MessageToDoItem todo) {
			this.newRec = newRec;
			this.todo = todo;
		}

		@Override
		public void run() {
			try {
				fetchReferredFiles(todo.getFile(), newRec);
				
				nodeThreads--;
			} catch (Throwable e) {
				e.printStackTrace();
			}
			finally {
				currentCount++;
			}
		}
	}

	/**
	 * Carrier class for XML files and their according XTAXMLFileItem objects - This is useful for 
	 * buffering XML files for later processing
	 * @author treichling
	 *
	 */
	class MessageToDoItem implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -114642393950925028L;
		
		private File file;
		private XTAXMLFileItem item;
		
		public MessageToDoItem() {
			super();
		}
		
		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
		public XTAXMLFileItem getItem() {
			return item;
		}
		public void setItem(XTAXMLFileItem item) {
			this.item = item;
		}
	}
	
	/**
	 * This class is used to store warnings, errors or fatals thrown by XML validation for later display
	 * @author treichling
	 *
	 */
	public class TransformListener implements Serializable, ErrorHandler{

		/**
		 * 
		 */
		private static final long serialVersionUID = -8050505923178579229L;
		
		private ArrayList<String> warnings = new ArrayList<>();
		private ArrayList<String> errors = new ArrayList<>();
		private ArrayList<String> fatals = new ArrayList<>();

		public ArrayList<String> getWarnings() {
			return warnings;
		}

		public ArrayList<String> getErrors() {
			return errors;
		}

		public ArrayList<String> getFatals() {
			return fatals;
		}

		@Override
		public void warning(SAXParseException exception) throws SAXException {
			this.warnings.add(exception.getMessage() + " - Line " + exception.getLineNumber());
		}

		@Override
		public void error(SAXParseException exception) throws SAXException {
			this.errors.add(exception.getMessage() + " - Line " + exception.getLineNumber());
		}

		@Override
		public void fatalError(SAXParseException exception) throws SAXException {
			this.fatals.add(exception.getMessage() + " - Line " + exception.getLineNumber());
		}
	}
}
