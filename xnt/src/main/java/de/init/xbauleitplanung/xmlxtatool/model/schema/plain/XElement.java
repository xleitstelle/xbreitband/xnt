package de.init.xbauleitplanung.xmlxtatool.model.schema.plain;

import java.io.Serializable;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;

/**
 * This class represents an XML element within a complex type including all of its properties (prefix, name, 
 * form, certain attributes, type references...)
 * 
 * @author treichling
 *
 */
public class XElement extends XTATreeItem implements Serializable, XTATreeItemI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 261692559398920826L;

	// Those fields contain typical properties of XML elements - each offering getters and setters
	private String prefix;
	private String elementName;
	private String form = "";
	private String defaultForm = "unqualified";
	private String minOccurs = "1";
	private String maxOccurs = "1";
	private String fixed;
	private boolean choiceChild = false;
	private String choiceBase = null;
	private String substitutionGroup = null;
	private boolean abstr = false;
	private boolean any = false;
	
	// elementType wird als String modelliert, weil zum Zeitpunkt des einlesens noch nicht klar ist, ob es ein 
	// simpleType oder ein complexType ist - beides ist erlaubt
	private String elementType;
	private XComplexType anonymousType;
	private XSimpleType anonymousSimpleType;
	
	// The schema file which contains this XElement
	private XTASchemaFileItem schema = null;
	
	public XElement() {
		super();
	}

	public void clone(XElement copy) {
		this.prefix = copy.getPrefix();
		this.elementName = copy.getElementName();
		this.elementType = copy.getElementType();
		this.form = copy.getForm();
		this.minOccurs = copy.getMinOccurs();
		this.maxOccurs = copy.getMaxOccurs();
		this.fixed = copy.getFixed();
		this.choiceChild = copy.isChoiceChild();
		this.choiceBase= copy.getChoiceBase();
		this.abstr = copy.isAbstr();
		this.any = copy.isAny();
		this.substitutionGroup = copy.getSubstitutionGroup();
		this.anonymousSimpleType = copy.getAnonymousSimpleType();
		this.anonymousType = copy.getAnonymousType();
		this.schema = copy.getSchema();
	}
	
	public String getAnyID() {
		return this.elementName;
	}
	
	public void setAnyID(String id) {
		this.elementName = id;
	}
	
	public String getAnyNamespace() {
		return this.prefix;
	}
	
	public void setAnyNamespace(String ns) {
		this.prefix = ns;
	}

	public String getElementName() {
		return elementName;
	}

	public String getFullyQualifiedName() {
		String ret = null;
		
		if(this.prefix != null) {
			if(this.elementName != null) {
				ret = this.prefix + ":" + this.elementName;
			}
		}
		else {
			if(this.elementName != null) {
				ret = this.elementName;
			}
		}
		
		return ret;
	}

	public void setElementName(String name) {
		this.elementName = name;
	}

	public String getElementType() {
		return elementType;
	}

	public void setElementType(String type) {
		this.elementType = type;
	}
	
	public XElement getChildByLocalName(String name) {
		XElement ret = null;
		
		for(XTATreeItemI child : this.getChildren()) {
			if(name.equals(((XElement) child).getElementName())) {
				ret = (XElement) child;
				break;
			}
		}
		
		return ret;
	}
	
	public XElement getChildByFullyQualifiedName(String name) {
		XElement ret = null;
		
		for(XTATreeItemI child : this.getChildren()) {
			if(name.equals(((XElement) child).getFullyQualifiedName())) {
				ret = (XElement) child;
				break;
			}
		}
		
		return ret;
	}

	public XComplexType getAnonymousType() {
		return anonymousType;
	}

	public void setAnonymousType(XComplexType anonymousType) {
		this.anonymousType = anonymousType;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getMinOccurs() {
		return minOccurs;
	}

	public void setMinOccurs(String minOccurs) {
		this.minOccurs = minOccurs;
	}

	public String getMaxOccurs() {
		return maxOccurs;
	}

	public void setMaxOccurs(String maxOccurs) {
		this.maxOccurs = maxOccurs;
	}

	@Override
	public String getItemName() {
		return this.getElementName();
	}

	@Override
	public String getItemType() {
		return "XElement";
	}

	@Override
	public int getItemChildCount() {
		return 0;
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		return null;
	}

	public boolean isChoiceChild() {
		return choiceChild;
	}

	public void setChoiceChild(boolean choiceChild, String choiceBase) {
		this.choiceChild = choiceChild;
		this.setChoiceBase(choiceBase);
	}

	@Override
	public String toString() {
		try {
			return this.prefix + ":" + this.elementName + " [" + this.getItemId() + "]";
		} catch (Exception e) {
			return this.elementName;
		}
	}

	public String getChoiceBase() {
		return choiceBase;
	}

	public void setChoiceBase(String choiceBase) {
		this.choiceBase = choiceBase;
	}

	public XSimpleType getAnonymousSimpleType() {
		return anonymousSimpleType;
	}

	public void setAnonymousSimpleType(XSimpleType anonymousSimpleType) {
		this.anonymousSimpleType = anonymousSimpleType;
	}

	public boolean isAbstr() {
		return abstr;
	}

	public void setAbstr(boolean abstr) {
		this.abstr = abstr;
	}

	public String getSubstitutionGroup() {
		return substitutionGroup;
	}

	public void setSubstitutionGroup(String substitutionGroup) {
		this.substitutionGroup = substitutionGroup;
	}

	public String getFixed() {
		return fixed;
	}

	public void setFixed(String fixed) {
		this.fixed = fixed;
	}

	public XTASchemaFileItem getSchema() {
		return schema;
	}

	public void setSchema(XTASchemaFileItem schema) {
		this.schema = schema;
	}

	public String getDefaultForm() {
		return defaultForm;
	}

	public void setDefaultForm(String defaultForm) {
		this.defaultForm = defaultForm;
	}

	public boolean isAny() {
		return any;
	}

	public void setAny(boolean any) {
		this.any = any;
	}
}
