package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeMap;
import java.util.TreeSet;

import de.init.xbauleitplanung.xmlxtatool.model.schema.hierarchical.XModelTreeNode;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XComplexType;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElement;
import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectReader;
import de.init.xbauleitplanung.xmlxtatool.utilities.ObjectWriter;

/**
 * Basic implementation of a resource in the context of this application. A folder recourse simply points to 
 * a folder on the file system
 * 
 * @author treichling
 *
 */
public class XTAFolderResource extends XTAFileItem implements XTAResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6930336640822108747L;
	
	// Possible methods for XML processing
	// OFFLINE = do not access remote schemas
	// ONLINE = access remote schemas
	// ONLINE_CACHED = access and cache remote schemas for further access
	public static int OFFLINE = 0;
	public static int ONLINE = 1;
	public static int ONLINE_CACHED = 2;
	
	// Desired method for XML processing (
	private int processMode = ONLINE_CACHED;
	
	// All root element covered by all XML schema files of this folder resource
	private TreeSet<XElement> rootElements = new TreeSet<>();
	
	// (Virtual) root node as a root of the entire XML type hierarchy defined by all schema files covered by this folder resource
	private transient XModelTreeNode rootNode = new XModelTreeNode();
	
	// List of all genericode lists covered by this folder resource
	private TreeMap<String, XTAGenericodeList> genericodeLists = new TreeMap<String, XTAGenericodeList>();

	// Map of implementors of abstract types covered by this folder resource
	private transient TreeMap<String, ArrayList<String>> implementors = new TreeMap<String, ArrayList<String>>();
	// Set of all orphans (that is complex types which have not yet been analyzed for implementors)
	private transient TreeSet<XComplexType> orphans = new TreeSet<XComplexType>();
	
	// Map of (potential) message meta data for covered XML files
	TreeMap<String, MessageMetaData> metaData = new TreeMap<String, MessageMetaData>();

	public XTAFolderResource() {
		super();
		
		this.readMetaInfo();
	}
//	
//	public XTAFolderResource clone() {
//		XTAFolderResource ret = new XTAFolderResource();
//		
//		ret.children = new ArrayList<>();
//		for(XTATreeItemI child : this.children) {
//			ret.children.add(cloneXML(child));
//		}
//		
//		ret.genericodeLists = this.genericodeLists;
//		ret.implementors = this.implementors;
//		ret.metaData = this.metaData;
//		ret.orphans = this.orphans;
//		ret.processMode = this.processMode;
//		ret.rootElements = this.rootElements;
//		ret.rootNode = this.rootNode;
//		
//		return ret;
//	}
//
//	private XTATreeItemI cloneXML(XTATreeItemI child) {
//		XTATreeItemI ret = null;
//		
//		if(child instanceof XTAXMLFileItem) {
//			ret = ((XTAXMLFileItem) child).clone();
//		}
//		else if(child.getItemChildCount() > 0){
//			ret = child.
//			for(int i=0;i<child.getItemChildCount();i++) {
//				lighten(child.getItemChildAt(i));
//			}
//		}
//		
//		return ret;
//	}

	@Override
	public String getItemType() {
		return "FOLDER_RESOURCE";
	}
	
	/**
	 * Add a genericode list to this folder resource which is then referenced by its name
	 * @param name	Name of the genericode list to be added
	 * @param list	The genericode list itself
	 */
	public void addGenericodeList(String name, XTAGenericodeList list) {
		this.genericodeLists.put(name, list);
	}
	
	/**
	 * Return a given genericode list identified by its name
	 * @param name	The name of the desired genericode list
	 * @return	The genericode list for the given name (if present) or null (if not present)
	 */
	public XTAGenericodeList getGenericodeList(String name) {
		return this.genericodeLists.get(name);
	}
	
	/**
	 * Create a mapping of all registered complex types to potential implementors. This method is called 
	 * after a folder resource has been fully analyzed for XML content and XML schemas 
	 */
	public void registerImplementors() {
		if(this.orphans != null) {
			for(XComplexType orphan : this.orphans) {
				if(this.implementors.containsKey(orphan.getFullyQualifiedName())) {
					orphan.addImplementors(this.implementors.get(orphan.getFullyQualifiedName()));
				}
			}
		}
	}

	/**
	 * Add a root element to this folder resource
	 * @param element	The new root element to be added
	 */
	public void addRootElement(XElement element) {
		if(!rootElements.contains(element)) {
			rootElements.add(element);
		}
	}

	/**
	 * Return all root elements of this folder resource
	 * @return	all root elements of this folder resource
	 */
	public TreeSet<XElement> getRootElements() {
		return rootElements;
	}

	/*
	 * Returns the (virtual) root node of the entire type hierarchy covered by this folder resource
	 */
	public XModelTreeNode getRootNode() {
		return rootNode;
	}

	/**
	 * Sets the (virtual) root node of this folder resource 
	 * @param rootNode	the (virtual) root node of this folder resource
	 */
	public void setRootNode(XModelTreeNode rootNode) {
		this.rootNode = rootNode;
	}

	@Override
	public XTAResourceType getRessourceType() {
		return XTAResourceType.FOLDER;
	}

	/**
	 * Returns the process mode of this folder resource
	 * @return	the process mode of this folder resource
	 */
	public int getProcessMode() {
		return processMode;
	}

	/**
	 * Set the process mode for this folder resource
	 * @param processMode	the desired process mode for this folder resource
	 */
	public void setProcessMode(int processMode) {
		this.processMode = processMode;
	}

	@Override
	public void init() {
	}

	@Override
	public void exit() {
	}

	/**
	 * Return all genericode lists covered by this folder resource
	 * @return	all genericode lists covered by this folder resource
	 */
	public TreeMap<String,XTAGenericodeList> getGenericodeLists() {
		return this.genericodeLists;
	}

	@Override
	public boolean validationDesired() {
		return true;
	}

	/**
	 * Add a new root type to this folder resource
	 * @param newType	a new root type for this folder resource
	 */
	public void addRootType(XComplexType newType) {
		if(this.orphans == null) {
			this.orphans = new TreeSet<XComplexType>();
		}
		if(this.implementors == null) {
			this.implementors = new TreeMap<String, ArrayList<String>>();
		}
		
		this.orphans.add(newType);
		
		if(newType.getComplexBase() != null) {
			if(!this.implementors.containsKey(newType.getComplexBase())){
				this.implementors.put(newType.getComplexBase(), new ArrayList<String>());
			}
			this.implementors.get(newType.getComplexBase()).add(newType.getFullyQualifiedName());
		}
	}
	
	/**
	 * Reads the meta info for XML files covered by this folder resource. Called on startup
	 */
	void readMetaInfo() {
		try {
			File meta = new File(this.getPath() + "/meta.dat");
			if(meta.exists()) {
				try {
					this.metaData = (TreeMap<String, MessageMetaData>) ObjectReader.readObject(meta.getAbsolutePath());
				} catch (Exception e) {
					e.printStackTrace();
					
					this.metaData = new TreeMap<String, MessageMetaData>();
				}
			}
			else {
				this.metaData = new TreeMap<String, MessageMetaData>();
			}
		} catch (Exception e) {
		}
	}
	
	/**
	 * Writes themeta data of XML files. Called on shutdown
	 */
	void writeMetaInfo() {
		// Vor dem Schreiben Bereinigen - wenn Dateien nicht mehr existieren können sie entfernt werden
		Object[] keys = this.metaData.keySet().toArray();
		for(int i=0;i<keys.length;i++) {
			String path = keys[i].toString();
			File meta = new File(path);
			if(!meta.exists()) {
				this.metaData.remove(path);
			}
		}
		
		ObjectWriter.writeObject(this.metaData, this.getPath() + "/meta.dat");
	}
	
	/**
	 * Returns the meta data for a given path of an XML file
	 * @param path	The path of the XML file in question
	 * @return	The according meta data for path (if present) or null (otherwise)
	 */
	public MessageMetaData getMetaData(String path) {
		return this.metaData.get(path.replace("\\", "/"));
	}
	
	/**
	 * Add meta data for a given path of an XML file
	 * @param path	The path of the XML file
	 * @param metaData	The desired meta data to be linked to the path of the XML file
	 */
	public void addMetaData(String path, MessageMetaData metaData) {
		try {
			this.metaData.put(path.replace("\\", "/"), metaData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Called after the standard resource update procedure has been finished. 
	 */
	public void resourceUpdated() {
	}
	
	public Comparator<XTATreeItemI> getComparator(){
		return null;
	}
}
