package de.init.xbauleitplanung.xmlxtatool.model.schema.plain;

import java.io.Serializable;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;

/**
 * This class represents a simple type including all of its properties (patterns, length, digits, ...)
 * 
 * @author treichling
 *
 */
public class XSimpleType extends XTATreeItem implements Serializable, XTATreeItemI{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2838636779982995472L;
	
	private String prefix;
	private String localName;
	
	private String annotation;
	
	private String restriction;
	private String pattern;
	private String minLenght;
	private String maxLenght;
	private String lenght;
	private String minInclusive;
	private String maxInclusive;
	private String minExclusive;
	private String maxExclusive;
	private String fractionDigits;
	private String totalDigits;
	
	public String getName() {
		return this.prefix != null ? this.prefix + ":" + this.getLocalName() : this.getLocalName();
	}
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public XSimpleType() {
		super();
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	@Override
	public String getItemName() {
		return this.getName();
	}

	@Override
	public String getItemType() {
		return "XSimpleType";
	}

	@Override
	public int getItemChildCount() {
		return 0;
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		return null;
	}

	public String getFullyQualifiedName() {
		String ret = null;
		
		if(this.prefix != null) {
			if(this.localName != null) {
				ret = this.prefix + ":" + this.localName;
			}
		}
		else {
			if(this.localName != null) {
				ret = this.localName;
			}
		}
		
		return ret;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getRestriction() {
		return restriction;
	}

	public void setRestriction(String restriction) {
		this.restriction = restriction;
	}

	public String getMinLenght() {
		return minLenght;
	}

	public void setMinLenght(String minLenght) {
		this.minLenght = minLenght;
	}

	public String getMaxLenght() {
		return maxLenght;
	}

	public void setMaxLenght(String maxLenght) {
		this.maxLenght = maxLenght;
	}

	public String getLenght() {
		return lenght;
	}

	public void setLenght(String lenght) {
		this.lenght = lenght;
	}

	public String getMinInclusive() {
		return minInclusive;
	}

	public void setMinInclusive(String minInclusive) {
		this.minInclusive = minInclusive;
	}

	public String getMaxInclusive() {
		return maxInclusive;
	}

	public void setMaxInclusive(String maxInclusive) {
		this.maxInclusive = maxInclusive;
	}

	public String getMinExclusive() {
		return minExclusive;
	}

	public void setMinExclusive(String minExclusive) {
		this.minExclusive = minExclusive;
	}

	public String getMaxExclusive() {
		return maxExclusive;
	}

	public void setMaxExclusive(String maxExclusive) {
		this.maxExclusive = maxExclusive;
	}

	public String getFractionDigits() {
		return fractionDigits;
	}

	public void setFractionDigits(String fractionDigits) {
		this.fractionDigits = fractionDigits;
	}

	public String getTotalDigits() {
		return totalDigits;
	}

	public void setTotalDigits(String totalDigits) {
		this.totalDigits = totalDigits;
	}
}
