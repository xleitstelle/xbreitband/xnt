package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * This class collects meta data for XML files that are considered to be messages. That is: They get sent from one 
 * location to another. Since XML messages typically do not cover sender, receiver or send date, they may be linked 
 * to an instance of this class in order to add meta data
 */
public class MessageMetaData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5480148667546716897L;
	
	public static int UNKNOWN = 0;
	public static int SENT = 1;
	public static int RECEIVED = 2;
	
	/**
	 * Sender of the XML message
	 */
	private String sender;
	/**
	 * List of receivers of the XML message
	 */
	private ArrayList<String> recipients;
	/**
	 * Date of sending
	 */
	private long sendDate;
	
	/**
	 * indicates whether this message was sent or received
	 */
	private int via = SENT;
	
	public MessageMetaData() {
		super();
	}
	
	public static MessageMetaData create() {
		return new MessageMetaData();
	}
	
	public String getSender() {
		return sender;
	}
	public MessageMetaData setSender(String sender) {
		this.sender = sender;
		
		return this;
	}
	public ArrayList<String> getRecipients() {
		return recipients;
	}
	public MessageMetaData setRecipients(ArrayList<String> recipients) {
		this.recipients = recipients;
		
		return this;
	}
	public long getSendDate() {
		return sendDate;
	}
	public MessageMetaData setSendDate(long sendDate) {
		this.sendDate = sendDate;
		
		return this;
	}

	public MessageMetaData addRecipient(String receiver) {
		if(this.recipients == null) {
			this.recipients = new ArrayList<String>();
		}
		
		this.recipients.add(receiver);
		return this;
	}

	public int getVia() {
		return via;
	}

	public MessageMetaData setVia(int via) {
		this.via = via;
		
		return this;
	}
}