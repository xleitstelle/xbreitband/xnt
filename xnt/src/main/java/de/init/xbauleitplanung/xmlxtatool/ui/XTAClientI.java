package de.init.xbauleitplanung.xmlxtatool.ui;

import java.util.List;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAResource;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;

public interface XTAClientI {

	/**
	 * Indicates to this client instance whether the controller object is busy and hence not ready to handle 
	 * user interaction
	 * @param working	working status of the controller object (true = busy, false = not busy)
	 */
	void setWorkingStatus(boolean working);

	/**
	 * Indicates to this client instance that another root tree element was created which is ready to be 
	 * displayed in the UI of this client instance
	 * @param resource	The newly created resource object
	 */
	void resourceAdded(XTAResource resource);

	/**
	 * Indicates to this client instance that a root tree element was removed and hence needs to be removed 
	 * within the UI of this client instance. 
	 * @param id	ID of the removed tree element
	 */
	void resourceRemoved(String id);

	/**
	 * Indicates to this client instance that some root tree element was updated in the background and needs 
	 * to be completely reread into the UI of this client instance. 
	 * @param id	ID of the updated tree element
	 */
	void resourceUpdated(String id);

	/**
	 * Indicates to this client instance that some (non-root) tree element was added to a specified parent 
	 * element and the UI needs to be updated. 
	 * @param item	The item added
	 * @param parent	The parent item of the item added
	 */
	void itemAdded(XTATreeItemI item, XTATreeItemI parent);

	/**
	 * Indicates to this client instance that some (non-root) tree element was updated in the background and 
	 * needs to be completely reread into the UI of this client instance. 
	 * @param item	The item updated
	 */
	void itemUpdated(XTATreeItemI item);

	/**
	 * Indicates to this client instance that some (non-root) tree element was restructured in the background and 
	 * needs to be recursively reread into the UI of this client instance. 
	 * @param item	The item restructured
	 */
	void itemRestructured(XTATreeItemI item);

	/**
	 * Indicates to this client instance that some (non-root) tree element was removed in the background and 
	 * needs to be removed in the UI of this client instance. 
	 * @param item	The item updated
	 */
	void itemRemoved(XTATreeItemI item);

	/**
	 * Called to request the UI to make the progress bar visible on screen
	 * @param vis	true, if the progress bar shall be visible, false otherwise
	 */
	void setProgressVisible(boolean vis);

	/**
	 * Indicates the current progress of some process which is displayed in the progress bar with regard 
	 * to the maximum progress. 
	 * @param progress	Current progress of some process which progress is displayed in the progress bar
	 */
	void setProgressValue(int progress);

	/**
	 * Indicates the maximum progress of some process which is displayed in the progress bar. 
	 * @param max	Maximum progress of some process which is displayed in the progress bar
	 */
	void setProgressMax(int max);

	/**
	 * Indicates the current progress of some process which is displayed in the progress bar. 
	 * @param prog	Current progress must be an element of [0,1] U {-1}
	 */
	void setProgress(double prog);

	/**
	 * Sets the text of the status bar of this UI to the specified text
	 * @param text	Desired text to be displayed on the status bar
	 */
	void setStatusText(String text);

	/**
	 * Opens a message dialog on screen with the specified title, message and type (info, warn, fatal). 
	 * @param title	The title to be displayed
	 * @param mesasage	The message to be displayed
	 * @param type	The desired type of the message dialog (info, warn, fatal)
	 */
	void showMessageDialog(String title, String mesasage, int type);

	/**
	 * Opens a message dialog on screen with the specified title, message and select options. 
	 * @param title	The title to be displayed
	 * @param mesasage	The message to be displayed
	 * @param options	Array of options - a user may must select one of them
	 * @return the index of the selected option
	 */
	int showSelectDialog(String title, String mesasage, List<String> options);
	/**
	 * OPens a confirm dialog on screen with the specified title, message and select options. 
	 * @param title	The title to be displayed
	 * @param mesasage	The message to be displayed
	 * @return	0, 1 or 2 in case the user clicks CANCEL, YES or NO respectively
	 */
	int showConfrmDialog(String title, String message);
	
	/**
	 * Indicates whether elementary function triggers shall be enabled or not
	 * @param function	Specifies the funtion to be enabled or disabeld ("xta-newfolder", "xta-remove", "xta-restaccount" or 
	 * "xta-newxtaacount")
	 * @param enable	true if the trigger shall be enabled, false otherwise
	 */
	void enable(String function, boolean enable);
}