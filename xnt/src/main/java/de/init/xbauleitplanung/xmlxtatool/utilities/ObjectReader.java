package de.init.xbauleitplanung.xmlxtatool.utilities;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class ObjectReader
{
	private List<String> lines;
	private int counter = -1;
	
	private String last;
	
	public ObjectReader(String fileName){
		try{
			this.lines = new ArrayList<String>();
			try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			    for(String line; (line = br.readLine()) != null; ) {
			    	this.lines.add(line);
			    }
			}
			this.counter = 0;
		}	
		catch (IOException e) {
		}
	}
	
	public ObjectReader(InputStream in){
		try{
			this.lines = new ArrayList<String>();
			try(BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
			    for(String line; (line = br.readLine()) != null; ) {
			    	this.lines.add(line);
			    }
			}
			this.counter = 0;
		}	
		catch (IOException e) {
		}
	}

	public static Object readObject(String file){
		try{
			Object ret;
	        FileInputStream istream = new FileInputStream(file);
		    ObjectInputStream p = new ObjectInputStream(istream);

			ret=p.readObject();
			istream.close();

			return ret;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public int getInt(){
		try{
			last=readLine();

			return Integer.valueOf(last).intValue();
		}
		catch(Exception e){
			System.err.println(e.getMessage());

			return 0;
		}
	}

	public long getLong(){
		try{
			last=readLine();

			return Long.valueOf(last).longValue();
		}
		catch(Exception e){
			e.printStackTrace();

			return 0;
		}
	}

	public String getString(){
		try{
			last=readLine();
//
//			last = last.replace("__a__", "\u00E4");
//			last = last.replace("__o__", "\u00F6");
//			last = last.replace("__u__", "\u00FC");
//			last = last.replace("__A__", "\u00C4");
//			last = last.replace("__O__", "\u00D6");
//			last = last.replace("__U__", "\u00DC");
//			last = last.replace("__s__", "\u00DF");
//			
//			last = last.replace("__E__", "\u20AC");
//			last = last.replace("__D__", "\u0024");
			
//			last = last.replace("\u00E4", "�");
//			last = last.replace("\u00F6", "�");
//			last = last.replace("\u00FC", "�");
//			last = last.replace("\u00C4", "�");
//			last = last.replace("\u00D6", "�");
//			last = last.replace("\u00DC", "�");
//			last = last.replace("\u00DF", "�");
//			
//			last = last.replace("\u20AC", "�");
//			last = last.replace("\u0024", "$");
			
			return last;
		}
		catch(Exception e){
			return null;
		}
	}

	// Left for compatibility reasons only
	public void close(){
		try{
		}
		catch(Exception e){
		}
	}
	
	private String readLine(){
		if(this.counter < this.lines.size()){
			return this.lines.get(counter++);
		}
		else{
			return null;
		}
	}
}
