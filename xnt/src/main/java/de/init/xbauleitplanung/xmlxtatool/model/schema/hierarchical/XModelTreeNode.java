package de.init.xbauleitplanung.xmlxtatool.model.schema.hierarchical;

import java.io.Serializable;
import java.util.ArrayList;

import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XAttribute;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XComplexType;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XElement;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XSimpleType;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;

/**
 * This class represents a node within the hierarchical XML schema model that was derived from the 
 * schema files analyzed in prior steps. An instance of XModelTreeNode represents an XML element, its type, all of
 * its attributes and children. Since instances of this type are directly derived from either XElement or XComplexType
 * references to those templates are kept here instead of copies. 
 * 
 * @author treichling
 *
 */
public class XModelTreeNode implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2844378575850447958L;
	
	/**
	 * Child elements and parent of this element
	 */
	private ArrayList<XModelTreeNode> children = new ArrayList<>();
	private XModelTreeNode parent = null;
	
	/**
	 * Label for this element node (for debug purposes)
	 */
	private String label = null;

	/**
	 * namespace prefix for this element
	 */
	private String namespacePrefix = null;
	
	/**
	 * Element and complex type from which this XModelTreeNode was derived
	 */
	private XElement element;
	private XComplexType type;

	/**
	 * form (qualified / unqualified) of this XModelTreeNode
	 */
	private transient String form = null;
	/**
	 * is this a substitution of some abstract type? 
	 */
	private boolean substitution = false;
	/**
	 * when an instance of this XModelTreeNode is created, transparent indicates that no XML element shall be created. Instead, 
	 * all sub elements will be added to the parent of this XModelTreeNode
	 */
	private boolean transparent = false;
	
	/**
	 * Attributes of this XModelTreeNode
	 */
	private ArrayList<XAttribute> attributes = new ArrayList<>();
	
	/**
	 * location path of the schema file in which this element is defined
	 */
	private String schemaLocation = null;

	/**
	 * Flags this XModelTreeNode as a deeper occurrence of an equal XModelTreeNode. This prevents infinite loops when creating 
	 * an instance of this XModelTreeNode
	 */
	private boolean circle = false;
	
	/**
	 * Default constructor
	 */
	public XModelTreeNode() {
		super();
	}
	
	/**
	 * Constructs an XModelTreeNode based on corresponding type, element definition and schema location
	 * @param type	XComplexType of this XModelTreeNode
	 * @param el	XElement of this XModelTreeNode
	 * @param schemaLocation	Schema location of this XModelTreeNode
	 */
	public XModelTreeNode(XComplexType type, XElement el, String schemaLocation) {
		this.schemaLocation = schemaLocation;
		
		this.element = el;
		this.type = type;
	}
	
	/**
	 * Creates a clobe instance of this XModelTreeNode
	 */
	public XModelTreeNode clone() {
		XModelTreeNode ret = new XModelTreeNode();
		
		ret.setNamespacePrefix(this.getNamespacePrefix());
		ret.setSubstitution(this.isSubstitution());
		
		ret.label = this.label;
		ret.schemaLocation = this.schemaLocation;
		
		ret.element = this.element;
		ret.type = this.type;
		
		ret.children = this.children;
		ret.attributes = this.attributes;
		
		return ret;
	}
	
	@Override
	public String toString() {
		return getLabel();
	}

	/**
	 * Computes a label string for this XModelTreeNode for display on UI or debug purposes. 
	 */
	private void computeLabel() {
		if(this.element != null) {
	
			label = this.element.getElementName();
			if(this.element.getElementType() != null) {
				label = label + " [" + element.getElementType();
				
				if(type != null) {
					if(type.getComplexBase() != null) {
						if(type.getDerivationType() == XComplexType.EXTENSION) {
							label = label + " --> Extension of " + type.getComplexBase() + "]";
						}
						else if(type.getDerivationType() == XComplexType.RESTRICTION) {
							label = label + " --> Restriction of " + type.getComplexBase() + "]";
						}
					}
					else if(type.getSimpleBase() != null) {
						if(type.getDerivationType() == XComplexType.EXTENSION) {
							label = label + " --> Extension of " + type.getSimpleBase() + "]";
						}
						else if(type.getDerivationType() == XComplexType.RESTRICTION) {
							label = label + " --> Restriction of " + type.getSimpleBase() + "]";
						}
					}
					else {
						label = label + "]";
					}
				}
				else {
					label = label + "]";
				}
			}
			else if(this.element.getAnonymousType() != null) {
				label = label + " [Anonymous subtype: " + this.element.getAnonymousType().getName() + "]";
			}
		}
		else {
			label = "NULL (2)";
		}
	}

	/**
	 * Returns the derivation type of the corresponding XComplexType
	 * @return	the derivation type of the corresponding XComplexType
	 */
	public int getDerivationType() {
		int ret = XComplexType.NONE;
		
		if(this.type != null) {
			ret = this.type.getDerivationType();
		}
		
		return ret;
	}

	/**
	 * Returns the computed label string for this XModelTreeNode for display on UI or debug purposes. 
	 */
	public String getLabel() {
		if(this.label == null || label.length() == 0) {
			computeLabel();
		}
		
		return label;
	}


	/**
	 * Returns the local name of the element of this XModelTreeNode
	 * @return	the local name of the element of this XModelTreeNode
	 */
	public String getElementName() {
		String ret = null;
		
		if(this.element != null) {
			ret = XTATreeItem.getLocalName(this.element.getElementName());
		}
		
		return ret;
	}
//
//	/**
//	 * Returns the fully qualified name of this XMOdelTreeNode
//	 * @return	the fully qualified name of this XMOdelTreeNode
//	 */
//	public String getFullyQualifiedElementName() {
//		if(namespacePrefix == null) {
//			this.namespacePrefix = XTATreeItem.getPrefix(this.getElementName());
//		}
//		if(this.getElementName() != null) {
//			if(this.getElementName().indexOf(":") < 0) {
//				if(this.namespacePrefix != null) {
//					return this.namespacePrefix + ":" + this.getElementName();
//				}
//				else {
//					return this.getElementName();
//				}
//			}
//			else {
//				return this.getElementName();
//			}
//		}
//		else {
//			return this.getElementName();
//		}
//	}
	
	
	
	/**
	 * Returns the fully qualified name of this XMOdelTreeNode
	 * @return	the fully qualified name of this XMOdelTreeNode
	 */
	public String getFullyQualifiedElementName() {
		if(namespacePrefix == null) {
			this.namespacePrefix = XTATreeItem.getPrefix(this.getElementName());
		}
		if(this.getElementName() != null) {
			if(this.getElementName().indexOf(":") < 0) {
				if(this.namespacePrefix != null) {
					String ret = this.namespacePrefix + ":" + this.getElementName();
					
					if(namespacePrefix.startsWith("ppp-")) {
						try {
							Integer.parseInt(namespacePrefix.substring(4));
							ret = this.getElementName();
						} catch (Exception e) {
						}
					}
					
					return ret;
				}
				else {
					return this.getElementName();
				}
			}
			else {
				return this.getElementName();
			}
		}
		else {
			return this.getElementName();
		}
	}
	
	
	
	

	/**
	 * Returns the standard element name of this XModelTreeNode. That is either the fully qualified name (in case form equals qualified)
	 * or the local name (in case form equals unqualified)
	 * @return	the standard element name of this XModelTreeNode
	 */
	public String getStandardElementName() {
		if(this.getForm() == null || "".equals(this.getForm())) {
			if("qualified".equals(this.getDefaultForm())) {
				return this.getFullyQualifiedElementName();
			}
			else {
				return this.getElementName();
			}
		}
		else if("qualified".equals(this.getForm())) {
			return this.getFullyQualifiedElementName();
		}
		else {
			return this.getElementName();
		}
	}

	/**
	 * Returns the type name of the XElement of this XModelTreeNode
	 * @return	the type name of the XElement of this XModelTreeNode
	 */
	public String getElementTypeName() {
		if(this.element != null) {
			return this.element.getElementType();
		}
		
		return null;
	}

	/**
	 * Returns the name of the (complex or simple) base type of the XCompleType of this XModelTreeNode
	 * @return	the name of the (complex or simple) base type of the XCompleType of this XModelTreeNode
	 */
	public String getBaseTypeName() {
		String ret = null;
		
		if(this.type != null) {
			ret = this.type.getComplexBase();
			
			if(ret == null) {
				ret = this.type.getSimpleBase();
			}
		}
		
		return ret;
	}

	/**
	 * Returns all attributes of this XModelTreeNode
	 * @return	all attributes of this XModelTreeNode
	 */
	public ArrayList<XAttribute> getAttributes() {
		return attributes;
	}
	
	/**
	 * Returns the namespace prefix of the XElement of this XModelTreeNode
	 * @return	the namespace prefix of the XElement of this XModelTreeNode
	 */
	public String getAnyNamespacePrefix() {
		return this.element.getPrefix();
	}

	/**
	 * Returns the namespace prefix of this XModelTreeNode
	 * @return	the namespace prefix of this XModelTreeNode
	 */
	public String getNamespacePrefix() {
		return namespacePrefix;
	}

	/**
	 * Sets the the namespace prefix of this XModelTreeNode
	 * @param namespacePrefix	The new namespace prefix of this XModelTreeNode
	 */
	public void setNamespacePrefix(String namespacePrefix) {
		this.namespacePrefix = namespacePrefix;
	}

	/**
	 * Returns the child with index i of this XModelTreeNode
	 * @param i	The index of the desired child
	 * @return	the child with index i of this XModelTreeNode
	 */
	public XModelTreeNode getChildAt(int i) {
		return this.children.get(i);
	}

	/**
	 * Returns the number of childs of this XModelTreeNode
	 * @return	the number of childs of this XModelTreeNode
	 */
	public int getChildCount() {
		return this.children.size();
	}
	
	/**
	 * Returns the child with the specified element name of this XModelTreeNode
	 * @param name	The desired name of the child to be returned
	 * @return	the child with the specified element name of this XModelTreeNode
	 */
	public XModelTreeNode getChildForName(String name) {
		for(XModelTreeNode child : this.children) {
			if(name.equals(child.getElementName())) {
				return child;
			}
		}
		return null;
	}
	
	/**
	 * Returns the child with the specified element name and schemaLocation of this XModelTreeNode
	 * @param name	The desired name of the child to be returned
	 * @param schemaLocation	The desired schemaLocation of the child to be returned
	 * @return	the child with the specified element name and schemaLocation of this XModelTreeNode
	 */
	public XModelTreeNode getChildForName(String name, String schemaLocation) {
		for(XModelTreeNode child : this.children) {
			if(name.equals(child.getElementName())) {
				if(schemaLocation.equals(child.schemaLocation)) {
					return child;
				}
			}
		}
		return null;
	}

	/**
	 * Returns the minOccurs property of the XElement of this XModelTreeNode
	 * @return	the minOccurs property of the XElement of this XModelTreeNode
	 */
	public String getMinOccurs() {
		return this.element.getMinOccurs();
	}

	/**
	 * Returns the maxOccurs property of the XElement of this XModelTreeNode
	 * @return	the maxOccurs property of the XElement of this XModelTreeNode
	 */
	public String getMaxOccurs() {
		return this.element.getMaxOccurs();
	}

	/**
	 * Returns whether the XElement of this XModelTreeNode is a direct child element of a choice element
	 * @return	true if the XElement of this XModelTreeNode is a direct child element of a choice element, false otherwise
	 */
	public boolean isChoiceChild() {
		return this.element.isChoiceChild();
	}

	/**
	 * Adds a new child member to this XModelTreeNode
	 * @param newNode	The new child member to be added
	 */
	public void add(XModelTreeNode newNode) {
		this.children.add(newNode);
		newNode.setParent(this);
	}

	/**
	 * Returns the parent node of this XMOdelTreeNode or null of this XModelTreeNode represents a root element 
	 * @return	the parent node of this XMOdelTreeNode or null of this XModelTreeNode represents a root element
	 */
	public XModelTreeNode getParent() {
		return parent;
	}

	/**
	 * Sets the parent node of this XModelTreeNode
	 * @param parent	The new parent node of this XModelTree
	 */
	public void setParent(XModelTreeNode parent) {
		this.parent = parent;
	}

	/**
	 * Liefert die Form dieses Elementes (qualified / unqualified), die ggf. für diese Instanz überschrieben wurde und nicht mit der des Elementes 
	 * übereinstimmt
	 * @return	Form dieses Elementes
	 */
	public String getForm() {
		if(this.form != null) {
			return this.form;
		}
		else {
			return this.element.getForm();
		}
	}

	/**
	 * Mit dieser Methode lässt sich Form der vorliegenden Instanz überschreiben
	 * @param form	Die Form des Elemente (qualified / unqualified)
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * Returns the default form of the XElement of this XModelTreeNode
	 * @return	the default form of the XElement of this XModelTreeNode
	 */
	public String getDefaultForm() {
		return this.element.getDefaultForm();
	}

	/**
	 * Returns the name of the choice element of this XModelTreeNode (if any) or null (otherwise)
	 * @return	the name of the choice element of this XModelTreeNode (if any) or null (otherwise)
	 */
	public String getChoiceBase() {
		return this.element.getChoiceBase();
	}

	/**
	 * Returns the anonymous simple type of this XModelTreeNode (if any)
	 * @return	the anonymous simple type of this XModelTreeNode (if any)
	 */
	public XSimpleType getAnonymousSimpleType() {
		return this.element.getAnonymousSimpleType();
	}

	/**
	 * Returns whether the XElement of this XModelTreeNode is an abstract element
	 * @return	true in case the XElement of this XModelTreeNode is an abstract element, false otherwise
	 */
	public boolean isAbstr() {
		return this.element.isAbstr();
	}

	/**
	 * Returns whether the XElement of this XModelTreeNode is of type xs:any
	 * @return	true in case the XElement of this XModelTreeNode is of type xs:any, false otherwise
	 */
	public boolean isAny() {
		return this.element.isAny();
	}

	/**
	 * Returns the name of the substitution group of the XElement of this XModelTreeNode (if any) or null (otherwise)
	 * @return	the name of the substitution group of the XElement of this XModelTreeNode (if any) or null (otherwise)
	 */
	public String getSubstitutionGroup() {
		return this.element.getSubstitutionGroup();
	}

	/**
	 * Returns the schema file item of the XElement of this XModelTreeNode
	 * @return	The schema file item of the XElement of this XModelTreeNode
	 */
	public XTASchemaFileItem getSchema() {
		try {
			return this.element.getSchema();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Returns the schema location (String) of the XElement of this XModelTreeNode
	 * @return	the schema location (String) of the XElement of this XModelTreeNode
	 */
	public String getSchemaLocation() {
		return this.element.getSchema().getPath();
	}

	/**
	 * Returns whether this XModelTreeNode is a substitution of another element
	 * @return	true in case this XModelTreeNode is a substitution of another element, false otherwise
	 */
	public boolean isSubstitution() {
		return substitution;
	}

	/**
	 * Sets whether this XModelTreeNode is a substitution of another element
	 * @param substitution	The new substitution status of this XModelTreeNode
	 */
	public void setSubstitution(boolean substitution) {
		this.substitution = substitution;
	}
	
	/**
	 * Returns the number of child members that are substitutions of another element
	 * @return	the number of child members that are substitutions of another element
	 */
	public int getSubstitutionCount() {
		int ret = 0;
		
		for(XModelTreeNode child : children) {
			if(child.isSubstitution()) {
				ret++;
			}
		}
		
		return ret;
	}

	/**
	 * Set a flag to mark this XModelTreeNode as a deeper occurrence of an equal element
	 * @param b	The new flag to mark this XModelTreeNode as a deeper occurrence of an equal element
	 */
	public void setCircle(boolean b) {
		this.circle = true;
	}

	/**
	 * Returns whether this XModelTreeNode is marked to be a deeper occurrence of an equal element
	 * @return	True in case this ModelTreeNode is marked to be a deeper occurrence of an equal element, false otherwise
	 */
	public boolean isCircle() {
		return this.circle;
	}

	/**
	 * Returns the fixed value of the XElement of this XModelTreeNode (if any)
	 * @return	the fixed value of the XElement of this XModelTreeNode (if any), null otherwise
	 */
	public String getFixed() {
		return this.element.getFixed();
	}

	/**
	 * Returns the XComplexType of this XModelTreeNode
	 * @return	the XComplexType of this XModelTreeNode
	 */
	public XComplexType getComplexType() {
		return this.type;
	}

	/**
	 * Returns the XElement of this XModelTreeNode
	 * @return	the XElement of this XModelTreeNode
	 */
	public XElement getElement() {
		return this.element;
	}

	/**
	 * Returns whether this XModelTreeNode is marked to be transparent
	 * @return	True in case this XModelTreeNode is marked to be transparent, false otherwise
	 */
	public boolean isTransparent() {
		return transparent;
	}

	/**
	 * Sets whether this XModelTreeNode shall be marked to be transparent
	 * @param transparent	The new transparent status of this XModelTreeNode
	 */
	public void setTransparent(boolean transparent) {
		this.transparent = transparent;
	}
	
	public XModelTreeNode getRoot() {
		if(this.parent == null) {
			return this;
		}
		else {
			return this.parent.getRoot();
		}
	}
	
	public void show() {
		this.show(0);
	}
	
	public void show(int pre) {
		if(pre > 20) {
			return;
		}
		for(int i=0;i<pre;i++) {
			System.out.print(".");
		}
		System.out.println(this.toString());
		for(int i=0;i<this.getChildCount();i++) {
			this.getChildAt(i).show(pre + 1);
		}
	}
}