package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.JFrame;

/**
 * This class covers all relevant settings of the application to be persisted at end and read on startup
 * 
 * @author treichling
 *
 */
public class XTAViewerSettings implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8695572278952016153L;

	// Window state of the main window
	private int windowX = 10;
	private int windowY = 10;
	private int windowWidth = 800;
	private int windowHeight = 600;
	private int windowState = JFrame.NORMAL;
	
	// All folder resources including post office boxes
	private TreeMap<String, XTAResource> rootItems = new TreeMap<>();
	// Set for buffering all paths of existing folder resources in order to prevent doublets
	private TreeSet<String> folderIndex = new TreeSet<String>();

	// Key value store for arbitrary properties to be persisted
	private Map<String, Serializable> properties = new TreeMap<String, Serializable>();
	
	public XTAViewerSettings() {
		super();
	}

	public int getWindowX() {
		return windowX;
	}

	public void setWindowX(int windowX) {
		this.windowX = windowX;
	}

	public int getWindowY() {
		return windowY;
	}

	public void setWindowY(int windowY) {
		this.windowY = windowY;
	}

	public int getWindowWidth() {
		return windowWidth;
	}

	public void setWindowWidth(int windowWidth) {
		this.windowWidth = windowWidth;
	}

	public int getWindowHeight() {
		return windowHeight;
	}

	public void setWindowHeight(int windowHeight) {
		this.windowHeight = windowHeight;
	}

	public int getWindowState() {
		return windowState;
	}

	public void setWindowState(int windowState) {
		this.windowState = windowState;
	}
	
	/**
	 * Returns whether or not a certail item (ID) is already present
	 * @param id	The id of the item to be checked
	 * @return	True if the item is present, false otherwise
	 */
	public boolean containsItemKey(String id) {
		return this.rootItems.containsKey(id);
	}
	
	/**
	 * Add another folder resource to the settings
	 * @param id	The id of the folder resource to be added
	 * @param item	The folder resource itself
	 * @return	True if adding was successful, false otherwise (folder resource already present)
	 */
	public boolean addResource(String id, XTAResource item) {
		if(this.rootItems.containsKey(id)) {
			return false;
		}
		else {
			this.rootItems.put(id, item);
			
			if(item instanceof XTAFolderResource) {
				if(((XTAFolderResource) item).getPath() != null) {
					this.folderIndex.add(((XTAFolderResource) item).getPath());
				}
			}
			return true;
		}
	}

	/**
	 * Returns a folder recource for a given id
	 * @param key	The id of the desired folder resource
	 * @return	The desired folder resource or null if not rpesent
	 */
	public XTATreeItemI getResourceForId(String key) {
		return rootItems.get(key);
	}

	/**
	 * Remove a given folder resource from the settings
	 * @param key	The id of the folder resource to delete
	 * @return	True if deleting was successful, false otherwise
	 */
	public XTATreeItemI removeResourceById(String key) {
		XTATreeItemI item = this.rootItems.get(key);
		
		if(item != null && item instanceof XTAFolderResource) {
			if(((XTAFolderResource) item).getPath() != null) {
				this.folderIndex.remove(((XTAFolderResource) item).getPath());
			}
		}
		
		return rootItems.remove(key);
	}

	/**
	 * Returns whether a given folder resource (id) is present in these settings
	 * @param o	The id of the folder resource in question
	 * @return	True if a folder resource with the given id is present
	 */
	public boolean containsFolderResource(String o) {
		if(this.folderIndex == null) {
			this.folderIndex = new TreeSet<String>();
		}
		return folderIndex.contains(o);
	}

	/**
	 * Returns all folder resources
	 * @return	Map of all folder resources
	 */
	public TreeMap<String, XTAResource> getResources() {
		return rootItems;
	}

	/**
	 * Returns all properties
	 * @return	A map of all properties
	 */
	public Map<String, Serializable> getProperties() {
		if(this.properties == null) {
			this.properties = new TreeMap<String, Serializable>();
		}
		
		return this.properties;
	}
}
