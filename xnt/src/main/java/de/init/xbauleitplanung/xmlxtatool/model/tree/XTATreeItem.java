package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * This class represents a basic implementation of the XTATreeItemI interface. It shall be used as base class for 
 * specific implementations. Its methods may overwritten or be used out of the box. 
 * 
 * @author treichling
 *
 */
public class XTATreeItem implements Serializable, Comparable<XTATreeItemI>, XTATreeItemI{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2898266462676998570L;
	
	// basic implementations of id, name, children, parent, changes and advice
	// Each of them come with access methods defined in the interface XTATreeItemI
	private String id;
	private String name;
	protected ArrayList<XTATreeItemI> children = new ArrayList<>();
	private XTATreeItemI parent;
	private boolean changes = false;
	private String advice = null;
	
	public static String getPrefix(String expression) {
		String ret = null;
		
		if(expression != null) {
			int sep = expression.indexOf(":");
			
			if(sep > 0) {
				ret = expression.substring(0, sep);
			}
		}
		
		return ret;
	}
	
	public static String getLocalName(String expression) {
		String ret = null;
		
		if(expression != null) {
			int sep = expression.indexOf(":");
			
			if(sep > 0) {
				ret = expression.substring(sep + 1);
			}
			else {
				ret = new String(expression);
			}
		}
		
		return ret;
	}
	
	public static boolean isWebResource(String recource) {
		boolean ret = false;
		
		if(recource.startsWith("http://") || recource.startsWith("https://")) {
			ret = true;
		}
		
		return ret;
	}
	
	public static String trimPath(String path) {
		StringTokenizer tok = new StringTokenizer(path, "/");
		ArrayList<String> components = new ArrayList<String>();
		while(tok.hasMoreTokens()) {
			components.add(tok.nextToken());
		}
		
		int index = 1;
		while(index < components.size()) {
			if(components.get(index).contentEquals("..")) {
				components.remove(index - 1);
				components.remove(index - 1);
				index--;
			}
			else {
				index++;
			}
		}
		
		StringBuilder ret = new StringBuilder();
		for(int i=0;i<components.size();i++) {
			ret.append(components.get(i));
			
			if(i < components.size() - 1) {
				ret.append("/");
			}
		}
		
		return ret.toString();
	}
	
	public static String computePath(String absolutePath, String schemaLocation) {
		File referrerFile = new File(absolutePath);
		File referrerFolder = referrerFile.getParentFile();
		String rawPathToReferredSchema = referrerFolder.getAbsolutePath() + "/" + schemaLocation;
		rawPathToReferredSchema = rawPathToReferredSchema.replace("\\", "/");
		
		return rawPathToReferredSchema;
	}
	
	public XTATreeItem() {
		super();
	}
	
	public void clone(XTATreeItem copy) {
		this.id = new String(copy.id);
		this.name = new String(copy.name);
		
		copy.children = new ArrayList<>(this.children);
	}

	public void setItemId(String id) {
		this.id = id;
	}

	public void setItemName(String name) {
		this.name = name;
	}

	@Override
	public String getItemId() {
		return this.id;
	}

	@Override
	public String getItemName() {
		return this.name;
	}

	@Override
	public String getItemType() {
		return "XTA_TREE_ITEM";
	}

	@Override
	public int getItemChildCount() {
		return this.getChildren().size();
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		return this.getChildren().get(index);
	}

	public final void addChild(XTATreeItemI child) {
		this.getChildren().add(child);
		child.setParent(this);
	}

	public void removeChild(String itemId) {
		for(int i=0;i<this.getItemChildCount();i++) {
			if(itemId.equals(this.getItemChildAt(i).getItemId())) {
				this.getChildren().remove(i);
				break;
			}
		}
	}

	public XTATreeItemI getParent() {
		return parent;
	}

	public XTASchemaFileItem getRootSchema() {
		try {
			XTATreeItemI parent = this.parent;
			while(parent != null) {
				if(parent instanceof XTASchemaFileItem) {
					break;
				}
				else {
					parent = parent.getParent();
				}
			}
			
			return (XTASchemaFileItem) parent;
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}

	public void setParent(XTATreeItemI parent) {
//		if(XTATreeItem.getLocalName(parent.getItemName()).equals("ValuePropertyType")) {
//			System.out.println("hier");
//		}
		this.parent = parent;
	}

	@Override
	public int compareTo(XTATreeItemI o) {
		int ret;
		try {
			ret = this.getItemType().compareTo(o.getItemType());
			
			if(ret == 0) {
				ret = this.getItemId().compareTo(o.getItemId());
			}
		} catch (Exception e) {
			ret = -1;
		}
		
		return ret;
	}

	public void clear() {
		this.getChildren().clear();
	}

	public ArrayList<XTATreeItemI> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<XTATreeItemI> children) {
		this.children = children;
	}

	@Override
	public boolean changes() {
		return this.changes;
	}

	public void setChanges(boolean changes) {
		this.changes = changes;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

	@Override
	public boolean contains(XTATreeItemI item) {
		boolean ret = true;
		
		for(XTATreeItemI child : this.getChildren()) {
			if(child.getItemId().equals(item.getItemId())) {
				ret = true;
				break;
			}
		}
		return ret;
	}

	@Override
	public XTATreeItemI getRootItem() {
		if(this.getParent() == null) {
			return this;
		}
		else {
			return this.getParent().getRootItem();
		}
	}
}
