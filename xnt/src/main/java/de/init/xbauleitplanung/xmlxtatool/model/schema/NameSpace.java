package de.init.xbauleitplanung.xmlxtatool.model.schema;

import java.io.Serializable;

/**
 * This class represents an XML namespace entry including its prefix and URN
 * 
 * @author treichling
 *
 */
public class NameSpace implements Serializable, Comparable<NameSpace>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3261357771807185075L;
	
	private String namespaceURL;
	private String prefix;
	
	public NameSpace() {
		super();
	}
	public NameSpace(String namespace, String namespaceURL) {
		super();
		
		this.prefix = namespace;
		this.namespaceURL = namespaceURL;
	}
	public String getNamespaceURL() {
		return namespaceURL;
	}
	public void setNamespaceURL(String namespaceURL) {
		this.namespaceURL = namespaceURL;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String namespace) {
		this.prefix = namespace;
	}
	@Override
	public int compareTo(NameSpace o) {
		int ret = this.prefix.compareTo(o.prefix);
		
		if(ret == 0) {
			ret = this.namespaceURL.compareTo(o.namespaceURL);
		}
		
		return ret;
	}
	@Override
	public String toString() {
		try {
			return this.prefix + ":" + this.namespaceURL;
		} catch (Exception e) {
			return (this.prefix != null ? this.prefix : "...") + ":" + (this.namespaceURL != null ? this.namespaceURL : "...");
		}
	}
}