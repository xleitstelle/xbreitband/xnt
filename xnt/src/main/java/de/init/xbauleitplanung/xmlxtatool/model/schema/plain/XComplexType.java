package de.init.xbauleitplanung.xmlxtatool.model.schema.plain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;

public class XComplexType extends XTATreeItem implements Serializable, XTATreeItemI{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8220135134894863156L;
	
	// Different types of complex types. In this implementation, complex types are also used to 
	// model sequences or choices
	public static final int NONE = 0;
	public static final int RESTRICTION = 1;
	public static final int EXTENSION = 2;
	public static final int SEQUENCE = 3;
	public static final int CHOICE = 4;
	
	// prefix and local name 
	private String prefix;
	private String localName;
	
	// Is this complex type an abstract type? 
	private boolean abstr = false;
	
	// Potential complex base of this complex type
	private String complexBase;
	// Potential simple base of this complex type
	private String simpleBase;
	// type of derivation of this complex type - NONE is default
	private int derivationType = NONE;
	// In case this complex type is abstract, implementors is a container to keep all 
	// implementing types
	private ArrayList<String> implementors;
	
	// Shortcuts for listURI and listVersionID of a codelist element ("code") in case this 
	// complex type represents an XOEV  code type
	private String listURI = null;
	private String listVersionID = null;
	// XOEV type 1-4 of contained codelist element ("code")
	private int xoevType = 0;
	
	// Elements, attributes and attribute groups of this complex type
	private ArrayList<XElement> elements = new ArrayList<>();
	private ArrayList<XAttribute> attributes = new ArrayList<>();
	private ArrayList<String> attributeGroups = new ArrayList<>();
	
	// Schema file in which this complex type is defined
	private XTASchemaFileItem schema = null;

	public XComplexType() {
		super();
	}
	
	public String getName() {
		return this.prefix + ":" + this.getLocalName();
	}
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public String getComplexBase() {
		return complexBase;
	}

	public String getSimpleBase() {
		return simpleBase;
	}

	public void setSimpleBase(String simpleBase) {
		this.simpleBase = simpleBase;
	}

	public void setComplexBase(String base) {
		this.complexBase = base;
	}

	public int getDerivationType() {
		return derivationType;
	}

	public void setDerivationType(int derivationType) {
		this.derivationType = derivationType;
	}

	public ArrayList<XElement> getElements() {
		return elements;
	}

	public void addElement(XElement el) {
		this.elements.add(el);
		el.setParent(this);
	}
	
	public XElement getElementByLocalName(String name) {
		XElement ret = null;
		
		for(XElement child : this.elements) {
			if(name.equals(child.getElementName())) {
				ret = child;
				break;
			}
		}
		
		return ret;
	}

	public ArrayList<XAttribute> getAttributes() {
		return attributes;
	}

	public ArrayList<String> getAttributeGroups() {
		return attributeGroups;
	}

	@Override
	public String getItemName() {
		return this.getName();
	}

	@Override
	public String getItemType() {
		return "XComplexType";
	}

	@Override
	public int getItemChildCount() {
		return 0;
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		return null;
	}

	public String getFullyQualifiedName() {
		String ret = null;
		
		if(this.prefix != null) {
			if(this.localName != null) {
				ret = this.prefix + ":" + this.localName;
			}
		}
		else {
			if(this.localName != null) {
				ret = this.localName;
			}
		}
		
		return ret;
	}

	public String getListURI() {
		return listURI;
	}

	public void setListURI(String listURI) {
		this.listURI = listURI;
	}

	public String getListVersionID() {
		return listVersionID;
	}

	public void setListVersionID(String listVersionID) {
		this.listVersionID = listVersionID;
	}

	public int getXoevType() {
		return xoevType;
	}

	public void setXoevType(int xoevType) {
		this.xoevType = xoevType;
	}

	@Override
	public String toString() {
		try {
			return this.prefix + ":" + this.localName;
		} catch (Exception e) {
			e.printStackTrace();
			
			return this.localName;
		}
	}

	public XTASchemaFileItem getSchema() {
		return schema;
	}

	public void setSchema(XTASchemaFileItem schema) {
		this.schema = schema;
	}

	public boolean isAbstr() {
		return abstr;
	}

	public void setAbstr(boolean abstr) {
		this.abstr = abstr;
	}
	
	public void addImplementor(String imp) {
		if(this.implementors == null) {
			this.implementors = new ArrayList<String>();
		}
		
		if(!this.implementors.contains(imp)) {
			this.implementors.add(imp);
		}
	}
	
	public void addImplementors(List<String> impl) {
		for(String imp : impl) {
			this.addImplementor(imp);
		}
	}

	public ArrayList<String> getImplementors() {
		return implementors;
	}
	
	public ArrayList<String> getImplementors(XTASchemaFileItem schema) {
		TreeSet<String> prefixes = schema.getAllPrefixes();
		ArrayList<String> ret = new ArrayList<String>();
		
		if(implementors != null) {
			for(String impl : implementors) {
				String prefix = XTATreeItem.getPrefix(impl);
				if(prefixes.contains(prefix)) {
					ret.add(impl);
				}
			}
		}
		
		return ret;
	}
}
