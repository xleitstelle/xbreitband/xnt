package de.init.xbauleitplanung.xmlxtatool.model.schema;

import java.io.Serializable;

/**
 * This class represents an XML import relation between a schema file (importer) and another schema file (import). It is based on class 
 * Include which defines a local schema definition of the imported schema file
 * 
 * @author treichling
 *
 */
public class Import extends Include implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1354965969097797543L;
	
	private String namespace;
	private String prefix;
	
	public Import() {
		super();
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	@Override
	public int compareTo(Include o) {
		int ret = super.getSchemaLocation().compareTo(o.getSchemaLocation());
		
		if(ret == 0 && o instanceof Import) {
			if(this.namespace != null) {
				ret = this.namespace.compareTo(((Import)o).getNamespace());
			}
			else if(((Import)o).getNamespace() != null){
				ret = -1;
			}
			else {
				ret = 0;
			}
		}
		
		return ret;
	}
	
	@Override
	public String toString() {
		return "Namespace: " + this.namespace + ", Schema location: " + super.getSchemaLocation();
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
