package de.init.xbauleitplanung.xmlxtatool.model.schema.plain;

import java.io.Serializable;
import java.util.ArrayList;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;

/**
 * This a class used as facade for XML elements. Instances of this class may be regarded as simple elements. However, they 
 * inherit certain properties from existing ("template") elements. Template elements may be altered later on which has effect 
 * on the corresponding element proxies while certain properties remain untouched. This class is used when global elements 
 * are defined reused and altered in type definitions. 
 * 
 * @author treichling
 *
 */
public class XElementProxy extends XElement implements Serializable, XTATreeItemI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5610208102171479016L;
	
	private XElement templateElement;
	
	public XElementProxy(XElement template) {
		super();
		
		this.templateElement = template;
	}

	public XTASchemaFileItem getSchema() {
		return templateElement.getSchema();
	}

	public XElement getTemplateElement() {
		return templateElement;
	}

	public void clone(XTATreeItem copy) {
		templateElement.clone(copy);
	}

	public void setItemId(String id) {
		templateElement.setItemId(id);
	}

	public void clone(XElement copy) {
		templateElement.clone(copy);
	}

	public String getElementName() {
		return templateElement.getElementName();
	}

	public String getFullyQualifiedName() {
		return templateElement.getFullyQualifiedName();
	}

	public String getElementType() {
		return templateElement.getElementType();
	}

	public XElement getChildByLocalName(String name) {
		return templateElement.getChildByLocalName(name);
	}

	public int compareTo(XTATreeItemI o) {
		return templateElement.compareTo(o);
	}

	public XElement getChildByFullyQualifiedName(String name) {
		return templateElement.getChildByFullyQualifiedName(name);
	}

	public void clear() {
		templateElement.clear();
	}

	public boolean equals(Object obj) {
		return templateElement.equals(obj);
	}

	public void setItemName(String name) {
		templateElement.setItemName(name);
	}

	public String getForm() {
		return templateElement.getForm();
	}

	public void setForm(String form) {
		templateElement.setForm(form);
	}

	public void removeChild(String itemId) {
		templateElement.removeChild(itemId);
	}

	public XTASchemaFileItem getRootSchema() {
		return templateElement.getRootSchema();
	}

	public void setElementName(String name) {
		templateElement.setElementName(name);
	}

	public void setElementType(String type) {
		templateElement.setElementType(type);
	}

	public ArrayList<XTATreeItemI> getChildren() {
		return templateElement.getChildren();
	}

	public void setChildren(ArrayList<XTATreeItemI> children) {
		templateElement.setChildren(children);
	}

	public XComplexType getAnonymousType() {
		return templateElement.getAnonymousType();
	}

	public void setAnonymousType(XComplexType anonymousType) {
		templateElement.setAnonymousType(anonymousType);
	}

	public String getPrefix() {
		return super.getPrefix();
	}

	public void setPrefix(String prefix) {
		super.setPrefix(prefix);
	}

	public String getItemName() {
		return templateElement.getItemName();
	}

	public String getItemType() {
		return templateElement.getItemType();
	}

	public int getItemChildCount() {
		return templateElement.getItemChildCount();
	}

	public XTATreeItemI getItemChildAt(int index) {
		return templateElement.getItemChildAt(index);
	}

	public String toString() {
		return templateElement.toString();
	}

	public XSimpleType getAnonymousSimpleType() {
		return templateElement.getAnonymousSimpleType();
	}

	public void setAnonymousSimpleType(XSimpleType anonymousSimpleType) {
		templateElement.setAnonymousSimpleType(anonymousSimpleType);
	}

	public boolean isAbstr() {
		return templateElement.isAbstr();
	}

	public String getSubstitutionGroup() {
		return templateElement.getSubstitutionGroup();
	}

	public int hashCode() {
		return templateElement.hashCode();
	}

	public void setAbstr(boolean abstr) {
		templateElement.setAbstr(abstr);
	}

	public void setSubstitutionGroup(String substitutionGroup) {
		templateElement.setSubstitutionGroup(substitutionGroup);
	}
}
