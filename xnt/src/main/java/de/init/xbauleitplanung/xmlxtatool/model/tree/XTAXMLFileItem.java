package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import de.init.xbauleitplanung.xmlxtatool.model.xml.XNode;

/**
 * This class represents an XML file as a special case of a file. It is based on the class XTAFileItem 
 * and offers additional, XML specific methods. 
 * 
 * @author treichling
 *
 */
public class XTAXMLFileItem extends XTAFileItem implements XTATreeItemI{
	// Indicates no compression when exporting xml content to xml file
	public static int NO_COMPRESSION = 0;
	
	// leading and trailing line breaks will be eliminated when exporting xml content to xml file
	public static int ELIMINATE_LINE_BREAKS = 1;
	
	// leading and trailing white space (including line breaks) will be eliminated when exporting xml content to xml file
	public static int ELIMINATE_WHITE_SPACE = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = -1586737276875367013L;
	
	/**
	 * List of XML child nodes (XNode)
	 */
	// TODO: Check: Can we remove this list? The existing inherited child list should be sufficient! 
	private transient ArrayList<XNode> nodes = new ArrayList<>();
	
	/**
	 * Is this file already XML validated? 
	 */
	private boolean validated = false;
	/**
	 * Potential warnings from XML validation (schema validation failed)
	 */
	protected transient ArrayList<String> warnings = new ArrayList<>();
	/**
	 * Potential fatals from XML validation (XML structure corrupted)
	 */
	protected transient ArrayList<String> fatals = new ArrayList<>();
	
	/**
	 * List of potential attachments
	 */
	// TODO: Check: Can this list be moved to the MessageMetaData class? 
	private transient ArrayList<String> attachments = null;
	
	/**
	 * buffer for plain text content - In certain cases (e.g. desired violation of XML structure or validation failure)
	 */
	protected String plainText = null;
	
	public XTAXMLFileItem() {
		super();
	}
	
	/**
	 * Clone method for this kind of XTAItemI
	 */
	public XTAXMLFileItem clone() {
		XTAXMLFileItem copy = new XTAXMLFileItem();
		
		copy.setItemId(new String(this.getItemId()));
		copy.setItemName(new String(this.getItemName()));
		copy.setPath(new String(this.getPath()));
		
		copy.nodes = new ArrayList<>(this.nodes);
		copy.warnings = new ArrayList<>(this.warnings);
		copy.fatals = new ArrayList<>(this.fatals);
		copy.validated = this.validated;
		
		if(this.plainText != null) {
			copy.plainText = new String(this.plainText);
		}
		else {
			copy.plainText = null;
		}
		
		return copy;
	}
	
	/**
	 * Variant of the clone method for writing into an existing item
	 * @param copy	The item to write data of this item into
	 */
	public void clone(XTAXMLFileItem copy) {
		super.clone(copy);
		
		this.setItemId(copy(copy.getItemId()));
		this.setItemName(copy(copy.getItemName()));
		this.setPath(copy(copy.getPath()));
		this.setParent(copy.getParent());
		this.setValidated(copy.isValidated());
		
		this.nodes = new ArrayList<>();
		for(int i=0;i<copy.nodes.size();i++) {
			this.addNode(copy.nodes.get(i).cloneNode());
		}
		
		this.warnings = new ArrayList<>(copy.warnings);
		this.fatals = new ArrayList<>(copy.fatals);
		
		this.plainText = copy(copy.plainText);
	}
	
	/**
	 * Shortcut method used for copying strings
	 * @param origin	The original string to be copied
	 * @return	the new string instance (copy)
	 */
	private String copy(String origin) {
		if(origin == null) {
			return null;
		}
		else {
			return new String(origin);
		}
	}

	@Override
	public int getItemChildCount() {
		try {
			return this.nodes.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		return this.nodes.get(index);
	}

	@Override
	public String getItemType() {
		return "XMLFile";
	}
	
	/**
	 * Add a warning which may occur during validation
	 * @param warning	a warning which may occur during validation
	 */
	public void addWarning(String warning) {
		this.warnings.add(warning);
	}
	
	/**
	 * Returns all warnings that may occurred during validation
	 * @return	all warnings that may occurred during validation
	 */
	public ArrayList<String> getWarnings(){
		return this.warnings;
	}
	
	/**
	 * Add a fatal which may occur during validation
	 * @param fatal	a fatal which may occur during validation
	 */
	public void addFatal(String fatal) {
		this.fatals.add(fatal);
	}
	
	/**
	 * Returns all fatals that may occurred during validation
	 * @return	all fatals that may occurred during validation
	 */
	public ArrayList<String> getFatals(){
		return this.fatals;
	}

	/**
	 * Returns plain text of this XML file (only if it has been set before) - This method does NOT generate 
	 * plain text from XML nodes
	 * @return	plain text of this XML file (if it was set before)
	 */
	public String getPlainText() {
		return plainText;
	}

	/**
	 * Set the plain text of this XML file
	 * @param plainText	the plain text of this XML file
	 */
	public void setPlainText(String plainText) {
		this.plainText = plainText;
	}
	
	/**
	 * Recursively rebuild the org.w3c.Document (document) from the XNode content of this XML file
	 * @param document	The target org.w3c.Document
	 * @param node	The current XNode item to be processed recursively
	 * @return	The created org.w3c.dom.Node to be added to the XML structure
	 */
	private Node recurseNode(Document document, XNode node, int compressMode) {
		Node target = null;
		
		try {
			if(node.getType() == XNode.ELEMENT) {
				target = document.createElement(node.getName());
				
				if(node.getValue() != null) {
					target.appendChild(document.createTextNode(node.getValue()));
				}
			}
			else if(node.getType() == XNode.TEXT){
				if(compressMode == NO_COMPRESSION) {
					target = document.createTextNode(node.getValue());
				}
				else if(compressMode == ELIMINATE_LINE_BREAKS) {
					target = document.createTextNode(eliminateLineBreaks(node.getValue()));
				}
				else if(compressMode == ELIMINATE_WHITE_SPACE) {
					target = document.createTextNode(node.getValue().trim());
				}
				else {
					target = document.createTextNode(node.getValue());
				}
			}
			else if(node.getType() == XNode.COMMENT){
				target = document.createComment(node.getValue());
			}
			
			if(target != null) {
				if(node.getPrefix() != null) {
					try {
						target.setPrefix(node.getPrefix());
					} catch (DOMException e) {
						e.printStackTrace();
					}
				}
				
				for(String att : node.getAttributes().keySet()) {
					String val = node.getAttributes().get(att);
					Attr attribute = document.createAttribute(att);
					
					if(attribute.getName().equals("xsi:schemaLocation")) {
						int sep = val.indexOf(" ");
						if(sep > 0) {
							String start = val.substring(0, sep);
							String end = val.substring(sep + 1);
							attribute.setValue(start + " " + end.replace(" ", "%20"));
						}
						else {
							attribute.setValue(val);
						}
					}
					else if(attribute.getName().equals("schemaLocation")) {
						int sep = val.indexOf(" ");
						if(sep > 0) {
							String start = val.substring(0, sep);
							String end = val.substring(sep + 1);
							attribute.setValue(start + " " + end.replace(" ", "%20"));
						}
						else {
							attribute.setValue(val);
						}
					}
					else {
						attribute.setValue(val);
					}
					
					target.getAttributes().setNamedItem(attribute);
				}
				
				for(int i=0;i<node.getItemChildCount();i++) {
					XNode child = (XNode) node.getItemChildAt(i);
					Node childTarget = recurseNode(document, child, compressMode);
					
					if(childTarget != null) {
						target.appendChild(childTarget);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return target;
	}
	
	/**
	 * Recreate the XML structure back from the XNode content of this XML file item and write it to the 
	 * XML file on disk (or just write the plain text)
	 * @param plain	Write plain text only instead of recreating content
	 * @throws Throwable	Throwable is thrown in case of problems during the recreation process
	 */
	public void export(boolean plain) throws Throwable {
		if(plain) {
			BufferedWriter writer = new BufferedWriter(new FileWriter(this.getPath()));
			writer.write(this.getPlainText());
			
			writer.close();
		}
		else {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setFeature("http://apache.org/xml/features/honour-all-schemaLocations", true);
			factory.setNamespaceAware(true);
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document newDocument = builder.newDocument();
			
			for(int i=0;i<this.nodes.size();i++) {
				Node node = recurseNode(newDocument, this.nodes.get(i), ELIMINATE_WHITE_SPACE);
				newDocument.appendChild(node);
			}
			
			DOMSource source = new DOMSource(newDocument);
			FileWriter writer = new FileWriter(new File(this.getPath()));
			StreamResult result = new StreamResult(writer);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.transform(source, result);
		}
	}
	
	public static String eliminateLineBreaks(String input) {
		StringBuilder b = new StringBuilder(input);
		
		while(true) {
			boolean stop = true;
			char c = b.charAt(0);
			
			if(c == '\n') {
				stop = false;
			}
			else if(c == '\r') {
				stop = false;
			}
			else if(c == '\t') {
				stop = false;
			}
			else if(c == '\f') {
				stop = false;
			}
			
			if(stop) {
				break;
			}
			else {
				b.delete(0, 1);
			}
		}
		
		while(true) {
			boolean stop = true;
			int index = b.length() - 1;
			char c = b.charAt(index);
			
			if(c == '\n') {
				stop = false;
			}
			else if(c == '\r') {
				stop = false;
			}
			else if(c == '\t') {
				stop = false;
			}
			else if(c == '\f') {
				stop = false;
			}
			
			if(stop) {
				break;
			}
			else {
				b.delete(index, index + 1);
			}
		}
		String string = b.toString();
		return string;
	}

	/**
	 * Return the child nodes of this XML file
	 * @return	the child nodes of this XML file
	 */
	public ArrayList<XNode> getNodes(){
		return this.nodes;
	}
	
	/**
	 * Add a new child node to this XML file
	 * @param child	a new child node
	 */
	public void addNode(XNode child) {
		this.nodes.add(child);
		child.setParent(this);
	}

	/**
	 * Clear all content of this XML file (that is nodes, warnings, fatals and validation state)
	 */
	public void clear() {
		if(this.nodes == null) {
			this.nodes = new ArrayList<>();
		}
		this.nodes.clear();
		
		if(this.warnings == null) {
			this.warnings = new ArrayList<>();
		}
		this.warnings.clear();
		
		if(this.fatals == null) {
			this.fatals = new ArrayList<>();
		}
		
		this.fatals.clear();
		
		this.setValidated(false);
	}

	/**
	 * Is this file already validated? 
	 * @return	True in case this file is validated, false otherwise
	 */
	public boolean isValidated() {
		return validated;
	}

	/**
	 * Mark this file as validated (or not)
	 * @param validated	Is this file validated? 
	 */
	public void setValidated(boolean validated) {
		this.validated = validated;
	}
	
	/**
	 * Seek for a certain node among the child nodes of this XML file which has the specified local name (element) and text value
	 * @param element	The local name of the element to seek for
	 * @param value	The value of the element to seek for
	 * @return	The found node matching the search parameters or null if no according node was found
	 */
	public ArrayList<XNode> find(String element, String value) {
		ArrayList<XNode> ret = new ArrayList<XNode>();
		
		for(XNode child : this.nodes) {
			child.find(element, value, ret, -1);
		}
		
		return ret;
	}
	
	/**
	 * Seek for a certain node among the child nodes of this XML file which has the specified local name (element), text value and depth
	 * @param element	The local name of the element to seek for
	 * @param value	The value of the element to seek for
	 * @param depth	The depth of the element to seek for
	 * @return	The found node matching the search parameters or null if no according node was found
	 */
	public ArrayList<XNode> find(String element, String value, int depth) {
		ArrayList<XNode> ret = new ArrayList<XNode>();
		
		for(XNode child : this.nodes) {
			child.find(element, value, ret, depth);
		}
		
		return ret;
	}

	/**
	 * Getter method for attachments
	 * @return	Potential attachments of this XML file
	 */
	public ArrayList<String> getAttachments() {
		return attachments;
	}

	/**
	 * Setter method for potential attachments of this XML file
	 * @param attachments	Attachments to be set to this XML file
	 */
	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}
}
