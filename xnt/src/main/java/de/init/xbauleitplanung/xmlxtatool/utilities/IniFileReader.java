package de.init.xbauleitplanung.xmlxtatool.utilities;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;



/**
 * @author tim
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class IniFileReader extends ObjectReader {
	Hashtable<String, String> values=new Hashtable<String, String>();

	/**
	 * Constructor for IniFileReader.
	 * @param fileName
	 */
	public IniFileReader(String fileName) {
		super(fileName);
		
		String line, var, val;
		int index;
		
		for(;;){
			line = getString();
			if(line==null) break;
			
			try {
				while(line.length() > 0 && !Character.isLetterOrDigit(line.charAt(0))){
					line = line.substring(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			index = line.indexOf("=");
			if(index<0) continue;
			
			var = line.substring(0, index);
			val = line.substring(index+1);
			
			var = var.trim();
			val = val.trim();
			
			values.put(var, val);
		}
	}

	/**
	 * Constructor for IniFileReader.
	 * @param in
	 */
	public IniFileReader(InputStream in) {
		super(in);
		
		String line, var, val;
		int index;
		
		for(;;){
			line = getString();
			if(line==null) break;
			
			try {
				while(line.length() > 0 && !Character.isLetterOrDigit(line.charAt(0))){
					line = line.substring(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			index = line.indexOf("=");
			if(index<0) continue;
			
			var = line.substring(0, index);
			val = line.substring(index+1);
			
			var = var.trim();
			val = val.trim();
			
			values.put(var, val);
		}
	}
	
	public String getValue(String var){
		return values.get(var);
	}
	
	public int getIntValue(String var){
		try {
			String val = values.get(var);
			if(val != null){
				return Integer.parseInt(val);
			}
			else{
				return 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public long getLongValue(String var){
		try {
			String val = values.get(var);
			if(val != null){
				return Long.parseLong(val);
			}
			else{
				return 0L;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 0L;
		}
	}
	
	public double getDoubleValue(String var){
		try {
			String val = values.get(var);
			if(val != null){
				return Double.parseDouble(val);
			}
			else{
				return 0D;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 0D;
		}
	}
	
	public boolean getBooleanValue(String var){
		try {
			Boolean bool = new Boolean(values.get(var));
			return bool.booleanValue();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public Vector<String> keys(){
		Vector<String> ret=new Vector<String>();
		Enumeration<String> keys=values.keys();
		
		while(keys.hasMoreElements()){
			ret.addElement(keys.nextElement());
		}
		
		return ret;
	}
		
	
	public static void main(String[] args){
		IniFileReader read=new IniFileReader("initest.ini");
		
		Vector<String> test = read.keys();
		
		for(int i=0;i<test.size();i++){
			System.out.print(test.elementAt(i));
			System.out.print(" : ");
			System.out.println(read.getValue(test.elementAt(i)));
		}
		read.close();
		
		IniFileWriter write = new IniFileWriter("initest.ini");
		
		for(int i=0;i<test.size();i++){
			write.setValue(test.elementAt(i), read.getValue(test.elementAt(i)));
		}
		write.write();
	}
}
