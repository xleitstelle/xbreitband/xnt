package de.init.xbauleitplanung.xmlxtatool.model.schema;

import java.io.Serializable;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;

public class WorkLeft implements Serializable, Comparable<WorkLeft>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7467900452272034220L;
	
	public String todo;
	public XTATreeItem subject;
	public XTASchemaFileItem schema;
	public String reference;
	
	public WorkLeft() {
		super();
	}

	@Override
	public int compareTo(WorkLeft o) {
		int ret = 0;
		
		if(o != null) {
			if(todo != null) {
				ret = todo.compareTo(o.todo);
			}
			
			if(ret == 0 && subject != null && subject.getItemId() != null && o.subject.getItemId() != null) {
				ret = subject.getItemId().compareTo(o.subject.getItemId());
			}
			
			if(ret == 0 && schema != null && schema.getItemId() != null && o.schema.getItemId() != null) {
				ret = schema.getItemId().compareTo(o.schema.getItemId());
			}
			
			if(ret == 0 && reference != null) {
				ret = reference.compareTo(o.reference);
			}
		}
		else {
			return -1;
		}
		
		return ret;
	}
}