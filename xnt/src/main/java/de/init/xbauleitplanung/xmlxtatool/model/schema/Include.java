package de.init.xbauleitplanung.xmlxtatool.model.schema;

import java.io.Serializable;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTASchemaFileItem;

/**
 * This class represents an include relation between an XML schema file and another XML schema file. 
 * It contains references to the local schema definition of the include
 * 
 * @author treichling
 *
 */
public class Include implements Serializable, Comparable<Include> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5601663844741827705L;
	
	private String schemaLocation;
	private XTASchemaFileItem localFile = null;
	
	public Include() {
		super();
	}
	public String getSchemaLocation() {
		return schemaLocation;
	}
	public void setSchemaLocation(String schemaLocation) {
		this.schemaLocation = schemaLocation;
	}
	
	@Override
	public int compareTo(Include o) {
		int ret = this.schemaLocation.compareTo(o.schemaLocation);
		
		return ret;
	}
	
	@Override
	public String toString() {
		return "Schema location: " + this.schemaLocation;
	}
	public XTASchemaFileItem getLocalFile() {
		return localFile;
	}
	public void setLocalFile(XTASchemaFileItem localFile) {
		this.localFile = localFile;
	}
}
