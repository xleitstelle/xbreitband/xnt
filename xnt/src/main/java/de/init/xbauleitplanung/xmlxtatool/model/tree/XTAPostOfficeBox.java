package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import org.apache.commons.io.IOUtils;

import de.init.xbauleitplanung.xmlxtatool.controller.XTAController;
import de.init.xbauleitplanung.xmlxtatool.controller.XTAServerI;
import de.init.xbauleitplanung.xtaclient.XtaClient;
import de.init.xbauleitplanung.xtaclient.XtaClientConfig;
import de.init.xbauleitplanung.xtaclient.model.Attachment;
import de.init.xbauleitplanung.xtaclient.model.DeliveryOptions;
import de.init.xbauleitplanung.xtaclient.model.DeliveryStatus;
import de.init.xbauleitplanung.xtaclient.model.GetMessageResult;
import de.init.xbauleitplanung.xtaclient.model.Identifier;
import de.init.xbauleitplanung.xtaclient.model.Message;
import de.init.xbauleitplanung.xtaclient.model.MessageFilter;
import de.init.xbauleitplanung.xtaclient.model.MessageTransportReport;
import de.xoev.transport.xta._211.InvalidMessageIDException;
import de.xoev.transport.xta._211.MessageSchemaViolationException;
import de.xoev.transport.xta._211.PermissionDeniedException;
import de.xoev.transport.xta._211.XTAWSTechnicalProblemException;
import javafx.scene.control.ProgressBar;

/**
 * Basic implementation of a XTA post office box. That is a folder resource with XTA post office box data 
 * attached in order to send or receive XML messages via XTA infrastucture
 * 
 * @author treichling
 *
 */
public class XTAPostOfficeBox extends XTAFolderResource implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3999642281195657314L;
	
	// The XTA controller instance
	protected transient XTAServerI host;
	
	// Parameters for identification, sending and receiving messages and authentication
	private String managementServiceUrl = "";
	private String sendServiceUrl = "";
	private String msgBoxServiceUrl = "";
	private String identifierA1 = "";
	private String clientCertKeystoreA1 = "";
	private String clientCertKeystoreA1Password = "";
	private String trustCertKeystore = "";
	private String trustCertKeystorePassword = "";
	private String service = "";
	private String businessScenario = "";
	
	protected boolean stop = false;
	protected boolean check = false;
	protected boolean changes = false;
	
	// Mailbox folders (in, out, sent, ...)
	protected File inbox;
	protected File outbox;
	protected File sent;
	protected File drafts;
	protected File failurs;
	
	// time of last check in order to schedule checks
	protected Date lastCheck = new Date();
	// Set of files which are marked for deletion and shall be no longer considered for processing
	protected TreeSet<String> ignoreList = new TreeSet<>();

	// Current receiver for the next send process
	private String receiver = null;
	
	private static HashMap<String, String> mimetypeForExtension = new HashMap<String, String>();
	private static HashMap<String, String> extensionForMimetype = new HashMap<String, String>();
	
	// should be externalized in the future
	private static String[][] mapping = {{"aac", "audio/aac"}, 
			{"abw", "application/x-abiword"}, 
			{"arc", "application/x-freearc"}, 
			{"avif", "image/avif"}, 
			{"avi", "video/x-msvideo"}, 
			{"azw", "application/vnd.amazon.ebook"}, 
			{"bin", "application/octet-stream"}, 
			{"bmp", "image/bmp"}, 
			{"bz", "application/x-bzip"}, 
			{"bz2", "application/x-bzip2"}, 
			{"cda", "application/x-cdf"}, 
			{"csh", "application/x-csh"}, 
			{"css", "text/css"}, 
			{"csv", "text/csv"}, 
			{"doc", "application/msword"}, 
			{"docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"}, 
			{"eot", "application/vnd.ms-fontobject"}, 
			{"epub", "application/epub+zip"}, 
			{"gz", "application/gzip"}, 
			{"gif", "image/gif"}, 
			{"html", "text/html"}, 
			{"ico", "image/vnd.microsoft.icon"}, 
			{"ics", "text/calendar"}, 
			{"jar", "application/java-archive"}, 
			{"jpg", "image/jpeg"}, 
			{"js", "text/javascript (Specifications: HTML and its reasoning, and IETF)"}, 
			{"json", "application/json"}, 
			{"jsonld", "application/ld+json"}, 
			{"midi", "audio/midi audio/x-midi"}, 
			{"mjs", "text/javascript"}, 
			{"mp3", "audio/mpeg"}, 
			{"mp4", "video/mp4"}, 
			{"mpeg", "video/mpeg"}, 
			{"mpkg", "application/vnd.apple.installer+xml"}, 
			{"odp", "application/vnd.oasis.opendocument.presentation"}, 
			{"ods", "application/vnd.oasis.opendocument.spreadsheet"}, 
			{"odt", "application/vnd.oasis.opendocument.text"}, 
			{"oga", "audio/ogg"}, 
			{"ogv", "video/ogg"}, 
			{"ogx", "application/ogg"}, 
			{"opus", "audio/opus"}, 
			{"otf", "font/otf"}, 
			{"png", "image/png"}, 
			{"pdf", "application/pdf"}, 
			{"php", "application/x-httpd-php"}, 
			{"ppt", "application/vnd.ms-powerpoint"}, 
			{"pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"}, 
			{"rar", "application/vnd.rar"}, 
			{"rtf", "application/rtf"}, 
			{"sh", "application/x-sh"}, 
			{"svg", "image/svg+xml"}, 
			{"swf", "application/x-shockwave-flash"}, 
			{"tar", "application/x-tar"}, 
			{"tiff", "image/tiff"}, 
			{"ts", "video/mp2t"}, 
			{"ttf", "font/ttf"}, 
			{"txt", "text/plain"}, 
			{"vsd", "application/vnd.visio"}, 
			{"wav", "audio/wav"}, 
			{"weba", "audio/webm"}, 
			{"webm", "video/webm"}, 
			{"webp", "image/webp"}, 
			{"woff", "font/woff"}, 
			{"woff2", "font/woff2"}, 
			{"xhtml", "application/xhtml+xml"}, 
			{"xls", "application/vnd.ms-excel"}, 
			{"xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}, 
			{"xml", "text/xml"}, // application/xml is recommended as of RFC 7303 (section 4.1), but text/xml is still used sometimes. You can assign a specific MIME type to a file with .xml extension depending on how its contents are meant to be interpreted. For instance, an Atom feed is application/atom+xml, but application/xml serves as a valid default. 
			{"xul", "application/vnd.mozilla.xul+xml"}, 
			{"zip", "application/zip"}, 
			{"3gp", "video/3gpp; audio/3gpp if it doesn't contain video"}, 
			{"3g2", "video/3gpp2; audio/3gpp2 if it doesn't contain video"}, 
			{"7z", "application/x-7z-compressed"}};
	
	public XTAPostOfficeBox() {
		super();
	}

	protected void initMimetypes() {
		for(int i=0;i<mapping.length;i++) {
			String ext = mapping[i][0];
			String mmt = mapping[i][1];
			
			mimetypeForExtension.put(ext, mmt);
			extensionForMimetype.put(mmt, ext);
		}
	}
	
	/**
	 * Returns the common mime type for a given file type
	 * @param ext	The extension of the file
	 * @return	The likely common mime type for the extension
	 */
	public static String getMimetypeForExtension(String ext) {
		String mime = "text/plain";
		
		if(mimetypeForExtension.containsKey(ext)) {
			mime = mimetypeForExtension.get(ext);
		}
		
		return mime;
	}
	
	/**
	 * Returns the common extension for a given mimetype
	 * @param ext	The mimetype of the file
	 * @return	The likely common extension for that mimetype
	 */
	public static String getExtensionForMimetype(String mmt) {
		String ext = "txt";
		
		if(extensionForMimetype.containsKey(mmt)) {
			ext = extensionForMimetype.get(mmt);
		}
		
		return ext;
	}
	
	/**
	 * Returns the common mime type for a given file
	 * @param path	The path of the file
	 * @return	The likely common mime type for the extension of the file (as part of path)
	 */
	public static String getMimetypeForFile(String path) {
		String ext = "";
		
		int sep = path.toLowerCase().lastIndexOf(".");
		if(sep > 0) {
			ext = path.substring(sep + 1);
		}
		
		return getMimetypeForExtension(ext);
	}
	
	/**
	 * Called when a check for new messages shall be performed (on demand or periodically)
	 */
	public void checkForMessages() {
		try {
			Identifier idA1 = new Identifier(identifierA1);
			
			XtaClient a1XtaClient = initA1(idA1);
			
			File[] files = new File[0];
			
			this.exchangeMessages(files, a1XtaClient, idA1, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates and returns a file hash value in order to identify and distinguish XML files
	 * @param file	The file for which a hash value shall be created
	 * @return	The according hash value of the file
	 */
	protected String getFileHash(File file) {
		return file.getAbsolutePath() + "#" + file.lastModified();
	}

	/**
	 * Returns whether this post office box has sender capabilities (may be overwritten by derived classes)
	 * @return	True if this post office box has sender capabilities, false otherwise
	 */
	public boolean isSender() {
		return true;
	}

	/**
	 * Returns whether this post office box has receiver capabilities (may be overwritten by derived classes)
	 * @return	True if this post office box has receiver capabilities, false otherwise
	 */
	public boolean isReceiver() {
		return true;
	}
	
	/**
	 * Perform a message exchange (that is send all outgoing (files) and check 
	 * for potential incoming messages)
	 * @param files	Files to be sent	
	 * @param a1XtaClient	XTA client instance used for XTA access
	 * @param idA1	Identification data in order to authenticate against the XTA server
	 * @param checkMessages	Shall new messages be received?
	 */
	private void exchangeMessages(File[] files, XtaClient a1XtaClient, Identifier idA1, boolean checkMessages) {
	    try {
			host.getUi().setProgressVisible(true);
			host.getUi().setProgress(ProgressBar.INDETERMINATE_PROGRESS);
			
			//A1 sendet die Nachricht 'beteiligung2Planung.BeteiligungNeuOK.0411' und prueft ob an K1 uebermittelt wurde
			
			// Diese Einstellung funktioniert beim Senden von A1 nach K1! --> zum Testen! 
//			DeliveryOptions deliveryOptions = new DeliveryOptions("urn:xbauleitplanung-beteiligung2planung:1.0", BusinessScenario.XBAULEITPLANUNG_DATA);
			
			// Diese Einstellung funktioniert auch beim Senden von A1 nach K1, wenn der richtige service angegeben ist! --> zum Testen! 
//			BusinessScenario bs;
//			bs = BusinessScenario.valueOf(this.businessScenario);
			DeliveryOptions deliveryOptions = new DeliveryOptions(service);
			
			deliveryOptions.withCheckDeliveryStatus(DeliveryStatus.RECEIVED_IN_MSGBOX);

			for(File file : files) {
				if(this.ignoreList == null) {
					this.ignoreList = new TreeSet<>();
				}
				if(ignoreList.contains(getFileHash(file))) {
					continue;
				}
				
				try {
					host.getUi().setStatusText("Sende Nachricht " + file.getName());
					
					String path = file.getAbsolutePath();
					XTATreeItemI item = this.getDescendantForPath(path);
					if(item != null) {
						final Message message = createMessage(path); //("messages/beteiligung2Planung.BeteiligungNeuOK.0411.xml");
						
						ArrayList<Attachment> attachments = new ArrayList<Attachment>();
						if(item instanceof XTAXMLFileItem) {
							if(((XTAXMLFileItem) item).getAttachments() != null) {
								for(String pth : ((XTAXMLFileItem) item).getAttachments()) {
									File fle = new File(pth);
									byte[] data = Files.readAllBytes(fle.toPath());
									Attachment at1 = new Attachment(getMimetypeForFile(pth), data);
									at1.setFilename(fle.getName());
									attachments.add(at1);
								}
							}
						}
						
						Identifier idK1 = new Identifier(this.receiver); //"diplanfhh:0200");
						final MessageTransportReport transportReport = a1XtaClient.sendMessage(idK1, deliveryOptions, message, attachments);
						final String a1MessageId = transportReport.getMessageId();
						// TODO: Error handling
						
						XTAFileItem parent = (XTAFileItem) item.getParent();
						
						host.copyItem(item.getItemId(), sent.getAbsolutePath(), true, XTAController.ITERATE, null, XTAPostOfficeBox.this, false);
						parent.removeChild(item.getItemId());
						host.getUi().itemRemoved(item);
						
						host.getUi().setStatusText("Nachricht gesendet: " + file.getName());
					}
					else {
						if(file.exists()) {
							if(!file.delete()) {
								file.deleteOnExit();
								
								if(!ignoreList.contains(getFileHash(file))) {
									ignoreList.add(getFileHash(file));
								}
							}
						}
					}
				} catch (Throwable e) {
					e.printStackTrace();
					
					XTATreeItemI item = this.getDescendantForPath(file.getAbsolutePath());
					if(item != null) {
						XTAFileItem parent = (XTAFileItem) item.getParent();
						
						host.copyItem(item.getItemId(), failurs.getAbsolutePath(), true, XTAController.ITERATE, null, XTAPostOfficeBox.this, false);
						parent.removeChild(item.getItemId());
						host.getUi().itemRemoved(item);
						
						if(e.getCause() instanceof UnknownHostException) {
							host.getUi().showMessageDialog("Nachricht konnte nicht gesendet werden", "Kann keine Verbindung zum Host aufbauen: " + sendServiceUrl, 0);
						}
						else if(e.getCause() instanceof MessageSchemaViolationException) {
							host.getUi().showMessageDialog("Nachricht konnte nicht gesendet werden", "Schema-Validierung fehlgeschlagen", 0);
						}
						else if(e instanceof PermissionDeniedException){
							host.getUi().showMessageDialog("Nachricht konnte nicht gesendet werden", "Fehlende Berechtigung - bitte überprüfen Sie Ihr Zertifikat", 0);
						}
						else {
							host.getUi().showMessageDialog("Nachricht konnte nicht gesendet werden", "Annahme verweigert", 0);
						}
					}
					else {
						if(file.exists()) {
							if(!file.delete()) {
								file.deleteOnExit();
								
								if(!ignoreList.contains(getFileHash(file))) {
									ignoreList.add(getFileHash(file));
								}
							}
						}
					}
				}
			}

			if(checkMessages) {
				host.getUi().setStatusText("Pruefe auf neue Nachrichten...");
				
				//Als K1 neue Nachrichten holen
				MessageFilter messageFilter = new MessageFilter().newEntries().withAnzahl(500).withTimeFrom(lastCheck);
				List<MessageTransportReport> messageTransportReportList = null;
				try {
					messageTransportReportList = a1XtaClient.getMessageTransportReportList(messageFilter);
				} catch (Throwable e) {
					e.printStackTrace();
					
					if(e.getCause() instanceof UnknownHostException) {
						host.getUi().showMessageDialog("Nachricht konnte nicht gesendet werden", "Kann keine Verbindung zum Host aufbauen: " + sendServiceUrl, 0);
					}
					else if(e instanceof PermissionDeniedException){
						host.getUi().showMessageDialog("Nachricht konnte nicht gesendet werden", "Fehlende Berechtigung - bitte überprüfen Sie Ihr Zertifikat", 0);
					}
					else {
						host.getUi().showMessageDialog("Nachricht konnte nicht gesendet werden", "Annahme verweigert", 0);
					}
				}
				
				if(messageTransportReportList != null) {
					//Pruefen ob Nachricht enthalten ist
					//Achtung dies ist nur bei Verwendung des gleichen XTA-Brokers moeglich. Ansonsten unterscheiden sich die MessageId's.
					//D.h. die Referenz MessageID (Author) -> MessageId (Reader) gibt es nicht.

					for(MessageTransportReport k1Message : messageTransportReportList) {
						String k1MessageId = k1Message.getMessageId();
						
						//Als K1 die Nachricht abholen
						GetMessageResult getMessageResult;
						getMessageResult = a1XtaClient.getMessage(k1MessageId);
						
						String content = new String(getMessageResult.getMessage().getData());
						String fileName = getMessageResult.getMessage().getMessageTypeCode();
						
						if(!fileName.toLowerCase().endsWith(".xml")) {
							fileName = fileName + ".xml";
						}
						
						String destPath = this.inbox.getAbsolutePath() + "/" + fileName;
						File ck = new File(destPath);
						int sep = fileName.lastIndexOf(".");
						String ext = "";
						String stub = fileName;
						if(sep > 0) {
							stub = fileName.substring(0, sep);
							ext = fileName.substring(sep + 1);
						}
						
						int count = 1;
						while(ck.exists()) {
							ck = new File(this.inbox.getAbsolutePath() + "/" + stub + "_" + count + "." + ext);
							destPath = ck.getAbsolutePath();
							count++;
						}
						
						host.getUi().setStatusText("Abruf: " + fileName);
						
						try (Writer writer = new BufferedWriter(new OutputStreamWriter(
						              new FileOutputStream(destPath), "utf-8"))) {
						   writer.write(content);
						}
						
						XTAFileItem parentItem = this.getDescendantForPath(this.inbox.getAbsolutePath());
						
						XTAXMLFileItem child = new XTAXMLFileItem();
						child.setItemName(ck.getName());
						child.setItemId(ck.getAbsolutePath());
						child.setPath(ck.getAbsolutePath());
						parentItem.addChild(child);
						this.host.putItemForId(child.getItemId(), child);
					    
						host.validateAndStructXMLFile(child, this, false);
						host.getUi().itemAdded(child, parentItem);

						if(getMessageResult.getAttachmentList() != null && getMessageResult.getAttachmentList().size() > 0) {
							child.setAttachments(new ArrayList<String>());
							
							File folder = new File(child.getPath()).getParentFile();
							File attFolder = new File(folder + "/." + child.getItemName() + "_Attachments");
							attFolder.mkdirs();
							
							XTAFileItem attFolderChild = new XTAFileItem();
							attFolderChild.setItemName(attFolder.getName());
							attFolderChild.setItemId(attFolder.getAbsolutePath());
							attFolderChild.setPath(attFolder.getAbsolutePath());
							parentItem.addChild(attFolderChild);
							this.host.putItemForId(attFolderChild.getItemId(), attFolderChild);
							
							for(Attachment att : getMessageResult.getAttachmentList()) {
								String attName = att.getFilename();
								String attPath = attFolder.getAbsolutePath() + "/" + attName;
								
								child.getAttachments().add(attPath);
								
								try (FileOutputStream stream = new FileOutputStream(attPath)) {
								    stream.write(att.getData());
								}catch (Exception e) {
									e.printStackTrace();
								}
								
								XTAXMLFileItem attChild = new XTAXMLFileItem();
								attChild.setItemName(attName);
								attChild.setItemId(attPath);
								attChild.setPath(attPath);
								attFolderChild.addChild(attChild);
								this.host.putItemForId(attChild.getItemId(), attChild);
							}
						}
						
						//Hier koennte von k1 die Nachricht lokal weiterverarbeitet werden
						//Nach erfolgter Verarbeitung muss die Nachricht quitiert werden.
						//K1 Bestaetigt die Nachricht
						a1XtaClient.close(k1MessageId, getMessageResult.getRequestHandle());

						//Als A1 pruefen ob die Nachricht von K1 abgeholt wurde
						checkDeliveryStatus(a1XtaClient, k1MessageId, DeliveryStatus.RECEIPT_FROM_MSGBOX);
					}
					
					lastCheck = new Date();
				}
			}
			
			host.getUi().setProgressVisible(false);
		} catch (Throwable e) {
			e.printStackTrace();
			
			host.getUi().setProgressVisible(false);
		}
	}

	@Override
	public void init() {
		this.readMetaInfo();
		
		initMimetypes();
		
		this.ignoreList = new TreeSet<>();
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Identifier idA1 = new Identifier(identifierA1);
					
					XtaClient a1XtaClient = initA1(idA1);
					
					while(true) {
						if(stop) {
							break;
						}
						
						if(changes) {
							idA1 = new Identifier(identifierA1);
							a1XtaClient = initA1(idA1);
							
							changes = false;
						}
						
						if(a1XtaClient != null) {
							File[] files = outbox.listFiles();
							try {
								if(files == null) {
									System.out.println("Fehler beim Lesen des Outbox-Ordners");
									System.out.println("Pfad: " + outbox.getPath());
									System.out.println("Ist Verzeichnis: " + outbox.isDirectory());
									System.out.println("Leserechte: " + outbox.canRead());
									System.out.println("Schreibrechte: " + outbox.canWrite());
									host.getUi().showMessageDialog("Zugriffsfehler", "Zugriff auf Ordner gescheitert: " + outbox.getAbsolutePath(), 0);
								}
								if(files != null && files.length > 0) {
									exchangeMessages(files, a1XtaClient, idA1, false);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							if(check) {
								files = new File[0];
								exchangeMessages(files, a1XtaClient, idA1, true);
								check = false;
							}
						}
						
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					stop = false;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
	}

	/**
	 * Create the XTAClient instance from the set of identification data
	 * @param idA1	The set of identification data
	 * @return	The XTAClient instance used for message exchange
	 */
	private XtaClient initA1(Identifier idA1){
		try {
			URL manUrl = null;
			try {
				manUrl = new URL(managementServiceUrl);
			} catch (Exception e) {
				e.printStackTrace();
				
				this.host.getUi().showMessageDialog("Fehler beim Zugriff auf XTA-Postfach " + this.getItemName(), "Bitte überprüfen Sie die Adresse " + managementServiceUrl, 0);
			}
			
			URL sendUrl = null;
			try {
				sendUrl = new URL(sendServiceUrl);
			} catch (Exception e) {
				e.printStackTrace();
				
				this.host.getUi().showMessageDialog("Fehler beim Zugriff auf XTA-Postfach " + this.getItemName(), "Bitte überprüfen Sie die Adresse " + sendServiceUrl, 0);
			}
			
			URL msgUrl = null;
			try {
				msgUrl = new URL(msgBoxServiceUrl);
			} catch (Exception e) {
				e.printStackTrace();
				
				this.host.getUi().showMessageDialog("Fehler beim Zugriff auf XTA-Postfach " + this.getItemName(), "Bitte überprüfen Sie die Adresse " + msgBoxServiceUrl, 0);
			}
			
			if(manUrl != null && sendUrl != null && msgUrl != null) {
				XtaClient a1XtaClient = new XtaClient(XtaClientConfig.create(
				        manUrl,
				        sendUrl,
				        msgUrl,
				        idA1,
				        clientCertKeystoreA1, this.clientCertKeystoreA1Password,
				        this.trustCertKeystore, this.trustCertKeystorePassword)
				        .withSoapLogging()
				        .withValidation()
				        .build());
				return a1XtaClient;
			}
			else{
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			if(e.getCause() instanceof KeyStoreException) {
				this.host.getUi().showMessageDialog("Fehler beim Zugriff auf XTA-Postfach " + this.getItemName(), "Bitte überprüfen Sie die angegebenen Key-Stores", 0);
			}
			else if(e.getCause() instanceof Exception) {
				if("Exception in createSSLContext: Keystore was tampered with, or password was incorrect".equals(e.getMessage())) {
					this.host.getUi().showMessageDialog("Fehler beim Zugriff auf XTA-Postfach " + this.getItemName(), "Bitte überprüfen Sie die angegebenen Key-Stores einschließlich der Kennwörter", 0);
				}
				else {
					this.host.getUi().showMessageDialog("Fehler beim Zugriff auf XTA-Postfach " + this.getItemName(), "Bitte überprüfen Sie die angegebenen Service-URLs", 0);
				}
			}
			else {
				this.host.getUi().showMessageDialog("Fehler beim Zugriff auf XTA-Postfach " + this.getItemName(), "Es konnte keine Verbindung zum Postfach hergestellt werden. ", 0);
			}
						
			return null;
		} catch (Throwable e) {
			e.printStackTrace();
			
			this.host.getUi().showMessageDialog("Fehler beim Zugriff auf XTA-Postfach " + this.getItemName(), "Es konnte keine Verbindung zum Postfach hergestellt werden. ", 0);
			
			return null;
		}
	}

	@Override
	public void exit() {
		try {
			this.stop = true;
			this.host = null;
			
			this.writeMetaInfo();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create and return a Message instance from a file path
	 * @param path	The path of the file
	 * @return	The message instance created from the file
	 * @throws IOException	
	 */
	Message createMessage(final String path) throws IOException {
    	InputStream is = new FileInputStream(path);
		byte[] byteArray = IOUtils.toByteArray(is);
		return new Message("application/xml", byteArray);
    }

	/**
	 * This method checks the delivery status of a message and blocks until the message is marked to be dilivered
	 * @param client	The XTA client used for message exchange
	 * @param messageId	The id of the message
	 * @param status	The delivery status to be checked against
	 * @throws InvalidMessageIDException
	 * @throws PermissionDeniedException
	 * @throws XTAWSTechnicalProblemException
	 * @throws InterruptedException
	 */
    void checkDeliveryStatus(final XtaClient client, final String messageId, final DeliveryStatus status) throws InvalidMessageIDException, PermissionDeniedException, XTAWSTechnicalProblemException, InterruptedException {
        for (int i = 0; i < 5; i++) {
            final MessageTransportReport messageTransportReport = client.getMessageTransportReport(messageId);
            if (messageTransportReport.getDeliveryStatus().ordinal() >= status.ordinal()) {
                return;
            }
            Thread.sleep(30 * 1000);
        }
    }

	@Override
	public String getItemType() {
		return "POSTOFFICEBOX_RESOURCE";
	}

	@Override
	public XTAResourceType getRessourceType() {
		return XTAResourceType.XTA_ACCOUNT;
	}

	public String getManagementServiceUrl() {
		return managementServiceUrl;
	}

	public void setManagementServiceUrl(String managementServiceUrl) {
		this.managementServiceUrl = managementServiceUrl;
	}

	public String getSendServiceUrl() {
		return sendServiceUrl;
	}

	public void setSendServiceUrl(String sendServiceUrl) {
		this.sendServiceUrl = sendServiceUrl;
	}

	public String getMsgBoxServiceUrl() {
		return msgBoxServiceUrl;
	}

	public void setMsgBoxServiceUrl(String msgBoxServiceUrl) {
		this.msgBoxServiceUrl = msgBoxServiceUrl;
		
		this.changes = true;
	}

	public String getIdentifier() {
		return identifierA1;
	}

	public String getReceiverIdentifier() {
		return identifierA1;
	}

	public void setIdentifier(String identifierA1) {
		this.identifierA1 = identifierA1;
		
		this.changes = true;
	}

	public String getClientCertKeystoreA1() {
		return clientCertKeystoreA1;
	}

	public void setClientCertKeystoreA1(String clientCertKeystoreA1) {
		this.clientCertKeystoreA1 = clientCertKeystoreA1;
		
		this.changes = true;
	}

	public String getClientCertKeystoreA1Password() {
		return clientCertKeystoreA1Password;
	}

	public void setClientCertKeystoreA1Password(String clientCertKeystoreA1Password) {
		this.clientCertKeystoreA1Password = clientCertKeystoreA1Password;
		
		this.changes = true;
	}

	public String getTrustCertKeystore() {
		return trustCertKeystore;
	}

	public void setTrustCertKeystore(String trustCertKeystore) {
		this.trustCertKeystore = trustCertKeystore;
		
		this.changes = true;
	}

	public String getTrustCertKeystorePassword() {
		return trustCertKeystorePassword;
	}

	public void setTrustCertKeystorePassword(String trustCertKeystorePassword) {
		this.trustCertKeystorePassword = trustCertKeystorePassword;
		
		this.changes = true;
	}

	public void setHost(XTAServerI host) {
		this.host = host;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	@Override
	public void setPath(String path) {
		super.setPath(path);
		
		this.inbox = new File(this.getPath() + "/Posteingang");
		this.inbox.mkdirs();
		
		this.outbox = new File(this.getPath() + "/Postausgang");
		this.outbox.mkdirs();
		
		this.sent = new File(this.getPath() + "/Gesendet");
		this.sent.mkdirs();
		
		this.failurs= new File(this.getPath() + "/Sendefehler");
		this.failurs.mkdirs();
		
		this.drafts = new File(this.getPath() + "/Entwuerfe");
		this.drafts.mkdirs();
	}

	public File getInbox() {
		return inbox;
	}

	public File getOutbox() {
		return outbox;
	}

	public File getSent() {
		return sent;
	}

	public File getDrafts() {
		return drafts;
	}

	public File getFailurs() {
		return failurs;
	}

	public void setReceiver(String receiver) {
		this.receiver  = receiver;
	}
	
	public String getReceiver() {
		return this.receiver;
	}

	@Override
	public boolean validationDesired() {
		return false;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getBusinessScenario() {
		return businessScenario;
	}

	public void setBusinessScenario(String businessScenario) {
		this.businessScenario = businessScenario;
	}
	
	public String getDestination() {
		return this.identifierA1;
	}

	public ArrayList<String> getCaseIds() {
		return new ArrayList<String>();
	}

	public void setSelectedCaseId(String caseId) {
	}
}
