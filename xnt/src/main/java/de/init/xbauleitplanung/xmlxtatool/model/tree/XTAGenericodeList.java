package de.init.xbauleitplanung.xmlxtatool.model.tree;

import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XCodelist;

public class XTAGenericodeList extends XTASchemaFileItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3151376994848111808L;
	
	private String urn;
	private String version;

	public XTAGenericodeList() {
		super();
	}

	@Override
	public int getItemChildCount() {
		return 0;
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		return null;
	}

	@Override
	public String getItemType() {
		return "XTA_GenericodeList";
	}
	
	public XCodelist getCodelist() {
		try {
			return (XCodelist) super.getItemChildAt(0);
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}

	public String getUrn() {
		return urn;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
