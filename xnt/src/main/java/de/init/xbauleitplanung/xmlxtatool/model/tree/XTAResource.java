package de.init.xbauleitplanung.xmlxtatool.model.tree;

/**
 * Basic interface of a resource item in the context of this application. Typically, a resource is provided by a 
 * folder location but other resources are thinkable
 * 
 * @author treichling
 *
 */
public abstract interface XTAResource extends XTATreeItemI{
	/**
	 * Type (XTAResourceType) of this resource
	 * @return	The type of this resource
	 */
	public XTAResourceType getRessourceType();

	/**
	 * init method of this resource called on startup
	 */
	public void init();
	/**
	 * exit method of this resource called on shutdown
	 */
	public void exit();
	/**
	 * Flag on whether files within this resource shall be validated or not
	 * @return	True in case files within this resource shall be validated, false otherwise
	 */
	public boolean validationDesired();
}
