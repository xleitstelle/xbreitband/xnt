package de.init.xbauleitplanung.xmlxtatool.ui;

import java.io.Serializable;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAResource;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;

public class TreeViewItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9014057343790805066L;
	
	private String id;
	private XTATreeItemI userObject;
	private String name;
	
	public static TreeViewItem createByName(String name) {
		TreeViewItem ret = new TreeViewItem();
		ret.setName(name);
		ret.setId(name);
		
		return ret;
	}
	
	public static TreeViewItem createByUserObject(XTATreeItemI resource) {
		TreeViewItem ret = new TreeViewItem();
		ret.setUserObject(resource);
		ret.setId(resource.getItemId());
		ret.setName(resource.getItemName());
		
		return ret;
	}
	
	public static TreeViewItem createByUserObject(XTAResource resource) {
		TreeViewItem ret = new TreeViewItem();
		ret.setUserObject(resource);
		ret.setId(resource.getItemId());
		ret.setName(resource.getItemName());
		
		return ret;
	}
	
	public XTATreeItemI getUserObject() {
		return userObject;
	}
	public void setUserObject(XTATreeItemI userObject) {
		this.userObject = userObject;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		if(name != null) {
			return name;
		}
		else {
			if(this.userObject != null) {
				return this.userObject.toString();
			}
			else {
				return "<untitled>";
			}
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}