package de.init.xbauleitplanung.xmlxtatool.ui;

import java.io.File;
import java.io.IOException;

import de.init.xbauleitplanung.xmlxtatool.utilities.IniFileReader;
import de.init.xbauleitplanung.xmlxtatool.utilities.IniFileWriter;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class XTALAuncher extends Application implements EventHandler {
	private Stage stage;
	
	private Image appImage;
	private BorderPane mainPanel;
	private Label titleLabel;
	private VBox mainContent;
	private ResourceProperties properties;
	private HBox actionPanel;
	private Button startButton;
	private Button cancelButton;
	
	private String httpProxyServer;
	private String httpProxyPort;
	private String httpNonProxyHosts;
	private String httpsProxyServer;
	private String httpsProxyPort;
	private String httpsNonProxyHosts;

	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		
		this.mainPanel = new BorderPane();
		
		this.titleLabel = new Label("Konfiguration und Start XML-XTA-Tool");
		this.titleLabel.getStyleClass().add("xta-title-label");
		this.mainPanel.setTop(this.titleLabel);
		
		this.mainContent = new VBox();
		this.mainPanel.setCenter(this.mainContent);
		
		String source = System.getProperty("user.home") + "/xtaviewer/launch.ini";
		IniFileReader read = new IniFileReader(source);
		this.httpProxyServer = read.getValue("httpProxyServer");
		if(httpProxyServer == null) {
			httpProxyServer = "";
		}
		this.httpProxyPort = read.getValue("httpProxyPort");
		if(httpProxyPort == null) {
			httpProxyPort = "80";
		}
		this.httpNonProxyHosts = read.getValue("httpNonProxyHosts");
		if(httpNonProxyHosts == null) {
			httpNonProxyHosts = "";
		}
		this.httpsProxyServer = read.getValue("httpsProxyServer");
		if(httpsProxyServer == null) {
			httpsProxyServer = "";
		}
		this.httpsProxyPort = read.getValue("httpsProxyPort");
		if(httpsProxyPort == null) {
			httpsProxyPort = "443";
		}
		this.httpsNonProxyHosts = read.getValue("httpsNonProxyHosts");
		if(httpsNonProxyHosts == null) {
			httpsNonProxyHosts = "";
		}
		read.close();
		
		this.properties = new ResourceProperties(this, stage);
		this.properties.hideApplyButton();
		
		this.properties.defineProperty("httpProxyServer", "HTTP Proxy-Server", ResourceProperties.STRING, null, null, null);
		this.properties.defineProperty("httpProxyPort", "HTTP Proxy-Server Port", ResourceProperties.STRING, null, null, null);
		this.properties.defineProperty("httpNonProxyHosts", "HTTP Proxy-Ausnahmen", ResourceProperties.STRING, null, null, null);
		this.properties.defineProperty("httpsProxyServer", "HTTPS Proxy-Server", ResourceProperties.STRING, null, null, null);
		this.properties.defineProperty("httpsProxyPort", "HTTPS Proxy-Server Port", ResourceProperties.STRING, null, null, null);
		this.properties.defineProperty("httpsNonProxyHosts", "HTTPS Proxy-Ausnahmen", ResourceProperties.STRING, null, null, null);
		this.mainContent.getChildren().add(this.properties);
		
		properties.setPropertyValue("httpProxyServer", httpProxyServer);
		properties.setPropertyValue("httpProxyPort", httpProxyPort);
		properties.setPropertyValue("httpNonProxyHosts", httpNonProxyHosts);
		properties.setPropertyValue("httpsProxyServer", httpsProxyServer);
		properties.setPropertyValue("httpsProxyPort", httpsProxyPort);
		properties.setPropertyValue("httpsNonProxyHosts", httpsNonProxyHosts);
		
		this.actionPanel = new HBox();
		this.actionPanel.setId("xta-ribbon");
		this.mainPanel.setBottom(this.actionPanel);
		
		this.startButton = new Button("Starte XML-XTA-Tool");
		this.startButton.getStyleClass().add("xta-button");
		this.startButton.setOnMouseClicked(this);
		this.actionPanel.getChildren().add(this.startButton);
		
		this.cancelButton = new Button("Abbrechen");
		this.cancelButton.getStyleClass().add("xta-button");
		this.cancelButton.setOnMouseClicked(this);
		this.actionPanel.getChildren().add(this.cancelButton);
		
		File check = new File("resources/XML-XTA-Tool_t_64.png");
		this.appImage = new Image(check.toURI().toString());
		
		Scene scene = new Scene(mainPanel);
		// Add a style sheet to the scene        
		scene.getStylesheets().add("de/init/xbauleitplanung/xmlxtatool/ui/layoutstyles.css");
		this.stage.setScene(scene);
		this.stage.setTitle("XML-XTA-Tool");
		this.stage.setWidth(800);
		this.stage.setHeight(600);
		this.stage.getIcons().add(this.appImage);
		this.stage.show();
	}
	
	public static void main(String[] args) {
		System.setProperty("file.encoding", "UTF-8");
		launch(XTALAuncher.class);
	}

	@Override
	public void handle(Event arg0) {
		if(arg0.getSource() == this.startButton) {
			this.httpProxyServer = (String) this.properties.getPropertyValue("httpProxyServer");
			this.httpProxyPort = (String) this.properties.getPropertyValue("httpProxyPort");
			this.httpNonProxyHosts = (String) this.properties.getPropertyValue("httpNonProxyHosts");
			this.httpsProxyServer = (String) this.properties.getPropertyValue("httpsProxyServer");
			this.httpsProxyPort = (String) this.properties.getPropertyValue("httpsProxyPort");
			this.httpsNonProxyHosts = (String) this.properties.getPropertyValue("httpsNonProxyHosts");
			
			String source = System.getProperty("user.home") + "/xtaviewer/launch.ini";
			IniFileWriter write = new IniFileWriter(source);
			write.setValue("httpProxyServer", this.httpProxyServer);
			write.setValue("httpProxyPort", this.httpProxyPort);
			write.setValue("httpNonProxyHosts", this.httpNonProxyHosts);
			write.setValue("httpsProxyServer", this.httpsProxyServer);
			write.setValue("httpsProxyPort", this.httpsProxyPort);
			write.setValue("httpsNonProxyHosts", this.httpsNonProxyHosts);
			write.write();
			write.close();
			
			String javaHome = System.getProperty("java.home");
			
			String options = "";
			if(System.getProperty("java.version").compareTo("11") < 0) {
				// alles ok!
			}
			else {
				options = "--module-path \"javafx-sdk-17.0.2\\lib\" --add-modules javafx.controls,javafx.fxml ";
			}
			
			if(!"".equals(this.httpProxyServer)) {
				options = options + "-Dhttp.proxyHost=" + this.httpProxyServer + " -Dhttp.proxyPort=" + this.httpProxyPort + " ";
			}
			if(!"".equals(this.httpsProxyServer)) {
				options = options + "-Dhttps.proxyHost=" + this.httpsProxyServer + " -Dhttps.proxyPort=" + this.httpsProxyPort + " ";
			}
			if(!"".equals(this.httpNonProxyHosts)) {
				options = options + "-Dhttp.nonProxyHosts=\"" + this.httpNonProxyHosts + "\" ";
			}
			if(!"".equals(this.httpsNonProxyHosts)) {
				options = options + "-Dhttps.nonProxyHosts=\"" + this.httpsNonProxyHosts + "\" ";
			}
			
			try {
				// Die Option -Dprism.order=sw beugt Grafikfehlern vor, die bei Online-Meetings vorgekommen sind!
				String command = javaHome + "\\bin\\java -Dfile.encoding=UTF-8 -Xms200m -Xmx4000m -Dprism.order=sw " + options + "-classpath XTATool.jar de.init.xbauleitplanung.xmlxtatool.ui.XTAMainPanel";
//				String command = "java -Dfile.encoding=UTF-8 -Xms200m -Xmx4000m " + options + "-classpath XTATool.jar de.init.xbauleitplanung.xmlxtatool.ui.XTAMainPanel";
				
				System.out.println(command);
				Runtime.getRuntime().exec(command);
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if(arg0.getSource() == this.cancelButton) {
			System.exit(0);
		}
	}
}
