package de.init.xbauleitplanung.xmlxtatool.model.schema.plain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.TreeMap;

import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;

/**
 * This class represents a codelist including a facade interface for easy access to code list entries. 
 * 
 * @author treichling
 *
 */
public class XCodelist extends XTATreeItem implements Serializable, XTATreeItemI{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8150982816143425256L;
	
	private String prefix;
	private String localName;
	private String version;
	
	/**
	 * header entries of this codelist
	 */
	private ArrayList<String> header = new ArrayList<>();
	/**
	 * list of codelist row including string values
	 */
	private ArrayList<ArrayList<String>> values = new ArrayList<>();
	
	/**
	 * Map of handles to code value rows corresponding to their keys
	 */
	private TreeMap<String, ArrayList<String>> shortcut = null;

	public XCodelist() {
		super();
	}
	
	public String getName() {
		if(prefix != null && prefix.length() > 0) {
			return this.prefix + ":" + this.getLocalName();
		}
		else {
			return this.getLocalName();
		}
	}
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	@Override
	public String getItemType() {
		return "XCodelist";
	}

	@Override
	public int getItemChildCount() {
		return 0;
	}

	@Override
	public XTATreeItemI getItemChildAt(int index) {
		return null;
	}

	public ArrayList<String> getHeader() {
		return header;
	}

	public ArrayList<ArrayList<String>> getValues() {
		return values;
	}
	
	public void addValues(ArrayList<String> newRow) {
		this.values.add(newRow);
	}
	
	public void addValues(String v1) {
		ArrayList<String> newRow = new ArrayList<>();
		newRow.add(v1);
		
		this.values.add(newRow);
	}
	
	public void addValues(String v1, String v2) {
		ArrayList<String> newRow = new ArrayList<>();
		newRow.add(v1);
		newRow.add(v2);
		
		this.values.add(newRow);
	}
	
	public void addValues(String v1, String v2, String v3) {
		ArrayList<String> newRow = new ArrayList<>();
		newRow.add(v1);
		newRow.add(v2);
		newRow.add(v3);
		
		this.values.add(newRow);
	}
	
	public void addValues(String v1, String v2, String v3, String v4) {
		ArrayList<String> newRow = new ArrayList<>();
		newRow.add(v1);
		newRow.add(v2);
		newRow.add(v3);
		newRow.add(v4);
		
		this.values.add(newRow);
	}
	
	/**
	 * Returns a row of code values corresponding to a given code key
	 * @param key	The key of the desired row
	 * @return	row of code values corresponding to the code key
	 */
	public ArrayList<String> getRowForKey(String key){
		if(this.shortcut == null) {
			this.shortcut = new TreeMap<>();
			
			for(int i=0;i<this.values.size();i++) {
				ArrayList<String> row = this.values.get(i);
				
				this.shortcut.put(row.get(0), row);
			}
		}
		
		return this.shortcut.get(key);
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String getItemName() {
		return this.getName();
	}
}
