package de.init.xbauleitplanung.xmlxtatool.model.tree;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import java.util.UUID;

import org.apache.commons.io.IOUtils;

import com.nimbusds.jose.jwk.JWK;

import de.fitko.fitconnect.client.model.meta.Attachment;
import de.fitko.fitconnect.client.model.meta.ContentStructure;
import de.fitko.fitconnect.client.model.meta.MetadataSchema;
import de.init.fitconnect.client.FitConnectConfig;
import de.init.fitconnect.client.FitConnectSenderClient;
import de.init.fitconnect.client.FitConnectSubscriberClient;
import de.init.fitconnect.client.model.AttachmentData;
import de.init.fitconnect.client.model.Credentials;
import de.init.fitconnect.client.model.DeliveryInfo;
import de.init.fitconnect.client.model.Destination;
import de.init.fitconnect.client.model.Message;
import de.init.fitconnect.client.model.MessageContainer;
import de.init.fitconnect.client.model.MessageCreated;
import de.init.fitconnect.client.model.MessageReport;
import de.init.xbauleitplanung.xmlxtatool.controller.XTAController;
import de.init.xbauleitplanung.xmlxtatool.model.xml.XNode;
import javafx.scene.control.ProgressBar;

/**
 * Implementation of a post office box using the FIT connect infrastructure for message exchange
 * This class is based on the XTAPostOfficeBox implementation
 * 
 * @author treichling
 *
 */
public class XTAFITConnectPostOfficeBox extends XTAPostOfficeBox implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3999642281195657314L;
	
	// fields for receiver encryption, identification and authentication
	private String subscriberID = "";
	private String subscriberSecret = "";
	private String subscriberDestination = "";
	private String privateKeyFile = "";
	private String privateSignFile = "";
	private String publicSignFile = "";
	private String messageOrganization = "0";
	
    private transient Credentials subscriberCreds;
	private transient Destination subscriberDestinationData;

	// fields for sender identification and authentication
	private String senderID = "";
	private String senderSecret = "";
    private transient Credentials senderCreds;
    private transient String selecteCaseId = null;

    private String SERVICETPYE_INDENTIFIER_TIEFBAU = "";

//    private transient FitConnectClient client;
    private transient FitConnectSubscriberClient subscriberClient;
    private transient FitConnectSenderClient senderClient;
	
    // Flag to indicate when this post office is sending
	private transient boolean sending = false;

	// Case folder covering all cases
	private File cases;
	
	public XTAFITConnectPostOfficeBox() {
		super();
	}

    private static JWK parseJson(final String path) {
        try {
        	// "certs/privateKey_decryption.json"
        	InputStream is = new FileInputStream(path);
            return JWK.parse(IOUtils.toString(is, Charset.defaultCharset()));
        } catch (final Exception e) {
            throw new RuntimeException("cant parse json: " + path);
        }
    }
	
	@Override
	public void checkForMessages() {
		try {
	        host.getUi().setStatusText("Prüfe auf neue Nachrichten in Postfach " + this.getItemName());
			host.getUi().setProgressVisible(true);
			host.getUi().setProgress(ProgressBar.INDETERMINATE_PROGRESS);
	        
	        //Subscriber fragt Submissions ab
	        final List<MessageReport> reports = subscriberClient.getMessages(subscriberDestinationData);
			
	        if(reports.size() <= 0) {
		        host.getUi().setStatusText("Keine Nachrichten zum Abruf");
	        }
	        else {
		        host.getUi().setStatusText("Lese Nachrichten: " + reports.size());
				host.getUi().setProgress(ProgressBar.USE_COMPUTED_SIZE);
				host.getUi().setProgressMax(reports.size());
				host.getUi().setProgressValue(0);
	        }
	        for(int i=0;i<reports.size();i++) {
				host.getUi().setStatusText("Lese Nachricht " + (i + 1) + " von " + reports.size());
				host.getUi().setProgressValue(i);
				
				MessageReport fitMessage = reports.get(i);
				UUID submissionID = fitMessage.getSubmissionID();
				UUID caseID = fitMessage.getCaseID();
				UUID destinationID = subscriberDestinationData.getDestinationID();
				
				try {
					//Subscriber fragt einzelne submission ab
					final MessageContainer messageContainer = subscriberClient.getMessage(submissionID);
					Message message = messageContainer.getMessage();
					
					String content = new String(message.getData());
					String fileName = message.getMessageID().toString();
					
					// Speichere den Inhalt erst in einer temporären Datei, ermittele dann den XML-Typ (Root-Element)
					// und dann benenne die Datei entsprechend um. 
					if(!fileName.toLowerCase().endsWith(".xml")) {
						fileName = fileName + ".xml";
					}
					
					String destPath = this.inbox.getAbsolutePath() + "/" + fileName;
					File ck = new File(destPath);
					int sep = fileName.lastIndexOf(".");
					String ext = "";
					String stub = fileName;
					if(sep > 0) {
						stub = fileName.substring(0, sep);
						ext = fileName.substring(sep + 1);
					}
					
					int count = 1;
					while(ck.exists()) {
						ck = new File(this.inbox.getAbsolutePath() + "/" + stub + "_" + count + "." + ext);
						destPath = ck.getAbsolutePath();
						count++;
					}
					
					try (Writer writer = new BufferedWriter(new OutputStreamWriter(
					              new FileOutputStream(destPath), "utf-8"))) {
					   writer.write(content);
					}
					
					String tp = ((XTAController) host).getXMLType(new File(destPath));
					if(tp != null) {
						tp = XTATreeItem.getLocalName(tp);
					}
					
					stub = tp;
					destPath = this.inbox.getAbsolutePath() + "/" + tp + "." + ext;
					File ck2 = new File(destPath);
					count = 1;
					while(ck2.exists()) {
						ck2 = new File(this.inbox.getAbsolutePath() + "/" + stub + "_" + count + "." + ext);
						destPath = ck2.getAbsolutePath();
						count++;
					}
					
					// Datei umbenennen
					ck.renameTo(ck2);
					
					XTAFileItem parentItem = this.getDescendantForPath(this.inbox.getAbsolutePath());
					
					XTAXMLFileItem child = new XTAXMLFileItem();
					child.setItemName(ck2.getName());
					child.setItemId(ck2.getAbsolutePath());
					child.setPath(ck2.getAbsolutePath());
					parentItem.addChild(child);
					this.host.putItemForId(child.getItemId(), child);
					
					MessageReport report = messageContainer.getReport();
					UUID subissionId = report.getSubmissionID();
					String suid = subissionId.toString();
					
					// If case based ordering is desired...
					if("1".equals(this.messageOrganization)) {
						
						// Check whether case folder already exists (known case)...
						// otherwise it will be created
						File check = new File(this.cases.getAbsolutePath() + "/" + caseID.toString());
						if(!check.exists()) {
							check.mkdirs();
							XTAFileItem caseFolder = new XTAFileItem();
							caseFolder.setPath(this.cases.getAbsolutePath() + "/" + caseID.toString());
							
							XTAFileItem caseParent = this.getDescendantForPath(this.cases.getAbsolutePath());
							caseParent.addChild(caseFolder);
							
							host.getUi().itemAdded(caseFolder, caseParent);
						}
						
						// Make a copy of the item just arrived and put it into the case folder
						XTAFileItem newItem = host.copyItem(
								child.getItemId(), 
								this.cases.getAbsolutePath() + "/" + caseID.toString(), 
								false, 
								XTAController.ITERATE, 
								null, 
								XTAFITConnectPostOfficeBox.this, 
								false);
						
						// Add metadata about the case assignement to the message item
						MessageMetaData md = MessageMetaData.create()
								.addRecipient(this.senderID)
								.setSender(suid)
								.setSendDate(System.currentTimeMillis())
								.setVia(MessageMetaData.RECEIVED);
						this.addMetaData(newItem.getPath(), md);
						host.getUi().itemUpdated(newItem);
					}
					
					MessageMetaData md = MessageMetaData.create()
							.addRecipient(this.senderID)
							.setSender(suid)
							.setSendDate(System.currentTimeMillis())
							.setVia(MessageMetaData.RECEIVED);
					this.addMetaData(child.getPath(), md);
					host.getUi().itemUpdated(child);
					
					host.validateAndStructXMLFile(child, this, false);
					host.getUi().itemAdded(child, parentItem);

					if(messageContainer.getAttachments() != null && messageContainer.getAttachments().size() > 0) {
						child.setAttachments(new ArrayList<String>());
						
						// Create attachments folder...
						File folder = new File(child.getPath()).getParentFile();
						File attFolder = new File(folder + "/." + child.getItemName() + "_Attachments");
						attFolder.mkdirs();
						
						XTAFileItem attFolderChild = new XTAFileItem();
						attFolderChild.setItemName(attFolder.getName());
						attFolderChild.setItemId(attFolder.getAbsolutePath());
						attFolderChild.setPath(attFolder.getAbsolutePath());
						parentItem.addChild(attFolderChild);
						this.host.putItemForId(attFolderChild.getItemId(), attFolderChild);
						this.host.getUi().itemAdded(attFolderChild, parentItem);
						
						// If case based ordering is desired copy attachments folder into case folder
						if("1".equals(this.messageOrganization)) {
							
							// Make a copy of the item just arrived and put it into the case folder
							XTAFileItem newItem = host.copyItem(
									attFolderChild.getItemId(), 
									this.cases.getAbsolutePath() + "/" + caseID.toString(), 
									false, 
									XTAController.ITERATE, 
									null, 
									XTAFITConnectPostOfficeBox.this, 
									false);
							host.getUi().itemUpdated(newItem);
						}
						
						int numAttachments = messageContainer.getAttachments().size();
						int attIndex = 1;
					    
					    List<Attachment> attMetadata = messageContainer.getMetadatensatz().getContentStructure().getAttachments();
						HashMap<String, Attachment> metadata = new HashMap<String, Attachment>();
					    if(attMetadata != null) {
							for(Attachment att : attMetadata) {
								try {
									metadata.put(att.getAttachmentId().toString(), att);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					    
						// XBau-spezifisch - muss langfristig verlagert werden
						HashMap<String, String> attNamesForIDs = new HashMap<String, String>();
					    try {
							XTAXMLFileItem xml = child;
							
							XNode root = xml.getNodes().get(0);
							
							String prefix = root.getPrefix();
							if(prefix == null) {
								prefix = XTAXMLFileItem.getPrefix(root.getName());
							}
							
							XNode attNode = root.getNodeForName(prefix + ":anlagen");
							if(attNode != null) {
								for(int j=0;j<attNode.getItemChildCount();j++) {
									XNode nameNode = attNode.getItemChildAt(j);
									if("anlage".equals(nameNode.getName())) {
										String attId = nameNode.getNodeForName("anhangOderVerlinkung").getNodeForName("anhang").getNodeForName("dokumentid").getValue();
										String attName= nameNode.getNodeForName("anhangOderVerlinkung").getNodeForName("anhang").getNodeForName("dateiname").getValue();
										attNamesForIDs.put(attId, attName);
									}
								}
							}
					    }
					    catch (Exception e) {
					    	e.printStackTrace();
						}
					    
					    for(AttachmentData att : messageContainer.getAttachments()) {
					        host.getUi().setStatusText("Rufe Attachment ab (" + attIndex + " / " + numAttachments + ")");
					        
					        // Assign name and extension to attachment file...
					        // name will be assigned (1) from attachment object, (2) attaxchment metadata or (3) from information within the XML message
							String attName = "Attachment_" + attIndex;
							if(att.getFilename() != null) {
								attName = att.getFilename();
							}
							else if(metadata.get(att.getAttachmentId().toString()) != null && metadata.get(att.getAttachmentId().toString()).getFilename() != null) {
								attName = metadata.get(att.getAttachmentId().toString()).getFilename();
							}
							else if(attNamesForIDs.containsKey(att.getAttachmentId().toString())) {
								attName = attNamesForIDs.get(att.getAttachmentId().toString());
							}
							
							// extension will be guessed from mime type
							String aext = getExtensionForMimetype(att.getMimeType());
							
							if(aext != null) {
								if(!attName.toLowerCase().endsWith("." + aext)) {
									attName = attName + "." + aext;
								}
							}
							else {
								if(!attName.toLowerCase().endsWith(".txt")) {
									attName = attName + ".txt";
								}
							}
							
							String attPath = attFolder.getAbsolutePath() + "/" + attName;
							child.getAttachments().add(attPath);
							
							try (FileOutputStream stream = new FileOutputStream(attPath)) {
							    stream.write(att.getData());
							}catch (Exception e) {
								e.printStackTrace();
							}
							
							XTAFileItem attChild = new XTAFileItem();
							if("text/xml".equals(att.getMimeType()) || "application/xml".equals(att.getMimeType())) {
								attChild = new XTAXMLFileItem();
								attChild.setItemName(attName);
								attChild.setItemId(attPath);
								attChild.setPath(attPath);
								attFolderChild.addChild(attChild);
								this.host.putItemForId(attChild.getItemId(), attChild);
								this.host.validateAndStructXMLFile((XTAXMLFileItem) attChild, this, false);
							}
							else{
								attChild = new XTAFileItem();
								attChild.setItemName(attName);
								attChild.setItemId(attPath);
								attChild.setPath(attPath);
								attFolderChild.addChild(attChild);
								this.host.putItemForId(attChild.getItemId(), attChild);
							}
							this.host.getUi().itemAdded(attChild, attFolderChild);
							
							// If case based ordering is desired...
							if("1".equals(this.messageOrganization)) {
								
								// Make a copy of the item just arrived and put it into the case folder
								XTAFileItem newItem = host.copyItem(
										attChild.getItemId(), 
										this.cases.getAbsolutePath() + "/" + caseID.toString() + "/" + attFolder.getName(), 
										false, 
										XTAController.ITERATE, 
										null, 
										XTAFITConnectPostOfficeBox.this, 
										false);
								host.getUi().itemUpdated(newItem);
							}
							
							attIndex++;
						}
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				try {
					// Works since we made a change: No case ID must be specified when sending a message! 
					subscriberClient.acceptMessage(destinationID, caseID, submissionID);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
	        if(reports.size() > 0) {
		        host.getUi().setStatusText("Nachrichten wurden erfolgreich abgerufen");
	        }
			host.getUi().setProgressValue(reports.size());	
			host.getUi().setProgressVisible(false);
			
			lastCheck = new Date();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Called to initiate message sending
	 * @param files
	 */
	private void exchangeMessages(File[] files) {
		if(sending) {
			return;
		}
		sending = true;
		
	    try {
			host.getUi().setProgressVisible(true);
			host.getUi().setProgress(ProgressBar.INDETERMINATE_PROGRESS);
			
			String caseId = this.getSelecteCaseId();
			this.setSelecteCaseId(null);

			for(File file : files) {
				if(this.ignoreList == null) {
					this.ignoreList = new TreeSet<>();
				}
				if(ignoreList.contains(getFileHash(file))) {
					continue;
				}
				
				try {
					String path = file.getAbsolutePath();
					XTATreeItemI item = this.getDescendantForPath(path);
					if(item != null) {
						host.getUi().setStatusText("Sende Nachricht " + file.getName());
				        
			        	MetadataSchema metadatensatz = new MetadataSchema();
			        	metadatensatz.setContentStructure(new ContentStructure());
			        	metadatensatz.getContentStructure().getAttachments().clear();

				        //TODO: Anhänge müssen in der Nachricht mit documentID und dateiname referenziert werden! 
			        	final List<AttachmentData> attachments = new ArrayList<AttachmentData>();
						if(item instanceof XTAXMLFileItem) {
							if(((XTAXMLFileItem) item).getAttachments() != null) {
						        for(String pth : ((XTAXMLFileItem) item).getAttachments()) {
									File fle = new File(pth);
									byte[] data = Files.readAllBytes(fle.toPath());
									AttachmentData at1 = AttachmentData.builder().data(data).mimeType(getMimetypeForFile(pth)).build();
									attachments.add(at1);
									
						        	Attachment att = new Attachment();
						        	att.setAttachmentId(at1.getAttachmentId());
						        	att.setFilename(fle.getName());
						        	metadatensatz.getContentStructure().getAttachments().add(att);
								}
							}
						}

				        // Senden als Sender
						// Important: No case ID must be specified here. This would make it impossible to close a message after receiving
						// DO NOT DO THIS: .caseID(UUID.randomUUID())
				        DeliveryInfo deliveryInfo = null;
				        if(caseId != null) {
					        deliveryInfo = DeliveryInfo.builder()
					                .destinationID(UUID.fromString(this.getReceiver()))
					                .serviceTypeIdentifier(SERVICETPYE_INDENTIFIER_TIEFBAU)
					                .serviceTypeName(SERVICETPYE_INDENTIFIER_TIEFBAU)
					                .caseID(UUID.fromString(caseId))
					                .build();
				        }
				        else {
					        deliveryInfo = DeliveryInfo.builder()
					                .destinationID(UUID.fromString(this.getReceiver()))
					                .serviceTypeIdentifier(SERVICETPYE_INDENTIFIER_TIEFBAU)
					                .serviceTypeName(SERVICETPYE_INDENTIFIER_TIEFBAU)
					                .build();
				        }

				        MessageCreated messageCreated = senderClient.createMessage(deliveryInfo, attachments);
				        
				        // in deliveryInfo kann CaseID angegeben werden wenn sie bereits existiert! 
				        messageCreated.getCaseID();
				        
				        // Insert submission ID into the the head of the message in case it is has prefix xbau
						if(item instanceof XTAXMLFileItem) {
							XTAXMLFileItem xml = (XTAXMLFileItem) item;
							
							if(xml.getNodes() != null && xml.getNodes().size() > 0) {
						        try {
									XNode root = xml.getNodes().get(0);
									
									String prefix = root.getPrefix();
									if(prefix == null) {
										prefix = XTAXMLFileItem.getPrefix(root.getName());
									}
									
									try {
										XNode target = root.getNodeForName("nachrichtenkopf").getNodeForName("identifikation.nachricht").getNodeForName("nachrichtenUUID");
										target.setValue(messageCreated.getSubmissionID().toString());
									} catch (Exception e) {
										e.printStackTrace();
									}
									
									XNode attTarget = root.getNodeForName(prefix + ":anlagen");
									
									if(attTarget != null) {
										root.removeChild(attTarget);
									}
									
									if(xml.getAttachments() != null && xml.getAttachments().size() > 0) {
										attTarget = new XNode();
										attTarget.setName(prefix + ":anlagen");
										attTarget.setType(XNode.ELEMENT);
										attTarget.getChildren().clear();
										root.addChild(attTarget);
										
										for(int i=0;i<xml.getAttachments().size();i++) {
											XNode attachment = new XNode();
											attachment.setName("anlage");
											attachment.setType(XNode.ELEMENT);
											attTarget.addChild(attachment);
											
											XNode mime = new XNode();
											mime.setName("mimeType");
											mime.getAttributes().put("listURI", "");
											mime.getAttributes().put("listVersionID", "");
											mime.setType(XNode.ELEMENT);
											attachment.addChild(mime);
											
											XNode mc = new XNode();
											mc.setName("code");
											mc.setType(XNode.ELEMENT);
											mime.addChild(mc);
											
											XNode link = new XNode();
											link.setName("anhangOderVerlinkung");
											link.setType(XNode.ELEMENT);
											attachment.addChild(link);
											
											XNode al = new XNode();
											al.setName("anhang");
											al.setType(XNode.ELEMENT);
											link.addChild(al);
											
											XNode di = new XNode();
											di.setName("dokumentid");
											di.setType(XNode.ELEMENT);
											di.setValue(metadatensatz.getContentStructure().getAttachments().get(i).getAttachmentId().toString());
											al.addChild(di);
											
											XNode an = new XNode();
											an.setName("dateiname");
											an.setType(XNode.ELEMENT);
											an.setValue(metadatensatz.getContentStructure().getAttachments().get(i).getFilename());
											al.addChild(an);
										}
									}
									
									xml.export(false);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
				        
				        //TODO: Die submission ID muss als Identifikation.Nachricht eingetragen werden
						InputStream is = new FileInputStream(path);
						
				        final Message message = Message.builder()
				                .contentType("application/xml")
				                .data(IOUtils.toByteArray(is))
				                .build();
				        
				        final MessageReport report = senderClient.sendMessage(
				        		messageCreated.getSubmissionID(), 
				        		messageCreated.getDestinationID(), 
				        		deliveryInfo, 
				        		metadatensatz, 
				        		message, 
				        		attachments);
				        final UUID submissionID = report.getSubmissionID();
				        final UUID caseID = report.getCaseID();
				        System.out.println(caseID);
				        System.out.println(messageCreated.getSubmissionID());
				        System.out.println(messageCreated.getCaseID());
				        System.out.println(messageCreated.getDestinationID());
						
						XTAFileItem parent = (XTAFileItem) item.getParent();
						
						XTAFileItem copied = host.copyItem(
								item.getItemId(), 
								sent.getAbsolutePath(), 
								true, 
								XTAController.ITERATE, 
								null, 
								XTAFITConnectPostOfficeBox.this, 
								false);
						parent.removeChild(item.getItemId());
						host.getUi().itemRemoved(item);
						
						this.addMetaData(copied.getPath(), 
								MessageMetaData.create()
								.addRecipient(this.getReceiver())
								.setSender(this.senderID)
								.setSendDate(System.currentTimeMillis())
								.setVia(MessageMetaData.SENT));
						host.getUi().itemUpdated(copied);
						
						if("1".equals(this.messageOrganization)) {
							File check = new File(this.cases.getAbsolutePath() + "/" + messageCreated.getCaseID());
							if(!check.exists()) {
								check.mkdirs();
								
								XTAFileItem caseFolder = new XTAFileItem();
								caseFolder.setPath(this.cases.getAbsolutePath() + "/" + messageCreated.getCaseID());
								
								XTAFileItem caseParent = this.getDescendantForPath(this.cases.getAbsolutePath());
								caseParent.addChild(caseFolder);
								
								host.getUi().itemAdded(caseFolder, caseParent);
							}
							XTAFileItem newItem = host.copyItem(
									copied.getItemId(), 
									this.cases.getAbsolutePath() + "/" + messageCreated.getCaseID(), 
									false, 
									XTAController.ITERATE, 
									null, 
									XTAFITConnectPostOfficeBox.this, 
									false);
							
							this.addMetaData(newItem.getPath(), 
									MessageMetaData.create()
									.addRecipient(this.getReceiver())
									.setSender(this.senderID)
									.setSendDate(System.currentTimeMillis())
									.setVia(MessageMetaData.SENT));
							host.getUi().itemUpdated(newItem);
						}
						
						host.getUi().setStatusText("Nachricht gesendet: " + file.getName());
					}
					else {
						if(file.exists()) {
							if(!file.delete()) {
								file.deleteOnExit();

								if(!ignoreList.contains(getFileHash(file))) {
									ignoreList.add(getFileHash(file));
								}
							}
						}
					}
				} catch (Throwable e) {
					e.printStackTrace();
					
					XTATreeItemI item = this.getDescendantForPath(file.getAbsolutePath());
					if(item != null) {
						XTAFileItem parent = (XTAFileItem) item.getParent();
						
						host.copyItem(item.getItemId(), failurs.getAbsolutePath(), true, XTAController.ITERATE, null, XTAFITConnectPostOfficeBox.this, false);
						parent.removeChild(item.getItemId());
						host.getUi().itemRemoved(item);
						
						host.getUi().showMessageDialog("Nachricht konnte nicht gesendet werden", "Annahme verweigert", 0);
					}
					else {
						if(file.exists()) {
							if(!file.delete()) {
								file.deleteOnExit();

								if(!ignoreList.contains(getFileHash(file))) {
									ignoreList.add(getFileHash(file));
								}
							}
						}
					}
				}
			}
			
			host.getUi().setProgressVisible(false);
		} catch (Throwable e) {
			e.printStackTrace();
			
			host.getUi().setProgressVisible(false);
		}
	    sending = false;
	}

	@Override
	public void init() {
		try {
			this.ignoreList = new TreeSet<>();
			
			initMimetypes();
			
			createSubscriberData();
			createSenderData();
			
//			client = new FitConnectClient();
			try {
				subscriberClient = new FitConnectSubscriberClient(FitConnectConfig.builder().credentials(subscriberCreds).build());
				senderClient = new FitConnectSenderClient(FitConnectConfig.builder().credentials(senderCreds).build());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						while(true) {
							if(stop) {
								break;
							}
							
							if(changes) {
								createSubscriberData();
								createSenderData();
								
								subscriberClient = new FitConnectSubscriberClient(FitConnectConfig.builder().credentials(subscriberCreds).build());
								senderClient = new FitConnectSenderClient(FitConnectConfig.builder().credentials(senderCreds).build());
//								client = new FitConnectClient();
								
								changes = false;
							}
							
							if(subscriberClient != null) {
								File[] files = outbox.listFiles();
								try {
									if(files == null) {
										System.out.println("Fehler beim Lesen des Outbox-Ordners");
										System.out.println("Pfad: " + outbox.getPath());
										System.out.println("Ist Verzeichnis: " + outbox.isDirectory());
										System.out.println("Leserechte: " + outbox.canRead());
										System.out.println("Schreibrechte: " + outbox.canWrite());
										host.getUi().setStatusText("Zugriff auf Ordner gescheitert: " + outbox.getAbsolutePath());
//										host.getUi().showMessageDialog("Zugriffsfehler", "Zugriff auf Ordner gescheitert: " + outbox.getAbsolutePath(), 0);
									}
									if(files != null && files.length > 0) {
										exchangeMessages(files);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								if(check) {
									files = new File[0];
									checkForMessages();
									check = false;
								}
							}
							
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						
						stop = false;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Compose sender credentials from according fields
	 */
	private void createSenderData() {
		if(this.senderID != null && this.senderID.length() > 0 && 
				this.senderSecret != null && this.senderSecret.length() > 0) {
			try {
				senderCreds = Credentials.builder()
				        .clientID(senderID)
				        .clientSecret(senderSecret)
				        .build();
			} catch (Exception e) {
				e.printStackTrace();
				this.host.getUi().showMessageDialog("Sender-Angaben fehlerhaft", "Ihre Sender-Angaben sind fehlerhaft. Bitte überprüfen Sie diese. ", 0);
			}
		}
	}

	/**
	 * Compose subscriber credentials from according fields
	 */
	private void createSubscriberData() {
		if(this.privateKeyFile != null && this.privateKeyFile.length() > 0 && 
				this.privateSignFile != null && this.privateSignFile.length() > 0 && 
				this.publicSignFile != null && this.publicSignFile.length() > 0 && 
				this.subscriberID != null && this.subscriberID.length() > 0 && 
				this.subscriberSecret != null && this.subscriberSecret.length() > 0) {
			boolean ok = true;
			File check = new File(this.privateKeyFile);
			if(!check.exists()) {
				this.host.getUi().showMessageDialog("Datei nicht gefunden", "Die Datei " + this.privateKeyFile + " konnte nicht geöffnet werden. ", 0);
				ok = false;
			}
			
			if(ok) {
				check = new File(this.privateSignFile);
				if(!check.exists()) {
					this.host.getUi().showMessageDialog("Datei nicht gefunden", "Die Datei " + this.privateSignFile+ " konnte nicht geöffnet werden. ", 0);
					ok = false;
				}
			}
			
			if(ok) {
				check = new File(this.publicSignFile);
				if(!check.exists()) {
					this.host.getUi().showMessageDialog("Datei nicht gefunden", "Die Datei " + this.publicSignFile+ " konnte nicht geöffnet werden. ", 0);
					ok = false;
				}
			}
			
			if(ok) {
				try {
					subscriberCreds = Credentials.builder()
					        .clientID(subscriberID)
					        .clientSecret(subscriberSecret)
					        .privateCryptKey(parseJson(privateKeyFile))
					        .privateSignKey(parseJson(privateSignFile))
					        .publicSignKey(parseJson(publicSignFile))
					        .build();

					subscriberDestinationData = Destination.builder()
					        .destinationID(UUID.fromString(subscriberDestination))
					        .build();
				} catch (Exception e) {
					e.printStackTrace();
					this.host.getUi().showMessageDialog("Subscriber-Angaben fehlerhaft", "Ihre Subscriber-Angaben sind fehlerhaft. Bitte überprüfen Sie diese. ", 0);
				}
			}
		}
	}
	
	@Override
	public String getItemType() {
		return "FITCONNECT_POSTOFFICEBOX_RESOURCE";
	}

	@Override
	public XTAResourceType getRessourceType() {
		return XTAResourceType.REST_ENDPOINT;
	}
	
	public String getSubscriberID() {
		return subscriberID;
	}

	public void setSubscriberID(String subscriberID) {
		this.subscriberID = subscriberID;
		
		this.changes = true;
	}

	public String getSubscriberSecret() {
		return subscriberSecret;
	}

	public void setSubscriberSecret(String subscriberSecret) {
		this.subscriberSecret = subscriberSecret;
		
		this.changes = true;
	}

	public String getSubscriberDestination() {
		return subscriberDestination;
	}

	public void setSubscriberDestination(String subscriberDestination) {
		this.subscriberDestination = subscriberDestination;
		
		this.changes = true;
	}

	public String getPrivateKeyFile() {
		return privateKeyFile;
	}

	public void setPrivateKeyFile(String privateKeyFile) {
		this.privateKeyFile = privateKeyFile;
		
		this.changes = true;
	}

	public String getSenderID() {
		return senderID;
	}

	public void setSenderID(String senderID) {
		this.senderID = senderID;
		
		this.changes = true;
	}

	public String getSenderSecret() {
		return senderSecret;
	}

	public void setSenderSecret(String senderSecret) {
		this.senderSecret = senderSecret;
		
		this.changes = true;
	}

	public String getService() {
		return SERVICETPYE_INDENTIFIER_TIEFBAU;
	}

	public void setService(String service) {
		this.SERVICETPYE_INDENTIFIER_TIEFBAU = service;
		
		this.changes = true;
	}

	@Override
	public boolean isSender() {
		return this.senderID != null && this.senderID.length() > 0;
	}

	@Override
	public boolean isReceiver() {
		return this.subscriberID != null && this.subscriberID.length() > 0;
	}

	@Override
	public String getIdentifier() {
		String identifier = "FIT-Connect CLient";
		
		if(this.subscriberID != null && this.subscriberID.length() > 0) {
			identifier = "FIT-Connect Subscriber " + this.subscriberID;
		}
		if(this.senderID != null && this.senderID.length() > 0) {
			identifier = "FIT-Connect Sender " + this.senderID;
		}
		return identifier;
	}

	public String getReceiverIdentifier() {
		return subscriberDestinationData.getDestinationID().toString();
	}

	public String getPrivateSignFile() {
		return privateSignFile;
	}

	public String getPublicSignFile() {
		return publicSignFile;
	}

	public void setPrivateSignFile(String privateSignFile) {
		this.privateSignFile = privateSignFile;
	}

	public void setPublicSignFile(String publicSignFile) {
		this.publicSignFile = publicSignFile;
	}

	@Override
	public String getDestination() {
		return this.subscriberDestination;
	}

	public String getMessageOrganization() {
		if(this.messageOrganization == null) {
			this.messageOrganization = "0";
		}
		return messageOrganization;
	}

	public void setMessageOrganization(String messageOrganization) {
		try {
			if("0".equals(this.messageOrganization) && "1".equals(messageOrganization)) {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.messageOrganization = messageOrganization;
	}

	@Override
	public void resourceUpdated() {
		System.out.println("*************** Fertig ****************");
	}

	@Override
	public Comparator<XTATreeItemI> getComparator() {
		Comparator<XTATreeItemI> comparator = new Comparator<XTATreeItemI>() {
			
			@Override
			public int compare(XTATreeItemI o1, XTATreeItemI o2) {
				if(o1 instanceof XTAFileItem && o2 instanceof XTAFileItem) {
					XTAFileItem oo1 = (XTAFileItem) o1;
					XTAFileItem oo2 = (XTAFileItem) o2;
					
					if(oo1.getPath().startsWith(cases.getAbsolutePath())) {
						File c1 = new File(oo1.getPath());
						File c2 = new File(oo2.getPath());
						
						int ret = ((Long) c1.lastModified()).compareTo(c2.lastModified());
						
						if(ret == 0) {
							ret = oo1.getPath().compareTo(oo2.getPath());
						}
						
						return ret;
					}
					else {
						return oo1.getPath().compareTo(oo2.getPath());
					}
//					if(oo1.getPath().startsWith(getInbox().getAbsolutePath()) || 
//							oo1.getPath().startsWith(getOutbox().getAbsolutePath()) || 
//							oo1.getPath().startsWith(getDrafts().getAbsolutePath()) || 
//							oo1.getPath().startsWith(getFailurs().getAbsolutePath()) || 
//							oo1.getPath().startsWith(getSent().getAbsolutePath())) {
//						return oo1.getPath().compareTo(oo2.getPath());
//					}
//					else {
//						File c1 = new File(oo1.getPath());
//						File c2 = new File(oo2.getPath());
//						
//						return ((Long) c1.lastModified()).compareTo(c2.lastModified());
//					}
				}
				else {
					return o1.getItemId().compareTo(o2.getItemId());
				}
			}
		};
		
		return comparator;
	}

	@Override
	public void setPath(String path) {
		super.setPath(path);
		
		this.cases = new File(this.getPath() + "/Cases");
		this.cases.mkdirs();
	}

	@Override
	public ArrayList<String> getCaseIds() {
		ArrayList<String> ret = new ArrayList<String>();
		
		if(this.cases == null) {
			this.setPath(this.getPath());
		}
		try {
			File[] cases = this.cases.listFiles();
			for(int i=0;i<cases.length;i++) {
				if(cases[i].isDirectory()) {
					ret.add(cases[i].getName());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}

	@Override
	public void setSelectedCaseId(String caseId) {
		this.selecteCaseId = caseId;
	}

	public String getSelecteCaseId() {
		return selecteCaseId;
	}

	public void setSelecteCaseId(String selecteCaseId) {
		this.selecteCaseId = selecteCaseId;
	}
}
