package de.init.xbauleitplanung.xmlxtatool.controller;

import java.io.Serializable;
import java.util.ArrayList;

import de.init.xbauleitplanung.xmlxtatool.model.XTASchemaAnalyzer;
import de.init.xbauleitplanung.xmlxtatool.model.schema.hierarchical.XModelTreeNode;
import de.init.xbauleitplanung.xmlxtatool.model.schema.plain.XCodelist;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAFolderResource;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAResource;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAResourceType;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTATreeItemI;
import de.init.xbauleitplanung.xmlxtatool.model.tree.XTAXMLFileItem;
import de.init.xbauleitplanung.xmlxtatool.model.xml.XNode;
import de.init.xbauleitplanung.xmlxtatool.ui.XTAClientI;

public interface XTAServerI extends Serializable{
	// Overwrite options for copyItem() method
	public static final int OVERWRITE_NO = 0;
	public static final int OVERWRITE_YES = 1;
	public static final int ITERATE = 2;
	public static final int TIMESTAMP = 3;
	
	/**
	 * Returns an ArrayList of the names of all registered resources of a given resource type
	 * @param type	The type of resources whose names are returned
	 * @return	An ArrayList of the names of all registered resources of a given resource type
	 */
	public ArrayList<String> getResourceNames(XTAResourceType type);

	/**
	 * Returns an ArrayList of the IDs of all registered resources of a given resource type
	 * @param type	The type of resources whose IDs are returned
	 * @return	An ArrayList of the IDs of all registered resources of a given resource type
	 */
	public ArrayList<String> getResourceIds(XTAResourceType type);

	/**
	 * Returns an ArrayList of all registered resources of a given resource type
	 * @param type	The type of resources to be returned
	 * @return	An ArrayList of all registered resources of a given resource type
	 */
	public ArrayList<XTAResource> getResources(XTAResourceType type);

	/**
	 * Returns a resource with a given ID
	 * @param id	The (unique) ID of the resource to be returend
	 * @return	The resource with the specified ID or null if no resource is registered with that ID
	 */
	public XTATreeItemI getResourcesForId(String id);

	/**
	 * Removes the resource with the specified ID from the repository of registered resources. 
	 * If no resource is registered with the specified ID, nothing will happen. 
	 * @param id	The ID of the resource to be removed from the repository
	 */
	public void removeResource(String id);
	
	/**
	 * Returns a codelist record with a specified name from a specified XML schema file 
	 * @param schemaId	the ID of the schema file item
	 * @param codelistName	the name of the resource
	 * @return	A codelist record from the specified resource for the specified name
	 */
	public XCodelist getCodelistForId(String schemaId, String codelistName);
	
	/**
	 * Creates and adds a folder resource record with the specified name and folder (in case then 
	 * name and folder are not yet registered)
	 * @param name	The resource name
	 * @param folder	The resource (root) folder
	 * @return	The ID of the newly created resource or null in case name or folder are 
	 * already registered
	 */
	public String addFolderResource(String name, String folder);
	
	/**
	 * Creates and adds a new resource record of type XTAAccount with the specified name and folder 
	 * (in case then name and folder are not yet registered). Mailbox folders will be created in case 
	 * they don't exist. 
	 * @param name	The resource name
	 * @param folder	The resource (root) folder
	 * @return	The ID of the newly created resource or null in case name or folder are 
	 * already registered
	 */
	public String addXTAAccount(String name, String folder);
	
	/**
	 * Creates and adds a new resource record of type RESTAccount with the specified name and folder 
	 * (in case then name and folder are not yet registered). Mailbox folders will be created in case 
	 * they don't exist. 
	 * @param name	The resource name
	 * @param folder	The resource (root) folder
	 * @return	The ID of the newly created resource or null in case name or folder are 
	 * already registered
	 */
	public String addRESTAccount(String name, String folder);

	/**
	 * Performs an update of the specified folder resource. All XML files, schemas, types, 
	 * elements, codelists etc will be analyzed from scratch. Formerly collected information
	 * will be discared first. 
	 * @param resource	The folder resource to be updated
	 */
	public void updateFolderResource(XTAFolderResource resource);
	
	/**
	 * This method writes the contents of the specified XTAXMLFileItem to disk 
	 * @param item	The XTAXMLFileItem to be written (exported)
	 * @param plain	If plain is true, the plain text property of the XTAXMLFileItem object will 
	 * be considered. Otherwise the internal DOM structure of the XTAXMLFileItem object will be 
	 * considered to recreate and write the XML structure fromn scratch. 
	 */
	public void exportXMLFile(XTAXMLFileItem item, boolean plain);
	
	/**
	 * Performs a validation of the XML file specified by id within the resource specified by resourceId
	 * @param resourceId	The resource hosting the XML file
	 * @param id	The ID of the XML file within the specified resource
	 */
	public void validateXMLFile(String resourceId, String id, XTAXMLFileItem file);
	
	/**
	 * Performs a validation of the specified XML file within the specified resource
	 * @param resourceId	The resource hosting the XML file
	 * @param id	The ID of the XML file within the specified resource
	 */
	public void validateAndStructXMLFile(String resourceId, String id);
	
	/**
	 * Performs a validation of the specified XML file within the specified resource
	 * @param child	The ID of the XML file within the specified resource
	 * @param root	The resource hosting the XML file
	 * @param cacheReferencedResources	Shall referenced resources be loaded and cached within the offline folder on the fly?
	 * @return 
	 */
	public void validateAndStructXMLFile(XTAXMLFileItem child, XTAFolderResource root, boolean cacheReferencedResources);
	
	/**
	 * Called in order to persist and clean up before program termination
	 */
	public void exit();
//
//	/**
//	 * Persists the state of this application
//	 * @param force	forces write process regardless of present changes
//	 */
//	public void write(boolean force);
	
	/**
	 * Returns a logical tree element within the overall structure specified by id
	 * @param id	The ID of the element to be returned
	 * @return	The logical tree element within the overall structure specified by id
	 */
	public XTATreeItemI getItemForId(String id);

	/**
	 * Adds a new tree element to the overall structure of elements 
	 * @param id	the ID of the new tree element
	 * @param item	the tree element itself
	 */
	public void putItemForId(String id, XTATreeItemI item);

	/**
	 * Creates a copy of the XML file specified by itemID and writes it to disk. The according 
	 * tree element will be created and registered as well.  
	 * @param itemId	the ID of the XML file object to be duplicated
	 * @param name	The name of the file copy
	 * @return	Returns true if the duplication was successful and false otherwise
	 */
	public boolean duplicateFile(String itemId, String name);

	/**
	 * Creates instances of all schema root elements covered by the tree element specified by elementId. 
	 * In case elementId specifies a schema root element itself, an instance of that schema element is 
	 * created. Otherwise, the specified tree element will be traversed recursively for schema root elements
	 * to instantiated. The instances (XML files) are written into the folder specified by targetFolder. 
	 * @param elementId	The ID of the tree element covering schema root elements to be instantiated
	 * @param targetFolder	The target folder to store XML instances
	 */
	public void createInstance(String elementId, String targetFolder);

	/**
	 * This method is called to make sure that the folder specified by targetFolder and all of its parent 
	 * folders are registered within the resource specified by resourceId.  
	 * @param resourceID	The ID of the resource in question
	 * @param targetFolder	The absolute path of the folder in question 
	 */
	public void checkPath(String resourceID, String targetFolder);

	/**
	 * Returns the plain text of the XML file specified by itemId
	 * @param itemId	The ID of the XTAXMLFileItem in question
	 * @return	The plain text of the XML file specified by itemId
	 */
	public String getPlainText(String itemId);

	/**
	 * Creates a copy of the file specified by itemId and writes it to the location specified by destinationFolder. 
	 * If move is true, the origin file will be deleted afterwards. The parameter overwritePolicy specifies, how to 
	 * proceed in case the target file does already exist. In case the destination folder is covered by the resource 
	 * containing the file object specified by itemId, the file copy is registered as file object to the resource. 
	 * @param itemId	Specifies the file object to be copied
	 * @param destinationFolder	The destination folder to store the file copy
	 * @param move	Causes the origin file to be deleted after the copy process
	 * @param overwritePolicy	Specified whether a potentially existing target file will be overwritten (OVERWRITE_YES) 
	 * or kept (OVERWRITE_NO). Further options for keeping existing target files are ITERATE in order to rename the 
	 * target file by adding and incrementing numbers to the file name and TIMESTAMP in order to add a timestamp to the 
	 * file name
	 * @param name	if name is provided (name != null) then the file will become renamed to that name
	 * @param targetResource	resource of the target item. If not provided then it is assumes that the it is identical 
	 * to the source resource
	 * @param analyze	shall the inserted file be analyzed deeply (true) or just copied / moved (false)? 
	 * @return	the copied or moved item if this action was successful, null otherwise
	 */
	public XTAFileItem copyItem(String itemId, String destinationFolder, boolean move, int overwritePolicy, String name, XTAFolderResource targetResource, boolean analyze);
//
//	/**
//	 * Creates a copy of the file specified by itemId and hands it over to the resource specified by destResourceId. 
//	 * @param itemId	Specifies the source file to be copied
//	 * @param destResourceId	Specifies the destination resource
//	 * @return	true in case copying was successful, false otherwise
//	 */
//	public boolean copyItemToResource(String itemId, String destResourceId);

	/**
	 * Initiates an analysis of the item specified by itemId
	 * @param itemId	the id of the item to be analysed
	 * @param targetResource	the resource that contains the item to be analysed
	 * @return
	 */
	boolean analyzeItem(String itemId, XTAFolderResource targetResource);
	
	/**
	 * Returns a reference to the UI object handling user interaction
	 * @return	A reference to the UI object handling user interaction
	 */
	public XTAClientI getUi();

	/**
	 * Called to get environment value from this XTAServerI instance
	 * @param string	id of the environment value
	 * @return	the environment value
	 */
	public Serializable getProperty(String string);

	/**
	 * Called to set an environment value
	 * @param string	id of the environment value
	 * @param value	the environment value
	 */
	public void setProperty(String string, Serializable value);
	
	/**
	 * Returns the home directory of the user running this class
	 * @return	home directory of the user running this class
	 */
	public String getHomeDirectory();
//
//	/**
//	 * recursively add contents of the item to the instance repository of the resource - depending on the type of the item
//	 * @param item	selected item containing instances
//	 */
//	public void addInstanceToRepository(XTATreeItemI item);
//
//	/**
//	 * remove contents from the repository of the resource - depending on the type of the item
//	 * @param item	selected item containing instances
//	 */
//	public void removeInstanceFromRepository(String id);
	
	/**
	 * Deletes item specified by id from its parent object
	 * @param item	id of the item to delete
	 * @return	true, if the delete action was successful, false otherwise
	 */
	boolean deleteItem(String id);
	
	/**
	 * Create an instance of the element specified by template which belongs to the resource res and add it to parent (if present)
	 * @param template	Template element to be created
	 * @param res	Resource containing template
	 * @param parent	parent element of the newly created element node (if present)
	 * @return	the newly created element node
	 */
	public XNode createInstance(XModelTreeNode template, XTAFolderResource res, XNode parent);

	public XTASchemaAnalyzer getAnalyzer();
}