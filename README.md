## XML-Nachrichten-Tool
Das Nachrichten-Tool ist ein Java-basiertes Programm, mit dem - auf Grundlage von XÖV-Standards - Testnachrichten erzeugt, angezeigt, bearbeitet, validiert und versendet bzw. empfangen werden können.  
Dieses multifunktionale Testwerkzeug der XLeitstelle und BSW Hamburg kann dementsprechend in unterschiedlichen Anwendungsszenarien genutzt werden:
- XML-Nachrichten von XÖV-Standards wie XBau und XBreitband werden in strukturierter (baumartiger) Form angezeigt. Dabei lassen sich Äste ein- und ausklappen, um eine übersichtliche Darstellung der verschachtelten Nachrichteninhalte zu erhalten. 
- Die Schemavalidität der Nachrichten wird untersucht, evtl. vorhandene Fehler werden angezeigt. Bei der Verwendung von Codelisten werden die in XML-Nachrichten genutzten Codes nach Möglichkeit aufgelöst. 
- Änderungen an den Schemata eines Standards wie XBau und XBreitband lassen sich über Testnachrichten überprüfen, die "auf Knopfdruck" mit validen Daten (für Textfelder, Codelistenwerte etc.) ausgefüllt werden.
- Die erzeugten Nachrichten können innerhalb des Tools oder mit einem externen Editor bearbeitet werden, um fachlich korrekte Nachrichten zu erstellen. Es lässt sich z.B. zu überprüfen, ob sich alle Daten eines analogen Antrags in das digitale Format übertragen lassen. 
- Die Funktionalität des Standards lässt sich im Detail testen (z.B. die korrekte Einbindung externer Schemata und Codelisten). Die Nachrichten werden nach jeder Änderung automatisch validiert.
- Die Nachrichten können über die XTA-Infrastruktur oder über FIT-Connect versendet werden, sofern die entsprechen Zugangsdaten vorhanden sind. Damit lassen sich reale Anwendungsfälle der XÖV-Nachrichten simmulieren, indem z.B. Anträge an Behörden abgeschickt und Antworten empfangen werden (entweder innerhalb des Tools oder zwischen zwei NutzerInnen, die jeweils eine Rolle übernehmen).  


## Installation
Das Programm wird als ZIP-Archiv zusammen mit Java 11 ausgeliefert und kann in einem beliebigen Verzeichnis entpackt werden. Es lässt sich über die EXE-Datei _XML-Nachrichten-Tool_XX.exe_ starten. 
Innerhalb der Verwaltungsnetze müssen ausreichende Nutzerrechte vorhanden sein.
Im Benutzerverzeichnis wird ein Ordner "xtaviewer" angelegt, wo Einstellungen und Konten abgelegt sind. Neue Versionen des Tools greifen ebenso auf diesen Benutzerordner zu.

## Erste Schritte
Über den Button "Neu Ordner" wird ein Ordner mit XML-Nachrichten und -Schemata sowie Codelisten (im Genericode-Format) eines XÖV-Standards importiert. Anschließend liest das Tool diese und ggf. alle referenzierten externen Schemata ein bzw. speichert diese im Unterordner "offline" ab. In diesem Unterordner erhält man somit eine vollständige Offline-Kopie aller verwendeten Schemata und Codelisten, der für Analyse- oder Debug-Zwecke eingesetzt (oder einfach ignoriert) werden kann. Wenn der Einlese-Prozess abgeschlossen ist, wird die entsprechende Ordnerstruktur mit allen relevanten XML-Dateien im Verzeichnisbaum des Tools angezeigt. Die Schemata lassen sich über das Dreieck am linken Rand aufklappen. 

In gleicher Weise lassen sich über "Neues XTA-Konto" bzw. "Neues FIT-Connect-Konto" Ordner importieren, die als Postfächer zum Senden und Empfangen von Nachrichten über die jeweilige Transportinfrastruktur verwendet werden. Unterordner für den Nachrichtentransfer werden automatisch angelegt (u.a. Posteingang, Postausgang, Gesendet).  

## Nachrichtenansicht konfigurieren

Die Art der Generierung der Testnachrichten sowie ihre Darstellung lassen sich über einige Einstellungen steuern. Dafür wird die oberste Ebene im Verzeichnisbaum ("Alle Quellen") angeklickt, auf der rechten Seite werden anschließend die Optionen angezeigt. 

![Einstellungen anzeigen](Einstellungen.jpg)

Der Umfang der Testnachrichten lässt sich über die Auswahl von optionalen Elementen und Attributen steuern. 

Die Einstellung "Knoten expandieren bis ..." legt fest, inwieweit die verschachtelten Datentypen der Nachrichten ein- bzw. ausgeklappt werden. Die Anzahl der Ebenen bezieht sich auf das jeweilige Element.

Das Präfix des Standards (z.B. "xbau-tiefbau") lässt sich in der strukturierten Nachrichtenansicht ausblenden (in der Textansicht wird es angezeigt).

Änderungen müssen mit dem Button "Übernehmen" bestätigt werden. 

## Nachrichten erstellen

Im links abgebildeten Verzeichnisbaum wird das Schema der Nachricht ausgewählt. Anschließend wird der Button "Instanz erzeugen" angeklickt. 

![Menü Nachricht erzeugen](MenüNachricht.jpg)

Die erzeugte XML-Nachricht ist innerhalb des Verzeichnisbaums auffindbar. Sie wird also im gleichen Ordner wie die Schemata gespeichert.

![Instanz anzeigen](Nachricht.jpg)

## Nachrichten bearbeiten
Es stehen verschiedene Bearbeitungsmöglichkeiten zur Auswahl:

- Um die Struktur der Nachricht regelkonform zu verändern, wird der Button "XML bearbeiten" ausgewählt. Ein Dreieck am rechten Rand öffnet ein Menü, das die Bearbeitungsmöglichkeiten des jeweiligen Elements anzeigt: Die in hellgrauer Schrift dargestellten optionalen Elemente lassen sich z.B. löschen; ist die Multiplizität größer eins, kann das Element erneut hinzugefügt werden. Dabei ist auf die Bearbeitungsebene zu achten: Um in der oben gezeigten Nachricht das Element "trassenführung" zu löschen, muss (im hier nicht gezeigten Bearbeitungsmodus) das Dreieck im übergeordneten Element "bauabschnitte" ausgewählt werden. 

![XML bearbeiten](Bearbeiten.jpg)

- Mit "XML bearbeiten" werden ebenso die fachlichen Inhalte der Nachricht bearbeitet, z.B. lassen sich so die Zufallstexte in den String-Datentypen ersetzen. Für die in den Standard eingebundenen Typ-1-Codelisten werden die zutreffenden Werte über Pulldown-Menüs ausgewählt. Bei Typ-3-Codelisten muss zunächst die zutreffende Versionsnummer (*listVersionID*) ausgewählt bzw. überprüft werden, für die externen Typ-4-Codelisten sind sowohl die URN- oder Link-Adressen als auch die Versionsnummer einzutragen. (Hinweis: nur lokal verfügbare Typ-3-Codelisten müssen im gleichen Verzeichnis wie die Schemata liegen.) Die Validierung der Bearbeitung erfolgt - je nach Datentyp - sofort nach der Eingabe oder über den Button "Speichern". In einem Fenster unter der Nachricht werden die Fehler angezeigt, in der Nachricht sind sie farblich hervorgehoben. Fehlerhafte Nachrichten werden ebenso im Verzeichnisbaum markiert.

- Über "Öffnen" wird ein externer Editor zur Bearbeitung der XML-Datei gestartet. Eine mögliche Anwendung für den Editor ist die Hinzufügung von Nachrichten(teilen), um z.B. einen Antrag in eine Beteiligungsnachricht zu integrieren. Nach dem Sichern der Datei im Editor wird über "Neu analysieren" die Validierung der Nachricht angestoßen. 

- Über "Text bearbeiten" wird ein interner Editor geöffnet. Auf diesem Weg wird die Nachricht in der XML-Struktur angezeigt. Die Validierung von Änderungen im Code erfolgt über "Speichern".


## Schemata aktualisieren

Die XML-Schemata können in dem vorhandenen Ordner überschrieben werden. Anschließend müssen sie im Tool neu eingelesen werden. Dafür wird der entsprechende Ordner im Verzeichnisbaum ausgewählt, im rechten Fenster wird anschließend am unteren Rand der Button "Übernehmen" gedrückt (oder das Kreissymbol zur Aktualisierung). Die zuvor erzeugten Nachrichteninstanzen des Ordners werden anschließend auf Grundlage der aktualisierten Schemata validiert.  

## Nachrichten über FIT-Connect senden und empfangen

Nach der Registrierung auf der FIT-Connect-Testumgebung lassen sich IDs für den Zustellpunkt (der Genehmigungsbehörde), Sender-Client (= Antragssteller) und Subscriber-Client (= Antragsbearbeiter) angelegen ([Anleitung](https://docs.fitko.de/fit-connect/docs/getting-started/account)). Im Tool werden die IDs in die entsprechende Eingabemaske für das FIT-Connect-Konto eingetragen. Erreichbar ist diese Maske über die Auswahl des Kontos im Verzeichnisbaum. Sofern noch kein Konto angelegt wurde, muss zunächst ein Ordner importiert werden (s. "Erste Schritte").

Um für die Testumgebung public und private keys zu erzeugen, kann das [hier](https://git.fitko.de/fit-connect/fit-connect-tools) verfügbare Python-Script "createSelfSignedJwks.py" genutzt werden ([Anleitung](https://docs.fitko.de/fit-connect/docs/details/jwk-creation/)).

Nach der Einrichtung eines FIT-Connect-Sender-Kontos lassen sich erzeugte Nachrichten über die Submission-API verschicken. Im Hauptfenster der Nachricht wird zunächst auf "Senden..." geklickt, anschließend öffnet sich ein Auswahlbereich für Sender und Empfänger. 

![Instanz anzeigen](NachrichtSenden.jpg)

In diesem Auswahlbereich werden ebenso die Anhänge zur Nachricht hinzugefügt. Handelt es sich um Nachrichten der Standards XBau und XBreitband, erfolgt für die ausgewählten Anhänge die Eintragung des Dateinamens und einer UUID in den zugehörigen Metadaten der jeweiligen Nachricht. 

Über "Senden" wird die Nachricht abgeschickt.

Ist ein Subscriber-Konto eingerichtet, lassen sich Nachrichten über das Hauptmenü "Extras" abrufen. 

Gesendete und empfangene Nachrichten lassen sich im Verzeichnisbaum wie in einem Email-Programm für das jeweilige FIT-Connect-Konto anzeigen. Alternativ dazu kann in den Einstellungen des Kontos eine fallbasierte Sortierung der Nachrichten ausgewählt werden: die Zuordnung von gesendeten und empfangenen Nachrichten erfolgt dann über die von der Submission API zugewiesenen Case-ID. Für jeden Vorgang eines Senders sind damit die eigene Nachrichten (z.B. Antrag und Überarbeitung) und die empfangenen Antworten der Behörde in einem Ordner zusammengefasst. (Anmerkung: dieses Feature ist erst mit der Integration des Rückkanals vollständig funktionsfähig.) 

## Roadmap

Nutzer und Nutzerinnen des Tool sind aufgerufen, Vorschläge zur Implementierung weiter Features und Funktionalitäten einzureichen. 

Von Seiten der XLeitstelle ist die Implementierung des Rückkanals von FIT-Connect 1.3 geplant, durch den Anträge mit der entsprechenden Nachricht der Behörde beantwortet werden können. Es fehlt weiterhin die Bearbeitungsmöglichkeit des Metadatensatzes der FIT-Connect-Einreichung. 

Bislang sind die Erfahrung mit der Nutzung von XTA-Konten begrenzt. Wer Interesse an der Nutzung dieser Funktion hat, möge sich bei der XLeitstelle melden.

## Neue Features der Versionen 1.1 ff 

- Elemente können per "Copy and Paste" in eine andere Nachricht oder an anderer Stelle eingefügt werden. Das Symbol "Kopieren" lässt sich in der Nachrichtenansicht nutzen, beim "Einfügen" wird automatisch in den Bearbeitungsmodus gewechselt und eine valide Position innerhalb der Nachricht vorgeschlagen. Mit dieser Funktion kann u.a. das Weiterleiten von Anträgen getestet werden (z.B. das Element "breitbandvorhaben" in XBreitband-Nachrichten 3000/4000, oder das Element "bauvorhaben" in einer XBau-Nachricht 0300).

- Codelisten im XML-Format, die nicht im XRepository verfügbar sind, lassen sich als Typ-4-Codelisten in die Nachrichten einbinden (d.h. Ermöglichung der Werteauswahl über Pulldown-Menüs und Validierung der Adressen bzw. Versionsnummern). Ein Beispiel für eine externe Codeliste ist "Planarten" aus der GDI-Registry:  
      https://registry.gdi-de.org/codelist/de.xleitstelle.inspire_plu/LandUse/LandUse.de.xml
- Validierungsfehler werden in der XML- und Textansicht der Nachricht jeweils farblich hervorgehoben. 
- Fehlermeldungen im unteren Fenster lassen sich anklicken (und die entsprechende Stelle wird im Nachrichtenfenster angezeigt).  
- Die Textansicht hat eine Zeilenummerierung.

Aufgrund des laufenden Bugfixing wird empfohlen, die jeweils aktuellste Version zu nutzen.
