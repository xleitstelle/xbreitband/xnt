Start des XML-Nachrichtentools unter Linux

- Stellen Sie sicher, dass Java (JDK oder JRW in Version 11) auf Ihrem Rechner installiert ist
- Laden Sie das XML-Nachrichtentool (xnt_<Version>.run) herunter
- Stellen Sie sicher, dass die Datei xnt_<Version>.run ausführbar ist 
- Rufen Sie das Programm auf (bspw. durch Aufruf über Shell oder durch Rechtsklick --> Ausführen)
- Das Programm startet nach kurzer Zeit